var pageLoader = new function(){
    var _this = this;
    var allAutoStartFunc = null;

    var initAutostartFunctions = function(){
        allAutoStartFunc = Utils.getAllAutoStartFunc();
        if((isset(window.MainPage))&&(isFunction(MainPage.init))){
            setTimeout(MainPage.init,1);
        }

        for(var i=0;i<allAutoStartFunc.length;i++){
            setTimeout(allAutoStartFunc[i],1);
        }
    }

    this.init = function(){
        Utils.showPageMenuParts();
        initAutostartFunctions();
        InputLengthLimiter.init({elements:"text-field"});
    }
}

$(document).ready(pageLoader.init);
