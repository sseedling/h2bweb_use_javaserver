/**
 * Created by seedling on 16.12.13.
 */
var exportTableToCSV = new function() {
    var $rows = null;

    // Temporary delimiter characters unlikely to be typed by keyboard
    // This is to avoid accidentally splitting the actual contents
    var tmpColDelim = String.fromCharCode(11); // vertical tab character
    var tmpRowDelim = String.fromCharCode(0); // null character

    // actual delimiter characters for CSV format
    var textWrapper = '"';
    var colDelim = textWrapper+','+textWrapper;
    var rowDelim = '\r\n';

    // Data URI
    var csvData = null;

    var clearText = function(text){
        return Utils.cutDoubleSpaces(text.replaceAll('"','""'));
    };

    var parseLevels = function(columnName,tdElem){
        var outValues = [];
        if( $(tdElem).find("table").length > 0 ){

            $(tdElem).find("tr").each(function(){

                var title = $(this).find('.q-title');
                var data = $(this).find('.q-data');
                if(data.length > 0){
                    var tmpList = parseLevels(clearText(title.text()),data);

                    for(var i=0;i<tmpList.length;i++){
                        outValues.push({columnName:columnName+" :: "+tmpList[i].columnName,columnData:tmpList[i].columnData});
                    }
                }
            });
        }
        else{
            outValues.push({columnName:columnName,columnData:clearText($(tdElem).text())});//todo add line break foreach text item
        }
        return outValues;
    };

    var splitToCSV = function(columnsOut){
        var csvString = '';

        csvString += textWrapper+columnsOut.names.join(colDelim)+textWrapper + rowDelim;

        for(var i=0;i< columnsOut.data.length;i++){
            csvString += textWrapper+columnsOut.data[i].join(colDelim)+textWrapper + rowDelim;
        }

        return csvString;
    };

    var createCSV = function($reportTable){
        var columnsBaseNamesMap = [];
        var columnsOut = {"names":[],"data":[]};

        $reportTable.find(".report-title td").each(function(){
            columnsBaseNamesMap.push( clearText($(this).text()) );
        });

        $reportTable.find(".report-content").each(function(){//foreach row
            var cols = $(this).find("td.report-cell");
            var rowDataArr = [];
            var curNamesLength = columnsOut.names.length;

            for(var i=0;i<columnsBaseNamesMap.length;i++){ //foreach col
                var csvArrObj = parseLevels(columnsBaseNamesMap[i],cols[i]);

                for(var j=0;j < csvArrObj.length;j++){

                    if(curNamesLength == 0 ){//add unique names on first iteration
                        columnsOut.names.push(csvArrObj[j].columnName);
                    }

                    //collect column texts
                    rowDataArr.push(csvArrObj[j].columnData);

                }
            }
            columnsOut.data.push(rowDataArr);

        });

        var title = textWrapper+$(".quiz-name .text").text() + textWrapper +rowDelim;
        return title + splitToCSV(columnsOut);

    };

    this.init = function($initElem, $reportTable, filename){
        var csv = createCSV($reportTable);

        // Data URI
        csvData = 'data:application/csv;charset=utf-8,' + encodeURIComponent(csv);

        $initElem.attr({
            'download': filename,
            'href': csvData,
            'target': '_blank'
        });


    }
};
