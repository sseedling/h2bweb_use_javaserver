/**
 * Created by Ramotion.
 * User: seedling
 * Date: 05.06.13
 * Time: 10:51
 * To change this template use File | Settings | File Templates.
 */

var Utils = new function(){
    var _this = this;
    var REDIRECT_TIMER = null;
    var alertMsg="";
    var followUrl="";
    var allAutoStartFunc = [];
    var logEnabled = false;

    //открытие нового окна
    this.openUrl = function (url){
        window.open(url);
    };

    this.followUrl = function (url,msg){
        followUrl = url;
        alertMsg = (msg!=undefined)?msg:null;
        clearTimeout(REDIRECT_TIMER);
        REDIRECT_TIMER = setTimeout(function(){
            if(alertMsg!=null)alert(alertMsg);
            window.location=followUrl;
        },100);
    };

    this.networkErrorAlert = function(){
        alert("Network error.");
    };

    //block event from upping to parents
    this.eventStop = function (e) {
        if (e.stopPropagation) e.stopPropagation();
        else e.cancelBubble = true;

        if (e.preventDefault) e.preventDefault();
        else e.returnValue = false;
    };

    this.pushAutoStartFunc = function(actionName){
        allAutoStartFunc.push(actionName);
    };

    this.getAllAutoStartFunc = function(){
        return allAutoStartFunc;
    };

    //check some obj for existed and not null
    this.isset = function(obj){
        return ((typeof(obj)!="undefined")&&(null!=obj));
    };

    //return current time in timestamp
    this.time = function(){
        return ( new Date ).getTime();
    };

    //is cur obj function
    this.isFunction = function(obj){
        return (typeof(obj)=="function");
    };

    //Check for file select support
    this.isChoosFileSupported=function(){
        var elem;
        try {
            elem = document.createElement("input");
            elem.type = "file";
        } catch(e) {
            return false; // type=file is not implemented
        }

        try {
            elem.value = "Test";
            return !( elem.value == "Test" || elem.disabled );
        } catch(e) {
            return true; // type=file implemented correctly
        }
    };

    this.browserDetect = function(key){
        var browser_detect = {
            IE:     !!(window.attachEvent && !window.opera),
            Opera:  !!window.opera,
            WebKit: navigator.userAgent.indexOf('WebKit/') > -1,
            Gecko:  navigator.userAgent.indexOf('Gecko') > -1 && navigator.userAgent.indexOf('KHTML') == -1,
            MobileSafari: !!navigator.userAgent.match(/Apple.*Mobile.*Safari/)
        };
        switch(key.toLowerCase()){
            case "msie":
            case "ie":return browser_detect.IE;

            case "msie7":
            case "ie7":return browser_detect.IE;

            case "opera":return browser_detect.Opera;

            case "chrom":
            case "chrome":
            case "safari":
            case "webkit":return browser_detect.WebKit;

            case "gecko":
            case "firefox":
            case "mozilla":return browser_detect.Gecko;

            case "safari mobile":
            case "mobile safari":
            case "mobilesafari":
            case "safarimobile":return browser_detect.MobileSafari;
        }
        return false;
    };

    //Replace all searche to replace
    this.replaceAll = function(str, search, replace){
        return str.split(search).join(replace);
    };

    this.showPageMenuParts = function(){
        //first
        if(($("#pageMenu .firstPart").length>0)&&($("#middleContent .pageMenu-firstPart").length>0)){
            $("#pageMenu .firstPart").html($("#pageMenu .firstPart").html()+$("#middleContent .pageMenu-firstPart").html());
            $("#middleContent .pageMenu-firstPart").remove();
        }

        //second
        if(($("#pageMenu .secondPart").length>0)&&($("#middleContent .pageMenu-secondPart").length>0)){
            $("#pageMenu .secondPart").html($("#middleContent .pageMenu-secondPart").html());
            $("#middleContent .pageMenu-secondPart").remove();
        }
    };

    this.inArray = function(val,arr){
        for(var i=0;i<arr.length;i++)if(arr[i]==val)return true;
        return false;
    };

    this.drawImgToCanvas = function( params ) {
        params.mode = 'canvas';
        drawImg( params );
    };
    this.drawImgToImg = function( params ) {
        params.mode = 'img';
        drawImg( params );
    };
    this.isValidImageType = function( string ) {
        return /(png|jpg|gif|jpeg)$/.test( string );
    };
    var drawImg = function( params ) {
        if( ! isFunction( FileReader ) ) {
            return;
        }

        var file = params.inputElem.files[0];
        if( ! Utils.isValidImageType( file.type ) ) {
            return alert( 'File doesnt match png, jpg or gif' );
        }

        var reader = new FileReader;
        reader.onload = function() {
            switch( params.mode || 'canvas' ){
                case 'canvas' :
                    var tmpImg = document.createElement( 'img' );
                    tmpImg.onload = function() {
                        params.drawElem.getContext( '2d' ).drawImage( this , 0 , 0 , this.width , this.height );
                    };
                    tmpImg.src = this.result;
                    break;
                case 'img' :
                    $( params.drawElem ).on( 'load' , function() {
                        this.className = 'active';
                    } ).attr( 'src' , this.result );
            }
        };
        reader.readAsDataURL( file );
    };

    /**
     * Reset inputs (file,text etc), selects, textareas i.e.
     * to onload state.
     *
     * @param elem
     */
    this.resetElem = function(elem){
        $(elem).wrap('<form>').closest('form').get(0).reset();
        $(elem).removeClass("active").unwrap();
    };

    this.isNumber = function(n){
        return !isNaN(parseFloat(n)) && isFinite(n);
    };

    this.trim = function(str){
        return str.replace(/^\s+|\s+$/g, '');
    };

    this.cutDoubleSpaces = function(str){
        var tmpStr = str.replaceAll("  "," ");
        while(str!=tmpStr){
            str = tmpStr;
            tmpStr = tmpStr.replaceAll("  "," ");
        }
        return tmpStr;
    }

    this.clone = function(o){
        if(!o || "object" !== typeof o)  {
            return o;
        }
        var c = "function" === typeof o.pop ? [] : {};
        var p, v;
        for(p in o) {
            if(o.hasOwnProperty(p)) {
                v = o[p];
                if(v && "object" === typeof v) {
                    c[p] = _this.clone(v);
                }
            else c[p] = v;
            }
        }
        return c;
    }

    this.log = function() {
        if(!logEnabled)return;
        console.log.apply( console , [ Utils.time() ].concat( arguments ) );
    };

    this.bindDigitsOnly = function(objList,lesThenZero){
        var checkValue = function(elemObj){
            var curValue = (elemObj.value).trim();
            var prevValue = $(elemObj).attr("prevValue");
            if(curValue==prevValue)return;
            if(curValue!=elemObj.value)elemObj.value = curValue;
            var numberCheck = Utils.isNumber(elemObj.value);
            var lesThenZero = ($(elemObj).attr("lesThenZero")==1);

            if((!numberCheck) || ((!lesThenZero) && (elemObj.value<0))){
                elemObj.value = prevValue;
            }
            else {
                if(elemObj.value=="-0")elemObj.value = "0";
                $(elemObj).attr("prevValue",elemObj.value);
            }

        }

        for(var i=0;i<objList.length;i++){
            var elem = objList[i];
            $(elem).attr("prevValue",elem.value).attr("lesThenZero",(lesThenZero?1:0));
        }

        $(objList).on('change',function(){checkValue(this);});
        $(objList).on('keyup',function(){checkValue(this);});
        $(objList).on('keydown',function(){checkValue(this);});
        $(objList).on('keypress',function(){checkValue(this);});
    }
};

//================ Small functions ===================

function isset(obj){
    return Utils.isset(obj);
}

function isFunction(obj){
    return Utils.isFunction(obj);
}

function time(){
    return Utils.time();
}

if(!isFunction(String.prototype.replaceAll)){
    String.prototype.replaceAll = function(search, replace){
        return Utils.replaceAll(this,search,replace);
    }
}

if(!isFunction(String.prototype.cutDoubleSpaces)){
    String.prototype.cutDoubleSpaces = function(){
        return Utils.cutDoubleSpaces(this);
    }
}

if(!isFunction(String.prototype.trim)) {
    String.prototype.trim = function() {
        return Utils.trim(this);
    }
}

if(!isFunction(String.prototype.toLowerCase)) {
    String.prototype.toLowerCase = function() {
        return Utils.toLowerCase(this);
    }
}


Utils.createSelect = function( options , onCreateOption ) {
    var select = $( '<select></select>' );
    var lastOption;

    if( !isFunction( onCreateOption ) ) {
        onCreateOption = function() {};
    }

    $.each( options , function( optionValue , optionText ) {
        lastOption = $( '<option value="' + optionValue + '">' + optionText + '</option>' );

        if( onCreateOption.call( lastOption , optionValue ) !== false ) {
            select.append( lastOption );
        }
    } );

    return select;
};

Utils.togglePasswordChars = function() {
    $( '.password .ico-lock' ).on( 'mousedown touchstart' , function( event ){
        var node  = $( this );
        var input = node.siblings( '.text-field' );
        var type  = input.attr( 'type' ) == 'text' ? 'password' : 'text';

        input.attr( 'type' , type ).attr( 'placeholder' , type == 'text' ? input.data( 'placeholder' ) : '••••••••' );
        node[ type == 'text' ? 'addClass' : 'removeClass' ]( 'active' );

        Utils.eventStop( event );
    } ).mousedown().mousedown();
};
Utils.togglePasswordChars();

Utils.SwitchBox = function( object ) {
    if( !$.isPlainObject( object ) ) {
        object = {};
    }

    var node            = $( '<div class="switch-box"><div class="text"></div><div class="switcher"></div></div>' );

    node.setStatus      = function( value ) {
        var oldValue    = object.status;
        object.status   = value = value == void 0 ? object.status : value;

        object.status   = value;
        object.classOff = object.classOff || 'unpublish';
        object.classOn  = object.classOn || 'publish';

        if( object.classOff == object.classOn ) {
            if( !this.hasClass( object.classOn ) ) {
                this.addClass( object.classOn );
            }
        } else {
            this[ value ? 'removeClass' : 'addClass' ]( object.classOff )[ value ? 'addClass' : 'removeClass' ]( object.classOn );
        }

        this[ value ? 'addClass' : 'removeClass' ]( 'active' ).attr( 'title' , value ? object.titleOn : object.titleOff );

        this.find( '.text' ).html( value ? ( object.textOn || 'Published' ) : ( object.textOff || 'Pending' ) );

        if( isFunction( object.onChangeStatus ) && value != oldValue ) {
            object.onChangeStatus.call( node , value , object );
        }

        return this;
    };

    node.toggleStatus   = function() {
        this.setStatus( !object.status );
    };
    node.getStatus      = function() {
        return object.status;
    };
    node.setDisabled    = function( value ) {
        this[ ( object.disabled = value ) ? 'addClass' : 'removeClass' ]( 'disabled' );
        if( isFunction( object.onChangeDisabled ) ) {
            object.onChangeDisabled.call( node , object.disabled , object );
        }
        return this;
    };
    node.toggleDisabled = function() {
        this.setStatus( !object.disabled );
    };
    node.click( function() {
        if( isFunction( object.onClick ) ) {
            object.onClick.call( node );
        }
        if( ! object.disabled ) {
            node.toggleStatus();
        }
    } );
    node.setStatus(object.status);

    return node;
};

Utils.isValidEmail = function( email ) {
    return /\w+@\w+\.\w{2,}/.test( email );
};

Utils.checkFileApi = function (){
    var debuggers="<h4>browser supports following</h4>";
    if(window.File) debuggers+="Supports File api<br>";
    if(window.FileReader) debuggers+="Supports FileReader api<br>";
    if(window.FileList) debuggers+="Supports FileList api<br>";
    if(window.Blob) debuggers+="Supports Blob api<br>";
    //console.log(debuggers)
};

Utils.createTimerFunc = new function(){
    var _this = this;
    var defaultTimeOut = 1000;
    var animateTimeOut = 260;
    this.options = {
        hide:{
            name:"hideBlock",
            css:"hideOnTimeout"
        }
    }
    this.init = function(funcName,timeOut){
        switch (funcName){
            case _this.options.hide.name:{
                setTimeout(function(){
                    $(('.'+_this.options.hide.css)).removeClass("in");
                    setTimeout(function(){
                        $(('.'+_this.options.hide.css+":not(.in)")).remove();
                    },animateTimeOut);
                },(timeOut||defaultTimeOut));
                break;
            }
            default :{

            }
        }
    }
}

$( function() {
    var dropdownBtns = $( '.dropdown-btn' ).click( function() {
        $( this ).toggleClass( 'active' );
    } );
    $( document ).click( function( event ) {
        if( $( event.target ).hasClass( 'dropdown-btn' ) || $( event.target ).parents( '.dropdown-btn' ).length ) {
            return;
        }
        dropdownBtns.removeClass( 'active' );
    } );
} );

Utils.isMobile = navigator.userAgent.match( /Android|BlackBerry|iPhone|iPad|iPod|Opera Mini|IEMobile/i );


/*
//  hover
if( ! Utils.isMobile ) {
    $( document ).on( 'mouseover' , '*' , function() {
        $( this ).addClass( 'hover' );
    } ).on( 'mouseout' , '*' , function() {
            $( this ).removeClass( 'hover' );
        } );
}
    */