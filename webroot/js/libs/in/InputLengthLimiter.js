/**
 * Created by Ramotion.
 * User: seedling
 * Date: 25.07.13
 * Time: 14:28
 * To change this template use File | Settings | File Templates.
 */
var InputLengthLimiter = new function(){
    var _this = this;
    _this.maxDefaultLength = 255;
    this.init = function(params){
        if(!isset(params.elements))return false;
        for(var i=0;i<params.elements.length;i++){
            $(params.elements[i]).on("input",onInput);
        }
    }

    var onInput = function(e){
        var max = $(this).attr("maxlength");

        if(max==undefined){
            max = _this.maxDefaultLength;
        }

        if(this.value.length > max)
            this.value = this.value.slice(0,max);
    }
}