var ClassicChart = function( length ) {
    return function( options , response ) {
        if( ! options ) {
            options = {};
        }

        var node = document.createElement( 'div' );
        var chartId;

        node.id = 'classic-chart' + length++;

        return {
            node     : node
            , render : function() {
                if( options.control ) {
                    var chartBox = document.createElement( 'div' );
                    chartId = chartBox.id = 'classic-chart-box' + length;
                    chartBox.style.height = ( options.height - 60 ) + 'px';
                    if( options.width ) {
                        chartBox.style.width = options.width + 'px';
                    }
                    node.appendChild( chartBox );
                } else {
                    chartId = node.id;
                    node.style.height = options.height + 'px';
                    if( options.width ) {
                        node.style.width = options.width + 'px';
                    }
                }

                var chart = new google.visualization.ChartWrapper( {
                    chartType : 'LineChart'
                    , containerId : chartId
                    , options : {
                        chartArea : {
                            height : '80%'
                            , width : '90%'
                        }
                        , hAxis : {
                            slantedText : false
                        }
                        , legend : {
                            position : 'bottom'
                        }
                        , curveType : 'function'
                    }
                    , view : {
                        columns : [ {
                            calc : function( dataTable , rowIndex ) {
                                return dataTable.getFormattedValue( rowIndex , 0 );
                            }
                            , type : 'string'
                        } ]
                    }
                } );

                var data = new google.visualization.DataTable;
                data.addColumn( 'date' , 'Date' );

                var datesObject = {};
                var datesArray = [];
                var locations = {};
                var reportIndex = 0;
                var reportLength = response.reports.length;
                while( reportIndex < reportLength ) {
                    if( ! datesObject[ response.reports[ reportIndex ].date ] ) {
                        datesObject[ response.reports[ reportIndex ].date ] = datesArray.length;
                        datesArray.push( {} );
                    }
                    if( ! locations[ response.reports[ reportIndex ].locationId ] ) {
                        locations[ response.reports[ reportIndex ].locationId ] = true;
                    }
                    datesArray[ datesObject[ response.reports[ reportIndex ].date ] ][ response.reports[ reportIndex ].locationId ] = response.reports[ reportIndex ].grade;
                    reportIndex++;
                }

                for( var i in locations ) {
                    if( options.locationStep instanceof Function ) {
                        options.locationStep.call( null , i );
                    }
                    chart.getView().columns.push( chart.getView().columns.length );
                    data.addColumn( 'number' , MainPage.locations[ i ].name );
                }

                function getGrade( dateIndex , locationId , flag ) {
                    if( dateIndex < 0 ) {
                        return getGrade( dateIndex + 1 , locationId , true );
                    }
                    if( dateIndex > datesArray.length - 1 ) {
                        return getGrade( dateIndex - 1 , locationId );
                    }
                    if( datesArray[ dateIndex ] && locationId in datesArray[ dateIndex ] ) {
                        return datesArray[ dateIndex ][ locationId ];
                    }
                    return getGrade( dateIndex + ( flag ? 1 : -1 ) , locationId , flag );
                }

                var date;
                for( date in datesObject ) {
                    var row = [ new Date( date ) ];
                    var locationId;
                    for( locationId in locations ) {
                        row.push( getGrade( datesObject[ date ] , locationId ) );
                    }
                    data.addRow( row );
                }

                if( options.control ) {
                    var controlBox = document.createElement( 'div' );
                    controlBox.id = 'chart-control-box' + length;
                    controlBox.style.height = '60px';
                    if( options.width ) {
                        controlBox.style.width = options.width + 'px';
                    }
                    node.appendChild( controlBox );

                    var control = new google.visualization.ControlWrapper( {
                        controlType : 'ChartRangeFilter'
                        , containerId : controlBox.id
                        , options : {
                            filterColumnIndex : 0
                            , ui : {
                                chartType : 'LineChart'
                                , chartOptions : {
                                    chartArea : {
                                        width : '90%'
                                    }
                                }
                                , minRangeSize : 60 * 60 * 1000
                            }
                        }
                    } );

                    google.visualization.events.addListener( control , 'statechange' , function( event ) {
                        if( ( event.startChanged || event.endChanged ) && ( options.changeRange instanceof Function ) ) {
                            options.changeRange( control.getState().range , event.inProgress );
                        }
                    } );

                    new google.visualization.Dashboard( node ).bind( control , chart ).draw( data );
                } else {
                    chart.setDataTable( data );
                    chart.draw();
                }

                return response.reports;

            }
        };
    };
}( 0 );

var Chart = {
    block                   : $( '#weatherReportBase' ).html()
    , nodeTool              : $( '.tools-cols[data-type-of-chart=classic]' )
    , exemplarQuestion      : $( '#exeChartQuestion' ).html()
    , exemplarSubQuestion   : $( '#exeChartSubQuestion' ).html()
    , reportsCount      : function( data ) {
        return data.reports.length;
    }
    , getTitle          : function( data ) {
        var quizId;
        $.each( data.quizzes , function( index , quiz ) {
            quizId = quiz;
            return false;
        } );
        return MainPage.quizzes[ quizId ].name;
    }
    , drawChart         : function( response , data , block , draw ) {

        block.find( '.report-count .top-info' ).remove();

        var tool = this;

        var chart = ClassicChart( {
            control  : false
            , height : 360
            , width  : 480
        } , response );
        draw( chart.node );
        chart.render();

        var locations = [];
        $.each( response.reports , function( index , reportData ) {
            ( locations.indexOf( reportData.locationId ) < 0 ) && locations.push( reportData.locationId );
        } );

        var locationsListed = [];
        $.each( locations , function( index , locationId ) {
            locationsListed.push( MainPage.locations[ locationId ].name );
        } );
        block.find( '.locationsList .list' ).html( "<span>"+locationsListed.join( '</span>, <span> ' )+"</span>" );

        if( locations.length > 1 ) {
            return;
        }

        $.each( response.reports , function( index , reportData ) {
            $.each( reportData.answers , function( index , answerData ) {
                var question = response.questions[ answerData.question ];

                question.grade = question.grade || {
                    amount   : 0
                    , length : 0
                };
                question.grade.amount += answerData.grade;
                question.grade.length++;

                question.notes = question.notes || {
                    length : 0
                };
                if( answerData.details.note ) {
                    question.notes.length++;
                }

                question.subAnswers = question.subAnswers || [];
                $.each( answerData.details.subAnswers , function( index , flag ) {
                    if( ! question.subAnswers[ index ] ) {
                        question.subAnswers[ index ] = [ 0 , 0 , 0 ];
                    }
                    if( ! flag ) {
                        return;
                    }

                    if( answerData.grade > 2 ) {
                        question.subAnswers[ index ][0]++;//positive
                    } else if( answerData.grade < 2 ) {
                        question.subAnswers[ index ][1]++;//negative
                    } else {
                        question.subAnswers[ index ][2]++;//neutral
                    }
                } );
            } );
        } );

        var nodeQuestions = $( '<div style="float: left;"></div>' );
        var averageGrade;
        var nodeQuestion;
        var nodeSubQuestions;
        var nodeSubQuestion;
        $.each( response.questions , function( questionId , questionData ) {
            nodeQuestions.append( nodeQuestion = $( tool.exemplarQuestion ) );

            averageGrade = questionData.grade.amount / questionData.grade.length;

            nodeQuestion.find( '.title' ).text( questionData.name );
            nodeQuestion.find( '.grade' ).addClass( 'grade-' + averageGrade.toFixed(0)).find( 'span' ).text( ( ( averageGrade - 2 ) / 2 * 100 ).toFixed(0) + '%' );
            nodeQuestion.find( '.notes-count span' ).text( questionData.notes.length );

            nodeSubQuestions = nodeQuestion.find( '.sub-questions' );
            $.each( questionData.subQuestions , function( index , title ) {
                nodeSubQuestions.append( nodeSubQuestion = $( tool.exemplarSubQuestion ) );
                nodeSubQuestion.find( '.sub-title' ).text( title );
                nodeSubQuestion.find( '.grade-plus' ).text( questionData.subAnswers[ index ][0] );
                nodeSubQuestion.find( '.grade-minus' ).text( questionData.subAnswers[ index ][1] );
                nodeSubQuestion.find( '.grade-neutral' ).text( questionData.subAnswers[ index ][2] );
            } );
        } );

        block.append( nodeQuestions );

    }
    , load              : function() {
        var tool = this;

        tool.nodeTool.on( 'click' , '.locationsList .oneLocationItem' , function() {
            tool.selectedLocationId = $( this ).data( 'id' );
            tool.nodeTool.find( '.previewBtn' ).removeClass( 'disabled' );
            tool.renderTool();
        } );

        tool.nodeTool.on( 'click' , '.quizzesList .oneQuizzItem' , function() {
            tool.selectedQuizId = $( this ).data( 'id' );
            tool.selectedLocations = ( tool.selectedQuizId in MainPage.quizzes ) ? JSON.parse( JSON.stringify( MainPage.quizzes[ tool.selectedQuizId ].locations ) ) : [];
            tool.nodeTool.find( '.previewBtn' ).removeClass( 'disabled' );
            tool.renderTool();
        } );

        tool.selectedLocations = [];
        tool.nodeTool.on( 'click' , '.data .item' , function() {
            var node = $( this );
            var locationId = node.data( 'id' );
            var locationIndex = tool.selectedLocations.indexOf( locationId );

            if( node.hasClass( 'active' ) ) {
                ( locationIndex > -1 ) && tool.selectedLocations.splice( locationIndex , 1 );
            } else {
                ( locationIndex < 0 ) && tool.selectedLocations.push( locationId );
            }

            tool.nodeTool.find( '.previewBtn' ).removeClass( 'disabled' );

            tool.renderTool();
        } );

        tool.nodeTool.on( 'change' , '.date-selectors .minDate' , function() {
            tool.minDate = $( this ).datepicker( 'getDate' );
            tool.nodeTool.find( '.previewBtn' ).removeClass( 'disabled' );
            tool.renderTool();
        } );
        tool.nodeTool.on( 'change' , '.date-selectors .maxDate' , function() {
            tool.maxDate = $( this ).datepicker( 'getDate' );
            tool.nodeTool.find( '.previewBtn' ).removeClass( 'disabled' );
            tool.renderTool();
        } );

        tool.nodeTool.on( 'click' , '.previewBtn' , function() {
            var node = $( this );

            if( node.hasClass( 'disabled' ) ) {
                return;
            }

            node.addClass( 'disabled' );

            tool.createPreview( function( data ) {
                tool.data = data;

                var chart = ClassicChart( {
                    control         : true
                    , height        : 360
                    , changeRange   : function( event , inProgress ) {
                        if( ! inProgress ) {
                            tool.minDate = event.start;
                            tool.maxDate = event.end;
                            tool.renderTool();
                        }
                    }
                } , data );

                tool.nodeTool.prepend( $( chart.node ).addClass( 'exe-preview' ) );

                chart.render();
                tool.hidePreloader();

                return false;
            } );
        } );
    }
    , renderTool        : function() {
        var tool = this;
        var nodeDates = tool.nodeTool.find( '.date-selectors' );
        var nodeData = tool.nodeTool.find( '.selectedResalts .data' ).empty();

        tool.nodeTool
            .find( '.locationsList .oneLocationItem' )
            .removeClass( 'active' )
            .filter( tool.selectedLocationId ? '[data-id="' + tool.selectedLocationId + '"]' : '#loc-all' )
            .addClass( 'active' );

        tool.nodeTool
            .find( '.quizzesList .oneQuizzItem' )
            .removeClass( 'active' )
            .each( function() {
                var node = $( this );

                if( tool.selectedLocationId ) {
                    node[ ( MainPage.locations[ tool.selectedLocationId ].quizzes.indexOf( node.data( 'id' ) ) < 0 ) ? 'hide' : 'show' ]();
                } else {
                    node.show();
                }

                if( node.is( '[data-id="' + tool.selectedQuizId + '"]' ) ) {
                    node.addClass( 'active' );
                }
            } );


        if( ( ( tool.selectedLocationId in MainPage.locations )
            ? ( MainPage.locations[ tool.selectedLocationId ].quizzes.indexOf( tool.selectedQuizId ) > -1 )
            : true )
            && ( tool.selectedQuizId in MainPage.quizzes )
            ) {
            nodeData.append( '<h4>' + MainPage.quizzes[ tool.selectedQuizId ].name + '</h4>' );
            $.each( MainPage.quizzes[ tool.selectedQuizId ].locations , function( index , locationId ) {
                var loc = $( $( '#exemplarResultLocation' ).html() )
                    .addClass( 'item' + ( tool.selectedLocations.indexOf( locationId ) < 0 ? '' : ' active' ) )
                    .attr( 'id' , 'result-location-' + locationId )
                    .data( 'id' , locationId );
                var item = loc.find( '.tools-row-item-cell' );
                item.find( 'span' ).text( MainPage.locations[ locationId ].name );
                item.find( '.icon' ).addClass( tool.selectedLocations.indexOf( locationId ) < 0 ? 'icon-plus' : 'icon-close' );
                nodeData.append( loc );
            } );
        }

        nodeDates.find( '.minDate' ).datepicker( 'setDate' , tool.minDate );
        nodeDates.find( '.maxDate' ).datepicker( 'setDate' , tool.maxDate );

        var isCompleteToSend = tool.isCompleteToSend();
        tool.nodeTool.find( '.saveBtn' )[ isCompleteToSend ? 'removeClass' : 'addClass' ]( 'disabled' );
        tool.nodeTool.find( '.previewBtn' )[ ( isCompleteToSend && ! tool.data ) ? 'removeClass' : 'addClass' ]( 'disabled' );
    }
    , isCompleteToSend  : function() {
        return ( this.selectedQuizId in MainPage.quizzes )
            && (
            ( this.selectedLocationId in MainPage.locations )
                ? ( MainPage.locations[ this.selectedLocationId ].quizzes.indexOf( this.selectedQuizId ) > -1 )
                : true
            )
            && this.selectedLocations.length
            && this.minDate instanceof Date
            && this.maxDate instanceof Date;
    }
    , dataToSend        : function() {
        var quizzes = {};
        quizzes[ this.selectedQuizId ] = this.selectedLocations;
        return {
            minDate     : this.minDate.getTime()
            , maxDate   : this.maxDate.getTime()
            , quizzes   : quizzes
        };
    }
    , clearData         : function() {
        this.selectedLocationId = '';
        this.selectedQuizId = '';
        this.selectedLocations = [];
        this.minDate = null;
        this.maxDate = null;

        this.nodeTool.find( '.exe-preview' ).remove();
    }
    , createPreview     : function( success ) {
        if( this.isCompleteToSend() ) {
            this.send( this.dataToSend() , success );
        }
    }
    , create            : function( data , success ) {
        var tool = this;

        if( ! tool.data ) {
            return tool.send( data , success );
        }

        if( success instanceof Function ) {
            var date;
            var reports = [];
            $.each( tool.data.reports , function( index , reportData ) {
                date = new Date( reportData.date ).getTime();
                if( date >= data.minDate && date <= data.maxDate ) {
                    reports.push( reportData );
                }
            } );
            tool.data.reports = reports;
            success.call( tool , tool.data );
        }

        delete this.data;
    }
};