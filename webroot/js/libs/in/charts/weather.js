var Emotion = {
    image       : function( src , onload ) {
        var img    = new Image;
        img.src    = src;
        img.onload = onload;
    }
    , addText   : function( context , text , fontSize , fontWeight , translate , degrees ) {
        if( ! ( translate instanceof Function ) ) {
            translate = function( text ) {};
        }
        var textWidth;

        context.save();
        context.font = ( fontWeight ? fontWeight + ' ' : '' ) + fontSize + 'px ' + 'Source Sans Pro';
        context.fillStyle = 'rgba( 255 , 255 , 255 , .8 )';
        context.shadowColor = "rgba( 0 , 0 , 0, .41 )";
        context.shadowOffsetX = 0;
        context.shadowOffsetY = 4;
        context.shadowBlur = 12;
        translate = translate.call( text , textWidth = context.measureText( text ).width );
        context.translate( translate.x || 0 , translate.y || 0 );
        context.rotate( ( degrees || 0 ) * Math.PI / 180 );
        context.fillText( text , 0 , fontSize / 4 );
        context.restore();

        return textWidth;
    }
    , create    : function( quiz , chartWidth , chartHeight , callback ) {
        var questions = {};
        var grade     = {
            length   : 0
            , amount : 0
        };
        $.each( quiz.locations , function( locationId , locationQuestions ) {
            $.each( locationQuestions , function( questionId , questionData ) {
                questions[ questionId ] = questions[ questionId ] || {
                    grade      : {
                        length   : 0
                        , amount : 0
                    }
                    , priority : {
                        length   : 0
                        , amount : 0
                    }
                };

                grade.length++;
                questions[ questionId ].grade.length++;
                questions[ questionId ].priority.length++;

                grade.amount += questionData.grade;
                questions[ questionId ].grade.amount += questionData.grade;
                questions[ questionId ].priority.amount += questionData.priority;
            } );
        } );
        grade = ( grade.amount / grade.length ).toFixed();

        var questionsLength = 0;
        $.each( quiz.questions , function() {
            questionsLength++;
        } );

        var result = [];
        function getPosition( value ) {
            return result[ value ] ? arguments.callee( value + 1 ) : value;
        }
        $.each( quiz.questions , function( questionId , questionData ) {
            result[ getPosition( +( questions[ questionId ].priority.amount / questions[ questionId ].priority.length ).toFixed() ) ] = {
                text       : questionData.name
                , grade    : ( questions[ questionId ].grade.amount / questions[ questionId ].grade.length ).toFixed()
            };
        } );

        function draw() {
            if( ! Emotion.imagesList ) {
                $.getJSON( '/api/reports/chart/weather/img-list/' , function( event ) {
                    Utils.log( 'weather chart: images list' );
                    Emotion.imagesList = event.data;
                    draw();
                } );
                return;
            }

            var imagesList = Emotion.imagesList;
            var canvas     = document.createElement( 'canvas' );
            var context    = canvas.getContext( '2d' );

            chartHeight = 1536;
            chartWidth  = 2048;

            var fontSize = 48;
            var fontWeight = 'bold';

            var stick;
            var divider;

            var padding = {
                top      : 260
                , right  : 6
                , bottom : 240
                , left   : 6
            };

            var cloudWidth = ( chartWidth - padding.left - padding.right ) / 10;

            var box = {
                height  : 0
                , width : 0
            };

            var inverseGrade;
            var x;
            var y;

            function add( index ) {
                if( index > ( questionsLength - 1 ) ) {
                    context.drawImage( imagesList.topCloud[ grade ] , 0 , 0 , chartWidth , imagesList.topCloud[ grade ].height * chartWidth / imagesList.topCloud[ grade ].width );
                    context.drawImage( imagesList.bottomCloud[ grade ] , 0 , chartHeight - imagesList.bottomCloud[ grade ].height * chartWidth / imagesList.bottomCloud[ grade ].width , chartWidth , imagesList.bottomCloud[ grade ].height * chartWidth / imagesList.bottomCloud[ grade ].width );

                    var irrelevantTextWidth = Emotion.addText( context , 'Least important' , fontSize , fontWeight , function( textWidth ) {
                        return {
                            x   : chartWidth - cloudWidth / 5 - textWidth
                            , y : box.height / 2
                        };
                    } );
                    var importantTextWidth = Emotion.addText( context , 'More important' , fontSize , fontWeight , function() {
                        return {
                            x   : cloudWidth / 4
                            , y : box.height / 2
                        };
                    } );
                    context.drawImage( imagesList.priority_low , chartWidth - irrelevantTextWidth - cloudWidth / 3 - imagesList.priority_low.width , box.height / 2 - imagesList.priority_low.height / 2 , imagesList.priority_low.width , imagesList.priority_low.height );
                    context.drawImage( imagesList.priority_hight , importantTextWidth + cloudWidth / 3 , box.height / 2 - imagesList.priority_hight.height / 2 , imagesList.priority_hight.width , imagesList.priority_hight.height );

                    if( callback instanceof Function ) {
                        callback.call( canvas );
                    }

                    Utils.log( 'weather chart: complete' );

                    return;
                }

                if( ! ( imagesList.texture instanceof Image ) ) {
                    return Emotion.image( imagesList.texture , function() {
                        Utils.log( 'weather chart: texture' );
                        imagesList.texture = this;
                        add( index )
                    } );
                }
                if( ! ( imagesList.topCloud[ grade ] instanceof Image ) ) {
                    return Emotion.image( imagesList.topCloud[ grade ] , function() {
                        Utils.log( 'weather chart: top cloud of grade (' + grade + ')' );
                        imagesList.topCloud[ grade ] = this;
                        add( index );
                    } );
                }
                if( ! ( imagesList.bottomCloud[ grade ] instanceof Image ) ) {
                    return Emotion.image( imagesList.bottomCloud[ grade ] , function() {
                        Utils.log( 'weather chart: bottom cloud of grade (' + grade + ')' );
                        imagesList.bottomCloud[ grade ] = this;
                        add( index );
                    } );
                }
                if( ! ( imagesList.priority_low instanceof Image ) ) {
                    return Emotion.image( imagesList.priority_low , function() {
                        Utils.log( 'weather chart: pic priority low' );
                        imagesList.priority_low = this;
                        add( index );
                    } );
                }
                if( ! ( imagesList.priority_hight instanceof Image ) ) {
                    return Emotion.image( imagesList.priority_hight , function() {
                        Utils.log( 'weather chart: pic priority hight' );
                        imagesList.priority_hight = this;
                        add( index );
                    } );
                }
                if( ! ( imagesList.stick instanceof Image ) ) {
                    return Emotion.image( imagesList.stick , function() {
                        Utils.log( 'weather chart: stick' );
                        imagesList.stick = this;
                        add( index );
                    } );
                }
                if( !( index in result ) ) {
                    result.splice( index , 1 );
                    return add( index );
                }
                if( typeof imagesList.grade[ result[ index ].grade ] == 'string' ) {
                    return Emotion.image( imagesList.grade[ result[ index ].grade ] , function() {
                        imagesList.grade[ result[ index ].grade ] = {
                            height  : this.height * ( cloudWidth - 23 ) / this.width
                            , image : this
                            , width : cloudWidth - 23
                        };
                        add( index );
                    } );
                }
                if( !box.width ) {
                    box.height = ( chartHeight - padding.top - padding.bottom ) / 5;
                    box.width = ( chartWidth - padding.left - padding.right ) / questionsLength;
                }

                if( ! index ) {
                    context.fillStyle = 'rgba( ' + imagesList.backgroundColors[ grade ] + ' )';
                    context.fillRect( 0 , 0 , chartWidth , chartHeight );
                    context.drawImage( imagesList.texture , 0 , 0 , chartWidth , chartHeight );
                }


                inverseGrade = 4 - result[ index ].grade;
                x = padding.left + ( box.width ) * index;
                y = padding.top + box.height * inverseGrade;

                context.drawImage( imagesList.stick , x + box.width / 2 - imagesList.stick.width / 2 , inverseGrade > 2 ? y + box.height / 2 : 0 , imagesList.stick.width , inverseGrade > 2 ? chartHeight - y - box.height / 2 : chartHeight - ( chartHeight - y ) + box.height / 2 );

                if( index % 2 ) {
                    context.fillStyle = 'rgba( 53 , 104 , 148 , .06 )';
                    context.fillRect( x , 0 , box.width , chartHeight );
                }

                context.drawImage( imagesList.grade[ result[ index ].grade ].image , x + box.width / 2 - imagesList.grade[ result[ index ].grade ].width / 2 , y + box.height / 2 - imagesList.grade[ result[ index ].grade ].height / 2 , imagesList.grade[ result[ index ].grade ].width , imagesList.grade[ result[ index ].grade ].height );

                Emotion.addText( context , result[ index ].text , fontSize , fontWeight , function( textWidth ) {
                    return {
                        x   : x + box.width / 2
                        , y : inverseGrade < 3 ? y + textWidth + box.height : y - 5
                    };
                } , -90 );

                add( index + 1 );
            }

            canvas.height = chartHeight;
            canvas.width  = chartWidth;

            add(0)
        }
        draw();
    }
};

var Chart = {
    block               : $( '#weatherReportBase' ).html()
    , nodeTool          : $( '.tools-cols[data-type-of-chart=weather]' )

    , reportsCount      : function( data ) {
        return data.reports.count;
    }
    , getTitle          : function( data ) {
        return MainPage.quizzes[ data.quizId ].name;
    }
    , drawChart         : function( response , data , block , draw ) {
        Emotion.create( response , 480 , 360 , function() {
            var canvas = this;
            draw( $( this ).hide() );

            Emotion.image( this.toDataURL( 'image/jpeg' ) , function() {
                $( canvas ).after( $( '<a href="' + this.src + '" target="_blank" title="' + $( '#viewLargeChart' ).text() + '"></a>' ).append(
                    $( this ).css( {
                        height  : 360
                        , width : 480
                    } )
                ) );
            } );
        } );

        if( $.isPlainObject( response.locations ) ) {
            var locationsListed = [];
            $.each( response.locations , function( locationId ) {
                locationsListed[ locationsListed.length ] = MainPage.locations[ locationId ].name;
            } );
            block.find( '.locationsList .list' ).html( "<span>"+locationsListed.join( '</span>, <span> ' )+"</span>" );
            //block.find( '.locationsList .list' ).text( locationsListed.join( ', ' ) );
        }

        if( $.isArray( response.topSubAnswers ) && response.topSubAnswers.length ) {
            var index  = 1;
            var length = 3;
            var exemplar = '<div class="question"><div class="count"></div><div class="title"></div></div>';
            var topQuestions = block.find( '.top-questions' );
            $.each( response.topSubAnswers , function( question , questionData ) {
                question = $( exemplar );
                question.find( '.title' ).text( questionData.name );
                question.find( '.count' ).text( questionData.val > 999 ? ( questionData.val / 1000 ).toFixed(1).replace( '.' , ',' ) + 'K' : '' + questionData.val );
                topQuestions.append( question );

                if( ( index++ ) == length ) {
                    return false;
                }
            } );
            block.find( '.top-info' ).show();
            block.find( '.top-count' ).text( index - 1 < 2 ? '' : ( index - 1 || '0' ) );
        }
        else{
            block.find( '.top-info' ).remove();
        }
    }
    , load              : function() {
        var tool = this;

        tool.nodeTool.on( 'click' , '.locationsList .oneLocationItem' , function() {
            tool.selectedLocationId = $( this ).data( 'id' );
            tool.renderTool();
        } );

        tool.nodeTool.on( 'click' , '.quizzesList .oneQuizzItem' , function() {
            tool.selectedQuizId = $( this ).data( 'id' );
            tool.selectedLocations = ( tool.selectedQuizId in MainPage.quizzes ) ? JSON.parse( JSON.stringify( MainPage.quizzes[ tool.selectedQuizId ].locations ) ) : [];
            tool.renderTool();
        } );

        tool.selectedLocations = [];
        tool.nodeTool.on( 'click' , '.data .item' , function() {
            var node = $( this );
            var locationId = node.data( 'id' );
            var locationIndex = tool.selectedLocations.indexOf( locationId );

            if( node.hasClass( 'active' ) ) {
                ( locationIndex > -1 ) && tool.selectedLocations.splice( locationIndex , 1 );
            } else {
                ( locationIndex < 0 ) && tool.selectedLocations.push( locationId );
            }

            tool.renderTool();
        } );

        tool.nodeTool.on( 'change' , '.date-selectors .minDate' , function() {
            tool.minDate = $( this ).datepicker( 'getDate' );
            tool.renderTool();
        } );
        tool.nodeTool.on( 'change' , '.date-selectors .maxDate' , function() {
            tool.maxDate = $( this ).datepicker( 'getDate' );
            tool.renderTool();
        } );
    }
    , renderTool        : function() {
        var tool = this;
        var nodeDates = tool.nodeTool.find( '.date-selectors' );
        var nodeData = tool.nodeTool.find( '.selectedResalts .data' ).empty();

        tool.nodeTool
            .find( '.locationsList .oneLocationItem' )
            .removeClass( 'active' )
            .filter( tool.selectedLocationId ? '[data-id="' + tool.selectedLocationId + '"]' : '#loc-all' )
            .addClass( 'active' );

        tool.nodeTool
            .find( '.quizzesList .oneQuizzItem' )
            .removeClass( 'active' )
            .each( function() {
                var node = $( this );

                if( tool.selectedLocationId ) {
                    node[ ( MainPage.locations[ tool.selectedLocationId ].quizzes.indexOf( node.data( 'id' ) ) < 0 ) ? 'hide' : 'show' ]();
                } else {
                    node.show();
                }

                if( node.is( '[data-id="' + tool.selectedQuizId + '"]' ) ) {
                    node.addClass( 'active' );
                }
            } );


        if( ( ( tool.selectedLocationId in MainPage.locations )
                ? ( MainPage.locations[ tool.selectedLocationId ].quizzes.indexOf( tool.selectedQuizId ) > -1 )
                : true )
            && ( tool.selectedQuizId in MainPage.quizzes )
        ) {
            nodeData.append( '<h4>' + MainPage.quizzes[ tool.selectedQuizId ].name + '</h4>' );
            $.each( MainPage.quizzes[ tool.selectedQuizId ].locations , function( index , locationId ) {
                var loc = $( $( '#exemplarResultLocation' ).html() )
                    .addClass( 'item' + ( tool.selectedLocations.indexOf( locationId ) < 0 ? '' : ' active' ) )
                    .attr( 'id' , 'result-location-' + locationId )
                    .data( 'id' , locationId );
                var item = loc.find( '.tools-row-item-cell' );
                item.find( 'span' ).text( MainPage.locations[ locationId ].name );
                item.find( '.icon' ).addClass( tool.selectedLocations.indexOf( locationId ) < 0 ? 'icon-plus' : 'icon-close' );
                nodeData.append( loc );
            } );
        }

        nodeDates.find( '.minDate' ).datepicker( 'setDate' , tool.minDate );
        nodeDates.find( '.maxDate' ).datepicker( 'setDate' , tool.maxDate );

        tool.nodeTool.find( '.saveBtn' )[ tool.isCompleteToSend() ? 'removeClass' : 'addClass' ]( 'disabled' );
    }
    , isCompleteToSend  : function() {
        return ( this.selectedQuizId in MainPage.quizzes )
            && (
            ( this.selectedLocationId in MainPage.locations )
                ? ( MainPage.locations[ this.selectedLocationId ].quizzes.indexOf( this.selectedQuizId ) > -1 )
                : true
            )
            && this.selectedLocations.length
            && this.minDate instanceof Date
            && this.maxDate instanceof Date;
    }
    , dataToSend        : function() {
        var quizzes = {};
        quizzes[ this.selectedQuizId ] = this.selectedLocations;
        return {
            minDate     : this.minDate.getTime()
            , maxDate   : this.maxDate.getTime()
            , quizzes   : quizzes
        };
    }
    , clearData         : function() {
        this.selectedLocationId = '';
        this.selectedQuizId = '';
        this.selectedLocations = [];
        this.minDate = null;
        this.maxDate = null;
    }
};