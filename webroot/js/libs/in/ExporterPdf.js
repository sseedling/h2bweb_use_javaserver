/**
 * Created by Ramotion.
 * User: seedling
 * Date: 06.08.13
 * Time: 10:15
 * To change this template use File | Settings | File Templates.
 * //todo recode core for html texts
 */
var ExporterPdf = new function(){
    //page portraitSize:210:297,landscapeSize:297:210
    var _this = this;
    var pdfCore = null;
    var pageOrientation = {landscape:"landscape",portrait:"portrait"};
    var curPageOrientation = pageOrientation.landscape;
    var chartImgType = {
        JPG:"JPEG"
    }
    var fontType = {bold:"bold",normal:"normal"};
    var pageStyle = {
        title:{
            top:20,
            left:10,
            position:1,
            font:{
                size:20,
                color:[115,172,191]
            }
        },
        date:{
            top:30,
            left:10,
            position:2,
            font:{
                size:16,
                color:[0,0,0]
            }
        },
        reportCount:{
            top:40,
            left:10,
            position:3,
            font:{
                size:16,
                color:[0,0,0]
            }
        },
        subQuestionsList:{
            top:50,
            left:10,
            position:5,
            list:{
                font:{
                    weight:600,
                    size:16,
                    color:[0,0,0]
                }
            }
        },
        chart:{
            top:70,
            left:10,
            position:4,
            width:120,
            height:90,
            font:{
                size:16,
                color:[0,0,0]
            }
        },
        locationsList:{
            top:180,
            left:10,
            position:6,
            title:{
                font:{
                    size:16,
                    color:[0,0,0]
                }
            },
            list:{
                font:{
                    weight:600,
                    size:16,
                    color:[0,0,0]
                }
            }
        }
    }
    var pageStyleLandScape = {
        page:{
            one:{
                topSubQuestions:{
                    title:{
                        top:43,
                        left:210,
                        font:{
                            weight:fontType.normal,
                            size:11,
                            color:[0,0,0]
                        },
                        content:"Most Popular Questions"
                    },
                    list:{
                        top:4,
                        left:210,
                        font:{
                            weight:fontType.normal,
                            size:8,
                            color:[102,102,102]
                        }
                    }
                },
                chart:{
                    title:{
                        top:43,
                        left:30,
                        font:{
                            weight:fontType.normal,
                            size:11,
                            color:[0,0,0]
                        },
                        content:"Chart"
                    },
                    img:{
                        top:50,
                        left:30,
                        width:170,
                        height:128
                    }
                },
                locationsList:{
                    title:{
                        top:73,
                        left:210,
                        font:{
                            weight:fontType.normal,
                            size:11,
                            color:[0,0,0]
                        }
                    },
                    list:{
                        top:4,
                        left:210,
                        font:{
                            weight:fontType.normal,
                            size:8,
                            color:[102,102,102]
                        }
                    }
                }
            },
            two:{
                questionsList:{
                    title:{
                        top:38,
                        left:30,
                        font:{
                            weight:fontType.normal,
                            size:11,
                            color:[0,0,0]
                        },
                        content:"Questions list"
                    },
                    items:{
                        grade:{
                            top:27,
                            left:0,
                            font:{
                                weight:fontType.normal,
                                size:9,
                                color:[0,0,0]
                            }
                        },
                        questionName:{
                            top:27,
                            left:12,
                            font:{
                                weight:fontType.bold,
                                size:8,
                                color:[0,0,0]
                            }
                        },
                        subQuestionName:{
                            left:12,
                            top:4,
                            font:{
                                weight:fontType.normal,
                                size:8,
                                color:[102,102,102]
                            }
                        },
                        leftPart:{
                            top:45,
                            left:31
                        },
                        rightPart:{
                            top:45,
                            left:159
                        },
                        line:{
                            x1:0,
                            y1:-4,
                            x2:89,
                            y2:-4,
                            height:0.05,
                            color:[153,153,153]
                        }
                    }
                },
                legend:{
                    left:{
                        title:{
                            top:184,
                            left:30,
                            font:{
                                weight:fontType.bold,
                                size:8,
                                color:[0,0,0]
                            },
                            content:"Legend"
                        },
                        items:[
                            {
                                text:{
                                    top:189,
                                    left:30,
                                    content:"Completely satisfied",
                                    font:{
                                        weight:fontType.normal,
                                        size:8,
                                        color:[0,0,0]
                                    }
                                },
                                description:{
                                    top:189,
                                    left:57,
                                    content:"(from 75% to 100%)",
                                    font:{
                                        weight:fontType.normal,
                                        size:8,
                                        color:[102,102,102]
                                    }
                                }
                            },
                            {
                                text:{
                                    top:193,
                                    left:30,
                                    content:"Mostly satisfied",
                                    font:{
                                        weight:fontType.normal,
                                        size:8,
                                        color:[0,0,0]
                                    }
                                },
                                description:{
                                    top:193,
                                    left:51,
                                    content:"(from 25% to 75%)",
                                    font:{
                                        weight:fontType.normal,
                                        size:8,
                                        color:[102,102,102]
                                    }
                                }
                            },
                            {
                                text:{
                                    top:197,
                                    left:30,
                                    content:"No feeling either way",
                                    font:{
                                        weight:fontType.normal,
                                        size:8,
                                        color:[0,0,0]
                                    }
                                },
                                description:{
                                    top:197,
                                    left:58,
                                    content:"(from -25% to 25%)",
                                    font:{
                                        weight:fontType.normal,
                                        size:8,
                                        color:[102,102,102]
                                    }
                                }
                            },
                            {
                                text:{
                                    top:189,
                                    left:90,
                                    content:"Mostly unsatisfied",
                                    font:{
                                        weight:fontType.normal,
                                        size:8,
                                        color:[0,0,0]
                                    }
                                },
                                description:{
                                    top:189,
                                    left:114,
                                    content:"(from -75% to -25%)",
                                    font:{
                                        weight:fontType.normal,
                                        size:8,
                                        color:[102,102,102]
                                    }
                                }
                            },
                            {
                                text:{
                                    top:193,
                                    left:90,
                                    content:"Completely unsatisfied",
                                    font:{
                                        weight:fontType.normal,
                                        size:8,
                                        color:[0,0,0]
                                    }
                                },
                                description:{
                                    top:193,
                                    left:120,
                                    content:"(from -100% to -75%)",
                                    font:{
                                        weight:fontType.normal,
                                        size:8,
                                        color:[102,102,102]
                                    }
                                }
                            }
                        ]
                    },
                    right:{
                        lineH:{
                            x1:159,
                            y1:193,
                            x2:277,
                            y2:193
                        },
                        lineV:[
                            {
                                x1:164,
                                y1:191,
                                x2:164,
                                y2:194
                            },
                            {
                                x1:182,
                                y1:191,
                                x2:182,
                                y2:194
                            },
                            {
                                x1:206,
                                y1:191,
                                x2:206,
                                y2:194
                            },
                            {
                                x1:230,
                                y1:191,
                                x2:230,
                                y2:194
                            },
                            {
                                x1:252,
                                y1:191,
                                x2:252,
                                y2:194
                            },
                            {
                                x1:270,
                                y1:191,
                                x2:270,
                                y2:194
                            }
                        ],
                        digits:{
                            style:{
                                top:197,
                                left:160,
                                font:{
                                    weight:fontType.normal,
                                    size:8,
                                    color:[0,0,0]
                                },
                                content:"-100%"
                            },
                            items:[
                                {
                                    left:160,
                                    content:"-100%"
                                },
                                {
                                    left:178,
                                    content:"-75%"
                                },
                                {
                                    left:202,
                                    content:"-25%"
                                },
                                {
                                    left:228,
                                    content:"25%"
                                },
                                {
                                    left:250,
                                    content:"75%"
                                },
                                {
                                    left:268,
                                    content:"100%"
                                }
                            ]
                        },
                        words:{
                            style:{
                                top:186,
                                secondTop:188,
                                left:168,
                                font:{
                                    weight:fontType.normal,
                                    size:7,
                                    color:[102,102,102]
                                },
                                content:"-100%"
                            },
                            items:[
                                {
                                    left:168,
                                    first:"completely",
                                    second:"unsatisfied"
                                },
                                {
                                    left:190,
                                    first:"mostly",
                                    second:"unsatisfied"
                                },
                                {
                                    left:212,
                                    first:"no feeling",
                                    second:"either way"
                                },
                                {
                                    left:236,
                                    first:"mostly",
                                    second:"satisfied"
                                },
                                {
                                    left:256,
                                    first:"completely",
                                    second:"satisfied"
                                }
                            ]
                        }
                    }
                },
                rectangles:[
                    {
                        x1:159,
                        y1:194,
                        x2:10,
                        y2:4
                    },{
                        x1:177,
                        y1:194,
                        x2:10,
                        y2:4
                    },{
                        x1:201,
                        y1:194,
                        x2:10,
                        y2:4
                    }
                ]
            }
        },
        title:{
            top:12,
            left:30,
            font:{
                weight:fontType.normal,
                size:16,
                color:[11,147,196]
            }
        },
        date:{
            top:18,
            left:30,
            font:{
                weight:fontType.normal,
                size:11,
                color:[0,0,0]
            }
        },
        reportCount:{
            top:23,
            left:30,
            font:{
                weight:fontType.normal,
                size:11,
                color:[0,0,0]
            }
        },
        line:{
            x1:30,
            y1:30,
            x2:280,
            y2:30,
            height:0.1,
            color:[0,0,0]
        },
        firstPage:{
            top:100,
            left:100,
            font:{
                weight:600,
                size:26,
                color:[0,0,0]
            }
        },
        pageIndex:{
            top:15,
            left:240,
            font:{
                weight:fontType.normal,
                size:8,
                color:[0,0,0]
            }
        }
    }
    var selectedElem = null;

    this.init = function(elem){
        if(isset(elem))selectedElem = elem;
        selectedElem = $( selectedElem );
        if(!isFunction(window.jsPDF))return false;
        pdfCore = new jsPDF(curPageOrientation);
        return true;
    }

    this.create = function(){
        if(!pdfCore)return false;
        printFirstPage();
        var allCharts = collectAllCharts();
        for(var i=0;i<allCharts.length;i++){
            if(curPageOrientation==pageOrientation.portrait){
                createOnePortraitChart(allCharts[i]);
            }
            else{
                createOneLandscapeChart(allCharts[i]);
            }

        }
        return true;
    }

    this.save = function(){
        if(!pdfCore)return;
        pdfCore.output('dataurl');
        //pdfCore.output('save','report.pdf');
    }

    var printFirstPage = function(){
        var reportInfo = {
            title: 'Chart Exporter',
            subject: 'This is the subject',
            author: 'Manager name',
            keywords: 'generated, javascript, web 2.0, ajax',
            creator: 'Business-Weather'
        }
        pdfCore.setProperties(reportInfo);
        //First Page
        pdfCore.setTextColor(pageStyleLandScape.firstPage.font.color[0],pageStyleLandScape.firstPage.font.color[1],pageStyleLandScape.firstPage.font.color[2]);
        pdfCore.setFontSize(pageStyleLandScape.firstPage.font.size);
        pdfCore.text(pageStyleLandScape.firstPage.left,pageStyleLandScape.firstPage.top,reportInfo.creator);

    }

    var collectAllCharts =function(){
        var usingClasses = {
            chartBox:".charts-list .chartBox",
            title:".quizzName",
            date:".date",
            reportCount:{
                title:".report-count",
                list:{
                    main:".top-questions .question",
                    count:".count",
                    title:".title"
                }
            },
            locationsList:{
                title:".locationsList",
                list:".list span"
            },
            subQuestionsList:{
                title:".top-questions",
                list:".top-questions .question"
            },
            questionsDetails:".chart-question"
        }

        if( !selectedElem.length ) {
            selectedElem = null;
        }

        var allCharts = $(selectedElem||usingClasses.chartBox);

        selectedElem = null;

        for(var i=0;i<allCharts.length;i++){
            allCharts[i].dataObj = {
                title : $(usingClasses.title,allCharts[i]).text().replaceAll("\n","").cutDoubleSpaces().trim(),
                date : $(usingClasses.date,allCharts[i]).text().replaceAll("\n","").replace("—", '-').cutDoubleSpaces().trim(),
                reportCount : makeCollectedInfo(usingClasses.reportCount,allCharts[i]),
                subQuestionsList : makeTopQuestionsList(usingClasses.subQuestionsList,allCharts[i]),
                locationsList : makeLocationsList(usingClasses.locationsList,allCharts[i]),
                questionsDetails : makeQuestionDetails($(usingClasses.questionsDetails,allCharts[i])),
                svg : false,
                canvas : $("canvas",allCharts[i])[0]
            };

            if(!isset(allCharts[i].dataObj.canvas)){
                allCharts[i].dataObj.canvas = $("svg",allCharts[i])[0];
                allCharts[i].dataObj.svg = true;
            }
        }
        return allCharts;
    }

    var makeLocationsList = function(keyObj,chartBox){
        return {
            title : $(keyObj.title,chartBox).data("title"),
            list : $(keyObj.list,chartBox).map(function(){
                return this.innerHTML.trim();
            }).get()
        };
    }

    var makeCollectedInfo = function(keyObj,chartBox){
        return {
            title:$(keyObj.title,chartBox).text().replaceAll("\n","").cutDoubleSpaces().trim(),
            list:$(keyObj.list.main,chartBox).map(function(){
                return {
                    count:$(keyObj.list.count,this).text().trim(),
                    title:$(keyObj.list.title,this).text().trim()
                }
            }).get()
        }
    }

    var makeTopQuestionsList = function(subQuestionsList,chart){
        var list = "";
        var topList = $(subQuestionsList.list,chart);

        if(topList.length!=0){
            for(var i=0;i<topList.length;i++){
                if(list!="")list+=", ";
                list+= $(".title",topList[i]).text()+"("+$(".count",topList[i]).text()+")";
            }
        }

        return {
            list : list
        };
    }

    var makeQuestionDetails = function(itemsList){
        var list = new Array();
        if(itemsList.length!=0){
            for(var i=0;i<itemsList.length;i++){
                var item = itemsList[i];

                list.push({
                    'name':$(".info .title",item).text(),
                    'grade':$(".grade",item).text(),
                    'subQuestions':makeSubQuestionsDetails($('.chart-sub-question',item))
                });
            }
        }
        return list;
    }

    var makeSubQuestionsDetails = function(itemsList){
        var list = [];
        if(itemsList.length!=0){
            for(var i=0;i<itemsList.length;i++){
                var item = itemsList[i];

                list.push({
                    'name':$(".sub-title",item).text(),
                    'plus':$(".grade-plus",item).text(),
                    'minus':$(".grade-minus",item).text(),
                    'neutral':$(".grade-neutral",item).text()
                });
            }
        }
        return list;
    }

    var createOnePortraitChart = function(chartBox){
        //page 1 of 1
        pdfCore.addPage();

        //Title
        pdfCore.setTextColor(pageStyle.title.font.color[0],pageStyle.title.font.color[1],pageStyle.title.font.color[2]);
        pdfCore.setFontSize(pageStyle.title.font.size);
        pdfCore.text(pageStyle.title.left,pageStyle.title.top,chartBox.dataObj.title);

        //Date
        pdfCore.setTextColor(pageStyle.date.font.color[0],pageStyle.date.font.color[1],pageStyle.date.font.color[2]);
        pdfCore.setFontSize(pageStyle.date.font.size);
        pdfCore.text(pageStyle.date.left ,pageStyle.date.top,chartBox.dataObj.date);

        //Line
        pdfCore.setLineWidth(0.5);
        pdfCore.line(20, 20, 60, 20); // horizontal line


        //Report count
        pdfCore.setTextColor(pageStyle.reportCount.font.color[0],pageStyle.reportCount.font.color[1],pageStyle.reportCount.font.color[2]);
        pdfCore.setFontSize(pageStyle.reportCount.font.size);
        pdfCore.text(pageStyle.reportCount.left,pageStyle.reportCount.top,chartBox.dataObj.reportCount);

        //Subquestions
        pdfCore.setTextColor(pageStyle.subQuestionsList.list.font.color[0],pageStyle.subQuestionsList.list.font.color[1],pageStyle.subQuestionsList.list.font.color[2]);
        pdfCore.setFontType("normal");
        pdfCore.setFontSize(pageStyle.subQuestionsList.list.font.size);
        pdfCore.text(pageStyle.subQuestionsList.left,pageStyle.subQuestionsList.top,chartBox.dataObj.subQuestionsList.list);

        //Chart
        pdfCore.addImage(
            getChartPicture(chartBox.dataObj.canvas,chartBox.dataObj.svg),
            chartImgType.JPG,
            pageStyle.chart.left,
            pageStyle.chart.top,
            pageStyle.chart.width,
            pageStyle.chart.height
        );

        //Location list title
        pdfCore.setTextColor(pageStyle.locationsList.title.font.color[0],pageStyle.locationsList.title.font.color[1],pageStyle.locationsList.title.font.color[2]);
        pdfCore.setFontType("bold");
        pdfCore.setFontSize(pageStyle.locationsList.title.font.size);
        pdfCore.text(pageStyle.locationsList.left,pageStyle.locationsList.top,chartBox.dataObj.locationsList.title);

        //Location list
        pdfCore.setTextColor(pageStyle.locationsList.list.font.color[0],pageStyle.locationsList.list.font.color[1],pageStyle.locationsList.list.font.color[2]);
        pdfCore.setFontType("normal");
        pdfCore.setFontSize(pageStyle.locationsList.list.font.size);
        pdfCore.text(pageStyle.locationsList.left,pageStyle.locationsList.top  + 10,chartBox.dataObj.locationsList.list);


    };

    var createOneLandscapeChart = function(chartBox){
        var pagesCount = (chartBox.dataObj.questionsDetails.length >0)?2:1;
        for(var page_index=1;page_index<=pagesCount;page_index++) {
            //page i of 2
            pdfCore.addPage();

            //Page counter
            addTextToPdf(pageStyleLandScape.pageIndex,"Page "+page_index+" of "+pagesCount);

            //Title
            addTextToPdf(pageStyleLandScape.title,chartBox.dataObj.title);

            //Date
            addTextToPdf(pageStyleLandScape.date,chartBox.dataObj.date);

            //Report count
            addTextToPdf(pageStyleLandScape.reportCount,chartBox.dataObj.reportCount.title);

            //Line
            addGraphObjToPdf(pageStyleLandScape.line);

            switch (page_index) {
                case 1:{
                    //Chart title
                    addTextToPdf(pageStyleLandScape.page.one.chart.title,pageStyleLandScape.page.one.chart.title.content);

                    //Chart img
                    var chartImgStyle = pageStyleLandScape.page.one.chart.img;
                    pdfCore.addImage(
                        getChartPicture(chartBox.dataObj.canvas,chartBox.dataObj.svg),
                        chartImgType.JPG,
                        chartImgStyle.left,
                        chartImgStyle.top,
                        chartImgStyle.width,
                        chartImgStyle.height
                    );

                    var locationsListTitleStyle = Utils.clone(pageStyleLandScape.page.one.locationsList.title);

                    if(chartBox.dataObj.reportCount.list.length > 0){
                        //Most popular questions title
                        addTextToPdf(pageStyleLandScape.page.one.topSubQuestions.title,pageStyleLandScape.page.one.topSubQuestions.title.content);

                        //Most popular questions list
                        for(var topSub_index=0;topSub_index<chartBox.dataObj.reportCount.list.length;topSub_index++){
                            var topItemStyle = Utils.clone(pageStyleLandScape.page.one.topSubQuestions.list);
                            topItemStyle.top += pageStyleLandScape.page.one.topSubQuestions.title.top + 4 + topSub_index * topItemStyle.top;
                            addTextToPdf(topItemStyle,chartBox.dataObj.reportCount.list[topSub_index].title + "(" +chartBox.dataObj.reportCount.list[topSub_index].count + ")");
                        }
                    }
                    else{
                        locationsListTitleStyle.top = pageStyleLandScape.page.one.topSubQuestions.title.top;
                    }

                    //Location list title
                    addTextToPdf(locationsListTitleStyle,chartBox.dataObj.locationsList.title);

                    //Location list items
                    for(var location_index = 0;location_index<chartBox.dataObj.locationsList.list.length;location_index++){
                        var locationStyle = Utils.clone(pageStyleLandScape.page.one.locationsList.list);
                        locationStyle.top += locationsListTitleStyle.top + 4 + locationStyle.top * location_index;
                        addTextToPdf(locationStyle,chartBox.dataObj.locationsList.list[location_index]);
                    }
                    break;
                }
                case 2:{

                    //constants
                    var QUESTION_ON_COLUMN = 5;

                    //All questions title
                    var allQuestionsTitleStyle = pageStyleLandScape.page.two.questionsList.title;
                    addTextToPdf(allQuestionsTitleStyle,allQuestionsTitleStyle.content);

                    //All questions list
                    for(var question_index=0;question_index<chartBox.dataObj.questionsDetails.length;question_index++){

                        //styles
                        var detailsStyle = Utils.clone(pageStyleLandScape.page.two.questionsList.items);
                        var detailsPart = Utils.clone((question_index<QUESTION_ON_COLUMN)?detailsStyle.leftPart:detailsStyle.rightPart);
                        var questionNameStyle = Utils.clone(detailsStyle.questionName);
                        var gradeStyle = Utils.clone(detailsStyle.grade);
                        var lineStyle = Utils.clone(detailsStyle.line);

                        //data
                        var questionObj = chartBox.dataObj.questionsDetails[question_index];

                        //grade
                        gradeStyle.left += detailsPart.left;
                        gradeStyle.top = (question_index % QUESTION_ON_COLUMN) * gradeStyle.top + detailsPart.top;
                        addTextToPdf(gradeStyle,questionObj.grade);

                        //grade rectangle
                        if(questionObj.grade.indexOf("-")>-1){
                            var rectStyle = {
                                x1: gradeStyle.left-1,
                                y1: (gradeStyle.top -3),
                                x2: 10,
                                y2: 4
                            };

                            addGraphObjToPdf(rectStyle,"rect");
                        }

                        //Line
                        if(question_index % QUESTION_ON_COLUMN){
                            lineStyle.y1 = gradeStyle.top + lineStyle.y1;
                            lineStyle.y2 = lineStyle.y1;
                            lineStyle.x1 += detailsPart.left;
                            lineStyle.x2 += detailsPart.left;
                            addGraphObjToPdf(lineStyle,"line");
                        }

                        //question name
                        questionNameStyle.left += detailsPart.left;
                        questionNameStyle.top = (question_index % QUESTION_ON_COLUMN) * questionNameStyle.top + detailsPart.top;
                        addTextToPdf(questionNameStyle,(1+question_index)+". "+questionObj.name);

                        //sub-questions
                        for(var sub_question_index=0;sub_question_index<questionObj.subQuestions.length;sub_question_index++){
                            var subQuestionObj = questionObj.subQuestions[sub_question_index];
                            var subQuestionStyle = Utils.clone(detailsStyle.subQuestionName);

                            //sub-question
                            subQuestionStyle.left += detailsPart.left;
                            subQuestionStyle.top += questionNameStyle.top + sub_question_index*subQuestionStyle.top;
                            addTextToPdf(subQuestionStyle,subQuestionObj.name+"("+subQuestionObj.plus+","+subQuestionObj.minus+","+subQuestionObj.neutral+","+")");
                        }
                    }

                    //draw legend and static rectangles
                    drawLegend();
                    drawRectangles(pageStyleLandScape.page.two.rectangles);
                    break;
                }
            }
        }
    }

    var getChartPicture = function(chartCanvas,isSvg){
        if((isSvg)&&(isFunction(window.canvg))){
            var svg = chartCanvas.parentNode.innerHTML;
            chartCanvas = document.createElement("canvas");
            chartCanvas.setAttribute('width', "480");
            chartCanvas.setAttribute('height', "360");
            canvg(chartCanvas, svg);
        }
        if(!chartCanvas)return null;
        var data = chartCanvas.toDataURL('image/jpeg');
        if (data.substring(0, 23) === 'data:image/jpeg;base64,') {
            data = atob(data.replace('data:image/jpeg;base64,', ''));
        }
        return data;
    }

    var addTextToPdf = function(styleObj,text){
        pdfCore.setFontType(styleObj.font.weight);
        pdfCore.setTextColor(styleObj.font.color[0],styleObj.font.color[1],styleObj.font.color[2]);
        pdfCore.setFontSize(styleObj.font.size);
        pdfCore.text(styleObj.left,styleObj.top,text);
    }

    var addGraphObjToPdf = function(styleObj,mode){
        var color = styleObj.color || [0,0,0];
        var height = styleObj.height || 0.1;
        pdfCore.setLineWidth(height);

        pdfCore.setDrawColor(color[0],color[1],color[2]);
        switch (mode){
            case "line":{
                pdfCore.line(styleObj.x1, styleObj.y1, styleObj.x2, styleObj.y2);
                break;
            }
            case "rect":{
                pdfCore.rect(styleObj.x1, styleObj.y1, styleObj.x2, styleObj.y2);
                break;
            }
        }
    }

    var drawLegend = function(){
        //footer Line
        var footerLineStyle = Utils.clone(pageStyleLandScape.line);
        footerLineStyle.y1 = 178;
        footerLineStyle.y2 = footerLineStyle.y1;
        addGraphObjToPdf(footerLineStyle,"line");

        //Legend-left title
        var legendStyle = pageStyleLandScape.page.two.legend.left;
        addTextToPdf(legendStyle.title,legendStyle.title.content);

        //Legend-left data
        var legendData  = pageStyleLandScape.page.two.legend.left.items;
        for(var legend_items = 0; legend_items<legendData.length;legend_items++ ){
            addTextToPdf(legendData[legend_items].text,legendData[legend_items].text.content);
            addTextToPdf(legendData[legend_items].description,legendData[legend_items].description.content);
        }

        //Legend-right lines
        addGraphObjToPdf(pageStyleLandScape.page.two.legend.right.lineH,"line");
        var vLines = pageStyleLandScape.page.two.legend.right.lineV;
        for(var line_index=0;line_index<vLines.length;line_index++){
            addGraphObjToPdf(vLines[line_index],"line");
        }

        //Legend-right digits
        var digits = pageStyleLandScape.page.two.legend.right.digits.items;
        var digitsStyle = Utils.clone(pageStyleLandScape.page.two.legend.right.digits.style);
        for(var digits_index=0;digits_index<digits.length;digits_index++){
            digitsStyle.left = digits[digits_index].left;
            addTextToPdf(digitsStyle,digits[digits_index].content);
        }

        //Legend-right words
        var words = pageStyleLandScape.page.two.legend.right.words.items;
        var wordsStyle = Utils.clone(pageStyleLandScape.page.two.legend.right.words.style);
        var wordsSecondStyle = Utils.clone(wordsStyle);
        wordsSecondStyle.top = wordsSecondStyle.secondTop;
        for(var word_index=0;word_index<words.length;word_index++){

            wordsStyle.left = words[word_index].left;
            wordsSecondStyle.left = words[word_index].left;

            addTextToPdf(wordsStyle,words[word_index].first);
            addTextToPdf(wordsSecondStyle,words[word_index].second);
        }
    }

    var drawRectangles = function(rectangles){
        for(var rect_index=0;rect_index<rectangles.length;rect_index++){
            addGraphObjToPdf(rectangles[rect_index],"rect");
        }
    }
}