var MainPage = new function(){

    var initExportCSV = function(){
        if((isset(window.exportTableToCSV))&&(isFunction(exportTableToCSV.init))){
            setTimeout(function(){
                exportTableToCSV.init($("#export_CSV_link"), $("#reportTable"), "ClassicDetailsReport.csv");
            },1);
        }
    };

    this.init = function(){
        initExportCSV();
    }
}