var MainPage = new function(){
    var nodeFilters;
    var nodeContent;
    var nodeEmpty;
    var nodeCheckedFilters;

    var role = [];
    var roleNames = [];
    var status;

    function rendered() {
        var index = 0;

        nodeFilters.find( '.row' ).each( function() {
            var filter = $( this ).removeClass( 'active' );

            if( ( role.indexOf( filter.data( 'role' ) ) > -1 ) || ( filter.is( '[data-status]' ) && filter.data( 'status' ) == status ) ) {
                filter.addClass( 'active' );
            }
        } );

        nodeContent.find( '.row' ).each( function() {
            var user = $( this ).hide();

            if( user.data( 'status' ) == status ) {
                index++;
                return user.css( 'display' , 'block' );
            }

            $.each( ( '' + user.data( 'role' ) ).split( ',' ) , function( i , roleName ) {
                if( role.indexOf( roleName.slice( 0 , -1 ) ) > - 1 ) {
                    index++;
                    user.css( 'display' , 'block' );
                }
            } );
        } );

        if( index ) {
            nodeContent.removeClass( 'empty-list' );

            var filters_ = ( '' + roleNames ).split( ',' );
            if( status == 0 ) {
                filters_[ !filters_[0] ? 0 : filters_.length ] = $( '.filters .row[data-status="0"] .row-title span' ).text();
            }

            nodeCheckedFilters.html( '<b>' + ( filters_.length - 1 ? filters_.slice( 0 , -1 ).join( '</b>, <b>' ) + '</b> ' + $( '#textAnd' ).text() + ' <b>' + filters_[ filters_.length - 1 ] : filters_[0] ) + '</b> ' + $( '#textUsersAreShown' ).text() );
        } else {
            nodeContent.addClass( 'empty-list' );
        }
    }

    this.init = function() {
        nodeFilters        = $( '.filters' );
        nodeContent        = $( '.content-list' );
        nodeEmpty          = nodeContent.find( '.empty' );
        nodeCheckedFilters = nodeContent.find( '.checked-filters' );

        nodeFilters.on( 'click' , '.row[data-role]' , function() {
            var roleFilter   = $( this ).data( 'role' );
            var roleName     = $( this ).find( '.row-title span' ).text();
            var indexOfArray = role.indexOf( roleFilter );
            var indexOfArrayNames = roleNames.indexOf( roleName );

            if( indexOfArray < 0 ) {
                role[ role.length ] = roleFilter;
            } else {
                role.splice( indexOfArray , 1 );
            }
            if( indexOfArrayNames < 0 ) {
                roleNames[ roleNames.length ] = roleName;
            } else {
                roleNames.splice( indexOfArrayNames , 1 );
            }

            rendered();
        } );

        nodeFilters.on( 'click' , '.row[data-status]' , function() {
            status = status == 0 ? void 0 : 0;

            rendered();
        } );

        nodeFilters.find( '.row' ).click();
    };
};