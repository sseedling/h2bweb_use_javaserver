var MainPage = new function(){
    var exemplarGroup;
    var exemplarPrize;
    var exemplarQuiz;

    var activeGroupId;

    function toggleTab( node , flag ) {
        flag = flag ? 'addClass' : 'removeClass';
        $( node )[ flag ]( 'active' );
        $( '#' + $( node ).data( 'id' ) )[ flag ]( 'active' );
    }
    function initTabs() {
        $( '.tabs' ).on( 'click' , '.tab' , function() {
            var node = $( this );

            if( node.hasClass( 'active' ) ) {
                return;
            }

            node.siblings( '.tab' ).each( function() {
                toggleTab( this );
            } );

            toggleTab( this , true );
        } );
    }

    function getPrizesIds( groupId ) {
        var presentPrizesIds = [];

        if( groupId && MainPage.data.groups[ groupId ] ) {
            $.each( MainPage.data.groups[ groupId ].prizes , function( index , prizeId ) {
                if( prizeId && MainPage.data.prizes[ prizeId ] ) {
                    presentPrizesIds[ presentPrizesIds.length ] = prizeId;
                }
            } );
        } else {
            $.each( MainPage.data.prizes , function( prizeId ) {
                presentPrizesIds[ presentPrizesIds.length ] = prizeId;
            } );
        }

        return {
            existed : presentPrizesIds.length ? presentPrizesIds : null
            , all   : groupId ? MainPage.data.groups[ groupId ].prizes : null
        };
    }

    function appendPrize( nodePrizesList , prizeId , prizeData , isWrite ) {
        var prize = ( exemplarPrize = exemplarPrize || $( $("#exemplarPrize").html() ) ).clone().attr( 'id' , 'prize-' + prizeId ).data( 'id' , prizeId ).attr( 'data-count' , prizeData.count );
        var view = prize.find( '.view' );
        var edit = prize.find( '.edit' );

        function successSetCount() {
            if(!prizeData.count)prizeData.count = 0;
            prize.attr( 'data-count' , prizeData.count ).removeClass( 'editable' ).find( '.prize-count' ).text( prizeData.count ).val( prizeData.count );
        }

        prize.get(0).setCount = function( count ) {
            if( ( count == void 0 ) || ( count == prizeData.count ) ) {
                successSetCount();
            } else {
                prizeData.count = count;
                successSetCount();
                $.get( '/api/prizes/set-prise-count/' , {
                    prizeId      : prizeId
                    , prizeCount : prizeData.count = count
                } );
            }
        };

        prize.find( '.prize-title span' ).text( prizeData.name );
        prize.get(0).setCount();
        view.click( function() {
            if( !isWrite ) {
                return;
            }
            nodePrizesList.find( '.prize-item' ).each( function() {
                this.setCount();
            } );
            prize.addClass( 'editable' ).find( 'input.prize-count' ).focus();
        } );

        edit.find( '.cancel' ).click( function() {
            prize.get(0).setCount();
        } );
        edit.find( '.done' ).click( function() {
            prize.get(0).setCount( edit.find( '.prize-count' ).val() );
        } );

        Utils.bindDigitsOnly(edit.find( 'input.prize-count' ),false);


        if( !isWrite ) {
            edit.remove();
        }

        nodePrizesList.append( prize );
    }


    this.init = function() {
        initTabs();

        var nodeGroupsPrize = $( '.groups-prize .rows' );
        var nodePrizesList  = $( '#prizes-list' );
        var nodeAddQuizzes  = $( '#add-quizzes' );
        var isWrite         = nodePrizesList.hasClass( 'write' );
        var nodeGroupInformation = $( '.information' );

        nodeGroupsPrize.on( 'click' , '.group-item' , function() {
            var prizesShowed = 0;
            var quizzesShowed = 0;

            activeGroupId = $( this ).data( 'id' );

            nodeGroupsPrize.find( '.group-item' ).removeClass( 'active' );
            $( '#group-' + ( activeGroupId || 'all' ) ).addClass( 'active' );

            var rows = nodePrizesList.find( '.row' )[ activeGroupId ? 'hide' : 'show' ]();

            $( '.information' )[ activeGroupId ? 'show' : 'hide' ]();

            if( activeGroupId && MainPage.data.groups[ activeGroupId ] ) {
                nodeGroupInformation.find( '.btnEdit' ).attr( 'href' , '/prizes/edit/' + activeGroupId );
                nodeGroupInformation.find( '.group-name' ).text( MainPage.data.groups[ activeGroupId ].name );
                nodeGroupInformation.find( '.group-note' ).text( MainPage.data.groups[ activeGroupId ].note );
                $.each( MainPage.data.groups[ activeGroupId ].prizes , function( index , prizeId ) {
                    $( '#prize-' + prizeId ).show();
                    prizesShowed++;
                } );

                var quizzesList = $( '#quizzes-container .list' ).empty();
                if( Object.keys( MainPage.data.groups[ activeGroupId ].quizzes ).length ) {
                    $.each( MainPage.data.groups[ activeGroupId ].quizzes , function( quizId , quizText ) {
                        var quiz = (exemplarQuiz = exemplarQuiz || $( $("#exemplarQuiz2").html() )).clone();
                        quiz.find( '.row-title span' ).text( quizText );
                        quizzesList.append( quiz );
                        quizzesShowed++;
                    } );
                    nodeAddQuizzes.attr( 'href' , '' ).hide();
                } else {
                    nodeAddQuizzes.attr( 'href' , '/prizes/edit/' + activeGroupId + '#quizzes' ).css( 'display' , 'block' );
                }
            } else {
                $( '[data-id=prizes-container]' ).click();
            }

            $( '#prizes-container .placeholder' )[ ( activeGroupId ? prizesShowed : rows.length ) ? 'hide' : 'show' ]();
            $( '#quizzes-container .placeholder' )[ quizzesShowed ? 'hide' : 'show' ]();
        } );


        $.each( MainPage.data.groups || {} , function( groupId , groupData ) {
            var group = (exemplarGroup = exemplarGroup || $( $("#exemplarGroup").html() ) ).clone().attr( 'id' , 'group-' + groupId ).data( 'id' , groupId );
            group.find( '.group-title span' ).text( groupData.name );
            nodeGroupsPrize.append( group );
        } );

        $.each( MainPage.data.prizes || {} , function( prizeId , prizeData ) {
            appendPrize( nodePrizesList , prizeId , prizeData , isWrite );
        } );

        $( '#add-prizes' ).click( function() {
            if( $( this ).hasClass( 'disabled' ) ) {
                return;
            }

            var node = $( this ).addClass( 'disabled' );
            var icon = node.find( '.icon' ).addClass( 'rotation' );

            $.post( '/api/prizes/get-prizes-list/' , {
                count         : 100
                , prizeIdList : getPrizesIds( activeGroupId )
            } , function( event ) {
                $.each( event.data , function( prizeId , prizeData ) {
                    if( prizeId && !MainPage.data.prizes[ prizeId ] ) {
                        MainPage.data.prizes[ prizeId ] = prizeData;
                        appendPrize( nodePrizesList , prizeId , prizeData , isWrite );
                    }
                } );
                node.removeClass( 'disabled' );
                icon.removeClass( 'rotation' );
            } );
        } );

        $( '#group-all' ).click();
    };
};