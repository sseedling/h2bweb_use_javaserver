/**
 * View cur location page
 *
 */

var MainPage = new function(){
    var _this = this;

    var initAttachQuizz = function(){
        $(".attach-survey").on("change",function(){
            if(this.value=="")return;
            var confirmMsg = ($(this).attr("confirmMsg"));
            confirmMsg = confirmMsg.replace("%",$("option[value='"+this.value+"']",this).html());
            if(confirm(confirmMsg)){
                var locationUrl = $(this).attr("locationUrl");
                var quizzUrl = this.value;
                Utils.followUrl("/survey/attach/"+locationUrl+"/"+quizzUrl+"/attach-survey");
            }
            else{
                Utils.resetElem(this);
                $(this).val("");
            }
        });
    }

    var initSurveysList = function(){
        $(".surveysList .row-item").on("click",function(){
            Utils.followUrl($(this).attr("survey-name")+"/");
        });
    }

    this.init = function(){
        initSurveysList();
        initAttachQuizz();
    }
}