/**
 * View cur location page
 *
 */

var MainPage = new function(){
    var _this = this;

    var initSubmitLocale = function(){
        var submitBtn = "div.saveBtn";
        var submitForm = "#localeForm";

        $(submitBtn).on("click",function(){
            $(submitForm).submit();
        });
    }

    var initSelImgBtn = function() {
        var classPull = {
            imgElem:'.file-preview img',
            inputBtn:'.file-selector div',
            inputElem:'.file-selector input'
        }

        $(classPull.inputBtn).click( function() {
            $("input", this.parentNode).click();
        } );

        $(classPull.inputElem).change( function() {
            var imgElem = $(classPull.imgElem,this.parentNode.parentNode)[0];
            $(imgElem).data( 'default' , 'false' );
            Utils.drawImgToImg( {
                inputElem   : this
                , drawElem  : imgElem
            } );
        } );
    };

    var initOnTypeLocaleData = function(){
        $(".text-field").on("blur",function(){
            $("[type='hidden']",this.parentNode).val(this.value);
        });
    }

    this.init = function(){
        initSubmitLocale();
        initSelImgBtn();
        initOnTypeLocaleData();
    }
}