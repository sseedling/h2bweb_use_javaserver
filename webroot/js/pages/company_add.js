var MainPage = new function(){
    this.checkSend  = function() {
        var name            = ! MainPage.nodeName.val();
        var credits         = ! MainPage.nodeCredits.val();
        var email           = MainPage.nodeEmail.val();
        var isInvalidEmail  = ! Utils.isValidEmail( email );

        if( name ) {
            MainPage.nodeName.addClass( 'error' );
        }
        if( credits ) {
            MainPage.nodeCredits.addClass( 'error' );
        }
        if( ! email || isInvalidEmail ) {
            MainPage.nodeEmail.addClass( 'error' );
        }
        return name || credits || ! email || isInvalidEmail;
    };

    this.init  = function() {
        function err_() {
            $( this ).removeClass( 'success' ).removeClass( 'error' );
        }
        
        MainPage.nodeName       = $( '[name="company[name]"]' ).on( 'focus click' , err_ );
        MainPage.nodeCredits    = $( '[name="company[credits]"]' ).on( 'focus click' , err_ );
        MainPage.nodeEmail      = $( '[name="owner[email]"]' ).on( 'focus click' , err_ );

        var nodeStatusBox       = $( '.status-box' );
        nodeStatusBox.append( Utils.SwitchBox( {
            status           : nodeStatusBox.find( 'input[type=hidden]' ).val() == "true"
            , textOn         : nodeStatusBox.data("trusted-text")
            , textOff        : nodeStatusBox.data("usual-text")
            , onChangeStatus : function( status ) {
                nodeStatusBox.find( 'input[type=hidden]' ).val( status );
            }
        } ) );

        $( '.check-email' ).click( function() {
            if( !MainPage.nodeEmail.val() ) {
                MainPage.nodeEmail.addClass( 'error' );
                return;
            }

            if( $( this ).hasClass( 'disabled' ) ) {
                return;
            }

            var btn = $( this ).addClass( 'disabled' );

            $.getJSON( '/api/user/check/email/' + MainPage.nodeEmail.val() + '/' + MainPage.nodeEmail.data("owner-id") + '/' , function( event ) {
                MainPage.nodeEmail.addClass( event.data ? 'error' : 'success' );
                btn.removeClass( 'disabled' );
            } ).fail( function() {
                    MainPage.nodeEmail.addClass( 'error' );
                    btn.removeClass( 'disabled' );
                } );
        } );

        MainPage.nodeEmail.change( function() {
            $( '[name=username]' ).val( this.value );
        } );

        $( '.saveBtn' ).click( function() {
            if( !MainPage.checkSend() ) {
                $( '.itemsForm' ).submit();
            }
        } );

        $( '.generate-new-password' ).on( 'click' , function() {
            var node = $( this ).off( 'click' ).addClass( 'send' );

            if( ! node.data( 'owner-id' ) ) {
                return;
            }

            $.get( '/api/company/generate-new-password/' + node.data( 'owner-id' ) + '/' , function() {
                node.removeClass( 'send' ).addClass( 'success' ).text( node.data( 'on-generate' ) );
            } );
        } );

        $('.btnCredits').on('click',function(){
            var inputElem =  $(this).parent().find("input")[0];
            var curValue = inputElem.value *1;
            var newValue = $(this).data("action")=="plus"?curValue+1:curValue-1;

            inputElem.value = newValue<0?curValue:newValue;

            $(inputElem).attr("prevValue",inputElem.value);
        });

        $('.creditsBlock input').on('change',function(){checkCredits(this);});
        $('.creditsBlock input').on('keyup',function(){checkCredits(this);});
        $('.creditsBlock input').on('keydown',function(){checkCredits(this);});
        $('.creditsBlock input').on('keypress',function(){checkCredits(this);});

        var checkCredits = function(elem){
            var curValue = (elem.value).trim();
            var prevValue = $(elem).attr("prevValue");
            if(curValue==prevValue)return;
            if(curValue!=elem.value)elem.value = curValue;
            if(!Utils.isNumber(elem.value))elem.value = prevValue;
            else {
                    $(elem).attr("prevValue",elem.value);
            }
        }
    };
};