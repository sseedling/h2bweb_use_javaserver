/**
 * View cur location page
 *
 */

var MainPage = new function(){
    var _this = this;

    var initSubmitLocale = function(){
        var submitBtn = "div.saveBtn";
        var submitForm = "#localeForm";

        $(submitBtn).on("click",function(){
            $(submitForm).submit();
        });
    }

    this.init = function(){
        initSubmitLocale();
    }
}