$.getScript('/js/pages/company_add.js', function() {
    MainPage.init();


    var nodeForm    = $( '#form' );
    var confirmForm = $( '#confirm-form' ).slideUp(0);
    var confirmPass = confirmForm.find( '[name=password]' ).on( 'focus click' , function() {
        $( this ).removeClass( 'error' );
    } );
    var fullOverlap = nodeForm.find( '.full-overlap' ).click( function() {
        if( submited ) {
            return;
        }
        confirmForm.slideUp();
        $( this ).removeClass( 'active' );
    } );
    $( '.remove-company' ).unbind( 'click' ).bind( 'click' , function() {
        confirmForm.slideDown();
        fullOverlap.addClass( 'active' );
        $( document ).scrollTop(0);
    } );
    var submited;

    function err() {
        submited = false;
        confirmPass.addClass( 'error' );
        $( '#confirm' ).removeClass( 'disabled' );
    }

    $( '#confirm' ).click( function() {
        var pass = confirmPass.val();

        $( '#confirm' ).addClass( 'disabled' );

        if( pass ) {
            submited = true;

            $.ajax( '/api/companies/drop/' , {
                data        : {
                    password    : pass
                    , companyId : nodeForm.data( 'company-id' )
                }
                , dataType  : 'json'
                , method    : 'post'
                , success   : function( event ) {
                    if( event.status.code == 200 ) {
                        confirmForm.slideUp();
                        window.location.href = '/companies/';
                    } else err();
                }
                , error     : err
            } );
        } else err();
    } );
});