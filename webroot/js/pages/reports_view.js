var MainPage = new function(){
    var _this = this;

    this.locations      = {};
    this.quizzes        = {};

    this.init = function() {
        MainPage.chartBoxesLength = 0;

        $( '.date-field' )
            .on( 'change' , function() {
                var maxDate = $( '.maxDate' );
                var minDate = $( '.minDate' );

                if( !maxDate.val() || !minDate.val() ) {
                    return;
                }

                var maxTime = maxDate.datepicker( 'getDate' ).getTime();
                var minTime = minDate.datepicker( 'getDate' ).getTime();

                if( maxTime < minTime ) {
                    ( this == minDate.get(0) ? maxDate : minDate ).datepicker( 'setDate' , ( this == minDate.get(0) ? minDate : maxDate ).val() );
                }
            } )
            .datepicker( {
                changeMonth     : true
                , changeYear    : true
            } );

        ( function( chartTypes , chartType ) {
            var chartTypesLength = chartTypes.length;
            $.each( chartTypes , function( index , key ) {
                var Chart_ = window.Chart;
                $.getScript( '/js/libs/in/charts/' + key + '.js' , function() {
                    chartType[ key ] = window.Chart;
                    window.Chart = Chart_;

                    chartType[ key ].type = key;
                    chartType[ key ].load();
                    completeType.call( chartType[ key ] );

                    !--chartTypesLength && complete( chartType );
                } );
            } );
        } )( [ 'weather' , 'classic' ] , {} );

        var nodePreloader = $( '.chart-tool-preloader' );
        var nodeChartsList = $( '.charts-list' );
        var nodesChartTypes = $( '.chart-types [data-type-of-chart]' );
        var nodesTools = $( '.tools-cols[data-type-of-chart]' );
        var exportBtn = $( '.exportBtn' ).on( 'click' , exportToPdf );

        function exportToPdf() {
            if( $( this ).hasClass( 'disabled' ) ) {
                return;
            }
            ExporterPdf.init( $( this ).parents( '.chartBox:eq(0)' ) );
            ExporterPdf.create();
            ExporterPdf.save();
        }
        function openTypeOfChart( tool ) {
            nodesChartTypes.filter( '[data-type-of-chart="' + ( tool.type ) + '"]' ).addClass( 'active' );
            nodeChartsList.slideUp();
            tool.hidePreloader();
            tool.nodeTool.slideDown();
        }
        function closeTypesOfChart( tool , isActive ) {
            nodesChartTypes.removeClass( 'active' );
            isActive && nodeChartsList.slideDown();
            nodesTools.filter( tool ? tool.nodeTool : '*' ).slideUp();
        }
        function toggleDisabledExportBtn() {
            exportBtn[ nodeChartsList.find( '.chartArea:not(.noDataToDisplay)' ).parents( '.chartBox' ).length ? 'removeClass' : 'addClass' ]( 'disabled' );
        }

        function addMethods( tool ) {
            tool.showPreloader = function() {
                nodePreloader.slideDown();
            };
            tool.hidePreloader = function() {
                nodePreloader.slideUp();
            };
            tool.send = function( data , success ) {
                tool.showPreloader();

                $.ajax( {
                    type        : 'post'
                    , dataType  : 'json'
                    , url       : '/api/reports/chart/' + tool.type + '/collect/'
                    , data      : data
                    , success   : function( event ) {
                        if( success instanceof Function ) {
                            try{
                                var flag = success.call( tool , event.data );
                            }
                            catch (e){
                                //alert("Server Error");
                            }

                            if( typeof flag == 'boolean' ? flag : true ) {
                                tool.hidePreloader();
                            }

                        } else {
                            tool.hidePreloader();
                        }
                    }
                    , error     : function() {
                        tool.hidePreloader();
                    }
                } );
            };
        }

        function drawBlock( response , data , tool ) {
            var block = this;
            var exportBtn = block.find( '.export' ).click( exportToPdf );
            var reportsCount = block.find( '.report-count' );
            nodeChartsList.append( block );

            if( ! tool.reportsCount( response ) ) {
                block.find( '.chartArea' ).addClass( 'noDataToDisplay' ).append( $( '#noDataToDisplay' ).html() );
                exportBtn.remove();
                block.find( '.locationsList' ).remove();
                reportsCount.remove();
            }

            var title = '' + tool.getTitle( response );
            block.find( '.quizzName' ).text( title.length > 30 ? title.slice( 0 , 26 ) + '...' : title ).attr( 'title' , title.length > 30 ? title : '' );

            block.on( 'click' , '.remove' , function() {
                block.slideUp( 400 , function() {
                    block.remove();
                    toggleDisabledExportBtn();
                } );
            } );

            var minDateObject = new Date( data.minDate );
            var maxDateObject = new Date( data.maxDate );
            var minMonth    = minDateObject.getMonth() + 1;
            var minDate     = minDateObject.getDate();
            var minYear     = minDateObject.getFullYear();
            var maxMonth    = maxDateObject.getMonth() + 1;
            var maxDate     = maxDateObject.getDate();
            var maxYear     = maxDateObject.getFullYear();
            block.find( '.date' ).text( ( ( minMonth == maxMonth && minDate == maxDate && minYear == maxYear ) ? '' : minDate + '/' + ( ( ( '' + minMonth ).length - 1 ) ? minMonth : '0' + minMonth ) + '/' + minYear + ' — ' ) + maxDate + '/' + ( ( ( '' + maxMonth ).length - 1 ) ? maxMonth : '0' + maxMonth ) + '/' + maxYear );

            reportsCount.find( 'a' ).text( tool.reportsCount( response ) + ' reports' ).attr( 'href' , '/reports/' + tool.type + '/details/?' + $.param( data ) );

            toggleDisabledExportBtn();
        }
        function completeType() {
            var tool = this;

            addMethods( tool );

            nodesChartTypes.filter( '[data-type-of-chart="' + tool.type + '"]' ).on( 'click' , function() {
                var isActive = $( this ).hasClass( 'active' );
                closeTypesOfChart( null , isActive );
                isActive || openTypeOfChart( tool );
            } );

            $.each( {
                '.close'    : function() {
                    closeTypesOfChart( null , nodesChartTypes.filter( '[data-type-of-chart].active').length );
                }
                , '.clear'  : function() {
                    tool.clearData();
                    tool.renderTool();
                }
                , '.create' : function() {
                    if( ! tool.isCompleteToSend() ) {
                        return;
                    }

                    var data = tool.dataToSend();
                    tool.clearData();
                    tool.renderTool();

                    closeTypesOfChart( null , true );

                    ( tool.create || tool.send ).call( tool , data , function( response ) {
                        if( tool.reportsCount( response ) ) {
                            var block = $( tool.block );
                            tool.drawChart( response , data , block , function( chart ) {
                                tool.hidePreloader();
                                drawBlock.call( block , response , data , tool );
                                block.find( '.chartArea' ).append( chart );
                            } );
                        } else {
                            tool.hidePreloader();
                            drawBlock.call( $( tool.block ) , response , data , tool );
                        }
                        return false;
                    } );
                }
            } , function( selector , callback ) {
                tool.nodeTool.on( 'click' , selector , callback );
            } );
        }
        function complete( types ) {
            closeTypesOfChart();
            openTypeOfChart( types.weather );
        }
    };
};