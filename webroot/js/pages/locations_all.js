/**
 * View list of all Locations page
 *
 */

var MainPage = new function(){
    var _this = this;

    var initLocationList = function(){
        $(".locationsList .row-item").on("click",function(){
            Utils.followUrl($(this).attr("location-name")+"/");
        });
    }

    this.init = function(){
        initLocationList();
    }
}