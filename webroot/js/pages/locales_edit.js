/**
 * View cur location page
 *
 */

var MainPage = new function(){
    var _this = this;
    var editBoxLists = new Array();

    var initChangeLocale = function(){
        var localeBtn = "nav.languages .locale";
        var localeTab = "#editWrapper .locale-tab";
        $(localeBtn).on("click",function(){
            var activeClass = "active";
            if($(this).hasClass(activeClass))return;
            $(localeBtn).removeClass(activeClass);
            $(this).addClass(activeClass);

            var dataLocaleId  = $(this).data("locale");

            $(localeTab).removeClass("active");
            $(localeTab+"[data-locale='"+dataLocaleId+"']").addClass("active");

            setTimeout(function(){refreshEditors();},100);
        });
    }

    this.showEditBoxList = function(){
        console.log(editBoxLists);
    }

    var initSubmitLocale = function(){
        var submitBtn = "div.saveBtn";
        var submitForm = "#localeForm";

        $(submitBtn).on("click",function(){
            $(submitForm).submit();
        });
    }

    var initEditors = function(){
        $.getScript('/js/libs/out/jquery.cleditor.min.js', function() {
            $.getScript('/js/libs/out/jquery.cleditor.table.min.js',function(){
                setTimeout(function(){
                    //values
                    var preKey = ["emails[","][data]"];
                    var elemKey = "";
                    var elemItem = null;
                    var locale = null;
                    var localesTabsPull = $("#pageMenu .secondPart .languages .locale");

                    //list of editable text-areas
                    var editTextAreaKeys = ["[report][body]","[rarePrize][body]","[prize][body]","[password][body]","[creditsExpire][body]"];

                    //init editors
                    for(var i=0;i< editTextAreaKeys.length;i++){
                        for(var j=0;j< localesTabsPull.length;j++){
                            locale = $(localesTabsPull[j]).attr("data-locale");
                            elemKey = preKey[0]+locale+preKey[1]+editTextAreaKeys[i];
                            elemItem = document.getElementsByName(elemKey);

                            $(elemItem).cleditor();

                            editBoxLists.push({
                                editor:$(elemItem).cleditor()[0],
                                elem:elemItem,
                                name:elemKey,
                                localeId:locale
                            });
                        }
                    }
                },1);
            });
        });
    }

    var refreshEditors = function(){
        for(var i=0;i< editBoxLists.length;i++){
            editBoxLists[i].editor.refresh();
        }
    }

    this.init = function(){
        initSubmitLocale();
        initChangeLocale();
        initEditors();
    }
}