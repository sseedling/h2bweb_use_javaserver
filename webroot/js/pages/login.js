/**
 * Home page
 *
 */

var MainPage = new function(){
    var _this = this;

    this.init = function(){
        $(".text-field").on("change",function(){
            this.className=(this.value=="")?"text-field":"text-field active";
        });

        $("#login").on("submit",function(){
            return _this.checkLoginPass();
        });

    }

    this.checkLoginPass = function(){
        var result = true;
        if($(".text-field[name='login']").val()=="")result = false;
        if($(".text-field[name='password']").val()=="")result = false;
        if(!result)$("#loginForm .loginMsg").html('<div class="text-error">'+$(".msg-empty").html()+'</div>');
        return result;
    }
}