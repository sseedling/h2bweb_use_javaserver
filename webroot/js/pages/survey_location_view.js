/**
 * View cur location page
 *
 */

var MainPage = new function(){
    var _this = this;
    var classes;

    var initStatusSwitcher = function(){
        classes = ["publish","unpublish"];
        $(".firstPart .surveyStatusBtn").hover(function(){
            if(!$(this).hasClass(classes[0]))return;
            $("span",this).html($(this).attr("hover-on"));
        },function(){
            if(!$(this).hasClass(classes[0]))return;
            $("span",this).html($(this).attr("hover-off"));
        });

    };

    this.init = function(){
        initStatusSwitcher();

        var tabs = $( '.lang-tabs .tab' ).click( function() {
            tabs.removeClass( 'active' );
            $( this ).addClass( 'active' );
            $( '.question-lang-list' ).removeClass( 'active' );
            $( '#tab-content-' + $( this ).data( 'tab' ) ).addClass( 'active' );
        } );

        tabs.eq(0).click();
    };
};