/**
 * View list of all Locations page
 *
 */

var MainPage = new function(){
    var _this = this;

    var checkSurveyForm = function(mode){
        var elem = ".itemsForm .location_name";
        var testVal = $(elem).val().trim();
        if(testVal=="")return {status:false,elem:elem};
        return {status:true};
    };

    var initSaveLocation = function(){
        $(".saveBtn").on("click",function(){
            var check = checkSurveyForm($(this).attr("mode"));
            if(check.status)$(".itemsForm").submit();
            else showError(check);
        });
    };

    var showError = function(error){
        $(error.elem).addClass("error");
    };

    var initShowErrorStatus = function(){
        $(".itemsForm .text-field").on("focus",function(){$(this).removeClass("success").removeClass("error")});
    };

    var initSelImgBtn = function() {
        var classPull = {
            imgElem:'.file-preview img',
            inputBtn:'.file-selector div',
            inputElem:'.file-selector input'
        }

        $(classPull.inputBtn).click( function() {
            $("input", this.parentNode).click();
        } );

        $(classPull.inputElem).change( function() {
            var imgElem = $(classPull.imgElem,this.parentNode.parentNode)[0];
            $(imgElem).data( 'default' , 'false' );
            Utils.drawImgToImg( {
                inputElem   : this
                , drawElem  : imgElem
            } );
        } );
    };

    var initCheckUsernameBtn = function(){
        $(".itemsForm .checkUsernameBtn").on("click",function(){
            var elem = this;
            var objectId = $(".itemsForm .device_username").attr("objectId");
            var username = $(".itemsForm .device_username").val().trim();

            var params = {username:username,objectId:objectId};

            if((params.username=="")||($(elem).hasClass("active")))return false;
            $(elem).addClass("active");
            $.ajax({
                type: "post",
                async:true,
                dataType: "json",
                url: "/api/user/check/username/",
                data: params,
                success: function(data) {
                    onCheckUsernameAvailabe(data);
                    $(elem).removeClass("active");
                },
                error:function(){
                    Utils.networkErrorAlert();
                    $(elem).removeClass("active");
                }
            });
        });
    };

    var onCheckUsernameAvailabe = function(resp){
        if(resp.status.code!=200)return Utils.networkErrorAlert();
        if(resp.data=="empty")$(".itemsForm .device_username").removeClass("error").addClass("success");
        else $(".itemsForm .device_username").addClass("error").removeClass("success");
    };

    var intAttrationScreenClick = function(){
        $(".file-preview").on("click",function(){
            if($("img",this).data("default")=="true")Utils.openUrl($("img",this)[0].src);
        });
    };

    this.init = function(){
        Utils.togglePasswordChars();
        initSelImgBtn();
        initSaveLocation();
        initShowErrorStatus();
        initCheckUsernameBtn();
        intAttrationScreenClick();

        var nodeBanners       = $( '#banners' );
        var nodeImageUpload   = $( '.image-upload' );
        var exemplarInputFile = $( '#exemplarInputFile' ).html();
        var exemplarBanner    = $( '#exemplarBanner' ).html();

        function addBanner( object ) {
            var banner = $( exemplarBanner ).addClass( object.file ? 'new' : '' );
            var image  = new Image;

            if( object.file ) {
                if( ! Utils.isValidImageType( object.file.value ) ) {
                    return false;
                }

                banner.find( '.title' ).text( object.file.value.slice( object.file.value.lastIndexOf( '\\' ) + 1 ) || 'Title' );
                Utils.drawImgToImg( {
                    inputElem   : object.file
                    , drawElem  : image
                } );
            } else {
                banner.find( '.title' ).text( object.name || 'Title' );
                image.src = object.url;
            }

            banner.find( '.image' ).append( image );
            banner.find( '.remove' ).click( function() {
                banner.remove();
                !object.file && window.locationId && $.get( '/api/locations/banner/drop/' + window.locationId + '/' + object.objectId + '/' );
            } );

            nodeBanners.append( banner.append( object.file ? $( object.file ).after( exemplarInputFile ) : '' ) );

            return true;
        }

        nodeImageUpload.on( 'click' , '.btnGradient' , function() {
            nodeImageUpload.find( 'input[type=file]' ).click();
        } );
        nodeImageUpload.on( 'change' , 'input[type=file]' , function() {
            addBanner( { file : this } );
        } );

        nodeImageUpload.append( exemplarInputFile );

        $.each( window.bannersList || [] , function( bannerId , bannerData ) {
            addBanner( bannerData );
        } );
    };
};