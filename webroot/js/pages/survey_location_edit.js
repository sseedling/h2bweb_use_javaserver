var QuizLocales = function() {
    var node = {};
    var exemplar = {};

    function tmpl( string , data ) {
        if( ! data ) {
            data = {};
        }
        return ( '' + string ).replace( /%([^%]+)%/g , function( all , param ) {
            return data[ param ] || '';
        } );
    }

    function createTab( name , text ) {
        var tab = $( exemplar.tab = exemplar.tab || $( '#baseLocaleTab' ).html() ).attr( 'id' , 'tab-' + name ).attr( 'data-tab' , name );
        tab.find( '.flag' ).addClass( 'flag-' + name ).attr("src","/img/flags/"+name+".png").attr("alt",name);
        tab.find( '.text' ).text( text );
        return tab;
    }
    function createContent( node , viewIndex , length , callbackBefore , callback , flag , name , localeData , objectId ) {
        var nodeContent = $( '<div></div>' );
        var index = 0;
        var line;
        var data;
        var data_;

        if( !isFunction( callback ) ) {
            callback = function() {};
        }
        if( !isFunction( callbackBefore ) ) {
            callbackBefore = function() {};
        }

        while( index++ < length ) {
            data_ = ( localeData || [] )[ index - 1 ];
            data = flag ? { text : data_ } : JSON.parse( JSON.stringify( ( localeData || [] )[ index - 1 ] || {} ) );
            callbackBefore.call( line , data , index );
            data.lang = name;
            data.index = viewIndex == void 0 ? index : viewIndex;
            data.objectId = data.objectId || objectId;
            nodeContent.append( line = $( tmpl( node.html() , data ) ) );
            callback.call( line , data , index );
        }

        return nodeContent;
    }
    function createQuestions( name , localeData ) {
        var content = createContent(
            exemplar.question = exemplar.question || $( '#baseLocaleQuestion' )
            , null
            , 10
            , function( data ) {
                data.subQuestionsEnabled = data.subQuestionsEnabled ? 'true' : 'false';
            }
            , function( data , index ) {
                this.attr( 'data-question-index' , index ).find( '.second-level-of-questions' ).append( createSubQuestions( index , name , data.subQuestions ) );
            }
            , false
            , name
            , localeData
        ).addClass( 'tab-content' ).attr( 'id' , 'tab-content-' + name );

        content.find( '[data-question-index]' ).each( function() {
            var input = $( this ).find( '.question [type=hidden]' );
            if( ! input.val() ) {
                input.val( $( '[data-question-index=' + $( this ).data().questionIndex + '] .question [type=hidden]' ).val() );
            }
        } );

        content.find( '.second-level-of-questions' ).each( function() {
            var node         = $( this );
            var input        = node.find( 'input[type=hidden]' ).change( function() {
                switchBox.setStatus( this.value == 'true' );
            } );
            var switchBox    = Utils.SwitchBox( {
                textOn           : '<div class="icon icon-view"></div>'
                , textOff        : '<div class="icon icon-not-view"></div>'
                , status         : input.val() == 'true'
                , onChangeStatus : function( status ) {
                    input.val( status );
                    subQuestions.attr( 'readonly' , ! status );
                }
            } );
            var subQuestions = node.find( 'input' ).not( input ).change( function() {
                switchBox.setStatus( true );
            } );

            node.prepend( switchBox );
        } );

        return content;
    }
    function createSubQuestions( index , name , localeData ) {
        return createContent(
            exemplar.subQuestion = exemplar.subQuestion || $( '#baseSubQuestion' )
            , index
            , 5
            , null
            , function( data , index ) {
                this.attr( 'data-sq-index' , index );
            }
            , true
            , name
            , localeData
        );
    }

    return {
        addTab                  : function( name , questionsData ) {
            if( ! name || ! QuizLocales.appLocales[ name ] || ! $( '#locale-' + name ).addClass( 'active' ).length || $( '#tab-' + name ).length || $( '#tab-content-' + name ).length ) {
                return false;
            }

            var tab = createTab( name , name );

            node.addTab = node.addTab || $( '.lang-tabs .tab[data-tab=add]' );
            $( '#locales' ).append( tab );
            ( node.tabsContent = node.tabsContent || $( '.questionsList' ) ).append(
                createQuestions( name , questionsData )
                    .attr( 'data-id' , name )
            );

            tab.click();

            QuizLocales.toggleAddTab();

            return true;
        }
        , removeTab             : function( name ) {
            if( ! name || ! confirm( $( '#confirmRemoved' ).text().replace( /\\n/g , '\n' ) ) ) {
                return false;
            }

            $( '#locale-' + name ).removeClass( 'active' );
            $( '#tab-' + name ).remove();
            $( '#tab-content-' + name ).remove();

            QuizLocales.openFirst();
            QuizLocales.toggleAddTab();
            MainPage.updateStatusSwitchBox();
            return true;
        }
        , openFirst             : function() {
            $( '.lang-tabs .tab:eq(0)' ).click();
        }
        , toggleAddTab          : function() {
            $( node.addTab )[ $( '#tab-content-add .locale-item:not(.active)' ).length ? 'show' : 'hide' ]();
        }
        , checkStatusQuestions  : function() {
            var nodesLangTab        = $( '.tab' ).removeClass( 'attention' );
            var nodesTabContent     = $( '.questionsList .tab-content' );
            var nodesQuestionItem   = nodesTabContent.find( '.question-item' ).removeClass( 'attention' );

            nodesQuestionItem.each( function() {
                var nodeQuestionItem = $( this );
                if( nodeQuestionItem.hasClass( 'attention' ) ) {
                    return;
                }

                var pathInputName = '.question input.text-field';
                var pathTextareaName = '.question textarea.text-field';
                var pathSubQuestions = '.second-level-of-questions .text-field';

                var nodeInputName = nodeQuestionItem.find( pathInputName );
                var nodeTextareaName = nodeQuestionItem.find( pathTextareaName );

                if( ! nodeInputName.val() ) {
                    return;
                }

                var nodesQuestionIndex = nodesQuestionItem.not( nodeQuestionItem ).filter( '[data-question-index=' + nodeQuestionItem.data( 'question-index' ) + ']' );

                nodesQuestionIndex.find( pathInputName ).each( function() {
                    if( nodeInputName.hasClass( 'error' ) || ! nodeInputName.removeClass( 'error' ).val() ) {
                        return;
                    }
                    if( ! this.value ) {
                        $( this ).addClass( 'error' );
                    }
                } );
                nodesQuestionIndex.find( pathTextareaName ).each( function() {
                    if( nodeTextareaName.hasClass( 'error' ) || ! nodeTextareaName.removeClass( 'error' ).val() ) {
                        return;
                    }
                    if( ! this.value ) {
                        $( this ).addClass( 'error' );
                    }
                } );

                nodeQuestionItem.find( pathSubQuestions ).each( function() {
                    var subQuestion = $( this );
                    if( subQuestion.hasClass( 'error' ) || ! subQuestion.removeClass( 'error' ).val() ) {
                        return;
                    }
                    $( nodesQuestionIndex.find( pathSubQuestions ).filter( '[data-sq-index=' + subQuestion.data( 'sq-index' ) + ']' ) ).each( function() {
                        if( ! this.value ) {
                            $( this ).addClass( 'error' );
                        }
                    } );
                } );
            } ).each( function() {
                $( this ).find( '.error' ).length && $( this ).addClass( 'attention' );
            } );

            nodesTabContent.each( function() {
                var nodeTabContent = $( this );

                if( nodeTabContent.find( '.attention' ).length ) {
                    $( '#tab-' + nodeTabContent.data( 'id' ) ).addClass( 'attention' );
                }
            } );

            var attentionTabs       = nodesLangTab.filter( '.attention' );
            var attentionQuestions  = nodesQuestionItem.filter( '.attention' );

            attentionTabs.eq(0).trigger( 'click' , [ true ] );
            attentionQuestions.eq(0).find( '.error:eq(0)' ).focus();

            return ! attentionTabs.length;
        }
    };
}();



var MainPage = new function(){
    var isInit = false;
    var statusSwitchBox;

    var checkItemsForm = function(){
        var elem;

        elem = "#survey-details .survey-name";
        var survName = $(elem).val();
        survName = survName.trim();
        if(survName=="")return {status:false,elem:elem};

        elem = "#survey-details .survey-default-lang";
        var survDefaultLang = $(elem).val();
        if(survDefaultLang=="")return {status:false,elem:elem};

        var elems = $(".itemsForm .question-text");
        for(var i=0;i<elems.length;i++){
            elem = $(".question-name",elems[i].parentNode)[0];
            if((elems[i].value!="")&&(elem.value==""))return {status:false,elem:elem};
        }
        if(survDefaultLang=="")return {status:false,elem:elem};

        $(".itemsForm .survey-status").val(( statusSwitchBox == void 0 ? false : statusSwitchBox.getStatus instanceof Function && statusSwitchBox.getStatus() )?"publish":"unpublish");
        return {status:true};
    }

    var initSaveSurvey = function(){
        $(".saveBtn").on("click",function(){
            if( ! QuizLocales.checkStatusQuestions() ) {
                return;
            }
            var check = checkItemsForm();
            if(check.status)$(".itemsForm").submit();
            else showError(check);
        });
    }

    var showError = function(error){
        $(error.elem).addClass("error");
    }

    var initShowErrorStatus = function(){
        $( '.itemsForm' ).on( 'blur' , '.error' , function() {
            $( this ).val() && $( this ).removeClass( 'error' );
        } );
    };

    this.updateStatusSwitchBox = function(){
        if($("#locales .tab").length == 0)statusSwitchBox.setDisabled(true);
    }

    this.init = function(){
        if(isInit)return;
        isInit = true;

        initSaveSurvey();
        initShowErrorStatus();

        var questionList = $( '.questionsList' );

        isFunction( window.initParts ) && initParts();

        $( '#status-box' ).prepend( statusSwitchBox = Utils.SwitchBox( {
            textOn           : MainPage.statusTextOn
            , textOff        : MainPage.statusTextOff
            , status         : MainPage.statusFlag
            , disabled       : MainPage.statusDisabled == void 0 ? true : MainPage.statusDisabled
            , onChangeDisabled : function( disabled ) {
                if( disabled ) {
                    this.setStatus( false );
                }
            }
        } ) );

        questionList.on( 'blur' , '.question input:not([type=hidden])' , function() {
            var emptyNamesLength = 0;
            var questionName     = questionList.find( '.question input:not([type=hidden])' );
            var nameLength       = questionName.each( function() {
                if( !this.value ) {
                    emptyNamesLength++;
                }
            } ).length;

            statusSwitchBox.setDisabled( emptyNamesLength == nameLength );
        } );

        $( '.back > .btnGradient' ).click( function() {
            $( this ).parent().animate( { opacity : 0 } , 100 , function() {
                $( this ).remove();
            } );
        } );

        $.each( QuizLocales.appLocales , function( locale , localeData ) {
            $( '#tab-content-add .rows' ).append( $( '<div class="row locale-item" id="locale-' + locale + '"><div class="row-title"><img alt="'+locale+'" src="/img/flags/'+locale+'.png" class="flag flag-big flag-' + locale + '" />' + localeData.text + '</div></div>' ).click( function() {
                QuizLocales.addTab( locale );
            } ) );
        } );

        $.each( QuizLocales.allQuestions , function( locale , dataLocale ) {
            QuizLocales.addTab( locale , dataLocale );
        } );

        $( '.lang-tabs' ).on( 'click' , '.tab' , function( event , flag ) {
            var tab = $( this );
            if( ! flag && tab.data().tab != 'add' && tab.hasClass( 'active' ) ) {
                return QuizLocales.removeTab( tab.data().tab );
            }

            $( '.questionsList .tab-content , .lang-tabs .tab' ).removeClass( 'active' );
            $( '#tab-content-' + tab.addClass( 'active' ).data( 'tab' ) ).addClass( 'active' );
        } );
        QuizLocales.openFirst();
    }
};