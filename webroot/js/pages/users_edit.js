var MainPage = new function(){
    this.checkSend  = function() {
        var name         = ! MainPage.nodePublicName.val();
        var email        = MainPage.nodeEmail.val();
        var isInalidEmail = ! Utils.isValidEmail( email );

        if( name ) {
            MainPage.nodePublicName.addClass( 'error' );
        }
        if( ! email || isInalidEmail ) {
            MainPage.nodeEmail.addClass( 'error' );
        }
        return name || !email || isInalidEmail;
    };
    this.init       = function() {
        Utils.togglePasswordChars();

        function err_() {
            $( this ).removeClass( 'success' ).removeClass( 'error' );
        }

        var nodeRolesList       = $( '.roles-list' );
        var nodeStatusBox       = $( '.status-box' );
        var nodeLocationsList   = $( '#locations-list' );
        var nodeSelectLocation  = $( '#select-locations' );
        MainPage.nodeEmail          = $( '[name=email]' ).on( 'focus click' , err_ );
        MainPage.nodePublicName     = $( '[name=publicName]' ).on( 'focus click' , err_ );
        MainPage.nodePassword       = $( '[name=password]' ).on( 'focus click' , err_ );
        var selectFlag;

        nodeRolesList.find( '.row' ).each( function() {
            var row = $( this );
            var switchBox = Utils.SwitchBox( {
                status           : row.find( 'input[type=hidden][name^=role][name$="W]"]' ).val() == 'true' ? true : false
                , classOff       : 'publish'
                , textOn         : '<div class="icon icon-edit-white"></div>'
                , textOff        : '<div class="icon icon-eye"></div>'
                , titleOn        : $( '#textWrite' ).text()
                , titleOff       : $( '#textRead' ).text()
                , onChangeStatus : function( status ) {
                    row.find( 'input[type=hidden][name^=role][name$="W]"]' ).val( !!status );
                }
            } );

            row.append( switchBox ).click( function( event ) {
                if( $( event.target ).hasClass( 'switch-box' ) || $( event.target ).parents( '.switch-box' ).length ) {
                    return;
                }
                switchBox.setStatus( !row.hasClass( 'active' ) );
                row.toggleClass( 'active' ).find( 'input' ).val( row.hasClass( 'active' ) );
            } );
        } );

        nodeStatusBox.append( Utils.SwitchBox( {
            status           : nodeStatusBox.find( 'input[type=hidden]' ).val() == 'true' ? true : false
            , textOn         : $( '#textActive' ).text()
            , textOff        : $( '#textDisabled' ).text()
            , onChangeStatus : function( status ) {
                nodeStatusBox.find( 'input[type=hidden]' ).val( !!status );
            }
        } ) );

        nodeLocationsList.on( 'click' , '.row' , function() {
            var row = $( this )[ typeof selectFlag == 'boolean' ? selectFlag ? 'addClass' : 'removeClass' : 'toggleClass' ]( 'active' );

            if( row.hasClass( 'active' ) ) {
                row.append( '<input type="hidden" name="locationList[]" value="' + row.data( 'id' ) + '">' );
            } else {
                row.find( 'input[type=hidden]' ).remove();
            }

            setTextToPermissionsBTN();
        } );

        nodeSelectLocation.click( function() {
            var locationLength        = 0;
            var activeLocationsLength = 0;

            var rows = nodeLocationsList.find( '.row' ).each( function() {
                locationLength++;
                if( $( this ).hasClass( 'active' ) ) {
                    activeLocationsLength++;
                }
            } );

            selectFlag = locationLength != activeLocationsLength;
            rows.click();
            selectFlag = void 0;
        } );

        function setTextToPermissionsBTN() {
            var locationLength        = 0;
            var activeLocationsLength = 0;

            nodeLocationsList.find( '.row' ).each( function() {
                locationLength++;
                if( $( this ).hasClass( 'active' ) ) {
                    activeLocationsLength++;
                }
            } );
            nodeSelectLocation.text( locationLength == activeLocationsLength ? $( '#textDeselectAll' ).text() : $( '#textSelectAll' ).text() );
        }
        setTextToPermissionsBTN();

        $( '.check-email' ).click( function() {
            if( $( this ).hasClass( 'disabled' ) ) {
                return;
            }

            if( !MainPage.nodeEmail.val() ) {
                MainPage.nodeEmail.addClass( 'error' );
                return;
            }

            var btn = $( this ).addClass( 'disabled' );

            $.getJSON( '/api/user/check/email/' + MainPage.nodeEmail.val() + '/' + MainPage.nodeEmail.data("user-id") + '/', function( event ) {
                MainPage.nodeEmail.addClass( event.data ? 'error' : 'success' );
                btn.removeClass( 'disabled' );
            } ).fail( function() {
                    MainPage.nodeEmail.addClass( 'error' );
                    btn.removeClass( 'disabled' );
                } );
        } );

        MainPage.nodeEmail.change( function() {
            $( '[name=username]' ).val( this.value );
        } );

        $( '.saveBtn' ).click( function() {
            if( !MainPage.checkSend() ) {
                $( '.itemsForm' ).submit();
            }
        } );


    };
};