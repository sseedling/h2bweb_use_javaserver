var MainPage = new function(){
    this.init = function() {
        $( '.date-field' ).datepicker( {
            changeMonth     : true
            , changeYear    : true
        } );

        $( '.date-field' ).on( 'change' , function() {
            var maxDate = $( '.maxDate' );
            var minDate = $( '.minDate' );

            var maxTime = maxDate.datepicker( 'getDate' ).getTime();
            var minTime = minDate.datepicker( 'getDate' ).getTime();

            if( maxTime < minTime ) {
                ( this == minDate.get(0) ? maxDate : minDate ).datepicker( 'setDate' , ( this == minDate.get(0) ? minDate : maxDate ).val() );
            }

            minDate.siblings( '[name=minDate]' ).val( minDate.datepicker( 'getDate' ).getTime() );
            maxDate.siblings( '[name=maxDate]' ).val( maxDate.datepicker( 'getDate' ).getTime() );
        } );

        $( '#middleContent' ).on( 'click' , '.added' , function() {
            var node = $( this );

            if( node.hasClass( 'disabled' ) ) {
                return;
            }

            var parent = node.parents( '.row-item:eq(0)' );
            var reportId = parent.data( 'report-id' );

            if( ! reportId ) {
                return;
            }

            node.addClass( 'disabled' );
            $.get( '/api/prizes/give/' + reportId + '/' , function() {
                node.hide( 150 , function() {
                    parent.addClass( 'success' );
                    node.remove();
                } );
            } );
        } );
    };
};