$.getScript('/js/pages/users_edit.js', function() {
    MainPage.checkSend = function() {
        if( !MainPage.nodePublicName.val() ) {
            MainPage.nodePublicName.addClass( 'error' );
        }
        if( !MainPage.nodeEmail.val() ) {
            MainPage.nodeEmail.addClass( 'error' );
        }
        if( !MainPage.nodePassword.val() ) {
            MainPage.nodePassword.addClass( 'error' );
        }
        return !MainPage.nodePublicName.val() || !MainPage.nodeEmail.val() || !MainPage.nodePassword.val();
    };
    MainPage.init();
    $( '#select-locations' ).click();
});