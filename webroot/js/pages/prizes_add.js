function tmpl( string , data ) {
    if( ! data ) {
        data = {};
    }
    return ( '' + string ).replace( /%(\w+)%/g , function( all , param ) {
        return data[ param ]||'';
    } );
}

var PrizeGroupLocales = function() {
    var node = {};
    var exemplar = {};

    function createLocaleTab( name , text ) {
        if( ! MainPage.locales.hasOwnProperty( name ) ) {
            return;
        }
        var tab = $( exemplar.tab = exemplar.tab || $( '#baseLocaleTab' ).html() ).attr( 'data-tab' , name );
        tab.find( '.flag' ).addClass( 'flag-' + name).attr("src","/img/flags/"+name+".png").attr("alt",name);
        tab.find( '.text' ).text( text );
        return tab;
    }

    function addLocaleCtx( name , groupDataLocale , prizeDataLocale ) {
        if( ! MainPage.locales.hasOwnProperty( name ) ) {
            return;
        }

        var prizeData;
        var groupData  = JSON.parse( JSON.stringify( ( groupDataLocale || {} )[ name ] || {} ) );
        groupData.lang = name;

        node.note      = node.note      || $( '#note' );
        node.unwinNote = node.unwinNote || $( '#unwinNote' );

        exemplar.note          = exemplar.note          || $( '#exemplarPrizeNote' );
        exemplar.unwinNote     = exemplar.unwinNote     || $( '#exemplarPrizeUnwinNote' );
        exemplar.prizeDataName = exemplar.prizeDataName || $( '#exemplarPrizeDataName' );
        exemplar.prizeDataNote = exemplar.prizeDataNote || $( '#exemplarPrizeDataNote' );

        node.note.append(      $( tmpl( exemplar.note.html()      , groupData ) ).attr( 'data-locale' , name ) );
        node.unwinNote.append( $( tmpl( exemplar.unwinNote.html() , groupData ) ).attr( 'data-locale' , name ) );

        $( '#prizes .item-row' ).each( function() {
            var nodePrize   = $( this );

            prizeData       = JSON.parse( JSON.stringify( ( prizeDataLocale || {} )[ name ] || {} ) );
            prizeData.lang  = name;
            prizeData.index = '' + nodePrize.data( 'index' );

            nodePrize.find( '.prize-data-name' ).append( $( tmpl( exemplar.prizeDataName.html() , prizeData ) ).attr( 'data-locale' , name ) );
            nodePrize.find( '.prize-data-note' ).append( $( tmpl( exemplar.prizeDataNote.html() , prizeData ) ).attr( 'data-locale' , name ) );
        } );
    }

    return {
        create  : addLocaleCtx
        , createTab : createLocaleTab
    }
}();

var MainPage = new function(){
    var index = 0;
    var prizesCount = 0;
    var selectedQuizzes = [];
    var toInt;

    this.quizzesObj = {};

    function renderedQuizzesSelect() {
        var isEmptyQuizzesList = $.isEmptyObject( MainPage.quizzesObj ) || ( Object.keys( MainPage.quizzesObj ).length <= selectedQuizzes.length );
        var select = Utils.createSelect( MainPage.quizzesObj , function( quizId ) {
            return selectedQuizzes.indexOf( quizId ) < 0;
        } ).change( function() {
            MainPage.addQuiz( this.value );
        } ).addClass( 'text-field' );

        select.prepend( '<option selected="selected">' + ( isEmptyQuizzesList ? 'No Quizzes for attach' : '+ Add Quizz' ) + '</option>' );
        isEmptyQuizzesList && select.attr( 'disabled' , true );

        $( '#quizzes' ).after( select ).siblings( 'select' ).not( select ).remove();
    }

    function createPrizeLocale( nodePrize , name , prizeData ) {
        if( ! MainPage.locales.hasOwnProperty( name ) ) {
            return;
        }
        nodePrize.find( '.prize-data-name' ).append( $( tmpl( $( '#exemplarPrizeDataName' ).html() , prizeData ) ) );
        nodePrize.find( '.prize-data-note' ).append( $( tmpl( $( '#exemplarPrizeDataNote' ).html() , prizeData ) ) );
    }

    this.addPrize = function( prizeId , prizeData , flag ) {
        prizesCount++;

        if( !$.isPlainObject( prizeData ) ) {
            prizeData = {};
        }

        var prizeData_ = JSON.parse( JSON.stringify( prizeData ) );
        prizeData_.index = index + '';

        var prizeForm  = $( tmpl( $( '#exemplarPrizeForm' ).html() , prizeData_ ) ).data( 'index' , index ).data( 'id' , prizeId );
        var nodeChance = prizeForm.find( '.chances input[type=hidden]' ).val( prizeData.chance || 15 );
        var outPrizeCount = (Utils.isNumber(prizeData.count))?prizeData.count:(prizeData.count||1);
        var elemNode = prizeForm.find( '.prize-count' );
        var nodeCount  = elemNode.val( outPrizeCount );
        Utils.bindDigitsOnly(elemNode,false);

        prizeForm.find( '.btnSum' ).click( function() {
            var number = parseInt( nodeCount.val() );
            nodeCount.val( number + ( $( this ).hasClass( 'plus' ) ? 1 : number < 1 ? 0 : -1 ) );
        } );
        nodeChance.siblings( '[data-chance]' ).click( function() {
            var node = $( this );

            node.siblings( '[data-chance]' ).removeClass( 'active' );
            node.addClass( 'active' );

            nodeChance.val( node.data( 'chance' ) );
        } );
        nodeChance.siblings( '[data-chance="' + ( prizeData.chance || 15 ) + '"]' ).addClass( 'active' );

        flag || $( '.lang-tabs [data-tab]' ).each( function() {
            var name = $( this ).data().tab;
            ! ( name == 'add' ) && createPrizeLocale( prizeForm , name , {
                lang    : name
                , index : index + ''
            } );
        } );

        if( prizeId ) {
            prizeForm.find( '.prize-remove' ).remove();
        } else {
            prizeForm.find( '.prize-remove' ).click( function() {
                prizeForm.slideUp( 400 , function() {
                    prizeForm.remove();
                } );
            } );
            prizeForm.find( '.prize-id' ).remove();
        }

        $( '#prizes' ).append( prizeForm );

        index++;

        $( '#lang [data-lang]' ).hide().filter( '[data-lang=' + $( '[data-tab].active' ).data( 'tab' ) + ']' ).show();

        return prizeForm;
    };

    this.addQuiz = function( quizId , removeFlag ) {
        if( !quizId || selectedQuizzes.indexOf( quizId ) > 0 ) {
            return;
        }

        var nodeQuizzes = $( '#quizzes' );

        var option = nodeQuizzes.siblings( 'select:eq(0)' ).find( 'option[value=' + quizId + ']' );

        var quiz = $( $( '#exemplarQuiz' ).html() );

        nodeQuizzes.append( quiz ).find( 'input[name^=removedQuizzes][value=' + quizId + ']' ).remove();

        quiz.find( 'input[type=hidden]' ).val( '' + quizId );
        quiz.find( 'span' ).text( MainPage.quizzesObj[ quizId ] );
        quiz.find( '.icon-close' ).click( function() {
            quiz.slideUp( 400 , function() {
                if( removeFlag ) {
                    quiz.after( '<input type="hidden" name="removedQuizzes[]" value="' + quizId + '">' );
                }

                quiz.remove();
                selectedQuizzes.splice( selectedQuizzes.indexOf( quizId ) , 1 );
                renderedQuizzesSelect();
            } );
        } );

        selectedQuizzes[ selectedQuizzes.length ] = quizId;
        renderedQuizzesSelect();
    };

    this.init = function() {
        $( '.btnAddPrize' ).click( function() {
            MainPage.addPrize();
        } );

        $( '.itemsForm' ).on( 'blur' , '.error' , function() {
            this.value && $( this ).removeClass( 'error' );
        } ).attr("data-doSubmit","false");

        $( '.saveBtn' ).click( function() {
            var elemNode = $('.itemsForm');

            if(elemNode.attr("data-doSubmit")!="false")return;

            elemNode.attr("data-doSubmit","test");
            if( MainPage.check() ) {
                elemNode.attr("data-doSubmit","true");
                elemNode.submit();
            }
            else{
                elemNode.attr("data-doSubmit","false");
            }
        } );

        if( Utils.isFunction( window.initParts ) ) {
            window.initParts();
            renderedQuizzesSelect();
        }


        var initTab = true;

        var nodeLang = $( '#lang' );

        var nodeAddLang = $( '#add-lang' ).on( 'click' , '.locale-item' , function() {
            addLang( $( this ).data().lang );
        } );

        var tabs = $( '.lang-tabs' ).on( 'click' , '[data-tab]' , function( event , flag ) {
            var nodeTab  = $( this );
            var tabName  = nodeTab.data( 'tab' );
            var show     = initTab ? 'show' : 'slideDown';
            var hide     = initTab ? 'hide' : 'slideUp';

            if( tabName == 'add' ) {
                nodeAddLang[ show ]();
                nodeLang[ hide ]();
            }
            else {
                if( ! flag && nodeTab.hasClass( 'active' ) && ! nodeTab.hasClass( 'initialized' ) ) {
                    return removeLang( tabName );
                }

                nodeAddLang[ hide ]();
                nodeLang[ show ]().find( '[data-lang]' ).hide().filter( '[data-lang=' + tabName + ']' ).show();
            }

            tabs.find( '[data-tab]' ).removeClass( 'active' );
            nodeTab.addClass( 'active' );

            initTab = false;
        } );

        var node = {};
        var exemplar = {};

        function addLang( name , data , isInitialized ) {
            if( ! MainPage.locales.hasOwnProperty( name ) || tabs.find( '[data-tab=' + name + ']' ).length ) {
                return;
            }

            if( ! data ) {
                data = {};
            }

            nodeAddLang.find( '[data-lang=' + name + ']' ).addClass( 'selected' );
            var tab = $( PrizeGroupLocales.createTab( name , name ) ).addClass( isInitialized ? 'initialized' : '' );
            $( '#locales' ).append( tab );

            var prizeData;
            var groupData  = JSON.parse( JSON.stringify( ( ( data || {} ).groupData || {} )[ name ] || {} ) );
            groupData.lang = name;

            node.note      = node.note      || $( '#note' );
            node.unwinNote = node.unwinNote || $( '#unwinNote' );

            exemplar.note          = exemplar.note          || $( '#exemplarPrizeNote' );
            exemplar.unwinNote     = exemplar.unwinNote     || $( '#exemplarPrizeUnwinNote' );

            var note;
            var unwinNote;
            node.note.append(      note      = $( tmpl( exemplar.note.html()      , groupData ) ).attr( 'data-locale' , name ) );
            node.unwinNote.append( unwinNote = $( tmpl( exemplar.unwinNote.html() , groupData ) ).attr( 'data-locale' , name ) );
            note.val()      || note.val(      note.attr(      'placeholder' ) );
            unwinNote.val() || unwinNote.val( unwinNote.attr( 'placeholder' ) );

            $( '#prizes .item-row' ).each( function() {
                var nodePrize   = $( this );

                var data_ = ( data.prizes || {} )[ nodePrize.data( 'id' ) ] || {};

                data_.prizeData = data_.prizeData || {};
                data_.prizeData[ name ] = data_.prizeData[ name ] || {};

                prizeData          = JSON.parse( JSON.stringify( data_.prizeData[ name ] || {} ) );
                prizeData.lang     = name;
                prizeData.index    = '' + nodePrize.data( 'index' );
                prizeData.chance   = data_.chance;
                prizeData.count    = data_.count;
                prizeData.objectId = data_.objectId;

                createPrizeLocale( nodePrize , name , prizeData );
            } );

            tab.click();
            toggleTabAdd();


            if( !prizesCount ) {
                MainPage.addPrize();
            }
        }

        function removeLang( name ) {
            tabs.find( '[data-tab=' + name + ']' ).remove();
            nodeAddLang.find( '[data-lang=' + name + ']' ).removeClass( 'selected' );
            nodeLang.find( '[data-lang=' + name + ']' ).remove();
            openFirstLang();
            toggleTabAdd();
        }

        function openFirstLang() {
            tabs.find( '[data-tab]:eq(0)' ).click();
        }

        function toggleTabAdd() {
            tabs.find( '[data-tab=add]' )[ nodeAddLang.find( '[data-lang]:not(.selected)' ).length ? 'show' : 'hide' ]();
        }

        $.each( ( ( MainPage.response || {} ).group || {} ).groupData || {} , function( lang ) {
            addLang( lang , ( MainPage.response || {} ).group , true );
        } );

        openFirstLang();
    };
};

MainPage.check = function() {
    var nodeForm = $( '.itemsForm' );

    var prizeGroupName = nodeForm.find(".text-field[name='name']");
    var errorNode = nodeForm.find(".group-name-help");

    if(prizeGroupName.val()==""){
        MainPage.showCheckError(errorNode,"data-name-alert");
        prizeGroupName.addClass( 'error' );
        return false;
    }

    if(nodeForm.find( '#locales .tab').length ==0) {
        MainPage.showCheckError(errorNode,"data-lang-alert");
        return false;
    }

    nodeForm.find( '[data-lang]' ).removeClass( 'error' ).each( function() {
        var nodeLang = $( this );
        if( nodeLang.hasClass( 'error' ) || ! nodeLang.val() ) {
            return;
        }
        nodeLang.siblings( '[data-lang]' ).each( function() {
            if( ! this.value ) {
                $( this ).addClass( 'error' );
            }
        } );
    } );

    var errorNodes = nodeForm.find( '.error' );

    $( '.lang-tabs [data-tab=' + errorNodes.eq(0).data("lang") + ']' ).trigger( 'click' , [ true ] );
    errorNodes.eq(0).focus();

    return ! errorNodes.length;
};

MainPage.showCheckError = function(elemNode,alertKey){
    var errAlert = elemNode.attr(alertKey);
    elemNode.html('<div class="popover fade in '+Utils.createTimerFunc.options.hide.css+'">'+errAlert+'</div>');
    Utils.createTimerFunc.init(Utils.createTimerFunc.options.hide.name,5000);
}