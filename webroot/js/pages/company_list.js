var MainPage = new function(){
    var nodeFilters;
    var nodeContent;
    var nodeEmpty;

    this.init = function() {
        nodeFilters        = $( '.filters' );
        nodeContent        = $( '.content-list' );
        nodeEmpty          = nodeContent.find( '.empty' );

        nodeFilters.on( 'click' , '.row[data-filter]' , function() {
            nodeFilters.find( '.row[data-filter]' ).removeClass( 'active' );

            var filter = $( this ).addClass( 'active' ).data( 'filter' );
            var showedLength = 0;

            nodeContent.find( '.row' ).each( function() {
                var row = $( this );
                if( !filter && typeof filter != 'number' || row.data( 'count' ) <= filter ) {
                    row.show();
                    showedLength++;
                } else {
                    row.hide();
                }
            } );

            nodeContent[ showedLength ? 'removeClass' : 'addClass' ]( 'empty-list' );
        } );

        nodeFilters.on( 'click' , '.row[data-status]' , function() {} );
    };
};