var MainPage = new function(){
    this.init = function() {
        Utils.togglePasswordChars();

        var nodeForm    = $( '#form' );
        var confirmForm = $( '#confirm-form' ).slideUp(0);
        var confirmPass = confirmForm.find( '[name=password]' ).on( 'focus click' , function() {
            $( this ).removeClass( 'error' );
        } );
        var nodePublicName = $( '[name=publicName]' ).on( 'focus click' , function() {
            $( this ).removeClass( 'success' ).removeClass( 'error' );
        } );
        var nodeEmail = $( '[name=email]' ).on( 'focus click' , function() {
            $( this ).removeClass( 'success' ).removeClass( 'error' );
        } );
        var fullOverlap = nodeForm.find( '.full-overlap' ).click( function() {
            if( submited ) {
                return;
            }
            $( '.saveBtn , .cancelBtn' ).removeClass( 'disabled' );
            confirmForm.slideUp();
            $( this ).removeClass( 'active' );
        } );
        var saveBtn     = $( '.saveBtn' ).unbind( 'click' ).bind( 'click' , function() {
            if( !nodeEmail.val() || !nodePublicName.val() ) {
                !nodePublicName.val() && nodePublicName.addClass( 'error' );
                !nodeEmail.val() && nodeEmail.addClass( 'error' );
                return;
            }
            if( $( this ).hasClass( 'disabled' ) ) {
                return;
            }
            $( '.saveBtn , .cancelBtn' ).addClass( 'disabled' );
            confirmForm.slideDown();
            fullOverlap.addClass( 'active' );
            $( document ).scrollTop(0);
        } );
        var submited;

        function err() {
            submited = false;
            confirmPass.addClass( 'error' );
            $( '#confirm' ).removeClass( 'disabled' );
        }

        $( '#confirm' ).click( function() {
            var pass = confirmPass.val();

            $( '#confirm' ).addClass( 'disabled' );

            if( pass ) {
                submited = true;

                $.getJSON( '/api/user/check/password/' + confirmPass.val() + '/' , function( event ) {
                    if( event.data ) {
                        confirmForm.slideUp();
                        nodeForm.submit();
                    } else err();
                } ).error( err );
            } else err();
        } );

        $( '.check-email' ).click( function() {
            if( $( this ).hasClass( 'disabled' ) ) {
                return;
            }

            if( !nodeEmail.val() ) {
                nodeEmail.addClass( 'error' );
                return;
            }

            var btn = $( this ).addClass( 'disabled' );

            $.getJSON( '/api/user/check/email/' + nodeEmail.val() + '/' , function( event ) {
                nodeEmail.addClass( event.data ? 'error' : 'success' );
                btn.removeClass( 'disabled' );
            } ).fail( function() {
                nodeEmail.addClass( 'error' );
                btn.removeClass( 'disabled' );
            } );
        } );

        nodeEmail.change( function() {
            $( '[name=username]' ).val( this.value );
        } );

        $( 'div.cancelBtn' ).click( function() {
            if( !$( this ).hasClass( 'disabled' ) ) {
                $( 'input.cancelBtn' ).click();
            }
        } );

        nodeForm.find( 'input' ).keyup( function() {
            $( '.saveBtn , .cancelBtn' ).removeClass( 'disabled' );
        } );

        nodeForm.find( '#defaultLocaleSelector' ).change( function() {
            $( '.saveBtn , .cancelBtn' ).removeClass( 'disabled' );
        } );
  };
};