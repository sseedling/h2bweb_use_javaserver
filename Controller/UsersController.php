<?php
class UsersController extends AppController {
    public $name = 'Users';
    public $uses = array();

    public function beforeFilter() {
        $this->set("sectionName","users");
        parent::beforeFilter();
    }

    public function users_profile(){
        $userData = Auth::getUser();

        Parse_User::$userMode = "editManager";
        if(Parse_User::validate($this->queryData)) {
            $user = new Parse_User();
            $user->companyObj = $this->Company;
            if($user->update(array("objectId"=>$userData->objectId))){
                $this->redirectSafe("/profile/");
            }
        }

        $this->set("postData",Parse_User::$postData);
        $this->set("userData",$userData);
    }

    public function users_list() {
        $this->set("userList", Parse_User::findAll($this->Company));
        $this->set("rolesList", Roles::getSharedRoles());

    }

    public function users_add() {
        Parse_User::$userMode = "addManager";
        if(Parse_User::validate($this->queryData)) {
            $user = new Parse_User();
            $user->companyObj = $this->Company;

            $alcArr = array();
            if(isset(Parse_User::$postData["roles"])){
                foreach(Parse_User::$postData["roles"] as $role=>$mode){
                    if($mode["R"]=="true")$alcArr[$role."R"] = array("read"=>true);
                    if($mode["W"]=="true")$alcArr[$role."W"] = array("read"=>true);
                }

            }
            if($user->save(array("ACL"=>$alcArr))){
                $this->redirectSafe("/users/");
            }
            $this->set("postData",Parse_User::$postData);
        }

        $this->set("rolesList", Roles::getSharedRoles());

        if(Auth::inRoles(array("Owner"))){
            $this->set("locationList",ParseLocation::findAll($this->Company->parseObj->objectId,array("name")));
        }
    }

    public function users_edit() {

        Parse_User::$userMode = "editManager";
        if(Parse_User::validate($this->queryData)) {

            $user = new Parse_User();
            $user->companyObj = $this->Company;

            $alcArr = array();
            if(isset(Parse_User::$postData["roles"])){
                foreach(Parse_User::$postData["roles"] as $role=>$mode){
                    if($mode["R"]=="true")$alcArr[$role."R"] = array("read"=>true);
                    if($mode["W"]=="true")$alcArr[$role."W"] = array("read"=>true);
                }

            }
            if(empty($alcArr))$alcArr = array($this->request->params["userId"]=>array("read"=>true,"write"=>true));
            if(!isset(Parse_User::$postData["locationList"]))Parse_User::$postData["locationList"]=array();
            if($user->update(array("ACL"=>$alcArr,"objectId"=>$this->request->params["userId"])))$this->redirectSafe("/users/");
        }

        $userData = Parse_User::get($this->request->params["userId"]);
        if(!$userData)$this->PageNotFoundException();
        else {
            $userData->status = (!isset($userData->status)||(!$userData->status))?"false":"true";
        }

        if(Auth::inRoles(array("Owner"))){
            $this->set("locationList",ParseLocation::findAll($this->Company->parseObj->objectId,array("name")));
        }
        $this->set("postData",Parse_User::$postData);
        $this->set("rolesList", Roles::getSharedRoles());
        $this->set("userData",$userData);
    }

    public function api_user_check_password(){
        $sessionUser = Auth::getUser();
        $check = Auth::login($sessionUser->username,$this->request->params["password"]);
        Utils::apiRender(200,"success",$check);
    }

    public function api_user_check_email(){
        if(!isset($this->request["userId"]))
            $sessionUser = Auth::getUser();

        $userId = (!isset($this->request["userId"]))?$sessionUser->objectId:$this->request["userId"];
        if($userId=='null')$userId = null;

        $test = ParseEntity::isExist("_User",array("where"=>array("email"=>$this->request->params["email"])),$userId);

        Utils::apiRender(200,"Success",$test);
    }

    public function api_user_check_username() {
        $username = isset($this->queryData["username"])?$this->queryData["username"]:$this->request->query["username"];
        $objectId = isset($this->queryData["objectId"])?$this->queryData["objectId"]:$this->request->query["objectId"];

        $test = ParseEntity::isExist("_User",array("where"=>array("username"=>$username)),$objectId);
        Utils::apiRender(200,"Success",(($test)?"exist":"empty"));
    }
}

/*
 * у пользователя есть
 *
 * шесть ролей
 * логин..............если нет логина то использовать в качестве логина емаил
 * пароль
 * email
 * список локаций
 *
 *
 *
 *
 * при попытке пользователя зайти на страницу
 *
 * проверяем роли пользователя на уровень доступа к странице
 * во всех случаях когда происходит выборака локации нужно проверять имеет ли пользователь доступ к онной
 *
 *
 */
