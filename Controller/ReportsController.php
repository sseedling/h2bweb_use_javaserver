<?php
class ReportsController extends AppController {
    public $name = 'Reports';
    public $uses = array();

    public function beforeFilter(){
        parent::beforeFilter();
        $this->set("sectionName","reports");
    }

    /*------------------ACTIONS------------------------------------*/

    /**
     * PAGE :: Chart dashboard
     */
    public function reports_view() {
        $allLocations = ParseLocation::findAll($this->Company->parseObj->objectId);

        if(isset($allLocations[0]))
            foreach($allLocations as $location){
                if(isset($location->quizzes[0])){
                    foreach($location->quizzes as $quizz)
                        $tmpList[$quizz->objectId] = true;
                }
            }


        if(isset($tmpList)){
            foreach ($tmpList as $quizzId=>$v)$quizzessList[] = $quizzId;
            $quizzes = ParseSurvey::find($quizzessList);
            $this->set("quizzes",$quizzes);
        }

        $this->set("allLocations",$allLocations);
    }
}
