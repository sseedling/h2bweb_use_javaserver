<?php
class SurveyController extends AppController {
    private $postData;
    public $name = 'Survey';
    public $uses = array();
    private $questionsOrder = false;

    public function beforeRender() {
        $this->set("sectionName","locations");
        parent::beforeRender();
    }

    public function survey_location_view() {
        $location = $this->getLocation();
        $survey = $this->getSurvey();
        $this->makeTitle($location,$survey);
        $this->set("location",$location);
        $this->set("survey",$survey);
        $this->set("allQuestions",$this->sortQuestionsByLocale($survey->questions));
    }

    public function survey_location_add() {
        if(($this->queryData)&&(ParseSurvey::validate($this->queryData))){
            $survey = new ParseSurvey();
            $survey->companyObj = $this->Company;
            if($survey->save()){
                $location = $this->getLocation();
                $location->attachSurvey($survey->objectId);
                $this->redirectSafe("/locations/".$location->urlName."/".$survey->urlName."/");
            }
        }
        $this->set("postData",ParseSurvey::$postData);
        $this->set("locationUrl",$this->request->params["location"]);
    }

    public function survey_location_edit() {
        $locationObj = $this->getLocation();
        $survey = $this->getSurvey();
        $curSurveyUrl = $survey->urlName;//save url before update change it
        $survey->companyObj = $this->Company;

        if((ParseSurvey::validate($this->queryData))&&($survey->update())){
            $this->redirectSafe("/locations/".$locationObj->urlName."/".$survey->urlName."/");
        }

        $survey->urlName = $curSurveyUrl;
        $this->makeTitle($locationObj,$survey);
        if($this->queryData)$this->set("postData",ParseSurvey::$postData);
        $this->set("survey",$survey);
        $this->set("allQuestions",$this->sortQuestionsByLocale($survey->questions));
        $this->set("locationUrl",$locationObj->urlName);
    }

    public function survey_set_status() {
        if(!in_array($this->request->params["status"],ParseSurvey::$avalibleSurveyStatus))$this->PageNotFoundException();
        $survey = $this->getSurvey();
        $survey->companyObj = $this->Company;
        $survey->setStatus($this->request->params["status"]);
        $this->redirectSafe("/locations/".$this->request->params["location"]."/".$survey->urlName."/");
    }

    public function survey_location_drop(){
    }

    public function survey_attach_to_location(){
        if(!in_array($this->request->params["mode"],ParseLocation::$availibleActions))$this->PageNotFoundException();
        $location = $this->getLocation();
        $survey = $this->getSurvey("light");

        $location->attachSurvey($survey->objectId,$this->request->params["mode"]);
        $this->redirectSafe("/locations/".$location->urlName."/");
    }

    private function getLocation(){
        $location = new ParseLocation();
        if(!$location->load($this->Company->parseObj->objectId,$this->request->params["location"]))$this->PageNotFoundException();
        return $location;
    }

    private function getSurvey($mode="full"){
        $survey = new ParseSurvey();
        if(!$survey->load($this->Company->parseObj->objectId,$this->request->params["survey"]))$this->PageNotFoundException();
        if($mode=="full"){
            $questionFilter = array("where"=>array("survey"=>$survey->objectId),"orderBy"=>array("order"));
            if($this->questionsOrder)$questionFilter["orderBy"] = $this->questionsOrder;
            $survey->questions = ParseEntity::parseGetQuery("Question",$questionFilter,array("questionData","objectId"));
        }
        return $survey;
    }

    private function makeTitle($location,$survey){
        $this->localeStrings->page->title = array(
            'root'=>"/locations/",
            "parts"=>array(
                array(
                    "url"=>$location->urlName,
                    "text"=>$location->name
                ),
                array(
                    "url"=>$survey->urlName,
                    "text"=>$survey->name
                )
            )
        );

        switch($this->action){
            case "survey_edit"://return $this->localeStrings->title["parts"][] = array("url"=>"edit","text"=>"Edit")
            case "survey_view":
            default:return true;
        }
    }

    private function sortQuestionsByLocale($allQuestions){
        $questionsPull = array();
        if(!empty($allQuestions)){
            foreach($allQuestions as $order=>$question){
                foreach($this->appLocales as  $locale=>$data){
                    if(isset($question["questionData"]->$locale)){
                        $questionsPull[$locale][$order]["name"] = $question["questionData"]->$locale->name;
                        $questionsPull[$locale][$order]["subQuestions"] = $question["questionData"]->$locale->subQuestions;
                        $questionsPull[$locale][$order]["subQuestionsEnabled"] = $question["questionData"]->$locale->subQuestionsEnabled;
                        $questionsPull[$locale][$order]["text"] = $question["questionData"]->$locale->text;
                        $questionsPull[$locale][$order]["objectId"] = $question["objectId"];
                    }
                }
            }
        }
        return $questionsPull;
    }
}

