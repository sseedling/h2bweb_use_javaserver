<?php

class ReportsClassicChartController extends AppController {
    public $name = 'ReportsClassicChart';
    public $uses = array();

    public function beforeFilter(){
        parent::beforeFilter();
        $this->set("sectionName","reports");
    }

    /*------------------ACTIONS------------------------------------*/

    /**
     * PAGE :: Print large report table
     * params:
     *      @locationsIdList : locations objectId list,
     *      @quizzId : quizz objectId
     *      @minDate : start date interval
     *      @maxDate : end date interval
     */
    public function reports_classic_details() {
//        Utils::_print($this->queryData);
        $chartHelper = new ChartHelper($this->queryData,$this->localeName);
        $reportsList = $chartHelper->getReportList();
        $reports = $chartHelper->makeDetailReportData(array("reportsList"=>$reportsList),"classic");

        $this->set("reports",$reports);
        $this->set("quizzList",ChartHelper::getQuizzList());
        $this->set("locationObjList",$chartHelper->getLocations($reportsList));
        $this->set("questionObjList",$chartHelper->getQuestions(array("objList"=>$reports,"mode"=>"table"),"classic"));

        $this->render(DS."Reports".DS.$this->action);
    }

    /**
     * API :: Collect from parse data for create-chart on web-page
     * params:
     *      @locationsIdList : locations objectId list,
     *      @quizzId : quizz objectId
     *      @minDate : start date interval
     *      @maxDate : end date interval
    */
    public function reports_classic_chart_collect() {
        $this->queryData["orderMode"] = "orderByAscending";
        $chartHelper = new ChartHelper($this->queryData,$this->localeName);
        $reportsList = $chartHelper->getReportList();
        if(empty($reportsList)){
            $report = $chartHelper->makeReportData(array(
                "answersIdList"=>array(),
                "reportsList"=>array(),
                "quizIdList"=>$chartHelper->getQuizzList(null,true),
            ),"classic");
            Utils::apiRender(200,"success",$report);
        }

        $answerIdList = $chartHelper->makeAnswerIdList($reportsList);
        $report = $chartHelper->makeReportData(array("answersIdList"=>$answerIdList,"reportsList"=>$reportsList),"classic");
        if(!$report)$report["reports"] = array();
        Utils::apiRender(200,"success",$report);
    }

    /*-------------------PRIVATE_METHODS---------------------------*/

}
