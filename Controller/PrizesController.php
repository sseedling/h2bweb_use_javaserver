<?php
class PrizesController extends AppController {
    public $name = 'Prizes';
    public $uses = array();

    private $countPerPage = 100;

    public function beforeFilter(){
        $this->set("sectionName","prizes");
        parent::beforeFilter();
    }

    /**
     * Page
     *
     */
    public function prizes_list() {
        $groups = ParsePrizeGroup::findAll($this->Company->parseObj->objectId);
        $prizes = ParsePrize::getList($this->countPerPage,$this->Company->parseObj->objectId,$this->Company->parseObj->objectId);

        $response = array("allPrizeCount"=>ParsePrize::countingAll($this->Company->parseObj->objectId));

        if(isset($groups[0])){
            foreach($groups as $group){

                $groupPrizes = array();
                if(isset($group->prizes[0]))
                    foreach($group->prizes as $prize) $groupPrizes[] = $prize->objectId;

                $quizzList = ParseSurvey::findByPrizeGroup($group->objectId);
                $groupQuizzes = array();
                if(isset($quizzList[0]))
                    foreach($quizzList as $quizz) $groupQuizzes[$quizz->objectId] = $quizz->name;

                $response["groups"][$group->objectId] = array(
                    "name" => $group->name,
                    "note" => $this->extractGroupNote($group->groupData,"note"),
                    "unwinNote" => $this->extractGroupNote($group->groupData,"unwinNote"),
                    "prizes" => $groupPrizes,
                    "quizzes" => $groupQuizzes,
                );
            }
        }
        $response["prizes"] = !empty($prizes)?Utils::simplifyLocaleObjData($prizes,$this->localeName,array("name","note"),"prizeData"):array();
        $this->set("response",json_encode($response));
    }

    /**
     * Page
     *
     * Edit existed prize group
     *
     * check for data :: if exist -> try to uodate :: if ok -> redirect to list
     */
    public function prizes_edit(){
//        $this->_print($this->queryData);
        //check for update data
        if(!empty($this->queryData) && (ParsePrizeGroup::validate($this->queryData))){
            $prizeGroup = new ParsePrizeGroup();
            $prizeGroup->companyId = $this->Company->parseObj->objectId;

            if($prizeGroup->update(array("objectId"=>$this->request->params["groupId"])))
                $this->redirectSafe("/prizes/");
            else $this->set("postData",ParseSurvey::$postData);//contain all update errors
        }

        //collect data
        $response = $this->collectPrizeGroupForEdit();
        //set to view
        $this->set("response",$response);
    }

    /**
     * Page
     *
     * Add new one prize group
     *
     * check for data :: if exist -> try to save :: if ok -> redirect to list
     */
    public function prizes_add(){
        if(!empty($this->queryData) && (ParsePrizeGroup::validate($this->queryData))){
            $prizeGroup = new ParsePrizeGroup();
            $prizeGroup->companyId = $this->Company->parseObj->objectId;

            if($prizeGroup->save()){
                $this->redirectSafe("/prizes/");
            }
        }
                //load all quizzes
        $allQuizes = ParseSurvey::finadAllQuizes(array("where"=>array("company"=>$this->Company->parseObj->objectId),"whereIn"=>array("prizeGroup"=>array(null,""))),array("name"));
        $this->set("postData",ParsePrizeGroup::$postData);
        $this->set("allQuizes",$allQuizes);
    }

    /**
     * Api
     *
     * Get from parse prizes list
     *
     * @param null $count - max result items
     * @param null $prizeIdList - array of prizes id - if null - any id
     * @return null
     */
    public function prizes_get_prizes_list($count=null,$prizeIdList=null){
        if(!isset($this->queryData["count"]))
            $this->queryData["count"] = $this->countPerPage;

        if(!isset($this->queryData["prizeIdList"]))
            $this->queryData["prizeIdList"] = $this->Company->parseObj->objectId;

        $response = ParsePrize::getList($this->queryData["count"],$this->queryData["prizeIdList"],$this->Company->parseObj->objectId);
        $code = 200;
        $msg = "success";

        Utils::apiRender($code,$msg,$response);
    }

    /**
     * Api
     * Set new count of prize
     *
     * @param null $prizeId
     * @param null $prizeCount
     */
    public function prizes_set_prise_count($prizeId=null,$prizeCount = null) {
        $error = array();
        if(!isset($this->queryData["prizeCount"]))$error["prizeCount"]="require";
        if(!isset($this->queryData["prizeId"]))$error["prizeId"]="require";

        if(!empty($error)){
            $response = $error;
            $code = 500;
            $msg = "success";
        }
        else{
            $prize = new ParsePrize();
            $response = $prize->update($this->queryData["prizeId"],array("count"=>1*$this->queryData["prizeCount"]));
            $code = 200;
            $msg = "success";
        }
        Utils::apiRender($code,$msg,$response);
    }

    /**
     * Api
     *  set given prize status in report
     */
    public function prizes_giv_prise(){
        ParseLocationReport::givePrize($this->request->params["reportId"],$this->ParseUSER->objectId);
        Utils::apiRender();
    }

    private function collectPrizeGroupForEdit(){
        //load group
        $group = ParsePrizeGroup::loadPrizeGroup($this->request->params);
        if(!$group)$this->PageNotFoundException();

        //load prizes
        if(isset($group->prizes[0]))
            foreach($group->prizes as $prize)
                $prizeList["all"][] = $prize->objectId;

        $groupPrizes = isset($prizeList)?ParsePrize::getList(null,$prizeList,$this->Company->parseObj->objectId):array();

        //load all quizzes
        $allQuizes = ParseSurvey::finadAllQuizes(array("where"=>array("company"=>$this->Company->parseObj->objectId),"whereIn"=>array("prizeGroup"=>array(null,"",$group->objectId))),array("name","prizeGroup"));

        //filter groupQuizes
        $groupQuizzes = array();
        if($allQuizes)foreach($allQuizes as $quizzId=>$quizData){
            if((isset($quizData["prizeGroup"]))&&($quizData["prizeGroup"]==$group->objectId))
                $groupQuizzes[] = $quizzId;
        }

        //make response
        return array(
            "group"=>array(
                "objectId" => $group->objectId,
                "name" => $group->name,
                "groupData" => Utils::objToArr($group->groupData),
                "prizes" => $this->preparePrizes($groupPrizes),
                "quizzes" => $groupQuizzes,
            ),
            "quizzes" => $allQuizes
        );
    }

    private function preparePrizes($groupPrizes){
        if(empty($groupPrizes))return $groupPrizes;
        $groupPrizes = Utils::objToArr($groupPrizes);
        return $groupPrizes;
    }

    private function extractGroupNote($groupData,$field){
        if(is_object($groupData))$groupData = Utils::objToArr($groupData);
        $localeName = $this->localeName;
        $allLocalesNames = AppLocale::getLocalesNames();
        foreach($allLocalesNames as $locale)
            if(isset($groupData[$locale])){
                $firstFoundLocale = $locale;
                break;
            }

        $usedLocale = (isset($groupData[$localeName]))?$localeName:$firstFoundLocale;
        return isset($groupData[$usedLocale][$field])?$groupData[$usedLocale][$field]:null;
    }
}

