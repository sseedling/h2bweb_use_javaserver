<?php
class CompanyController extends AppController {
    public $name = 'Company';
    public $uses = array();

    public function beforeFilter() {
        $this->set("sectionName","company");
        parent::beforeFilter();
    }

    public function company_list() {
        $companyList = ParseCompany::findAll(array("name","comment","credits","ownerEmail","trusted"),array("whereNotEqualTo"=>array("objectId"=>$this->Company->parseObj->objectId)));
        foreach($companyList as $k=>$v){
            if($v["name"]==$this->Company->base){
                unset($companyList[$k]);break;
            }
        }
        $this->set("companiesList",$companyList);

    }

    public function company_add() {
        Parse_User::$userMode = "CompanyOwner";

        //validate
        $Validate = null;
        $Validate->owner = ((isset($this->queryData["owner"]))&&(Parse_User::validate($this->queryData["owner"])));
        $Validate->company = ((isset($this->queryData["company"]))&&(ParseCompany::validate($this->queryData["company"])));

        if($Validate->company && $Validate->owner){
            //collect all roles
            $ownerAcl = array();
            $allRoles = Roles::getAllNames(1,array("DeviceManager","LocalesW","LocalesR"));//todo change roles structure
            foreach($allRoles as $role)$ownerAcl[$role] = array("read"=>true);

            //prepare new owner
            $owner = new Parse_User();
            $newOwnerData = array(
                "ACL"=>$ownerAcl,
                "password"=>$this->passwordGenerate(8),//tmp password
                "publicName"=>"Owner",
                "status"=>true
            );

            //save new owner
            if($owner->save($newOwnerData)){
                //save new company
                $companyObj = new ParseCompany();
                if($companyObj->save(array("ownerId"=>$owner->objectId))){
                    Parse_User::setCompany($companyObj->objectId,$owner->objectId);
                    $this->company_generate_password($owner->objectId);
                    $this->redirectSafe("/companies/");
                }
                else{
                    $owner->drop("simple");
                }
            }
        }
        $postData["company"] = ParseCompany::$postData;
        $postData["owner"] = Parse_User::$postData;
        $this->set("postData",$postData);
    }

    public function company_edit() {
        Parse_User::$userMode = "CompanyOwner";

        //validate
        $Validate = null;
        $Validate->owner = ((isset($this->queryData["owner"]))&&(Parse_User::validate($this->queryData["owner"])));
        $Validate->company = ((isset($this->queryData["company"]))&&(ParseCompany::validate($this->queryData["company"])));
        //Utils::_print($Validate);

        if($Validate->company && $Validate->owner){
            $companyObj = new ParseCompany();
            $update["company"] = $companyObj->update($this->request->params["companyId"]);

            $owner = Parse_User::get($this->queryData["owner"]["objectId"]);
            $ownerObj = new Parse_User();
            $update["owner"] = $ownerObj->update();

            if($update["owner"] && ($owner->email!=$ownerObj->email))
                $this->company_generate_password($owner->objectId);

            if($update["owner"] && $update["company"])
                $this->redirectSafe("/companies/");
        }

        $postData["company"] = ParseCompany::$postData;
        $postData["owner"] = Parse_User::$postData;

        $company = ParseCompany::get($this->request->params["companyId"],array("name","comment","credits","ownerId","trusted"));
        if(!$company)$this->PageNotFoundException();
        $owner = Parse_User::get($company["ownerId"],array("username","email","objectId"));
        if(!$owner)$this->PageNotFoundException();
        $company["objectId"] = $this->request->params["companyId"];

        $this->set("postData",$postData);
        $this->set("company",$company);
        $this->set("owner",$owner);
    }

    //post params : [SuperAdmin password] & [companyId]
    public function api_company_drop(){
        if(!Auth::login(@$this->ParseUSER->username,@$this->queryData["password"])){
            //Auth::logout();
            Utils::apiRender(403,"error");
        }
        $company = new ParseCompany();
        $company->drop($this->queryData["companyId"]);
        Utils::apiRender();
    }

    public function company_generate_password($ownerId=null){
        //validate
        if(isset($this->request->params["ownerId"]))$ownerId = $this->request->params["ownerId"];
        if(!$ownerId) $this->PageNotFoundException($ownerId);

        //get owner
        $owner = Parse_User::get($ownerId);
        if(!$owner)$this->PageNotFoundException();

        //generate autority
        $password = $this->passwordGenerate(8);
        $login = $owner->username;

        //save owner authority
        Parse_User::setPassword($password,$ownerId);

        //prepare email
        $emailHelper = new EmailHelper();
        $emailHelper->setStrings($this->localeStrings->emails,$this->Company->default->lang);
        $passwordEmail = $emailHelper->get("password",array("password"=>$password,"login"=>$login),$this->Company->default->lang);

        //send email
        App::uses('CakeEmail', 'Network/Email');
        $Email = new CakeEmail();
        $Email->from(array($this->Company->email => $passwordEmail->title));
        $Email->to($owner->email);
        $Email->emailFormat("html");
        $Email->subject($passwordEmail->subject);
        $Email->send($passwordEmail->body);

        if(isset($this->request->params["ownerId"]))Utils::apiRender();
    }

    private function passwordGenerate($length){
        $pass = '';
        $alphabet = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";
        $alphaLength = strlen($alphabet) - 1;
        for ($i = 0; $i < $length; $i++)
            $pass .= $alphabet[mt_rand(0, $alphaLength)];
        return $pass;
    }
}