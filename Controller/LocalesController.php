<?php
class LocalesController extends AppController {
    public $name = 'Locales';
    public $uses = array();

    public function beforeRender(){
        $this->set("sectionName","locales");
        parent::beforeRender();
    }

    /**
     * Load and show short list of available text-pools
     *
     */
    public function locales_all() {
        $model = Utils::loadModels(ModelHelper::$DbLocaleKey);
        $allKeys = $model->find("all",array('conditions' => array('parent' => null)));

        $this->set("localesKeys",$allKeys);
    }

    /**
     *
     * search subKeyList for selected key
     * if( count == 0 )redirect to edit page
     * else show subKeyList
     */
    public function locales_view() {
        $keyId = @$this->request["keyId"];

        $model = Utils::loadModels(ModelHelper::$DbLocaleKey);

        //load parent-Key
        $model->id = $keyId;
        $parentKey = $model->Read();
        if(!$parentKey)$this->PageNotFoundException();

        //get all sub-keys
        $allSubKeys = $model->find("all",array('conditions' => array('parent' => $parentKey[$model->name]["id"])));
        if(!isset($allSubKeys[0]))$this->redirectSafe("/locales/edit/".$keyId);//sub-keys not found do edit parent

        //sub-keys exist - show them and parent-Key
        $this->set("parentKey",$parentKey);
        $this->set("subKeys",$allSubKeys);

    }

    /**
     *
     * Load @key and all localize @keyData
     * pram: @keyId
     *
     */
    public function locales_edit() {
        $keyId = @$this->request["keyId"];

        //create models----------------------
        $webLocaleModel = Utils::loadModels(ModelHelper::$DbWebLocale);
        $keyModel = Utils::loadModels(ModelHelper::$DbLocaleKey);
        $dataModel = Utils::loadModels(ModelHelper::$DbLocaleString);

        //load webLocales
        $allLocales = $webLocaleModel->findAndClear("all");

        //load Key
        $keyModel->id = $keyId;
        $key = $keyModel->readAndClear();
        if(!$key)$this->PageNotFoundException();

        //Save if submit
        if($this->queryData){
            foreach($this->queryData[$key["key"]] as $localeData){
                $dataModel->read(null,$localeData["id"]);
                $dataModel->set("data",json_encode($localeData["data"]));
                $dataModel->save();
            }
            $this->redirectSafe("/locales/".(($key["parent"])?"view/".$key["parent"]."/":""));
        }

        //load key-data
        $keyData = $dataModel->findAndClear("all",array("conditions"=>array("key"=>$key["id"])));
        if(!isset($keyData[0]))$this->PageNotFoundException();

        $this->set("obj",array("key"=>$key,"data"=>$keyData,"webLocales"=>$allLocales));
    }

    /**
     *
     * Edit existed locales text-name - English,Русский i.e
     */
    public function locales_names_edit(){
        //create models----------------------
        $webLocaleModel = Utils::loadModels(ModelHelper::$DbWebLocale);
        $appLocaleModel = Utils::loadModels(ModelHelper::$DbAppLocale);

        //load webLocales
        $allLocales["web"] = $webLocaleModel->findAndClear("all");

        //load appLocales  -  unused
        //$allLocales["app"] = $this->$appLocaleModel->findAndClear("all");

        $localeModelTypes = array("web"=> $webLocaleModel,"app" => $appLocaleModel);
        //Save if submit
        if($this->queryData){
            foreach($this->queryData["localeNames"] as $localeType=>$localeList){
                foreach($localeList as $localeId=>$localeData){
                    $localeModelTypes[$localeType]->read(null,$localeId);
                    $localeModelTypes[$localeType]->set("name",$localeData["name"]);
                    $localeModelTypes[$localeType]->save();
                }
            }
            $this->redirectSafe("/locales/");
        }
        $this->set("obj",$allLocales);
    }

    /**
     * Add new one locale and fill it with default locale texts
     */
    public function locales_names_add(){
        //create models----------------------
        $webLocaleModel = Utils::loadModels(ModelHelper::$DbWebLocale);
        $appLocaleModel = Utils::loadModels(ModelHelper::$DbAppLocale);
        $localeStringsModel = Utils::loadModels(ModelHelper::$DbLocaleString);

        //validate-----------------------------
        if(($webLocaleModel->validateNewLocale($this->queryData))&&($appLocaleModel->validateNewLocale($this->queryData))){
            //save new app-locale------------------------------
            $appLocaleModel->save($this->queryData[$webLocaleModel->name]);

            //save new web-locale------------------------------
            $localeNameId = $webLocaleModel->saveAndGetId($this->queryData[$webLocaleModel->name]);

            //load default locale------------------------------
            $defaultLocaleName = $webLocaleModel->findByCode($this->Company->default->lang);

            //load default locale strings------------------------------
            $allDefaultStrings = $localeStringsModel->find("all",array("conditions"=>array("lang"=>$defaultLocaleName[$webLocaleModel->name]["id"])));

            //attach default locale strings to new locale------------------------------
            foreach($allDefaultStrings as $k=>$defaultString){
                $allDefaultStrings[$k][$localeStringsModel->name]["lang"] = $localeNameId;
                unset($allDefaultStrings[$k][$localeStringsModel->name]["id"]);
            }

            //save locale strings for new locale
            $localeStringsModel->saveAll($allDefaultStrings);

            //save img
            FileHelper::save("new-locale",$this->queryData[$webLocaleModel->name]["code"]);

            //redirect
            $this->redirectSafe("/locales/names/edit/");
        }
        $this->set("errors",$webLocaleModel->getValidationErrors());
    }

}
