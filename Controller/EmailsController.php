<?php
class EmailsController extends AppController {
    public $name = 'Emails';
    public $uses = array();
    private $defaultEmailLogo;
    private $dateFormat = "d F Y";
    private $reportData = null;
    private $emailHelper = null;
    private $emailCakeCore = null;
    private $pageNameForLocaleString = "undefined";

    public function beforeFilter() {
        parent::beforeFilter();
        $this->prepareLocaleEmail();
        $this->defaultEmailLogo = $this->Company->serverName.DS.$this->Company->default->path->logo;
    }

    private function prepareLocaleEmail(){
        $this->emailHelper = new EmailHelper();
        $localeList = LocaleStrings::getLocalesNames();
        foreach($localeList as $locale){
            $tmp = LocaleStrings::get($locale,$this->pageNameForLocaleString);
            $this->emailHelper->setStrings($tmp->emails,$locale);
        }
    }

    /**
     * Send email core to valid email
     * @param $from
     * @param $title
     * @param $to
     * @param $subject
     * @param $body
     */
    private function sendEmail($from,$title,$to,$subject,$body){
        if(!filter_var($to, FILTER_VALIDATE_EMAIL))return;

        if(!$this->emailCakeCore){
            App::uses('CakeEmail', 'Network/Email');
            $this->emailCakeCore = new CakeEmail();
        }

        $this->emailCakeCore->from(array($from => $title));
        $this->emailCakeCore->to(array($to));
        $this->emailCakeCore->emailFormat("html");
        $this->emailCakeCore->subject($subject);
        $this->emailCakeCore->send($body);
    }

    /**
     * Send email to manager,client
     */
    public function emails_send_report_new_notice(){
        $this->reportData = $this->collectReportData();
        //Check for send report notice
        foreach($this->reportData["answers"][$this->reportData["report"]->survey] as $answer){
            if($answer->grade < 2){ //negative satisfaction
                $this->create_and_send_email("report",@$this->reportData["report"]->emailList);
                break;
            }
        }

        //Check for send prize notice
        if($this->reportData["report"]->winStatus == "win"){
            $this->create_and_send_email("prize",array($this->reportData["report"]->email=>$this->reportData["report"]->locale));
        }

        //Check for send RARE-prize notice
        if(($this->reportData["report"]->winStatus == "win")&&($this->reportData["prize"]->chance <= 25)){
            $this->create_and_send_email("rarePrize",@$this->reportData["report"]->emailList);
        }

        Utils::apiRender();
    }

    /**
     * Send email to owner and clear db from answer-data
     */
    public function emails_send_credits_expire_notice(){
        $to = array($this->reportData["to"]=>$this->Company->default->lang);
        $this->create_and_send_email("creditsExpire",$to);

        //drop answerList ---------------------------------------------------------------------
        $answerPointersList = json_decode($this->queryData["answerPointers"]);
        $answerObjIdList = array();
        foreach($answerPointersList as $answerPointer)$answerObjIdList[] = $answerPointer->objectId;
        ParseAnswer::dropAll($answerObjIdList);

        Utils::apiRender();
    }

    //--------------------------------------------------------------
    private function create_and_send_email($emailName,$toList){//$toList => array(email_1=>locale_1,email_2=>locale_2)
        if(empty($toList))return;
        $methodName = "create".ucfirst($emailName)."EmailParams";//combine params collect method name
        foreach($toList as $to=>$locale){//make email data and send to each email with locale

            $params = $this->$methodName($locale);//collect params with locale

            //Email image-server
            $params["server"] = $this->Company->serverName;

            $reportEmail = $this->emailHelper->get($emailName,$params,$locale);//create email-body with params and locale
            $this->sendEmail(
                $this->Company->email,
                $reportEmail->title,
                $to,
                $reportEmail->subject,
                $reportEmail->body
            );
        }
    }

    //--------------------------------------------------------------
    private function createReportEmailParams($locale){
        //create params
        $params = array(
            "location"=>$this->reportData["location"]->name,
            "quiz"=>$this->reportData["quiz"]->name,
            "email"=>isset($this->reportData["report"]->email)?$this->reportData["report"]->email:"don`t set",
            "date"=>date($this->dateFormat,strtotime($this->reportData["report"]->date->iso)),
            "questions"=>array(),
        );

        foreach($this->reportData["answers"][$this->reportData["report"]->survey] as $answer){
            $questions = Utils::simplifyLocaleObjData($this->reportData["questions"],$locale,array("name"),"questionData");
            foreach($questions as $question){
                if($question["objectId"] == $answer->question){
                    $gradeTexts = $this->localeStrings->defaults->satisfiedValue[$answer->grade];
                    $params["questions"][$answer->priority] = array(
                        "name"=>$question["name"],
                        "grade"=>array("val"=>$gradeTexts->val,"text"=>$gradeTexts->text),
                    );
                }
            }
        }
        $qCount = count($params["questions"]);
        $tmp = array();
        for($i=0;$i<$qCount;$i++)$tmp[] = $params["questions"][$i];
        $params["questions"] = $tmp;

        //Email logo
        $params["logo"] = $this->getLocationLogo($this->reportData["location"]);

        return $params;
    }

    private function createRarePrizeEmailParams($locale){
        $prize = Utils::simplifyLocaleObjData(array($this->reportData["prize"]),$locale,array("name"),"prizeData");
        //create params
        $params = array(
            "location"=>$this->reportData["location"]->name,
            "email"=>$this->reportData["report"]->email,
            "prizeName"=>$prize[0]["name"],
            "date"=>date($this->dateFormat,strtotime($this->reportData["report"]->date->iso)),
        );

        //Email logo
        $params["logo"] = $this->getLocationLogo($this->reportData["location"]);

        return $params;
    }

    private function createPrizeEmailParams($locale){
        $prize = Utils::simplifyLocaleObjData(array($this->reportData["prize"]),$locale,array("name","note"),"prizeData");
        //create params
        $params = array(
            "location"=>array("name"=>$this->reportData["location"]->name),
            "prize"=>array(
                "name"=>$prize[0]["name"],
                "note"=>$prize[0]["note"],
            ),
            "date"=>date($this->dateFormat,strtotime($this->reportData["report"]->date->iso)),
        );

        //Email logo
        $params["logo"] = $this->getLocationLogo($this->reportData["location"]);

        return $params;
    }

    private function createCreditsExpireEmailParams($locale){

        //get company ---------------------------------------------------------------------
        $company = ParseLocation::get(ParseCompany::$parseEntityName,$this->queryData["companyId"]);
        if(!$company)$this->PageNotFoundException("location not found");
        $result["company"] = $company;

        //get company owner----------------------------------------------------------------
        $companyOwner = Parse_User::get($company->ownerId);
        if(!$companyOwner)$this->PageNotFoundException("location not found");
        $result["owner"] = $companyOwner;

        //Email logo
        $result["logo"] = $this->defaultEmailLogo;

        //get SuperAdminEmail -------------------------------------------------------------
        $result["superAdmin"] = $this->Company->superAdmin;
        $this->reportData["to"] = $result["owner"]->email;
        return array("email"=>$result["superAdmin"]["email"]);
    }

    //--------------------------------------------------------------//
    /**
     * Collect all report data for all email types
     * @return array
     */
    private function collectReportData(){

        //get report  ---------------------------------------------------------------------
        $report = ParseLocationReport::get(ParseLocationReport::$parseEntityName,$this->request->params["reportId"]);
        if(!$report)$this->PageNotFoundException();
        $result = array("report"=>$report);

        //get users ---------------------------------------------------------------------
        $result["report"]->emailList = $this->collectEmailList($report);

        //get answers ---------------------------------------------------------------------
        if(isset($report->answers[0])){
            foreach($report->answers as $v){
                $answersIdList[] = $v->objectId;
            }
        }
        if(!isset($answersIdList))$this->PageNotFoundException("answers not found");
        $answersList = ParseAnswer::find(array($report->survey=>$answersIdList));
        if(!$answersList)$this->PageNotFoundException("answers not found");
        $result["answers"] = $answersList;


        //get questions ---------------------------------------------------------------------
        if(isset($answersList[$report->survey][0])){
            foreach($answersList[$report->survey] as $answer){
                $questionsIdList[] = $answer->question;
            }
        }
        if(!isset($questionsIdList))$this->PageNotFoundException("questions not found");
        $questionsList = ParseQuestion::find(array("whereIn"=>array("objectId"=>$questionsIdList)),array("objectId","questionData"));
        if(!$questionsList)$this->PageNotFoundException("questions not found");
        $result["questions"] = $questionsList;


        //get location ---------------------------------------------------------------------
        $location = ParseLocation::get(ParseLocation::$parseEntityName,$report->location);
        if(!$location)$this->PageNotFoundException("location not found");
        $result["location"] = $location;


        //get quiz  ---------------------------------------------------------------------
        $quiz = ParseSurvey::get(ParseSurvey::$parseEntityName,$report->survey);
        if(!$quiz)$this->PageNotFoundException("quiz not found");
        $result["quiz"] = $quiz;


        //get prize ---------------------------------------------------------------------
        if(isset($report->prizeId)){
            $prize = ParsePrize::get(ParsePrize::$parseEntityName,$report->prizeId);
            if(!$prize)$this->PageNotFoundException("prize not found");
            $result["prize"] =  $prize;
        }


        //---------------------------------------------------------------------
        return $result;
    }

    //collect emails receivers
    private function collectEmailList($report){
        $emailList = array();

        if(isset($report->emailList)){//use email with default locale : app v1 capability
            foreach($report->emailList as $email){
                $emailList[$email] = $this->Company->default->lang;
            }
        }

        else{//use user list - load users, extract user-email and user-locales
            $userList = ParseEntity::parseGetQuery(
                Parse_User::$parseEntityName,
                array(
                    "whereContainedIn"=>array(
                        "objectId"=>$report->userList,
                    ),
                    "where"=>array(
                        "status"=>true
                    )
                ),
                array(
                    "email","defaultLocale"
                )
            );

            if($userList){
                foreach($userList as $user){
                    $emailList[$user["email"]] = ($user["defaultLocale"])?$user["defaultLocale"]:$this->Company->default->lang;
                }
            }
        }
        return $emailList;
    }

    private function getLocationLogo($location){
        $tmp = null;
        if(isset($location->logo)){
            $tmp = ParseImage::getUrl($this->reportData["location"]->logo);
        }
        return ($tmp)?$tmp:$this->defaultEmailLogo;

    }

}