<?php
class ParseController extends AppController {
    public $name = 'Parse';
    public $uses = array();

    public function parse_get_user_list() {
        Utils::_print(Parse_User::parseGetQuery(Parse_User::$parseEntityName,array("where"=>array("company"=>$this->request->params["companyId"]))));
    }

    public function parse_get_answer_list() {
        $tmp = ParseEntity::parseGetQuery(ParseAnswer::$parseEntityName);
        Utils::apiRender(200,"success",count($tmp));
    }

    public function parse_refresh_session(){
        Utils::apiRender(200,"success",SesHelper::status());
    }
}