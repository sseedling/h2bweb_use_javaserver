<?php //opusBoom
class LocationsController extends AppController{
    public $name = 'Locations';
    public $uses = array();
    private $companyObj = null;


    public function beforeRender(){
        $this->set("sectionName","locations");
        parent::beforeRender();
    }

    public function locations_all(){
        $foundedLocations = ParseLocation::find(array("where"=>array("company"=>$this->Company->parseObj->objectId)));
        $this->set("locations",$foundedLocations);
    }

    public function locations_view() {
        $location = $this->getLocation();

        $usedQuizzes = $this->getQuizzes($location);
        $this->set("surveys",ParseEntity::getCount("Question","survey",$usedQuizzes));

        $quizzAvailableList = $this->getQuizzAvailableList($location);
        $this->localeStrings->page->title = $location->name;
        $this->set("location",$location);
        $this->set("quizzAvailableList",$quizzAvailableList);
    }

    private function getQuizzAvailableList($location) {
        $conditions = array("where"=>array("company"=>$this->Company->parseObj->objectId));
        if(isset($location->quizzes[0])){
            foreach($location->quizzes as $quizz){
                $quizzList[]= $quizz->objectId;
            }
            $conditions["whereNotContainedIn"] = array("objectId"=>$quizzList);
        }
        return ParseEntity::parseGetQuery("Survey",$conditions);
    }

    public function locations_add() {
        //Validation
        if(ParseLocation::validate(@$this->queryData["location"])){
            //Save location
            $location = new ParseLocation();
            $location->companyObj = $this->Company;
            if($location->save()){
                //Save attraction-img
                $imgId = $this->saveUserImgOrDefault("screen","attraction");

                //Save and attach logo-img
                $logoId = $this->saveUserImgOrDefault("logo","logo");
                $location->addLogo($logoId);

                //Add banner to location
                if(isset($_FILES["banners"])){
                    $bannersIdList = ParseImage::saveAllImg($_FILES["banners"]);
                    $location->addNewBanners($location->objectId,$bannersIdList);
                }

                //Save attraction texts
                $attraction = new ParseAttraction();
                $attraction->save($this->queryData["attraction"],$location->objectId,$imgId,$this->localeStrings->defaults->attraction);

                //add location to owner location list
                Parse_User::shareLocation($this->ParseUSER->objectId,$location->objectId);
                return $this->redirectSafe("/locations/".$location->urlName."/");
            }
        }

        //Create new|error location
        $this->queryData["imageExist"] = (isset($_FILES["screen"]["error"]) && ($_FILES["screen"]["error"]!=4));
        $this->queryData["location"] = ParseLocation::$postData;
        if(!isset($this->queryData["defaultLanguage"]))$this->queryData["defaultLanguage"] = $this->localeName;
        $this->set("postData",$this->queryData);
        $this->set("attractionScreen",$this->Company->default->path->attraction);
        $this->set("locationLogo",$this->Company->default->path->logo);

    }

    public function api_locations_drop_banner(){
        ParseLocation::removeBanner($this->request->params["locationId"],$this->request->params["bannerId"]);
        ParseImage::dropById($this->request->params["bannerId"]);
        Utils::apiRender();
    }

    public function locations_edit(){
        $location = $this->getLocation();
        $curLocationUrl = $location->urlName;
        if(isset($_FILES["banners"]))ParseLocation::addNewBanners($location->objectId,ParseImage::saveAllImg($_FILES["banners"]));

        //Validation
        if((isset($this->queryData["location"]))&&(ParseLocation::validate($this->queryData["location"]))){
            if($location->update()){
                //Save attraction-img
                if((isset($_FILES["screen"]))&&($_FILES["screen"]["error"]==0)) {
                    $imgId =  ParseImage::saveOneImg($_FILES["screen"]["tmp_name"],$_FILES["screen"]["type"]);
                }
                else $imgId = null;

                //Save logo-img
                $this->saveLogo($location);

                //Save attraction texts
                $attractionsParse = new ParseAttraction();
                $attractionsParse->update($this->queryData["attraction"],$location->objectId,$imgId,$this->localeStrings->defaults->attraction);

                $this->redirectSafe("/locations/".$location->urlName."/");
            }
            $location->urlName = $curLocationUrl;
        }

        //----------------------------
        if(!isset($this->queryData["defaultLanguage"]))$this->queryData["defaultLanguage"] = $this->localeName;
        $this->queryData["imageExist"] = (isset($_FILES["screen"]["error"]) && ($_FILES["screen"]["error"]!=4));
        $this->queryData["location"] = ParseLocation::$postData;

        //get attraction----------------------------
        $attraction = $this->getAttraction($location->objectId,$this->queryData);
        $attractionScreen = $this->getAttractionScreen($attraction);
        //----------------------------
        $this->set("banners",$this->getBanners(@$location->banners));
        $this->set("location",$location);
        $this->set("attractionScreen",$attractionScreen);
        $this->set("attraction",$attraction);
        $this->set("logoImg",$this->getLogo($location));
        $this->set("postData",$this->queryData);
    }
    //========================================================
    private function getLocation(){
        $location = new ParseLocation();
        $location->companyObj = $this->Company;
        if(!$location->load($this->Company->parseObj->objectId,$this->request->params["location"]))
            $this->PageNotFoundException();
        return $location;
    }

    private function getQuizzes($location){
        if(!isset($location->quizzes[0]))return null;
        foreach($location->quizzes as $quizz){
            $quizzList[]= $quizz->objectId;
        }
        return ParseEntity::parseGetQuery("Survey",array("whereContainedIn"=>array("objectId"=>$quizzList)));
    }

    private function getAttraction($locationId,$postData){
        $pars = new ParseAttraction();
        $attractions = $pars->load($locationId);
        if(!isset($attractions[0])){//if no exist attraction -> create new empty
            $pars->save(null,$locationId,null,$this->localeStrings->defaults->attraction);
            $attractions = $pars->load($locationId);
        }
        if(isset($postData["attraction"])){
            $attractions[0]->title = $postData["attraction"]["title"];
            $attractions[0]->text = $postData["attraction"]["text"];
        }
        return $attractions[0];
    }

    private function getBanners($bannersIdList){
        $resultList = array();
        if(!$bannersIdList)return $resultList;
        $pars = new ParseImage();
        $banners = $pars->find("all",array("whereIn"=>array("objectId"=>$bannersIdList)));
        if(!$banners)return $resultList;
        foreach($banners as $banner){
            $resultList[] = array("objectId"=>$banner->objectId,"url"=>$banner->imgData->url,"name"=>substr($banner->imgData->name,37));
        }
        return $resultList;
    }

    private function getAttractionScreen($attraction){
        if(!isset($attraction->image))return null;
        else return ParseImage::get("Image",$attraction->image);
    }

    private function getLogo($location){
        if(!isset($location->logo))return null;
        else return ParseImage::get("Image",$location->logo);
    }

    private function saveLogo($location){
        if((isset($_FILES["logo"]))&&($_FILES["logo"]["error"]==0)){
            $logoId =  ParseImage::saveOneImg($_FILES["logo"]["tmp_name"],$_FILES["logo"]["type"]);
            if((isset($location->logo))&&($location->logo))ParseImage::dropById($location->logo);
            $location->addLogo($logoId);
        }
    }

    private function saveUserImgOrDefault($name,$key){
        if(isset($_FILES[$name])&&(is_file($_FILES[$name]["tmp_name"]))){
            $imgId =  ParseImage::saveOneImg($_FILES[$name]["tmp_name"],$_FILES[$name]["type"]);
        }
        else {
            $imgId =  ParseImage::saveOneImg($this->Company->default->path->$key);
        }
        return $imgId;
    }

}
