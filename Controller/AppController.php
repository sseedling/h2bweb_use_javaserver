<?php

class AppController extends Controller {
    var $curSubPage="";
    var $_ERROR = false;
    var $sharedActions = array("login");
    var $Company = null;
    var $localeName = null;
    var $appLocales = null;
    var $localeStrings = null;
    var $ACL = null;
    var $ParseUSER = null;
    var $queryData = null;

    function beforeFilter(){
        LogHelper::write(array('=================INIT===============','init'.SesHelper::toString(),"sessionId: ".session_id(),"action: ".$this->action));

        //Load all roles in system
        ConfigHelper::initRoles();

        //Load ALL localised texts
        LocaleStrings::__init();

        //Load ALL app-locale
        AppLocale::__init();

        //Load required roles for this query
        if(isset($this->request->params["acl"]))
            $this->ACL = $this->request->params["acl"];

        //Load company info
        $this->Company = ConfigHelper::initCompany();

        //Load query params
        $this->getRequestData();

        //Check for redirect required
        $this->checkPrevUrl();

        //select locale for load
        $this->prepareLocaleName();

        //Load cur locale
        $this->loadLocale();

        //Check user roles
        $this->checkAuth();

        //correct loadedLocale
        $this->correctLoadedLocale();

        //Load user company details
        $this->loadParseCompany();
    }

    protected function getRequestData(){
        $this->queryData = !empty($this->data)?$this->data:$this->request->query;
        /* todo check for correct work */
    }

    function beforeRender(){
        $this->createVersionKey();
        if(SesHelper::check("ErrorPage")){
            $errorCode = SesHelper::read("ErrorPage");
            $this->localeStrings = LocaleStrings::get($this->localeName,"error".$errorCode);
            SesHelper::delete("ErrorPage");
        }
        $this->set("locales",array("allLang"=>LocaleStrings::getAllLang(),"selected"=>$this->localeName));
        $this->set("appLocales",$this->appLocales);
        $this->set("LocaleStrings",$this->localeStrings);
        $this->set("subPage",$this->curSubPage);
        $this->set("_ERROR",$this->_ERROR);
        $this->set("User",$this->ParseUSER);
        $this->set("Company",$this->Company);
        SesHelper::saveSession();
    }

    function createVersionKey(){
        if(!defined("VERSION_RND_KEY"))
            define("VERSION_RND_KEY",$this->Company->version);
    }

    private function loadParseCompany(){
        //load Super Admin company
        $superAdminCompany = ParseEntity::parseGetQuery(ParseCompany::$parseEntityName,array("where"=>array("name"=>$this->Company->base),"setLimit"=>1));
        if(isset($superAdminCompany[0]))$this->Company->superAdmin = Parse_User::get($superAdminCompany[0]->ownerId,array("email"));

        if((!Auth::isLogin())||($this->action=="logout")){
            LogHelper::write('loadParseCompany : Authorize:false || logout:action=='.$this->action);
            return;
        }
        $this->Company->parseObj = ParseEntity::get("Company",Auth::getUser()->company);
        if(!$this->Company->parseObj)$this->PageNotFoundException();
    }

    private function checkAuth(){
        //запоминание урла который хотел посетить пользователь не будучи авторизованым
        if((!Auth::isLogin())&&($this->ACL)){
            LogHelper::write('checkAuth->isLogin->false->redirect->login : '.SesHelper::toString());
            //todo Является ли этот URL view акшеном
            SesHelper::write("prevUrl",$this->here);
            SesHelper::write("prevUrlData",$this->queryData);
            $this->redirectSafe("/login/");
        }

        $this->ParseUSER = Auth::getUser();
        if(isset($this->ParseUSER->locationList)){
            ParseLocation::$avalibleLocations = $this->ParseUSER->locationList;
        }

        //Если на страницу указаны права, то проверить роли пользователя
        if(($this->ACL)&&(!Auth::inRoles($this->ACL))){
            if((!isset($this->ParseUSER->status))||(!$this->ParseUSER->status)){
                LogHelper::write('checkAuth->isLogin->true->roles->false->logout : '.SesHelper::toString());
                $this->redirectSafe("/logout/");
            }
            else $this->PermissionDeniedException();
        }
        //auth passed go next
    }

    private function checkPrevUrl(){
        if(SesHelper::check("prevUrl")){
            $prevUrl = SesHelper::read("prevUrl");
            if(($this->action!="login")&&(Auth::isLogin())){
                LogHelper::write('checkPrevUrl->isLogin->true->redirect-to-to-prev-url : '.SesHelper::toString());
                SesHelper::delete("prevUrl");
                $this->redirectSafe($prevUrl);
            }
        }
    }

    /**
     * Detect display locale
     *
     */
    private function prepareLocaleName(){
        if(!SesHelper::check("locale")){
            SesHelper::write("locale",$this->Company->default->lang);
        }

        $this->localeName = SesHelper::read("locale");

        if((isset($this->queryData["locale"]))&&(in_array($this->queryData["locale"],LocaleStrings::getLocalesNames()))){
            SesHelper::write("locale_change",true);
            $this->localeName = $this->queryData["locale"];
        }

        if(!in_array($this->localeName,LocaleStrings::getLocalesNames())){
            $this->localeName = $this->Company->default->lang;
        }
    }

    /**
     * Load localised texts
     */
    private function loadLocale(){
        //Save final selected locale
        SesHelper::write("locale",$this->localeName);

        //get localise str
        $this->localeStrings = LocaleStrings::get($this->localeName,$this->action);
        ParseEntity::$localeStrings = $this->localeStrings;

        //Load app  locale
        $this->appLocales = AppLocale::getAllLang();
        ParseEntity::$appLocales = $this->appLocales;
    }

    /**
     * Change cur locale with user profile context
     *
     */
    private function correctLoadedLocale(){
        if(!$this->ParseUSER){
            SesHelper::write("locale_change",false);
        }

        if(!SesHelper::read("locale_change")){//User don`t change locale manually
            if(isset($this->ParseUSER->defaultLocale)){//User selected default_user_locale
                if($this->ParseUSER->defaultLocale != $this->localeName){//cur_loaded_locale is different than default_user_locale
                    if(in_array($this->ParseUSER->defaultLocale,LocaleStrings::getLocalesNames())){//default_user_locale steel available
                        //All ok - do switch
                        $this->localeName = $this->ParseUSER->defaultLocale;//change cur locale to default_user_locale
                        $this->loadLocale();//load locales for new cur locale
                    }
                }
            }
        }

        if(!isset($this->ParseUSER->defaultLocale)){
            $this->ParseUSER->defaultLocale = "undefined";
        }
    }

    public function PageNotFoundException($mode="page") {
        switch($mode){
            case "api":{
                Utils::apiRender(404,"page not found");
                break;
            }
            case "page":
            default:{
                SesHelper::write("ErrorPage","404");
                SesHelper::write("ErrorCode","404");

                $localeStrings = LocaleStrings::get($this->localeName,"error404");

                SesHelper::write("mainPage",$localeStrings->page->mainPage);
                SesHelper::write("ErrorBodyTitle",$localeStrings->page->title);
                SesHelper::write("ErrorBodyText",$localeStrings->page->meta->description);

                throw new NotFoundException();
            }
        }
    }

    public function PermissionDeniedException($mode="page"){
        switch($mode){
            case "api":{
                Utils::apiRender(403,"permission denied");
                break;
            }
            case "page":
            default:{
                SesHelper::write("ErrorPage","403");
                SesHelper::write("ErrorCode","403");

                $localeStrings = LocaleStrings::get($this->localeName,"error403");

                SesHelper::write("mainPage",$localeStrings->page->mainPage);
                SesHelper::write("ErrorBodyTitle",$localeStrings->page->title);
                SesHelper::write("ErrorBodyText",$localeStrings->page->meta->description);

                throw new ForbiddenException();
            }
        }
    }

    public function loadModels($modelItem){
        if(is_string($modelItem)){
            $this->tryLoadModel($modelItem);
        }
        else{
            foreach($modelItem as $model){
                $this->tryLoadModel($model);
            }
        }
    }

    private function tryLoadModel($model){
        try{ $this->loadModel($model); }
        catch(Exception $e){ $this->PageNotFoundException();}
    }


    protected function apiValidation(){
        //Check for validation turn-on
        if(!$this->Company->useApiValidation)return;

        //check request auth
        if(!isset($this->queryData["authKey"]))$this->PermissionDeniedException("api");
        if(!isset($this->queryData["hashKey"]))$this->PermissionDeniedException("api");

        $checkHashCode = md5($this->queryData["authKey"].$this->Company->apiValidateKey);

        if($this->queryData["hashKey"]!=$checkHashCode)$this->PermissionDeniedException("api");

        //all ok
    }

    protected function redirectSafe($url,$status=200,$exit=true){
        SesHelper::close();
        $this->redirect($url,$status,$exit);
    }
}


