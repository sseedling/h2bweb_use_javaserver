<?php

class ReportsEmotionChartController extends AppController {
    public $name = 'ReportsEmotionChart';
    public $uses = array();

    public function beforeFilter(){
        parent::beforeFilter();
        $this->set("sectionName","reports");
    }

    /*------------------ACTIONS------------------------------------*/

    /**
     * PAGE :: Print large report table
     * params:
     *      @locationsIdList : locations objectId list,
     *      @quizzId : quizz objectId
     *      @minDate : start date interval
     *      @maxDate : end date interval
     */
    public function reports_emotion_details(){
        $chartHelper = new ChartHelper($this->queryData,$this->localeName);

        $reportsList = $chartHelper->getReportList();
        $reports = $chartHelper->makeDetailReportData($reportsList,"emotion");

        $this->set("reports",$reports);
        $this->set("quizzList",$chartHelper->getQuizzList());
        $this->set("locationObjList",$chartHelper->getLocations($reportsList));
        $this->set("questionObjList",ChartHelper::getQuestions(array("objList"=>$reports,"mode"=>"table")));
        $this->render(DS."Reports".DS.$this->action);
    }

    /**
     * API :: Collect from parse emotion images urls for current company
     * return:{data:{grade:[url-g1,url-g2,...],priority:[url-p1,...],background:[url-b1,...]}}
     */
    public function reports_emotion_img_list(){
        $parseImages = new ParseImage();
        Utils::apiRender(200,"success",$parseImages->findEmotionImgList($this->Company->parseObj->objectId));
    }

    /**
     * API :: Collect from parse data for create-chart on web-page
     * params:
     *      @locationsIdList : locations objectId list,
     *      @quizzId : quizz objectId
     *      @minDate : start date interval
     *      @maxDate : end date interval
     */
    public function reports_emotion_chart_collect(){
        $chartHelper = new ChartHelper($this->queryData,$this->localeName);
        $reportsList = $chartHelper->getReportList();
        $report = $chartHelper->makeReportData($reportsList,"emotion");
        $result = $chartHelper->call("formatReport",array("report"=>$report,"queryData"=>$this->queryData,"reportsList"=>$reportsList),"emotion");
        Utils::apiRender(200,"success",$result);
    }
}
