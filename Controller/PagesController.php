<?php
class PagesController extends AppController {
    public $name = 'Pages';
    public $uses = array();

    public function home() {//display all projects
        $this->set("sectionName","home");
        //при попытке посетить "/" проверяем указана ли у пользователя другой home и редиректнуть туда если да
        $role = Auth::getHighLevelUserRole();
        if(isset($role["home"]))$this->redirectSafe($role["home"]);
        else $this->PermissionDeniedException();
    }

    //input params - date range, search firld
    public function winners(){
        $this->set("sectionName","winners");
        $reportObj = new ParseLocationReport();
        $reportObj->companyId = $this->Company->parseObj->objectId;
        $reportObj->localeName = $this->localeName;
        $reports = $reportObj->findWinners($this->queryData);
        $this->set("reports",$reports);
    }

    public function design() {
        $this->set("sectionName","design");

    }

    public function login(){
        $this->set("sectionName","login");
        if((isset($this->queryData["login"]))&&(isset($this->queryData["password"]))){
            if(Auth::login($this->queryData["login"],$this->queryData["password"])){
                $this->redirectSafe("/");
            }
            else $this->set("errorMsg",Auth::getError());
        }
        else{
            if(Auth::isLogin())$this->redirectSafe("/");
        }
        $this->set("login",(!isset($this->queryData["login"]))?"":$this->queryData["login"]);
    }

    public function logout(){
        Auth::logout();
        $this->redirectSafe("/");
    }

    public function error404(){//error pages
        $this->PageNotFoundException();
    }

}
