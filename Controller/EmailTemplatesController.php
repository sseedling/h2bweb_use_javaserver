<?php
class EmailTemplatesController extends AppController {
    public $name = 'EmailTemplates';
    public $uses = array();

    private $defaultEmailLogo = null;
    private $dateFormat = "d F Y";
    private $emailData = null;
    private $emailHelper = null;
    private $emailLocale = null;
    private $pageNameForLocaleString = "undefined";
    private $emailHeader = array("first"=>'<html><meta http-equiv="Content-Type" content="text/html; charset=utf-8"><body>');

    public function beforeFilter() {
        parent::beforeFilter();

        $this->defaultEmailLogo = $this->Company->serverName.DS.$this->Company->default->path->logo;
        $this->emailLocale = $this->request->params["locale"];
        $this->prepareLocaleEmail();
    }

    private function prepareLocaleEmail(){
        $this->emailHelper = new EmailHelper();
        $localeList = LocaleStrings::getLocalesNames();
        foreach($localeList as $locale){
            $tmp = LocaleStrings::get($locale,$this->pageNameForLocaleString);
            $this->emailHelper->setStrings($tmp->emails,$locale);
        }
    }

    /**
     * Create new-report email
     */
    public function emails_view_report_alert(){
        $this->emailData = $this->collectReportData();

        $params = $this->createReportEmailParams($this->emailLocale);//collect params with locale
        $this->emailHelper->setHtmlWrapper($this->emailHeader);
        $reportEmail = $this->emailHelper->get("report",$params,$this->emailLocale);//create email-body with params and locale
        Utils::htmlRender(200,$reportEmail->body);
    }

    /**
     * Create new-report email
     */
    public function emails_view_credits_expire(){

        Utils::apiRender();
    }

    /**
     * Create new-report email
     */
    public function emails_view_rare_prize(){
        //get prize ---------------------------------------------------------------------
        $this->emailData = ParsePrize::get(ParsePrize::$parseEntityName,$this->request->params["prizeId"]);
        if(!$this->emailData)$this->PageNotFoundException("prize not found");

        $params = $this->createRarePrizeEmailParams($this->emailLocale);//collect params with locale

        $this->emailHelper->setHtmlWrapper($this->emailHeader);
        $reportEmail = $this->emailHelper->get("rarePrize",$params,$this->emailLocale);//create email-body with params and locale
        Utils::htmlRender(200,$reportEmail->body);
    }

    /**
     * Create new-report email
     */
    public function emails_view_win_a_prize(){
        //get prize ---------------------------------------------------------------------
        $this->emailData = ParsePrize::get(ParsePrize::$parseEntityName,$this->request->params["prizeId"]);
        if(!$this->emailData)$this->PageNotFoundException("prize not found");

        $params = $this->createPrizeEmailParams($this->emailLocale);//collect params with locales

        $this->emailHelper->setHtmlWrapper($this->emailHeader);
        $reportEmail = $this->emailHelper->get("prize",$params,$this->emailLocale);//create email-body with params and locale
        Utils::htmlRender(200,$reportEmail->body);
    }


    //--------------------------------------------------------------//
    /**
     * Collect all report data for all email types
     * @return array
     */
    private function collectReportData(){

        //get report  ---------------------------------------------------------------------
        $report = ParseLocationReport::get(ParseLocationReport::$parseEntityName,$this->request->params["reportId"]);
        if(!$report)$this->PageNotFoundException();
        $result = array("report"=>$report);

        //get answers ---------------------------------------------------------------------
        if(isset($report->answers[0])){
            foreach($report->answers as $v){
                $answersIdList[] = $v->objectId;
            }
        }
        if(!isset($answersIdList))$this->PageNotFoundException("answers not found");
        $answersList = ParseAnswer::find(array($report->survey=>$answersIdList));
        if(!$answersList)$this->PageNotFoundException("answers not found");
        $result["answers"] = $answersList;


        //get questions ---------------------------------------------------------------------
        if(isset($answersList[$report->survey][0])){
            foreach($answersList[$report->survey] as $answer){
                $questionsIdList[] = $answer->question;
            }
        }
        if(!isset($questionsIdList))$this->PageNotFoundException("questions not found");
        $questionsList = ParseQuestion::find(array("whereIn"=>array("objectId"=>$questionsIdList)),array("objectId","questionData"));
        if(!$questionsList)$this->PageNotFoundException("questions not found");
        $result["questions"] = $questionsList;


        //get location ---------------------------------------------------------------------
        $location = ParseLocation::get(ParseLocation::$parseEntityName,$report->location);
        if(!$location)$this->PageNotFoundException("location not found");
        $result["location"] = $location;

        //get quiz  ---------------------------------------------------------------------
        $quiz = ParseSurvey::get(ParseSurvey::$parseEntityName,$report->survey);
        if(!$quiz)$this->PageNotFoundException("quiz not found");
        $result["quiz"] = $quiz;

        //---------------------------------------------------------------------
        return $result;
    }

    /**
     * Create Rare email params
     * @param $locale
     * @return array
     *
     */
    private function createRarePrizeEmailParams($locale){
        $prize = Utils::simplifyLocaleObjData(array($this->emailData),$locale,array("name"),"prizeData");
        //create params
        $params = array(
            "location"=>"Test location name",
            "email"=>"testEmail@email.com",
            "prizeName"=>$prize[0]["name"],
            "date"=>date($this->dateFormat,time()),
        );

        //Email logo
        $params["logo"] = $this->getLocationLogo(null);

        //Email image-server
        $params["server"] = $this->Company->serverName;

        return $params;
    }

    /**
     * Create Email params
     * @param $locale
     * @return array
     */
    private function createPrizeEmailParams($locale){
        $prize = Utils::simplifyLocaleObjData(array($this->emailData),$locale,array("name","note"),"prizeData");
        //create params
        $params = array(
            "location"=>array("name"=>"Test location name"),
            "prize"=>array(
                "name"=>$prize[0]["name"],
                "note"=>$prize[0]["note"],
            ),
            "date"=>date($this->dateFormat,time()),
        );

        //Email logo
        $params["logo"] = $this->getLocationLogo(null);

        //Email image-server
        $params["server"] = $this->Company->serverName;

        return $params;
    }

    //--------------------------------------------------------------
    private function createReportEmailParams($locale){
        //create params
        $params = array(
            "location"=>$this->emailData["location"]->name,
            "quiz"=>$this->emailData["quiz"]->name,
            "email"=>isset($this->emailData["report"]->email)?$this->emailData["report"]->email:"don`t set",
            "date"=>date($this->dateFormat,strtotime($this->emailData["report"]->date->iso)),
            "questions"=>array(),
        );

        foreach($this->emailData["answers"][$this->emailData["report"]->survey] as $answer){
            $questions = Utils::simplifyLocaleObjData($this->emailData["questions"],$locale,array("name"),"questionData");
            foreach($questions as $question){
                if($question["objectId"] == $answer->question){
                    $gradeTexts = $this->localeStrings->defaults->satisfiedValue[$answer->grade];
                    $params["questions"][$answer->priority] = array(
                        "name"=>$question["name"],
                        "grade"=>array("val"=>$gradeTexts->val,"text"=>$gradeTexts->text),
                    );
                }
            }
        }
        $qCount = count($params["questions"]);
        $tmp = array();
        for($i=0;$i<$qCount;$i++)$tmp[] = $params["questions"][$i];
        $params["questions"] = $tmp;

        //Email logo
        $params["logo"] = $this->getLocationLogo($this->emailData["location"]);

        //Email image-server
        $params["server"] = $this->Company->serverName;

        return $params;
    }

    private function getLocationLogo($location){
        $tmp = null;
        if(isset($location->logo)){
            $tmp = ParseImage::getUrl($this->emailData["location"]->logo);
        }
        return ($tmp)?$tmp:$this->defaultEmailLogo;

    }

}