require_relative 'env.rb'

Before do |scenario| 
  if scenario.source_tag_names.include?('@no_browser') and Capybara.default_driver != :mechanize 
      Capybara.default_driver = :mechanize 
  elsif !scenario.source_tag_names.include?('@no_browser') and Capybara.default_driver != :selenium 
      Capybara.default_driver = :selenium 
  end 
end 

#---------------------------------------------------------------------------------

Before ('@new_user') do |scenario|
  $company = Company.new
  $company.create()
  $defaultUser = {"username" => $company.owner.username, "password" =>$company.owner.password}
end

After ('@new_user') do
  $company.delete()
end

#---------------------------------------------------------------------------------

Before ('@company_without_location') do |scenario|
  $company = Company.new
  $company.create()
  $defaultUser = {"username" => $company.owner.username, "password" =>$company.owner.password}
end

After ('@company_without_location') do
  $company.delete()
end

#---------------------------------------------------------------------------------

Before ('@company_with_location') do |scenario|
  $company = Company.new
  $company.create()
  $company.create_location();
  $defaultUser = {"username" => $company.owner.username, "password" =>$company.owner.password}
end

After ('@company_with_location') do
  $company.delete()
end

#---------------------------------------------------------------------------------

Before ('@user_without_permissions') do |scenario|
  $company = Company.new
  $company.create()
  no_permission_acl = {Authorize:{read: true}}  
  $company.owner.update(:ACL => no_permission_acl)
  $defaultUser = {"username" => $company.owner.username, "password" =>$company.owner.password}
end

After ('@user_without_permissions') do
  $company.delete()
end

#---------------------------------------------------------------------------------

Before ('@user_with_readonly_permissions') do |scenario|
  $company = Company.new
  $company.create()
  readonly_permission_acl = {:QuizzesR => {read: true},:ReportsR => {read: true},:PrizesR => {read: true},:Authorize => {read: true}}
  $company.owner.update(:ACL => readonly_permission_acl)
  $defaultUser = {"username" => $company.owner.username, "password" =>$company.owner.password}
end

After ('@user_with_readonly_permissions') do
  $company.delete()
end

#---------------------------------------------------------------------------------


Before ('@disabled_user') do |scenario|
  $company = Company.new
  $company.create()
  no_permission_acl = {Authorize:{read: true}}  
  $company.owner.update(status: false)
  $defaultUser = {"username" => $company.owner.username, "password" =>$company.owner.password}
end

After ('@disabled_user') do
  $company.delete()
end

#---------------------------------------------------------------------------------

Before ('@company_with_quizzes') do |scenario|
  $company = Company.new
  $company.create()
  $company.create_location();
  $company.locations.first.create_quizz()
  $defaultUser = {"username" => $company.owner.username, "password" =>$company.owner.password}
end

After ('@company_without_quizzes') do
  $company.delete()
end

#---------------------------------------------------------------------------------

Before ('@company_without_quizzes') do |scenario|
  $company = Company.new
  $company.create()
  $company.create_location();
  $defaultUser = {"username" => $company.owner.username, "password" =>$company.owner.password}
end

After ('@company_without_quizzes') do
  $company.delete()
end


# #---------------------------------------------------------------------------------

# Before ('@new_user_without_survey') do |scenario|
#   $company = RandomCompany.new
#   $company.create()
#   $user=RandomOwnerUser.new($company.id)
#   $user.create()
#   $location = RandomLocation.new(:company=>$company.id,:user=>$user.id)
#   $location.create()
#   $location.createDefaultAttraction()
#   $user.setLocations($location)
#   $defaultUser = {"username" => $user.username, "password" =>$user.password}
# end

#  After ('@new_user_without_survey') do
#   $company.clear()
#   $user.delete()
#  end

# #---------------------------------------------------------------------------------

# Before ('@new_user_without_locations') do |scenario|
#   $company = RandomCompany.new
#   $company.create()
#   $user=RandomOwnerUser.new($company.id)
#   $user.create()
#   $defaultUser = {"username" => $user.username, "password" =>$user.password}
# end

#  After ('@new_user_without_locations') do
#   $company.clear()
#   $user.delete()
#  end

#--------------------------------------------------------------------------------- 

Before ('@unregistered_user') do
  $defaultUser = {"username" => "unregistered", "password" => "password"}
end

After ('@unregistered_user') do
end


#---------------------------------------------------------------------------------

After do |scenario|
  Capybara.default_driver = :selenium 
	visit "/logout"
    if scenario.failed? 
        puts scenario.name
        puts scenario.exception.message        
    end 
end
