Given /^I am on the User's page$/ do
  steps %{
    Given I logged in
    When I visit "/users"
  }
end

#---------------------------------------------------------------------------------

Given /^I click Add New User$/ do
  click_link('Add user')
end

#---------------------------------------------------------------------------------

Given /^I create New User$/ do
  click_link('Add user')
  $randomCreatedUserName = "User" + rand(1234123).to_s
  $randomUserEmail = "TestUser" + rand(999999).to_s + "@test.ts"
  fill_in 'publicName', :with => "#{$randomCreatedUserName}"
  fill_in 'email', :with => "#{$randomUserEmail}"
  fill_in 'password', :with => "1234567890"
end

#---------------------------------------------------------------------------------

Given /^User should see red error indication/ do
  click_link('Add user')
    steps %{
      And I wait 2 seconds
     }
  page.execute_script("$('#pageMenu .btn.btn-short.saveBtn').click()")
  page.should have_css('.text-field.error', :count =>3)
end

#---------------------------------------------------------------------------------

Given /^I create new user without saving$/ do
  click_link('Add user')
  $randomCreatedUserName = "User" + rand(1234123).to_s
  $randomUserEmail = "TestUser" + rand(999999).to_s + "@test.ts"
  fill_in 'publicName', :with => "#{$randomCreatedUserName}"
  fill_in 'email', :with => "#{$randomUserEmail}"
  fill_in 'password', :with => "1234567890"
end

#---------------------------------------------------------------------------------

Given /^I check that email is not exist$/ do
  page.execute_script("$('#middleContent .btn-block.btn-big.btnGradient.check-email').click()")
    steps %{
      And I wait 5 seconds
    }
  page.should have_css('.text-field.success')
end

#---------------------------------------------------------------------------------

Given /^I check that email exist$/ do
  click_link('Add user')
  $randomCreatedUserName = "User" + rand(1234123).to_s
  $randomUserEmail = "TestUser" + rand(999999).to_s + "@test.ts"
  fill_in 'publicName', :with => "#{$randomCreatedUserName}"
  fill_in 'email', :with => $company.owner.username
  fill_in 'password', :with => "1234567890"
  page.execute_script("$('#middleContent .btn-block.btn-big.btnGradient.check-email').click()")
  steps %{
      And I wait 5 seconds
    }
  page.should have_css('.text-field.error')
end

#---------------------------------------------------------------------------------

Given /^I check my password by key button tap$/ do
  find(:xpath, '//*[@id="middleContent"]/form/div[3]/div[2]/div[2]/div').click
  page.should have_css('.ico-lock.active')
  find_field('password').value.should eq "1234567890"
end

#---------------------------------------------------------------------------------

Given /^I set all user permission$/ do
  find(:xpath, '//*[@id="role-Quizzes"]/div[1]').click
  find(:xpath, '//*[@id="role-Reports"]/div[1]').click
  find(:xpath, '//*[@id="role-Prizes"]/div[1]').click
  page.should have_css('.icon.icon-checkbox', :count =>4)
end

#---------------------------------------------------------------------------------

Given /^I set user's permission for Reports$/ do
  find(:xpath, '//*[@id="role-Reports"]/div[1]').click
  page.should have_css('.icon.icon-checkbox', :count =>2)
end

#---------------------------------------------------------------------------------

Given /^I check user's permission for RW Quizzes$/ do
  within(:xpath, '//*[@id="role-Quizzes"]') do
    find(:xpath, "//*[@class=\"switch-box publish active\" and @title=\"Write\"]")
  end
  page.should have_css('.icon.icon-checkbox', :count =>2)
end

#---------------------------------------------------------------------------------

Given /^I set user's permission for Quizzes in RW mode$/ do
  find(:xpath, '//*[@id="role-Quizzes"]/div[1]').click
end

#---------------------------------------------------------------------------------

Given /^I set permission for Quizzes in R mode$/ do
  find(:xpath, '//*[@id="role-Quizzes"]/div[1]').click
  find(:xpath, '//*[@id="role-Quizzes"]/div[3]').click
end

#---------------------------------------------------------------------------------

Given /^I check user's permissions for Reading quizzes$/ do
  within(:xpath, '//*[@id="role-Quizzes"]') do
    find(:xpath, "//*[@class=\"switch-box publish\" and @title=\"Read\"]")
  end
  page.should have_css('.icon.icon-checkbox', :count =>2)
end

#---------------------------------------------------------------------------------

Given /^I set user's permissions for RW Prizes$/ do
  find(:xpath, '//*[@id="role-Prizes"]/div[1]').click
end

#---------------------------------------------------------------------------------

Given /^I check user's permissions for RW in prizes$/ do
  within(:xpath, '//*[@id="role-Prizes"]') do
    find(:xpath, "//*[@class=\"switch-box publish active\" and @title=\"Write\"]")
  end
  page.should have_css('.icon.icon-checkbox', :count =>2)
end

#---------------------------------------------------------------------------------

Given /^I set user's permission for R Prizes$/ do
  find(:xpath, '//*[@id="role-Prizes"]/div[1]').click
  find(:xpath, '//*[@id="role-Prizes"]/div[3]').click
end

#---------------------------------------------------------------------------------

Given /^I check user's permissions for R in prizes$/ do
  within(:xpath, '//*[@id="role-Prizes"]') do
    find(:xpath, "//*[@class=\"switch-box publish\" and @title=\"Read\"]")
  end
  page.should have_css('.icon.icon-checkbox', :count =>2)
end

#---------------------------------------------------------------------------------



#---------------------------------------------------------------------------------



#---------------------------------------------------------------------------------



#---------------------------------------------------------------------------------

Given /^I set permission for Prizes in RW mode$/ do

end

#---------------------------------------------------------------------------------