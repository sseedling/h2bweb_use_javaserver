#encoding: utf-8
#require 'cucumber/formatter/unicode' 
require 'capybara'
require 'capybara/cucumber'
require 'capybara/rspec'
require 'rspec'
require 'selenium/webdriver'

Given /^I am on the home page$/ do
  visit "/"
end

Given /^Save screenshot$/ do  
  page.save_screenshot('screenshot.png')
end

Given /^I visit "([^\"]*)"$/ do |page|
  visit "#{page}"
end

Then /^I should see "([^\"]*)"$/ do |text|
	page.should have_content "#{text}" 
end

Given /^I should not see "([^\"]*)"$/ do |text|
	page.should have_no_content "#{text}" 
end

When /^I wait (\d+) seconds?$/ do |seconds|
  sleep seconds.to_i
end

Given /^I click "([^\"]*)" button$/ do |button|
  click_link ("#{button}")
end

When /^I refresh page$/ do
  visit(current_path)
end

When /^I click save button$/ do
  find(:xpath, '//*[@id="pageMenu"]/div[1]/div[3]').click
end

When /^I click "+" button$/ do
  click_link("+")
end

When /^I fill in "([^\"]*)" with "([^\"]*)"$/ do |field, text|
  fill_in field, :with=> text
end

When(/^I click "([^"]*)" menu item$/) do |name|
  within(:xpath, '//div[@class="leftMenu-items"]') do
    click_link(name)
  end
end


Then(/^I should see add button$/) do
  find(:xpath, "//*[@class='btn big-size over-right createBtn']")
end

Then(/^I should no see add button$/) do
  page.should_not have_xpath("//*[@class='btn big-size over-right createBtn']")
end

And(/^I logout$/) do
  visit("/logout")
end