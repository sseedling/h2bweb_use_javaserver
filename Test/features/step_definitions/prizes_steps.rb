Given(/^I am on prizes page$/) do
  steps %{
  Given I logged in
        }
  visit('/prizes')
end

When /^I select english$/ do
  find(:xpath,'//*[@id="add-lang"]/div[2]/div[2]/div/div[1]/div').click
end


When /^I fill (\d+) prizes$/ do |number|
  number=number.to_i
  find(:xpath,'//*[@id="add-lang"]/div[2]/div[2]/div/div[1]/div').click
  number.times do |i|
    fill_in "prizes[#{i}][prizeData][eng][name]", :with => "test_prize #{i+1}"
    fill_in "prizes[#{i}][count]", :with => (i+1).to_s
    fill_in "prizes[#{i}][prizeData][eng][note]", :with => "test_note #{i+1}"
    find(:xpath, '//*[@id="lang"]/div[4]/div[2]/div').click if i<number-1
  end
end

When(/^I attach quiz number (\d+) to the prize group$/) do |number|
  number=number.to_i-1
  find('//select[@class="text-field"]/option', :text => $company.quizzes[number].name).select_option
end

When(/^I attach first quiz to the prize group$/) do
  steps %{
    When I attach quiz number 1 to the prize group
  }
end

When(/^I create a prize group with (\d+) prizes$/) do |number|
  steps %{
    When I click "+" button
    And I fill in "name" with "test prize group"
    And I fill 3 prizes
    When I attach first quiz to the prize group
    And I click save button
  }
end

Then(/^I should see created prizes group$/) do
  within('//div[@class="col groups-prize"]') do
      page.should have_content("test prize group")
    end
end

When(/^I go to prize group number (\d+)$/) do |number|
  find(:xpath, "//div[@class=\"col groups-prize\"]/div[1]/div[#{number.to_i+1}]").click
  sleep(2)
end

Then(/^I should see (\d+) created prizes$/) do |number|
      number.to_i.times {|i| page.should have_content("test_prize #{i+1}")}
  #todo avoid hardcoded prize name
end

When(/^I go to quizzes tab$/) do
  find(:xpath, "//div[@class=\"tabs\"]/div[2]").click
end

Then(/^I should see attached quizzes$/) do
  page.should have_content($company.quizzes.first.name)
end

And(/^I create a prize group without prizes and quizzes$/) do
  steps %{
    When I click "+" button
    And I fill in "name" with "test prize group"
    And I select english
    And I click save button
  }
end

And(/^I remove prize number (\d+)$/) do |number|
  number=number.to_i-1
  all(:xpath, '//div[@class="prize-remove"]')[number].click
  sleep(1)
end