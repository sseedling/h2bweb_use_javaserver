And(/^I go to profile page$/) do
  find(:xpath,"//*[@class='icon icon-settings']").click
  steps %{
   And I should see "User Profile"
  }
end



And(/^My username should be "([^"]*)"$/) do |arg|
  find_field('publicName').value.should eq arg
end


And(/^My username should be default$/) do
  find_field('publicName').value.should eq $company.owner.username
end

And(/^I save profile$/) do
  steps %{
    When I click save button
    And I wait 1 seconds
    When I fill in "Password" with "123"
  }
  find(:xpath, '//*[@id="confirm"]').click
  sleep(4)
  #todo replace sleep to other step
end