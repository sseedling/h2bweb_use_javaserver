Given /^I am on the login page$/ do
	visit "/login"
  steps %{
    And I should see "Sign In"
  }
end

Given /^I login with "([^\"]*)" username and "([^\"]*)" password$/ do |username,password|
	fill_in 'Email', :with => "#{username}"
  fill_in 'Password', :with => "#{password}"
  page.execute_script("$('#doLoginBtn').click()")
end

When /^I login with valid credentials$/ do
  steps %{
    Given I login with "#{$defaultUser['username']}" username and "#{$defaultUser['password']}" password
  }
end

Given /^I logged in$/ do
  steps %{
    Given I am on the login page
    When I login with valid credentials
    Then I should be logged in
  }
end

When /^I login with inversed input$/ do
  steps %{
    Given I login with "#{$defaultUser['password']}" username and "#{$defaultUser['username']}" password
  }
end

When /^I login with invalid credentials$/ do
  steps %{
    Given I login with "#{$defaultUser['username']}" username and "#{$defaultUser['password']}123" password
  }
end

When /^I login with empty credentials$/ do
  steps %{
    Given I login with "" username and "" password
  }
end

Then /^I should see logout button$/ do
    page.should have_css('.topMenu-item.logOut')
end

Then /^I should not see logout button$/ do
    page.should_not have_css('.topMenu-item.logOut')
end

Then /^I click Log out button$/ do
    # page.execute_script("$('#topMenu .topMenu-item.logOut').click()")
    find(:xpath, '//*[@id="topMenu"]/div/div[@class="topMenu-item logOut"]/a').click
end

Then /^I should not be logged in$/ do
  steps %{
    And I should not see logout button
    And I should see "Sign In"
  }
end

Then /^I should be logged in$/ do
  steps %{
    And I should see logout button
    And I should not see "Sign In"
  }
end

And /^Admin disable permission$/ do
  acl_without_location = {:Authorize => {read: true}}
  $company.owner.update(:ACL=>acl_without_location)
end

