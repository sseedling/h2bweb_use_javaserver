When /^I should see locations$/ do
  steps %{Then I should see "Locations"}
  $company.locations.each {|x| steps %{Then I should see "#{x.name}"}}
end

Given /^I am on the locations page$/ do
  steps %{
    Given I logged in
    When I visit "/locations"
  }
end

When /^Admin delete the location$/ do
  $company.locations.first.delete()
end

When /^I visit a location$/ do 
  find(:xpath, '//*[@id="middleContent"]/div/div[1]/div[1]').click
  #visit ("/locations/#{$company.locations.first.name}")
end

Then /^I should see correct location information$/ do
  steps %{
    Then I should see information about location number 1
  }
end

Then /^I should see information about location number (\d+)$/ do |number|
  number=number.to_i-1
  steps %{
    Given I should see "#{$company.locations[number].about}"
    Given I should see "#{$company.locations[number].name}"
  }
end

When /^I go back$/ do
  page.evaluate_script('window.history.back()')
end

Given /^Admin adds new location$/ do
  $company.create_location()
  #todo add a location to owner's location list
end

When /^I visit a new location$/ do
  visit ("/locations/#{$company.locations.first.name}")
end

When /^I should see a new location$/ do
  steps %{
    Then I should see "#{$company.locations.first.name}"
  }
end

And /^Admin add phone number$/ do
    $company.locations.first.updatePhone()
end

And /^Admin edited description$/ do
  $company.locations.first.updateDescription()
end

And /^Admin delete description$/ do
  $company.locations.first.deleteDescription()
end

#And /^I should see survays$/ do
#
#end

Given /^I create valid location$/ do
  click_link('New Location')
  randomLocationName = "Location_web" + rand(1234123).to_s
  fill_in 'Location Name', :with => randomLocationName
  fill_in 'location[about]', :with => "some default Location description"
  fill_in 'location[phones][list][0]', :with => "123-456-789"
  fill_in 'location[phones][list][1]', :with => "123-456-789"
  fill_in 'location[phones][list][2]', :with => "123-456-789"
  fill_in 'Title', :with => "Test attraction message"
  fill_in 'attraction[text]', :with => "Tests creation description"
  page.execute_script("$('#pageMenu .btn.btn-short.saveBtn').click()")
  sleep(2)
end

Given /^I create existing location$/ do
  click_link('New Location')
  fill_in 'Location Name', :with => $company.locations.first.name
  fill_in 'location[about]', :with => "some default Location description"
  fill_in 'location[phones][list][0]', :with => "123-456-789"
  fill_in 'location[phones][list][1]', :with => "123-456-789"
  fill_in 'location[phones][list][2]', :with => "123-456-789"
  fill_in 'Title', :with => "Test attraction message"
  fill_in 'attraction[text]', :with => "Tests creation description"
  page.execute_script("$('#pageMenu .btn.btn-short.saveBtn').click()")
  sleep(2)
end

And /^I modify location$/ do
  steps %{
    And I visit a location
  }
  sleep(1)
  find(:xpath, '//*[@id="pageMenu"]/div[1]/a').click
  fill_in 'Location Name', :with => "Edited name"
  fill_in 'location[about]', :with => "edited description"
  fill_in 'location[phones][list][0]', :with => "123"
  fill_in 'location[phones][list][1]', :with => "321"
  fill_in 'location[phones][list][2]', :with => "312"
  fill_in 'Title', :with => "edited attraction text"
  fill_in 'attraction[text]', :with => "edited attraction description"
  page.execute_script("$('#pageMenu .btn.btn-short.saveBtn').click()")
  sleep(2)
end

Then /^I should see eddited information$/ do
  find_field('location[name]').value.should eq 'Edited name'
  find_field('location[phones][list][0]').value.should eq '123'
  find_field('location[phones][list][1]').value.should eq '321'
  find_field('location[phones][list][2]').value.should eq '312'
  find_field('Title').value.should eq 'edited attraction text'
  find_field('attraction[text]').value.should eq 'edited attraction description'
end

And /^I click edit button$/ do
  find(:xpath, '//*[@id="pageMenu"]/div[1]/a').click
  sleep(2)
end


Given /^I create correct location with name "([^\"]*)"$/ do |name|
  click_link('New Location')
  fill_in 'Location Name', :with => name
  fill_in 'location[about]', :with => "some default Location description"
  fill_in 'location[phones][list][0]', :with => "123-456-789"
  fill_in 'location[phones][list][1]', :with => "123-456-789"
  fill_in 'location[phones][list][2]', :with => "123-456-789"
  fill_in 'Title', :with => "Test attraction message"
  fill_in 'attraction[text]', :with => "Tests creation description"
  page.execute_script("$('#pageMenu .btn.btn-short.saveBtn').click()")
  sleep(4)
end

Given /^I create invalid location$/ do
  click_link('New Location')
  fill_in 'Location Name', :with => ""
  fill_in 'location[about]', :with => ""
  fill_in 'location[phones][list][0]', :with => ""
  fill_in 'location[phones][list][1]', :with => ""
  fill_in 'location[phones][list][2]', :with => ""
  fill_in 'Title', :with => ""
  fill_in 'attraction[text]', :with => ""
  page.execute_script("$('#pageMenu .btn.btn-short.saveBtn').click()")
end


And /^I should see location creation page$/ do
  steps %{
    Then I should see "New Location"
    Then I should see "Cancel"
    Then I should see "Save"
  }
end

And /^I should see all hints$/ do
    page.should have_css('#mainContainer .column3.item-help')
    steps %{
      Then I should see "256x192 px images are strongly recommended"
    }
end

And /^I go to location creation page$/ do
  click_link('New Location')
end

And /^I cancel location creation$/ do
  within ('//*[@id="pageMenu"]') { click_link('Cancel')}
end

Given /^I click edit Location button$/ do
  visit ("/locations/#{$company.locations.first.name}/edit/")
end

And /^admin deletes phones in the location$/ do
  $company.locations.first.update(phones: {list:[]})
end

And /^admin deletes description in the location$/ do
  $company.locations.first.update(about: "")
end

