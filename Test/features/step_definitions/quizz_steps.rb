$language = ["eng", "fra", "spa", "rus", "deu"]

Given /^I create valid survey$/ do
  click_link('New quiz')
  $randomSurveyName = "survey-web" + rand(1234123).to_s
  fill_in 'name', :with => "#{$randomSurveyName}"
  fill_in 'note', :with => "some default survey description"
  find(:xpath,'//*[@id="locale-eng"]/div').click
  #page.execute_script("$('#pageMenu .btn.btn-short.saveBtn').click()")
  find(:xpath, '//*[@id="pageMenu"]/div[1]/div[3]').click
end

#---------------------------------------------------------------------------------

Given /^I go to new quizz creation page$/ do
  click_link('New quiz')
end

#---------------------------------------------------------------------------------

Given /^I create exixts survey$/ do
  click_link('New quiz')
  $defaultSurveyName = $company.location.first.quizz.first
  fill_in 'name', :with => "#{$randomSurveyName}"
  fill_in 'note', :with => "some default survey description"
  fill_in 'questions[en][1][name]', :with => "test question"
  fill_in 'questions[en][1][text]', :with => "test question's description"
  page.execute_script("$('#pageMenu .btn.btn-short.saveBtn').click()")
end

#---------------------------------------------------------------------------------

Given /^I cancel creation$/ do
  within ('//*[@id="pageMenu"]') { click_link('Cancel')}
  sleep(1)
end

#---------------------------------------------------------------------------------

Given /^I create empty survey$/ do
  click_link('New quiz')
  $randomSurveyName = "survey_web" + rand(1234123).to_s
  $defaultSurveyName = $randomSurveyName
  fill_in 'name', :with => "#{$randomSurveyName}"
  fill_in 'note', :with => "some default survey description"
  page.execute_script("$('#pageMenu .btn.btn-short.saveBtn').click()")
end

#---------------------------------------------------------------------------------

Then /^I fill first question$/ do
  fill_in 'questions[en][1][name]', :with => "test question"
  fill_in 'questions[en][1][text]', :with => "test question's description"
end

#---------------------------------------------------------------------------------

Then /^I should see created survey$/ do
  steps %{
    Then I should see "#{$defaultSurveyName}"
  }
end

#---------------------------------------------------------------------------------

And /^I should see unpublish status$/ do
  # assert_selector(:css,".pending")
  # assert_no_selector(:css,".published")
  steps %{
    Then I should see "UNPUBLISH"
  }
end

#---------------------------------------------------------------------------------

Given /^I go to survey$/ do
  visit ("/locations/#{$location.name}/#{$survey.name}")
end

#---------------------------------------------------------------------------------

Given /^I create survey without question name$/ do
  click_link('New quiz')
  $randomSurveyName = "survey_web" + rand(1234123).to_s
  $defaultSurveyName = $randomSurveyName
  fill_in 'name', :with => "#{$randomSurveyName}"
  fill_in 'note', :with => "some default survey description"
  fill_in 'questions[en][1][text]', :with => "test question's description"
  page.execute_script("$('#pageMenu .btn.btn-short.saveBtn').click()")
end

#---------------------------------------------------------------------------------

Given /^I should see red error frame$/ do
  page.should have_css('.text-field.question-name.error')
end

#---------------------------------------------------------------------------------

And /^I should not see quizzes$/ do
  #assert_no_selector(:css,".unpublish")
  #assert_no_selector(:css,".publish")
  page.should_not have_css('row-item status-publish')
end

#---------------------------------------------------------------------------------

Given /^I create question without description$/ do
  click_link('New quiz')
  $randomSurveyName = "survey_web" + rand(1234123).to_s
  $defaultSurveyName = $randomSurveyName
  fill_in 'name', :with => "#{$randomSurveyName}"
  fill_in 'note', :with => "some default survey description"
  fill_in 'questions[en][1][name]', :with => "test question's Name"
  page.execute_script("$('#pageMenu .btn.btn-short.saveBtn').click()")
end

#---------------------------------------------------------------------------------

Given /^I should see created question$/ do
  steps %{
    And I should see "Publish"
  }
end

#---------------------------------------------------------------------------------

Given /^I create ten question$/ do
  click_link('New quiz')
  $randomSurveyName = "survey_web" + rand(1234123).to_s
  $defaultSurveyName = $randomSurveyName
  
  fill_in 'name', :with => "#{$randomSurveyName}"
  fill_in 'note', :with => "some default survey description"

  for i in 1...11 do

    $randomQuestionName = "Question" + rand(1234123).to_s
    puts "questions[#{i}][questionData][eng][name]"
    fill_in "questions[#{i}][questionData][eng][name]", :with => "#{$randomQuestionName}"    
  end
    page.execute_script("$('#pageMenu .btn.btn-short.saveBtn').click()")
end

#---------------------------------------------------------------------------------

Given /^I create Quiz with five questions$/ do
  click_link('New quiz')
  $randomSurveyName = "survey_web" + rand(1234123).to_s
  $defaultSurveyName = $randomSurveyName
  
  fill_in 'name', :with => "#{$randomSurveyName}"
  fill_in 'note', :with => "some default survey description"
  page.execute_script("$('#locale-eng').click()")

  for i in 1...6 do

    $randomQuestionName = "Question" + rand(1234123).to_s
    puts "questions[#{i}][questionData][eng][name]"
    fill_in "questions[#{i}][questionData][eng][name]", :with => "#{$randomQuestionName}"    
  end
    page.execute_script("$('#pageMenu .btn.btn-short.saveBtn').click()")
end

#---------------------------------------------------------------------------------

#Given /^I should not see empty questions$/ do
#      page.should_not have_css('questions[6][questionData][eng][name]')
#end


#Given /^I should see ten quizzes$/ do
#  steps %{
#    And I should see "10"
#  }
#end

#---------------------------------------------------------------------------------

Given /^I tap on Edit Survey button$/ do
  page.should have_css('#pageMenu .btn.btn-ico.btn-blue.editBtn')
  steps %{
    And I wait 2 seconds
  }
  visit ("/locations/#{$location.name}/#{$randomSurveyName}/edit/")
#  page.should have_css('.text-field survey-name .#{$randomSurveyName}"')
end

#---------------------------------------------------------------------------------

Given /^I create Quiz with subquestions$/ do
  click_link('New quiz')
  $randomSurveyName = "survey_web" + rand(1234123).to_s
  $defaultSurveyName = $randomSurveyName
  
  fill_in 'name', :with => "#{$randomSurveyName}"
  fill_in 'note', :with => "some default survey description"
  page.execute_script("$('#locale-eng').click()")

  for i in 1..5 do

    $randomQuestionName = "Question" + rand(1234123).to_s
    puts "questions[#{i}][questionData][eng][name]"
    fill_in "questions[#{i}][questionData][eng][name]", :with => "#{$randomQuestionName}"
    #within('//*[@id="tab-content-eng"]/div[3]') {5.times {|i| fill_in "questions[#{i+1}][questionData][eng][subQuestions][]", :with => "#{$randomQuestionName}"}}  
    5.times do |j|
      find(:xpath,"//*[@id=\"tab-content-eng\"]/div[#{i}]/div[2]/div[2]/input[#{j+1}]").set "random question #{i}"
    end
  end
    page.execute_script("$('#pageMenu .btn.btn-short.saveBtn').click()")
end

#---------------------------------------------------------------------------------

Given /^I fill in all fields for new quiz$/ do
  click_link('New quiz')
  $randomSurveyName = "survey_web" + rand(1234123).to_s
  $defaultSurveyName = $randomSurveyName
  
  fill_in 'name', :with => "#{$randomSurveyName}"
  fill_in 'note', :with => "some default survey description"
  page.execute_script("$('#locale-eng').click()")

  for i in 1..5 do

    $randomQuestionName = "Question" + rand(1234123).to_s
    puts "questions[#{i}][questionData][eng][name]"
    fill_in "questions[#{i}][questionData][eng][name]", :with => "#{$randomQuestionName}"
    5.times do |j|
      find(:xpath,"//*[@id=\"tab-content-eng\"]/div[#{i}]/div[2]/div[2]/input[#{j+1}]").set "random question #{i}"
    end
  end
end

#---------------------------------------------------------------------------------

Given /^I fill in ten question and subquestion$/ do
  click_link('New quiz')
  $randomSurveyName = "survey-web" + rand(1234123).to_s
  $defaultSurveyName = $randomSurveyName
  
  fill_in 'name', :with => "#{$randomSurveyName}"
  fill_in 'note', :with => "some default survey description"
  page.execute_script("$('#locale-eng').click()")

  for i in 1..10 do

    $randomQuestionName = "Question" + rand(1234123).to_s
    puts "questions[#{i}][questionData][eng][name]"
    fill_in "questions[#{i}][questionData][eng][name]", :with => "#{$randomQuestionName}"
    1.times do |j|
      find(:xpath,"//*[@id=\"tab-content-eng\"]/div[#{i}]/div[2]/div[2]/input[#{j+1}]").set "random question #{i}"
    end
  end
  page.execute_script("$('#middleContent .switch-box.unpublish').click()")
end

#---------------------------------------------------------------------------------

Given /^I add all languages$/ do
  click_link('New quiz')
  $randomSurveyName = "survey-web" + rand(1234123).to_s
  $defaultSurveyName = $randomSurveyName
  
  fill_in 'name', :with => "#{$randomSurveyName}"
  fill_in 'note', :with => "some default survey description"
  $language.each do |lang|
    page.execute_script("$('#middleContent .icon.icon-earth-plus').click()")
    find(:xpath,"//*[@id=\"locale-#{lang}\"]").click
    for i in 1..5 do
      $randomQuestionName = "Question" + rand(1234123).to_s
      puts "questions[#{i}][questionData][#{lang}][name]"
      fill_in "questions[#{i}][questionData][#{lang}][name]", :with => "#{$randomQuestionName}"
      5.times do |j|
        find(:xpath,"//*[@id=\"tab-content-#{lang}\"]/div[#{i}]/div[2]/div[2]/input[#{j+1}]").set "random question #{i}"
      end
    end
    if lang<$language[-1] then
      page.execute_script("$('#middleContent .icon.icon-earth-plus').click()")
      find(:xpath,"//*[@id=\"locale-#{lang}\"]").click
    end
  end
  page.execute_script("$('#middleContent .switch-box.unpublish').click()")
  page.execute_script("$('#pageMenu .btn.btn-short.saveBtn').click()")

end

#---------------------------------------------------------------------------------

Given /^Quiz should have all languages$/ do
  $language.each do |lang|
    within(:xpath,'//div[@class="lang-tabs"]')do 
      page.should have_content(lang.upcase)
    end
  end
end

#---------------------------------------------------------------------------------

Given /^I turn off first subquestion$/ do
  find(:xpath,'//*[@id="tab-content-eng"]/div[1]/div[2]/div[1]').click
end

#---------------------------------------------------------------------------------

Given /^I save quiz/ do
      page.execute_script("$('#pageMenu .btn.btn-short.saveBtn').click()")
end

#---------------------------------------------------------------------------------

Given /^I should see first subquestion turned off$/ do
    page.should have_css('#pageMenu .btn.btn-ico.btn-blue.editBtn')
  steps %{
    And I wait 2 seconds
  }
  visit ("/locations/#{$company.locations.first.name}/#{$randomSurveyName}/edit/")
    page.should have_css('#tab-content-eng .switch-box.unpublish')
end

#---------------------------------------------------------------------------------

Given /^I delete one language$/ do
  visit ("/locations/#{$company.locations.first.name}/#{$randomSurveyName}/edit/")
  find(:xpath,'//*[@id="tab-eng"]/div').click
    steps %{
      And I wait 1 seconds
    }
  page.driver.browser.switch_to.alert.accept
  page.execute_script("$('#pageMenu .btn.btn-short.saveBtn').click()")
end

#---------------------------------------------------------------------------------

Given /^User should not see deleted language$/ do
    within(:xpath,'//div[@class="lang-tabs"]')do
      page.should_not have_content($language.first.upcase)
    end
end

#---------------------------------------------------------------------------------

Then /^I should see created subquiestions$/ do
  steps %{
    Then I should see "Quiz Questions"
  }
  for i in 1..5 do
    page.should have_content("random question #{i}", :count => 5)
  end
end

#---------------------------------------------------------------------------------

Given /^I create uncomplete Survey$/ do
  click_link('New quiz')
  $randomSurveyName = "survey_web" + rand(1234123).to_s
  $defaultSurveyName = $randomSurveyName
  fill_in 'name', :with => "#{$randomSurveyName}"
  fill_in 'note', :with => "some default survey description"
  fill_in 'questions[en][1][name]', :with => "test question"
  fill_in 'questions[en][1][text]', :with => "test question's description"
end

#---------------------------------------------------------------------------------

Given /^I publish survey$/ do
  page.execute_script("$('#middleContent .switch-box.unpublish').click()")
end

#---------------------------------------------------------------------------------

Given /^I save created Survey$/ do
  page.execute_script("$('#pageMenu .btn.btn-short.saveBtn').click()")
end

#---------------------------------------------------------------------------------

Given /^I should see new published survey$/ do
  steps %{
    And I should see "Published"
  }
end

#---------------------------------------------------------------------------------

Given /^I create survey without name$/ do
  click_link('New quiz')
  $randomSurveyName = "survey-web" + rand(1234123).to_s
  $defaultSurveyName = $randomSurveyName
  fill_in 'name', :with => ""
  fill_in 'note', :with => "some default survey description"
  fill_in 'questions[en][1][name]', :with => "test question"
  fill_in 'questions[en][1][text]', :with => "test question's description"
end

#---------------------------------------------------------------------------------

Given /^I should see error during Survey creation$/ do
  page.should have_css('.text-field.survey-name.error')
end

#---------------------------------------------------------------------------------








