Feature: location
	As a company owner I want to create new quizz and see it in survey list

@smoke @company_without_quizzes @noSurveyLable
Scenario: User should see "There are no quizzes available" label in location doen's have quizzes
	Given I am on the locations page
	When I visit a location
	Then I should not see quizzes
	And I should see "There are no quizzes available"

#---------------------------------------------------------------------------------

@company_with_location @newSurvCreate @smoke
Scenario: User should be able to create new survey with valid data
	Given I am on the locations page
	And I visit a location
	When I create valid survey
	Then I should see created survey
	#todo compare created survey with the data in database

#---------------------------------------------------------------------------------

@smoke @company_with_location @descriptionDefault
Scenario: User should see survey details
	Given I am on the locations page
	And I visit a location
	When I create valid survey
	And I should see created survey
	Then I should see "some default survey description"

#---------------------------------------------------------------------------------

@smoke @company_with_location @emptyQuizStatus
Scenario: Survey without questions should have unpublish status
	Given I am on the locations page
	And I visit a location
	And I create empty survey
	Then I should see created survey
	Then I should see "PUBLISH" 

#---------------------------------------------------------------------------------

@company_with_location @smoke @editQuizBtn
Scenario: User can edit his quizzes 
	Given I am on the locations page
	And I visit a location
	And I create empty survey
	Then I should see created survey
	And I click edit button
	Then I should see "Cancel"
	Then I should see "Save"

#---------------------------------------------------------------------------------

@manual
Scenario: quizzes should be sorted by status

#---------------------------------------------------------------------------------

@manual
Scenario: Survey name length should be limited

#---------------------------------------------------------------------------------

@manual
Scenario: Survey description length should be limited

#---------------------------------------------------------------------------------

@manual
Scenario: Question name length should be limited

#---------------------------------------------------------------------------------

@manual
Scenario: Question description length should be limited

#---------------------------------------------------------------------------------

@smoke @company_with_location @emptyQuestionsIgnore
Scenario: empty questions shuld be ignored
	Given I am on the locations page
	And I visit a location
	And I create Quiz with five questions
#	Then I should not see empty questions
# todo Make some check that quiz have no empty questions

#---------------------------------------------------------------------------------

@company_with_location @smoke @work
Scenario: user should be able to cancel creation
	Given I am on the locations page
	And I visit a location
	And I go to new quizz creation page
	When I cancel creation
	Then I should not see "Cancel"
	Then I should see correct location information

#---------------------------------------------------------------------------------

@manual
Scenario: user should see permission denied if admin disable it during survey creation and survey should not be created

#---------------------------------------------------------------------------------

@manual
Scenario: survey description and name should be trimmed

#---------------------------------------------------------------------------------

@manual
Scenario: questions description and name should be trimmed

#---------------------------------------------------------------------------------

@manual:
Scenario: survey name in French should has correct address

#---------------------------------------------------------------------------------

@manual
Scenario: survey in Russian should hav address with hash

#---------------------------------------------------------------------------------

@manual
Scenario: user should not be able to see questions if admin disable permission

#---------------------------------------------------------------------------------

@manual
Scenario: user should not be able to see quizzes if admin disable permission

#---------------------------------------------------------------------------------

@new_user_without_survey @existSurveyCreate @manual
Scenario: User can't create survey with exists name
	Given I am on the locations page
	And I visit a location
	When I create valid survey
	Then I should see created survey
	And I visit a location
	And  I create exixts survey
	Then I should see "Quiz allready exist"

#---------------------------------------------------------------------------------

@smoke @new_user @editSurveyBtn @manual
Scenario: User can edit survey by edit button tap
	Given I am on the locations page
	And I visit a location
	And I create valid survey
	And I tap on Edit Survey button
#todo Make some checking that user realy on edit page	

#---------------------------------------------------------------------------------

@smoke @new_user @errorCreationSurvey @manual
Scenario: User can't save survey without name
	Given I am on the locations page
	And I visit a location
	And I create survey without name
	And I save created Survey
	Then I should see error during Survey creation

#---------------------------------------------------------------------------------

@smoke @new_user @publishing @manual
Scenario: User can publish or unpublish survey during editing
	Given I am on the locations page
	And I visit a location
	And I create uncomplete Survey
	And I publish survey
	And I save created Survey
	Then I should see new published survey

#---------------------------------------------------------------------------------

@manual
Scenario: User can delete questions during editing

#---------------------------------------------------------------------------------

@smoke @company_with_location @subquestions
Scenario: User can add second lvl questions for each questions
	Given I am on the locations page
	And I visit a location
	When I create Quiz with subquestions
	Then I should see created subquiestions

#---------------------------------------------------------------------------------

@company_with_location @smoke @subquestionSwitcher
Scenario: User can turn on/off second lvl questions 
	Given I am on the locations page
	And I visit a location
	And I fill in ten question and subquestion
	And I turn off first subquestion
	When I save quiz
	Then I should see first subquestion turned off 
	Then I wait 5 seconds

#---------------------------------------------------------------------------------

@smoke @company_with_location @languageSelection
Scenario: User need to select one of languages to create new quiz
	Given I am on the locations page
	And I visit a location
	And I add all languages
	Then Quiz should have all languages

#---------------------------------------------------------------------------------

@manual
Scenario: User can add up to 5 questions for the second lvl

#---------------------------------------------------------------------------------

@smoke @company_with_location @deleteLanguage
Scenario: User can delete some of added languages
	Given I am on the locations page
	And I visit a location
	And I add all languages
	When Quiz should have all languages
	And I delete one language
	Then User should not see deleted language

#---------------------------------------------------------------------------------


#---------------------------------------------------------------------------------


















