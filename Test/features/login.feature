Feature: login
  As an user
  I want to be able login
  To be able moderate content

#---------------------------------------------------------------------------------
@new_user
Scenario: Valid user can login
  Given I am on the login page
  When I login with valid credentials
  Then I should be logged in

#---------------------------------------------------------------------------------

@new_user
Scenario: invalid user can not login
  Given I am on the login page
  When I login with invalid credentials
  Then I should not be logged in
  And I should see "Wrong email or password"

#---------------------------------------------------------------------------------

@new_user 
Scenario: User can't login if he enter password to login field and login to password field 
  Given I am on the login page
  When I login with inversed input
  Then I should not be logged in
  And I should see "Wrong email or password"

#---------------------------------------------------------------------------------

@unregistered_user
Scenario: uregistered user can not login
  Given I am on the login page
  When I login with valid credentials
  Then I should not be logged in
  And I should see "Wrong email or password"

#---------------------------------------------------------------------------------

Scenario: empty user can not login
  Given I am on the login page
  When I login with empty credentials
  Then I should not be logged in
  And I should see "Please enter your credentials"

#---------------------------------------------------------------------------------

@disabled_user
Scenario: Disabled user should not be able to login
  Given I am on the login page
  When I login with valid credentials
  Then I should not be logged in
  And I should see "Wrong email or password"

#---------------------------------------------------------------------------------

@new_user @logout
Scenario: User should be able to logout by clicking logout button
  Given I am on the login page
  When I login with valid credentials
  Then I should be logged in
  When I click Log out button
  Then I should not be logged in


