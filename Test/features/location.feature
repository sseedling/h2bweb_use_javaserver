Feature: location
	As a manager I want to see list of locations

#---------------------------------------------------------------------------------

@smoke @company_with_location @locationsBrows
Scenario: Logged in user can see locations
	Given I am on the locations page
	Then I should see locations

#---------------------------------------------------------------------------------

@smoke @locationDetails @company_with_location
Scenario: User can see location's details 
	Given I am on the locations page
	And I should see locations
	And I visit a location
	Then I should see correct location information 

#---------------------------------------------------------------------------------

@smoke @company_without_location
Scenario: User should see No location placeholder if company doesn't have locations
	Given I am on the locations page
	And I should see "There are no locations available"

#---------------------------------------------------------------------------------

@smoke @company_with_location
Scenario: User can make logout from location screen
	Given I am on the locations page
 	When I click Log out button
 	Then I should not be logged in

#---------------------------------------------------------------------------------

@company_with_location @smoke
Scenario: User should see some notification if locations does not exists
	Given I am on the locations page
	And I visit a location
	When Admin delete the location
	And I refresh page
	Then I should see "404"

#---------------------------------------------------------------------------------
	
@smoke @company_with_location @PermissionNotify
Scenario: User should see some notification if permissions was changed 
	Given I am on the locations page
	When Admin disable permission
	And I refresh page
	Then I should see "403"

#---------------------------------------------------------------------------------	

@smoke @company_with_location @changePermission
Scenario: User should be able to go back to admin panel if permission was changed
	Given I am on the locations page
	And Admin disable permission
	And I refresh page
	And I should see "403"
	When I click "Main Page" button
	Then I should be logged in

#---------------------------------------------------------------------------------

@smoke @company_with_location @noPhone
Scenario: user should see "No Phones" if admin did not add it
	Given I am on the locations page
	And admin deletes phones in the location
	And I should see locations
	When I visit a location
	Then I should see "Phones"
	And I should see "No Phones"

#---------------------------------------------------------------------------------

@smoke @company_with_location
Scenario: user should see "No description" if admin didn't added it
	Given I am on the locations page
	And admin deletes description in the location
	And I should see locations
	When I visit a location
	Then I should see "No description"

#---------------------------------------------------------------------------------

@manual
Scenario: page should look correct if admin added long description

#---------------------------------------------------------------------------------

@smoke @company_without_quizzes @noQuizzes
Scenario: user should see quantity 0 if location doesn't have surveys
	Given I am on the locations page
	When I visit a location
	Then I should see "There are no quizzes available"

#---------------------------------------------------------------------------------

#@smoke @company_with_location @deleteSurvey
#Scenario: quantity should be updated when admin add and delete surveys
#	Given I am on the locations page
#	And I should see location
#	And I visit a location
#	And I should see survays

#---------------------------------------------------------------------------------

@manual
Scenario: user should see correct modified data and time in am/pm format

#---------------------------------------------------------------------------------

@manual
Scenario: locations should be sorted alphabetically

#---------------------------------------------------------------------------------

@manual
Scenario: long location name should work correctly in table 

#---------------------------------------------------------------------------------

@smoke @new_user @newLocation
Scenario: User can create new location
	Given I am on the locations page
	And I should see "There are no locations available"
	And I create valid location
	And I should see locations
	Then I should see information about location number 1

#---------------------------------------------------------------------------------

@smoke @company_without_location @emptyFields
Scenario: User can't create location with empty input field
	Given I am on the locations page
	And I create invalid location
	And I should see location creation page

#---------------------------------------------------------------------------------

@manual
Scenario: User can upload some picture for attraction screen

#---------------------------------------------------------------------------------

@manual
Scenario: User should see small preview for attraction screen

#---------------------------------------------------------------------------------

@new_user
Scenario: User can cancel location creation
	Given I am on the locations page
	And I go to location creation page
	When I cancel location creation
	And I should see "There are no locations available"

#---------------------------------------------------------------------------------

@company_with_location
Scenario: User can't create location with exists name
	Given I am on the locations page
	When I create existing location
	Then I should see location creation page
	And I should see "Location already exist"

#---------------------------------------------------------------------------------

@manual
Scenario: User can't enter letters to phone input fields

#---------------------------------------------------------------------------------

@smoke @company_with_location @hints
Scenario: User should see some hints for input fields
	Given I am on the locations page
	And I click edit button
	Then I should see all hints

#---------------------------------------------------------------------------------

@new_user
Scenario: Non-latin company name should be transliterated
	Given I am on the locations page
	When I create correct location with name "Локация"
	Then I should see correct location information  
	And I visit "/locations/lokacija/"
	Then I should see correct location information  

@company_with_location
Scenario: user should see correct information after he edit a location
	Given I am on the locations page
	When I modify location
	And I click edit button
	Then I should see eddited information






























