Feature: User's permissions
	As a company owner I want to be able to set permissions for users

#---------------------------------------------------------------------------------

@smoke @company_with_location @newUserCreate
Scenario: As Owner I can create new user
  Given I am on the User's page
  And I create New User

#---------------------------------------------------------------------------------

@smoke @company_with_location @emptyUserCredentials
Scenario: Owner can't create new user if he not fill First/Last name, email and password fields
  Given I am on the User's page
  And User should see red error indication

#---------------------------------------------------------------------------------

@smoke @company_with_location @emailCheck
Scenario: As owner I can check does entered email exist
  Given I am on the User's page
  And I create new user without saving
  Then I check that email is not exist

#---------------------------------------------------------------------------------

@smoke @company_with_location @userEmailExist
Scenario: Owner can't create new user witch exist is email
  Given I am on the User's page
  Then I check that email exist

#---------------------------------------------------------------------------------

@smoke @company_with_location @passwordShows
Scenario: Owner should see entered password by lock button click
  Given I am on the User's page
  And I create new user without saving
  Then I check my password by key button tap

#---------------------------------------------------------------------------------

@smoke @company_with_location @userPermission
Scenario: Owner can set user's permissions
  Given I am on the User's page
  And I create new user without saving
  Then I set all user permission

#---------------------------------------------------------------------------------

@smoke @company_with_location @reportsPermissions
Scenario: Owner can set user's permission for Reports
  Given I am on the User's page
  And I create new user without saving
  Then I set user's permission for Reports

#---------------------------------------------------------------------------------

@smoke @company_with_location @quizzesRW
Scenario: Owner can set user's permission for Quizes in Read/Write mode
  Given I am on the User's page
  And I create new user without saving
  When I set user's permission for Quizzes in RW mode
  Then I check user's permission for RW Quizzes

#---------------------------------------------------------------------------------

@smoke @company_with_location @quizzesR
Scenario: Owner can set user's permission for Quizzes in Read only mode
  Given I am on the User's page
  And I create new user without saving
  When I set permission for Quizzes in R mode
  Then I check user's permissions for Reading quizzes


#---------------------------------------------------------------------------------

@smoke @company_with_location @prizesRW
Scenario: Owner can set user's permission for Prizes in Read/Write mode
  Given I am on the User's page
  And I create new user without saving
  When I set user's permissions for RW Prizes
  Then I check user's permissions for RW in prizes


#---------------------------------------------------------------------------------

@smoke @company_with_location @prizesR
Scenario: Owner can set user's permission for Prizes in Read only mode
  Given I am on the User's page
  And I create new user without saving
  When I set user's permission for R Prizes
  Then I check user's permissions for R in prizes

#---------------------------------------------------------------------------------

@manual
Scenario: Owner can set user's permission for Quizes in Read/Write mode and Prizes in Read only

#---------------------------------------------------------------------------------

@manual
Scenario: Owner can set user's permission for Quizes in Read/Write mode and Prizes in Read/Write

#---------------------------------------------------------------------------------

@manual
Scenario: Owner can set user's permission for Quizes in Read only mode and Prizes in Read only

#---------------------------------------------------------------------------------

@manual
Scenario: Owner can set user's permission for Quizes in Read only mode and Prizes in Read/Write mode

#---------------------------------------------------------------------------------

@manual
Scenario: Owner can set user's permission for Quizes in Read only mode and Prizes in Read and Reports

#---------------------------------------------------------------------------------

@manual
Scenario: Owner can set user's permission for Quizes in Read only mode and Prizes in Read/Write mode and Reports

#---------------------------------------------------------------------------------

@manual
Scenario: Owner can set user's permission for Quizes in Read/Write mode and Prizes in Read/Write and Reports

#---------------------------------------------------------------------------------

@manual
Scenario: Owner can set user's permission for Quizes in Read/Write mode and Prizes in Read only mode and Reports

#---------------------------------------------------------------------------------

@manual
Scenario: Owner can set user's permission for Quizes in Read onnly mode and Prizes in Read/Write mode and Reports

#---------------------------------------------------------------------------------

@manual
Scenario: Owner can activate/deactivate user

#---------------------------------------------------------------------------------

@manual
Scenario: Owner can set locations for user to manage 

#---------------------------------------------------------------------------------

@manual
Scenario: Owner can set all locations by Select All button

#---------------------------------------------------------------------------------










