Feature: prizes
	As a company owner I want to create prizes group and see it on the list

#---------------------------------------------------------------------------------
@company_with_quizzes
Scenario: User should be able to create a prize group with prizes and attach it to a quiz
  Given I am on prizes page
  When I create a prize group with 3 prizes
  Then I should see created prizes group
  When I go to prize group number 1
  Then I should see 3 created prizes
  When I go to quizzes tab
  Then I should see attached quizzes

#---------------------------------------------------------------------------------
@company_with_location
Scenario: User should be able to delete prize during creation
  Given I am on prizes page
  When I click "+" button
  And I fill in "name" with "test prize group"
  And I fill 3 prizes
  And I remove prize number 3
  And I click save button
  Then I should see created prizes group
  And I go to prize group number 1
  Then I should see 2 created prizes
  And I should not see "test_prize 3"

#---------------------------------------------------------------------------------
@manual
Scenario: User can attach a prizes group for the the different quizes

#---------------------------------------------------------------------------------
@manual
Scenario: User can see about 100 prizes on the page and can download additional one

#---------------------------------------------------------------------------------

@manual
Scenario: User should see count of each prize

#---------------------------------------------------------------------------------
@manual
Scenario: User can edit quantity of each prize

#---------------------------------------------------------------------------------
@manual
Scenario: User should see full name of prize and eclipsed name of group

#---------------------------------------------------------------------------------
@manual
Scenario: User can't edit All tab

#---------------------------------------------------------------------------------
@manual
Scenario: User can attach group to quizes during creation/editing

#---------------------------------------------------------------------------------
@company_with_quizzes
Scenario: if prize group doesn't have quizzes user should see add quizzes button and can add quizz
  Given I am on prizes page
  And I create a prize group without prizes and quizzes
  And I should see created prizes group
  When I go to prize group number 1
  Then I should see "There are no prizes available"
  When I go to quizzes tab
  Then I should see "There are no quizzes available"
  And I should see "ADD QUIZZES"
  When I click "ADD QUIZZES" button
  And I attach first quiz to the prize group
  And I click save button
  Then I should see created prizes group
  When I go to prize group number 1
  When I go to quizzes tab
  Then I should see attached quizzes

#---------------------------------------------------------------------------------
@company_with_quizzes
Scenario: user should not be able to create prize group without language
  Given I am on prizes page
  When I click "+" button
  And I fill in "name" with "test prize group"
  And I click save button
  Then I should see "At least one locale required."


#---------------------------------------------------------------------------------
@manual
Scenario: User can edit quantity of prizes by keyboard entering

#---------------------------------------------------------------------------------
@manual
Scenario: User can edit quantity of prizes on the prizes list screen (he should see confirm and cancel buttons)

#---------------------------------------------------------------------------------
@manual
Scenario: User should see add new group button on the page title

#---------------------------------------------------------------------------------
@manual
Scenario: User should see edit group button on the group title

#---------------------------------------------------------------------------------
@manual
Scenario: User can add a discription for the group dring edit and description for the each prize

#---------------------------------------------------------------------------------
@manual
Scenario: User can edit a discription for the group and for the each prize

#---------------------------------------------------------------------------------
@manual
Scenario: User should see description in read mode and it can be eclipsed

#---------------------------------------------------------------------------------
@manual
Scenario: User can select one of five chances (five buttons) but third button must be selected by default

#---------------------------------------------------------------------------------
@manual
Scenario: Potential chances is 50, 25, 10, 1, 0.01

#---------------------------------------------------------------------------------

@manual
Scenario: User should receive localized won message 

#---------------------------------------------------------------------------------




















