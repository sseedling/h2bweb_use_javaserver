Feature: Emotional_Weather
  As a manager I want to see emotional
  Weather image for certain survey in location

#---------------------------------------------------------------------------------

@manual
Scenario: Create new chart tool appears under first chart
  Given I created a new chart
  When I start creating a new emotional weather chart
  Then create chart tools should appear under the chart

#---------------------------------------------------------------------------------

@manual
Scenario: All locations section selected by deafult and all quizes shown
  Given I start creating a new emotional weather chart
  Then All location section should be selected
  And I should see all quizes

#---------------------------------------------------------------------------------

@manual
Scenario: User should see certain location quizes 
  Given I start creating a new emotional weather chart
  When I select first location
  Then I should see first location quizes

#---------------------------------------------------------------------------------

@manual
Scenario: Locations and quizes sorted alphabeticaly

#---------------------------------------------------------------------------------

@manual
Scenario: When user select another location he should see quizes for new location
  Given I start creating a new emotional weather chart
  And I select first location
  And I should see first location quizes
  When I select second location
  And I should not see first location quizes
  And I should see second location quizes

#---------------------------------------------------------------------------------

@manual
Scenario: Date picker saves date when user switch location
  Given I start creating a new emotional weather chart
  And I select some date in data picker
  When I select another locaton
  Then I should see selected date

#---------------------------------------------------------------------------------

@manual
Scenario: When user tap cancel all data must be cleared and chart tool dissappears 

#---------------------------------------------------------------------------------

@manual
Scenario: Quiz saves selected statement when user switch location
  Given I start creating a new emotional weather chart
  And I select first location
  And I select first quiz
  When I select all locations
  Then I should see selected quiz

#---------------------------------------------------------------------------------

@manual
Scenario: Results column save it's statement when user navigate between locations
  Given I start creating a new emotional weather chart
  And I select a quiz
  When I select first location
  Then I should see selected quiz in result column

#---------------------------------------------------------------------------------

@manual
Scenario: Long titles should be eclipsed in first and second column

#---------------------------------------------------------------------------------

@manual
Scenario: Quiz name should be displayed in navigation after user select it
  Given I start creating a new emotional weather chart
  When I select first quiz
  Then I should see first quiz name in navigation

#---------------------------------------------------------------------------------

@manual
Scenario: Start date should match to end date in date picker

#---------------------------------------------------------------------------------

@manual
Scenario: Location gets disabled and enabled when user click on it
  Given I start creating a new emotional weather chart
  And I select a quiz that attached to several locationa
  And first location should be enabled
  When I click on first location in results column
  Then first location should be disabled
  When I click on first location in results column
  Then first locaion should be enabled

#---------------------------------------------------------------------------------

@manual
Scenario: User should see no data placeholder if there is no data to show in chart

#---------------------------------------------------------------------------------

@manual
Scenario: User should see no quizes if location is empty
  Given I start creating a new emotional weather chart
  When I select location without quizes
  Then I should see No Quizes label

#---------------------------------------------------------------------------------

@manual
Scenario: Manager should receive localized report with a bad feedback on default system language

#---------------------------------------------------------------------------------

@manual
Scenario: 

#---------------------------------------------------------------------------------
#---------------------------------------------------------------------------------
#---------------------------------------------------------------------------------

