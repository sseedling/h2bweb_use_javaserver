Feature: Profile

#---------------------------------------------------------------------------------

@manual
Scenario: User can open his Profile page

#---------------------------------------------------------------------------------

@new_user
Scenario: User can Edit his username
  Given I logged in
  And I go to profile page
  And My username should be default
  And I fill in "publicName" with "edited_name"
  And I save profile
  And I logout
  When I logged in
  And I go to profile page
  Then My username should be "edited_name"

#---------------------------------------------------------------------------------

@manual
Scenario: User can Edit his email address

#---------------------------------------------------------------------------------

@manual
Scenario: User can change his password

#---------------------------------------------------------------------------------

@manual
Scenario: User can check does email already exists on server

#---------------------------------------------------------------------------------

@manual
Scenario: User can see entered password by key button tap

#---------------------------------------------------------------------------------

@manual
Scenario: User can save changes by Save button tap

#---------------------------------------------------------------------------------

@manual
Scenario: User can cancel changes by Cancel button tap

#---------------------------------------------------------------------------------
