Feature: User's permissions
	As a company owner I want to be able to set permissions for users

#---------------------------------------------------------------------------------

@new_user
Scenario: User with owner permissions can create locations, users, prizes and see reports and winners
  Given I logged in
  When I click "Locations" menu item
  Then I should see "New Location"
  When I click "Users" menu item
  Then I should see add button
  When I click "Prizes" menu item
  Then I should see add button
  When I click "Reports" menu item
  Then I should see "Create Report"
  When I click "Winners" menu item
  Then I should see "Winners list page"

#---------------------------------------------------------------------------------

@user_with_readonly_permissions
Scenario: User with readonly content should not be able to create locations, prizes and should not see users but should see reports and winners
  Given I logged in
  When I click "Locations" menu item
  Then I should not see "New Location"
  When I click "Prizes" menu item
  Then I should no see add button
  When I click "Reports" menu item
  Then I should see "Create Report"
  When I click "Winners" menu item
  Then I should see "Winners list page"

#---------------------------------------------------------------------------------

@manual
Scenario: User with readonly content should not be able to edit locations and prizes

#---------------------------------------------------------------------------------

@user_without_permissions
Scenario: User without permissions should see his profile
  Given I am on the login page
  When I login with valid credentials
  Then I should be logged in
  And I should see "User Profile"

#---------------------------------------------------------------------------------








