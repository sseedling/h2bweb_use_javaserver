# Business Weather admin panel


## Environment

Cake php
https://github.com/cakephp/cakephp

## Installing

Clone  cake php

	git clone https://github.com/cakephp/cakephp.git

Put content of admin panel repo to cake `app` folder and set up database connection in `config\database.php`


#### Run tests 

To run tests you need to have Firefox browser and ruby >v2.0.0
	
	$ gem install bundler
	$ cd Test/
	$ bundle install
	$ cucumber --tags ~@manual TRACE=YES
	
You can disable log trace by sending TRACE=NO param

