<?php
/**
 * Application model for Cake.
 *
 * This file is application-wide model file. You can put all
 * application-wide model-related methods here.
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Model
 * @since         CakePHP(tm) v 0.2.9
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */

App::uses('Model', 'Model');

/**
 * Application model for Cake.
 *
 * Add your application-wide methods in the class below, your models
 * will inherit them.
 *
 * @package       app.Model
 */
class AppModel extends Model {
    protected $inserted_ids = array();

    private $baseParams = array(
        "limit"=>200,
        "order"=>"DESC",
        "orderField"=>"id",
        "fields"=>"*"
    );

    public function getAllLastInsertsId(){
        return $this->inserted_ids;
    }

    public function findAndClear($param,$conditions=null){
        $data = ($conditions)?$this->find($param,$conditions):$this->find($param);

        if(!isset($data[0]))return null;
        foreach($data as $item){
            foreach($item as $itemData)
                $result[] = $itemData;
        }
        return $result;
    }

    public function readAndClear(){
        $data = $this->Read();

        if(!$data)return null;
        foreach($data as $itemData){
            return $itemData;
        }
    }

    public function getLastQuery(){
        $dbo = $this->getDatasource();
        $logs = $dbo->getLog();
        $lastLog = end($logs['log']);
        return $lastLog['query'];
    }

    function afterSave($created) {
        if($created) {
            $this->inserted_ids[] = $this->getInsertID();
        }
        return true;
    }

    public function getBaseParams(){
        return $this->baseParams;
    }

    public function getAndClear($params) {
        if(!isset($params["limit"]))$params["limit"] = $this->baseParams["limit"];
        if(!isset($params["order"]))$params["order"] = $this->baseParams["order"];

        if(isset($params["conditions"])){
            $queryParams["conditions"] = $params["conditions"];
        }

        if($params["fields"]!=$this->baseParams["fields"]){
            $queryParams["fields"]=$params["fields"];
        }

        $queryParams["limit"]=$params["limit"];
        $queryParams["order"]=$params["orderField"]." ".$params["order"];

        return $this->findAndClear("all",$queryParams);
    }

}
