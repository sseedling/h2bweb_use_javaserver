<?php
App::uses('AppModel', 'Model');
class DbAppLocale extends AppModel{
    var $validationErrors = null;
    public $name = 'DbAppLocale';

    public function isExist($code){
        $result = $this->find("first",array("conditions"=>array("code"=>$code)));
        return !empty($result);
    }

    public function getValidationErrors(){
        return $this->validationErrors;
    }

    public function validateNewLocale($data){
        $this->validationErrors = array();
        while(1){

            if(!isset($data[$this->name])){
                return false;
            }
            $data = $data[$this->name];

            $testName = "code";
            if((!isset($data[$testName]))||(!trim($data[$testName]))){
                $this->validationErrors[$testName]= "empty";
                break;
            }

            $data[$testName] = trim($data[$testName]);
            if($this->isExist($data[$testName])){
                $this->validationErrors[$testName]= "exist";
                break;
            }

            break;
        }
        return empty($this->validationErrors);
    }
}