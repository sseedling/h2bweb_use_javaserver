<?php
App::uses('AppModel', 'Model');
class DbWebLocale extends AppModel{
    public $name = 'DbWebLocale';
    var $validationErrors = null;

    public function getValidationErrors(){
        return $this->validationErrors;
    }

    public function loadAllLocales(){
        $allLocales = $this->findAndClear("all");
        return $allLocales;
    }

    public function isExist($code){
        $result = $this->find("first",array("conditions"=>array("code"=>$code)));
        return !empty($result);
    }

    public function validateNewLocale($data){
        $this->validationErrors = array();
        while(1){

            if(!isset($data[$this->name])){
                return false;
            }
            $data = $data[$this->name];

            $testName = "code";
            if((!isset($data[$testName]))||(!trim($data[$testName]))){
                $this->validationErrors[$testName]= "empty";
                break;
            }

            $data[$testName] = trim($data[$testName]);
            if($this->isExist($data[$testName])){
                $this->validationErrors[$testName]= "exist";
                break;
            }


            $testName = "name";
            if((!isset($data[$testName]))||(!trim($data[$testName]))){
                $this->validationErrors[$testName]= "empty";
                break;
            }

            $testName = "new_locale_icon";
            if((!isset($_FILES[$testName]))||(!is_file($_FILES[$testName]["tmp_name"]))){
                $this->validationErrors[$testName]= "require";
                break;
            }

            break;
        }
        return empty($this->validationErrors);
    }

    public function saveAndGetId($data){
        $this->save($data);
        return $this->id;
    }
}