<script src="/js/libs/in/ExportCSV.js?rndkey=<?=VERSION_RND_KEY;?>" type="text/javascript"></script>
<? $this->layout="emptyPage";?>
<div class="content">
    <div class="quiz-name">
        <span class="text"><?=$LocaleStrings->page->quizName;?> \> <span><?=((isset($quizzList[0]))?$quizzList[0]->name:$LocaleStrings->page->quizz->nofound);?></span></span>
        <a href="/" style="color:#000000;display: inline-block;padding-left: 40px;" id="export_CSV_link">CSV - Export</a>
    </div>
    <div class="report-table">
        <table id="reportTable">
            <tr class="report-row report-title" id="columnsNames">
                <td class="report-cell date"><div><?=$LocaleStrings->page->date;?></div></td>
                <td class="report-cell location"><div><?=$LocaleStrings->page->locationName;?></div></td>
                <? if(isset($questionObjList[0] ) ) {
                    foreach($questionObjList as $question){
                        echo '<td class="report-cell question"><div>'.$question["name"].'</div></td>';
                    }
                } ?>
            </tr>
            <?
                if(count($reports)>0){
                    foreach($reports as $quizz=>$list){
                        if(!empty($list))
                            foreach($list as $report){
                                $locationName = "";
                                foreach($locationObjList as $locationObj){
                                    if($locationObj->objectId==$report->location){
                                        $locationName = $locationObj->name;
                                        break;
                                    }
                                }
                                echo'<tr class="report-row report-content">';
                                echo'<td class="report-cell date"><div>'.date("m/d/y h:i a",$this->Time->fromString($report->date->iso)).'</div></td>';
                                echo'<td class="report-cell location"><div>'.$locationName.'</div></td>';
                                $gradeLabels = array("-100","-50","0","50","100");
                                foreach($questionObjList as $questionObj){
                                    $displayData=null;
                                    foreach($report->answers as $answer){
                                        if((isset($answer->question))&&($answer->question==$questionObj["objectId"])){
                                            $displayData = $answer;
                                            break;
                                        }
                                    }
                                    $qHtml["grade"] = ($displayData)?$gradeLabels[$displayData->grade]:"";
                                    $qHtml["priority"] = ($displayData)?(1+$displayData->priority):"";
                                    ?>
                                        <td class="report-cell question">
                                            <? if($displayData) { ?>
                                            <table class="questionDetails">
                                                <tr><td class="q-title">grade:</td><td width="20"></td><td class="q-data"><?=$qHtml["grade"];?>%</td></tr>
                                                <tr><td class="q-title">priority:</td><td></td><td class="q-data"><?=$qHtml["priority"];?></td></tr>
                                            </table>
                                            <? } else {
                                            echo '<div class="empty">empty</div>';
                                        } ?>
                                        </td>
                                    <?
                                }
                                echo '</tr>';
                            }
                    }
                }
            ?>
        </table>
    </div>
</div>