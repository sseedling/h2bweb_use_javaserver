<script src="/js/libs/in/ExportCSV.js?rndkey=<?=VERSION_RND_KEY;?>" type="text/javascript"></script>
<? $this->layout="emptyPage";?>
<div class="content">
    <div class="quiz-name">
        <span class="text"><?=$LocaleStrings->page->quizName;?> \> <span><?=((isset($quizzList[0]))?$quizzList[0]->name:$LocaleStrings->page->quizz->nofound);?></span></span>
        <a href="/" style="color:#000000;display: inline-block;padding-left: 40px;" id="export_CSV_link">CSV - Export</a>
    </div>
    <div class="report-table ">
        <table id="reportTable">
            <tr class="report-row report-title">
                <td class="report-cell reportInfoTable"><div><?=$LocaleStrings->page->reportInfo;?></div></td>
                <? if(isset($questionObjList[0] ) ) {
                    foreach($questionObjList as $question){
                        echo '<td class="report-cell question"><div>'.$question["name"].'</div></td>';
                    }
                } ?>
            </tr>
            <?
                if(count($reports)>0){
                    foreach($reports as $quizz=>$list){
                        if(!empty($list)){
                            foreach($list as $report){
                                $locationName = "";
                                foreach($locationObjList as $locationObj){
                                    if($locationObj->objectId==$report->location){
                                        $locationName = $locationObj->name;
                                        break;
                                    }
                                } ?>
                                <tr class="report-row report-content">
                                    <td class="report-cell reportInfo">
                                        <table class="questionDetails">
                                            <tr><td class="q-title">email:</td><td width="20"></td><td class="q-data"><?=@$report->email;?></td></tr>
                                            <tr><td class="q-title">date:</td><td></td><td class="q-data"><?=date("m/d/y h:i a",$this->Time->fromString($report->date->iso));?></td></tr>
                                            <tr><td class="q-title">location:</td><td></td><td class="q-data"><?=$locationName;?></td></tr>
                                        </table>
                                    </td>
                                    <?
                                    if(!empty($questionObjList)){
                                    foreach($questionObjList as $questionObj) {
                                        $displayData=null;
                                        foreach($report->answers as $answer){
                                            if((isset($answer->question))&&($answer->question==$questionObj["objectId"])){
                                                $displayData = $answer;
                                                break;
                                            }
                                        }
                                        $gradeLabels = array("-100","-50","0","50","100");

                                        $qHtml["grade"] = isset($displayData->grade)?$gradeLabels[$displayData->grade]:"";
                                        $qHtml["priority"] = isset($displayData->priority)?(1+$displayData->priority):"";
                                        $qHtml["msg"] = isset($displayData->details->note)?$displayData->details->note:"";
                                        $qHtml["subquestions"] = "";
                                        if(isset($displayData->details->subAnswers))
                                            foreach($displayData->details->subAnswers as $k=>$isCheck){
                                                if($isCheck){
                                                    $qHtml["subquestions"] .=$qHtml["subquestions"]==""?"":",";
                                                    if(isset($questionObj["subQuestions"][$k])){
                                                        $qHtml["subquestions"] .= $questionObj["subQuestions"][$k];
                                                    }
                                                    else{
                                                        $qHtml["subquestions"] .= "R-E-M-O-V-E-D";
                                                    }
                                                }
                                            }
                                        else $qHtml["subquestions"] = json_encode($displayData);
                                        ?>
                                        <td class="report-cell question">
                                            <? if($displayData) { ?>
                                            <table class="questionDetails">
                                                <tr><td class="q-title">grade:</td><td width="20"></td><td class="q-data"><?=$qHtml["grade"];?>%</td></tr>
                                                <tr><td class="q-title">priority:</td><td></td><td class="q-data"><?=$qHtml["priority"];?></td></tr>
                                                <tr><td class="q-title">subquestions:</td><td></td><td class="q-data"><?=$qHtml["subquestions"];?></td></tr>
                                                <tr><td class="q-title msg-title">message:</td><td></td><td class="q-data msg-body" ><span><?=$qHtml["msg"];?></span></td></tr>
                                            </table>
                                            <? } else {
                                                echo '<div class="empty">empty</div>';
                                            } ?>
                                        </td>
                                    <? } }?>
                                </tr>
                            <? }
                        }
                    }
                }
            ?>
        </table>
    </div>
</div>