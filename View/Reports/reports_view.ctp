<?=$this->element("report",array("LocaleStrings"=>$LocaleStrings));?>

<link rel="stylesheet" type="text/css" href="/css/ui-darkness/jquery-ui.min.css?rndkey=<?=VERSION_RND_KEY;?>">
<script src="/js/libs/out/jquery-ui.min.js?rndkey=<?=VERSION_RND_KEY;?>" type="text/javascript"></script>
<script src="/js/libs/out/jspdf.min.js?rndkey=<?=VERSION_RND_KEY;?>" type="text/javascript"></script>
<script type="text/javascript" src="http://canvg.googlecode.com/svn/trunk/rgbcolor.js?rnd=<?=time();?>"></script>
<script type="text/javascript" src="http://canvg.googlecode.com/svn/trunk/StackBlur.js?rnd=<?=time();?>"></script>
<script type="text/javascript" src="http://canvg.googlecode.com/svn/trunk/canvg.js?rnd=<?=time();?>"></script>
<script src="/js/libs/in/ExporterPdf.js?rndkey=<?=VERSION_RND_KEY;?>" type="text/javascript"></script>

<script type="text/javascript" src="//www.google.com/jsapi?rnd=<?=time();?>"></script>
<script type="text/javascript">
    google.load( 'visualization' , '1.1' , {
        packages   : [ 'corechart' , 'controls' ]
        , language : 'es'
    } );
</script>

<div class="pageMenu-firstPart">
    <input type="file" id="testFileInput" style="display:none;" />
    <div class="btn btn-short exportBtn disabled" mode="save" ><?=$LocaleStrings->page->btns->export;?></div>
</div>
<div class="chartControls">
    <div class="chart-tools-menu">
        <div class="chart-types">
            <div class="chart-type-btn" data-type-of-chart="classic"><i></i><span><?=$LocaleStrings->page->chart->type->classic->name;?></span><div class="arrow"></div></div>
            <div class="chart-type-btn active" data-type-of-chart="weather"><i></i><span><?=$LocaleStrings->page->chart->type->weather->name;?></span><div class="arrow"></div></div>
        </div>
        <h2><?=$LocaleStrings->page->tools->title;?></h2>
        <div class="tools-title"><?=$LocaleStrings->page->tools->help;?></div>
    </div>
    <div class="chart-tool-preloader preloader"></div>
    <div class="create-chart-of-type">
        <div data-type-of-chart="classic" class="cols tools-cols">
            <div class="selectLists">
                <div class="locationsList">
                    <div class="row-items-list data" id="loc-list">
                        <div class="dropShadow"></div>
                        <div class="tools-row-item oneLocationItem active" id="loc-all">
                            <div class="row-item-cell"><?=$LocaleStrings->page->chart->type->weather->locationList;?><div class="selectedIco"></div><div class="selectArrow"></div></div>
                        </div>
                        <?
                        if( isset( $allLocations[0] ) ) {
                            $allLocationsId = array();
                            foreach( $allLocations as $k => $location ) {
                                $allLocationsId[] = $location -> objectId;
                                $allLocationsData[ $location -> objectId ] = array();
                                $allLocationsData[ $location -> objectId ][ 'name' ] = $location -> name;
                                $allLocationsData[ $location -> objectId ][ 'quizzes' ] = array();
                                if( isset( $location -> quizzes[0] ) )
                                    foreach( $location -> quizzes as $quizz ){
                                        $allLocationsData[ $location -> objectId ][ 'quizzes' ][] = $quizz -> objectId;
                                    }

                                echo '<div class="tools-row-item oneLocationItem" data-id="' . $location -> objectId . '" id="location-' . $location -> objectId . '"><div class="row-item-cell">' . $location -> name . '</div><div class="selectedIco"></div><div class="selectArrow"></div></div>';
                            }
                            echo '<script>MainPage.allLocationsId = ' . json_encode( $allLocationsId ) . ';MainPage.locations = ' . json_encode( $allLocationsData ). '</script>';
                        }
                        ?>
                    </div>
                </div>
                <div class="placeholder"><?=$LocaleStrings->page->chart->type->weather->quizzes->empty;?></div>
                <div class="quizzesList">
                    <div class="row-items-list data" id="quiz-list">
                        <?
                        if( isset( $quizzes[0] ) ) {
                            foreach( $quizzes as $k => $quizz ) {
                                $allQuizzesId[ $quizz -> objectId ] = array();
                                $allQuizzesId[ $quizz -> objectId ][ 'name' ] = $quizz -> name;
                                $allQuizzesId[ $quizz -> objectId ][ 'locations' ] = array();
                                foreach( $allLocations as $location ) {
                                    if( isset( $location -> quizzes[0] ) )
                                        foreach( $location -> quizzes as $Lquizz )
                                            if( $Lquizz -> objectId == $quizz -> objectId ){
                                                $allQuizzesId[ $quizz -> objectId ][ 'locations' ][] = $location -> objectId;
                                            }
                                }
                                echo '<div class="tools-row-item oneQuizzItem" data-id="' . $quizz -> objectId . '" id="quiz-' . $quizz -> objectId . '"><div class="row-item-cell">' . $quizz -> name . '</div><div class="selectedIco"><i></i></div></div>';
                            }
                            echo '<script>MainPage.quizzes = ' . json_encode($allQuizzesId). ';</script>';
                        }
                        ?>
                    </div>
                </div>
            </div>
            <div class="selectedResalts">
                <div class="row-items-list data" id="result-loc-list"></div>
                <div class="showMore"><span><i>31</i> <?=$LocaleStrings->page->chart->type->weather->locationsCountForQuiz;?></span></div>
                <div class="date-selectors">
                    <div><?=$LocaleStrings->page->chart->type->weather->calendar->from;?> <input class="text-field date-field minDate" readonly placeholder="Select" autocomplete="off"></div>
                    <div><?=$LocaleStrings->page->chart->type->weather->calendar->to;?> <input class="text-field date-field maxDate" readonly placeholder="Select" autocomplete="off"></div>
                </div>
                <div class="actionsBtns">
                    <div class="btn btn-short create saveBtn disabled"><?=$LocaleStrings->page->chart->type->weather->btn->create;?></div>
                    <div class="btn btn-short btn-grow cancelBtn previewBtn disabled">Preview</div>
                </div>
            </div>
        </div data-type>
        <div data-type-of-chart="weather" class="cols tools-cols">
            <div class="selectLists">
                <div class="locationsList">
                    <div class="row-items-list data" id="loc-list">
                        <div class="dropShadow"></div>
                        <div class="tools-row-item oneLocationItem active" id="loc-all">
                            <div class="row-item-cell"><?=$LocaleStrings->page->chart->type->weather->locationList;?><div class="selectedIco"></div><div class="selectArrow"></div></div>
                        </div>
                        <?
                        if( isset( $allLocations[0] ) ) {
                            $allLocationsId = array();
                            foreach( $allLocations as $k => $location ) {
                                $allLocationsId[] = $location -> objectId;
                                $allLocationsData[ $location -> objectId ] = array();
                                $allLocationsData[ $location -> objectId ][ 'name' ] = $location -> name;
                                $allLocationsData[ $location -> objectId ][ 'quizzes' ] = array();
                                if( isset( $location -> quizzes[0] ) )
                                    foreach( $location -> quizzes as $quizz ){
                                        $allLocationsData[ $location -> objectId ][ 'quizzes' ][] = $quizz -> objectId;
                                    }

                                echo '<div class="tools-row-item oneLocationItem" data-id="' . $location -> objectId . '" id="location-' . $location -> objectId . '"><div class="row-item-cell">' . $location -> name . '</div><div class="selectedIco"></div><div class="selectArrow"></div></div>';
                            }
                            echo '<script>MainPage.allLocationsId = ' . json_encode( $allLocationsId ) . ';MainPage.locations = ' . json_encode( $allLocationsData ). '</script>';
                        }
                        ?>
                    </div>
                </div>
                <div class="placeholder"><?=$LocaleStrings->page->chart->type->weather->quizzes->empty;?></div>
                <div class="quizzesList">
                    <div class="row-items-list data" id="quiz-list">
                        <?
                        if( isset( $quizzes[0] ) ) {
                            foreach( $quizzes as $k => $quizz ) {
                                $allQuizzesId[ $quizz -> objectId ] = array();
                                $allQuizzesId[ $quizz -> objectId ][ 'name' ] = $quizz -> name;
                                $allQuizzesId[ $quizz -> objectId ][ 'locations' ] = array();
                                foreach( $allLocations as $location ) {
                                    if( isset( $location -> quizzes[0] ) )
                                        foreach( $location -> quizzes as $Lquizz )
                                            if( $Lquizz -> objectId == $quizz -> objectId ){
                                                $allQuizzesId[ $quizz -> objectId ][ 'locations' ][] = $location -> objectId;
                                            }
                                }
                                echo '<div class="tools-row-item oneQuizzItem" data-id="' . $quizz -> objectId . '" id="quiz-' . $quizz -> objectId . '"><div class="row-item-cell">' . $quizz -> name . '</div><div class="selectedIco"><i></i></div></div>';
                            }
                            echo '<script>MainPage.quizzes = ' . json_encode($allQuizzesId). ';</script>';
                        }
                        ?>
                    </div>
                </div>
            </div>
            <div class="selectedResalts">
                <div class="row-items-list data" id="result-loc-list"></div>
                <div class="showMore"><span><i>31</i> <?=$LocaleStrings->page->chart->type->weather->locationsCountForQuiz;?></span></div>
                <div class="date-selectors">
                    <div><?=$LocaleStrings->page->chart->type->weather->calendar->from;?> <input class="text-field date-field minDate" readonly placeholder="Select" autocomplete="off"></div>
                    <div><?=$LocaleStrings->page->chart->type->weather->calendar->to;?> <input class="text-field date-field maxDate" readonly placeholder="Select" autocomplete="off"></div>
                </div>
                <div class="actionsBtns">
                    <div class="btn btn-short create saveBtn disabled"><?=$LocaleStrings->page->chart->type->weather->btn->create;?></div>
                    <div class="btn btn-short btn-grow clear close cancelBtn"><?=$LocaleStrings->page->chart->type->weather->btn->cancel;?></div>
                </div>
            </div>
        </div>
        <div class="clear-fix"></div>
    </div>
    <div class="charts-list"></div>

    <div class="controll">
    </div>
    <div class="clear-fix"></div>
</div>

<div class="hidden" id="exemplarResultLocation">
    <div><div class="tools-row-item-cell"><div class="icon over-right"></div><span></span></div></div>
</div>