<?=$this->element("quiz",array("LocaleStrings"=>$LocaleStrings,"Company"=>$Company));?>

<div class="pageMenu-secondPart">
</div>
<div class="pageMenu-firstPart">
    <div class="ico-edit-white"></div>
    <?=((isset($errorMsg))?$this->element("printMsg",array("msg"=>$errorMsg)):"");?>
    <div class="btn btn-short saveBtn"><?=$LocaleStrings->page->btns->save;?></div>
    <a href="/locations/<?=$locationUrl;?>/" class="btn btn-short btn-blue cancelBtn"><?=$LocaleStrings->page->btns->cancel;?></a>
</div>
<form method="post" class="itemsForm">
    <div class="hidden">
        <input type="hidden" name="status" class="survey-status" value="unpublish" />
    </div>
    <div class="survey-details" id="survey-details">
        <div class="item-row">
            <div class="column1 item-title">
                <span><?=$LocaleStrings->page->form->SurveyInfo->name->label;?></span>
            </div>
            <div class="column2 item-value">
                <div>
                    <input maxlength="<?=$Company->limits->inputs->survey["name"];?>" type="text" name="name" class="text-field survey-name" value="<?=@$postData["name"]?>" placeholder="<?=@$LocaleStrings->page->Label->surveyName->placeholder;?>">
                </div>
                <div>
                    <textarea maxlength="<?=$Company->limits->inputs->survey["note"];?>" class="survey-note text-field" placeholder="<?=@$LocaleStrings->page->Label->note->placeholder;?>" name="note"><?=@$postData["note"]?></textarea>
                </div>
            </div>
            <div class="column3 item-help">
                <? if(isset($postData["error"]["name"]))echo '<div class="popover">'.@$LocaleStrings->page->form->errors->name[$postData["error"]["name"]].'</div>';?>
            </div>
        </div>
        <div class="item-row">
            <div class="column1 item-title">
                <span><?=$LocaleStrings->page->form->SurveyInfo->status->label;?></span>
            </div>
            <div class="column2 item-value">
                <div class="status-box" id="status-box">
                    <script>
                        function initParts() {
                            MainPage.statusTextOn = '<?=$LocaleStrings->page->btns->status->publish;?>';
                            MainPage.statusTextOff = '<?=$LocaleStrings->page->btns->status->unpublish;?>';
                            QuizLocales.appLocales = <?= json_encode( $appLocales ); ?> || {};
                            QuizLocales.allQuestions = <?= json_encode( @$postData['questions'] ); ?> || {};
                        }
                    </script>
                    <div class="status-switch-info">
                        <table>
                            <tr>
                                <td><?=$LocaleStrings->page->btns->status->info;?></td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
            <div class="column3 item-help">
            </div>
        </div>
    </div>

    <h2><?=$LocaleStrings->page->Label->questions->title;?></h2>

    <div class="lang-tabs">
        <div class="col" id="locales"></div><div class="col"><div class="tab" data-tab="add"><i class="icon icon-earth-plus"></i></div></div>
    </div>

    <div class="data questionsList">
        <div class="tab-content" id="tab-content-add">
            <div class="item-row">
                <div class="column1 item-title"></div>
                <div class="column2 item-value">
                    <div class="section-title"><?=$LocaleStrings->page->Label->defaultLang->placeholder;?></div>
                </div>
            </div>
            <div class="item-row">
                <div class="column1 item-title"></div>
                <div class="column2 item-value">
                    <div class="rows"></div>
                </div>
            </div>
        </div>
    </div>

    <div class="bottomBtns">
        <div class="btn btn-short saveBtn"><?=@$LocaleStrings->page->btns->save;?></div>
        <a href="/locations/<?=$locationUrl;?>/" class="btn btn-short btn-grow cancelBtn"><?=@$LocaleStrings->page->btns->cancel;?></a>
    </div>
</form>