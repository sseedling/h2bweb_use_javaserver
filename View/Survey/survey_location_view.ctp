<div class="pageMenu-secondPart">
    <div class="survey-details">
        <div class="survey-description">
<!--            <h4>--><?//=$LocaleStrings->page->note->title;?><!--</h4>-->
            <p><?=(isset($survey->note))?$survey->note:$LocaleStrings->page->note->empty;?></p>
        </div>
        <div class="survey-other-info">
            <div class="survey-modfied">
                <h4><?=$LocaleStrings->page->modified->title;?></h4>
                <p><?=strtolower(date("j F Y, g:i a",$this->Time->fromString(@$survey->updatedAt)));?></p>
            </div>
        </div>
    </div>
    <div class="lang-tabs">
        <? foreach($appLocales as $locale=>$data) if( isset( $allQuestions[ $locale ] ) ) { ?><div class="tab initialized" data-tab="<?= $locale; ?>"><img alt="<?= $locale; ?>" src="/img/flags/<?= $locale; ?>.png" class="flag flag-<?= $locale; ?>" /><span class="text"><?= $locale; ?></span></div><?}?>
    </div>
</div>
<div class="pageMenu-firstPart">
    <? if(Auth::inRoles(array("QuizzesW"))){?>
       <a href="edit/" class="btn btn-ico btn-blue editBtn"><div class="ico ico-edit"></div></a>
       <? if($allQuestions){ ?>
            <a href="edit/<?=($survey->status=="publish")?"unpublish":"publish";?>/" class="btn btn-short btn-ico-right surveyStatusBtn <?=$survey->status;?>" hover-on="<?=$LocaleStrings->page->btns->status->hover;?>" hover-off="<?=$LocaleStrings->page->btns->status->published;?>">
                <span><?=($survey->status=="publish")?$LocaleStrings->page->btns->status->published:$LocaleStrings->page->btns->status->publish;?></span>
                <div class="ico ico-<?=$survey->status;?>"></div>
            </a>
       <? } else{ ?>
            <div class="survey-status-label"><?=($survey->status=="publish")?$LocaleStrings->page->btns->status->published:$LocaleStrings->page->btns->status->publish;?></div>
       <? }
    }else{?>
        <div class="survey-status-label"><?=($survey->status=="publish")?$LocaleStrings->page->btns->status->published:$LocaleStrings->page->btns->status->publish;?></div>
    <?}?>
</div>
<div class="data questionsList">
    <h2><?=$LocaleStrings->page->questionsList;?></h2>
    <?
        if($allQuestions) {
            foreach($allQuestions as $lang=>$questions) {
                echo '<ul id="tab-content-'.$lang.'" class="question-lang-list" >';
                foreach($questions as $k=>$question) {
                    echo '<li class="row-item"><strong>'.$question["name"].'.</strong>' . ( $question["text"] ? '<br />'.$question["text"] : '' ) . ( isset( $question["subQuestions"][0] ) ? '<br /><div class="sub-questions">' . implode( ', ' , $question["subQuestions"] ) . '</div>' : '' ) . '</li>';
                }
                echo '</ul>';
            }
        }
        else{
            echo "<div class='placeholder' style='display: block'>".$LocaleStrings->page->placeholder->noQuizes."</div>";
        }
    ?>
</div>