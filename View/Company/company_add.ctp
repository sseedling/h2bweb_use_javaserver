<div class="pageMenu-firstPart">
    <div class="ico-edit-white"></div>
    <div class="btn btn-short saveBtn"><?=$LocaleStrings->page->btn->save;?></div>
    <a href="/companies/" class="btn btn-short btn-blue cancelBtn"><?=$LocaleStrings->page->btn->cancel;?></a>
</div>

<form method="post" class="itemsForm" id="form" enctype="multipart/form-data">
    <div class="item-row">
        <div class="column1 item-title"></div>
        <div class="column2 item-value">
            <div class="section-title"><?=$LocaleStrings->page->form->companyData->title;?></div>
        </div>
        <div class="column3 item-help"></div>
    </div>
    <div class="item-row">
        <div class="column1 item-title">
            <span><?=$LocaleStrings->page->form->companyData->name->title;?></span>
        </div>
        <div class="column2 item-value">
            <div>
                <input class="text-field" maxlength="64" name="company[name]" value="<?=@$postData["company"]["name"];?>" placeholder="<?=$LocaleStrings->page->form->companyData->name->placeholder;?>" autocomplete="off" />
            </div>
        </div>
        <div class="column3 item-help">
            <? if(isset($postData["company"]["error"]["name"])){
                echo '<div class="popover">'.@$LocaleStrings->page->form->errors->company->name->$postData["company"]["error"]["name"].'</div>';
            } ?>
        </div>
    </div>
    <div class="item-row">
        <div class="column1 item-title">
            <span><?=$LocaleStrings->page->form->companyData->credits->title;?></span>
        </div>
        <div class="column2 item-value">
            <div class="creditsBlock">
                <input class="text-field" name="company[credits]" placeholder="0" value="10" prevValue="10"  autocomplete="off" />
                <b class="btnCredits btnGradient" data-action="plus">+</b>
                <b class="btnCredits btnGradient" data-action="minus">&mdash;</b>
            </div>
        </div>
        <div class="column3 item-help">
            <? if(isset($postData["error"]["credits"])){
                echo '<div class="popover">'.@$LocaleStrings->page->form->errors->credits->$postData["error"]["credits"].'</div>';
            } ?>
        </div>
    </div>
    <div class="item-row">
        <div class="column1 item-title">
            <span><?=$LocaleStrings->page->form->trusted->title;?></span>
        </div>
        <div class="column2 item-value">
            <div>
                <div class="status-box" data-trusted-text="<?=$LocaleStrings->page->form->trusted->true;?>" data-usual-text="<?=$LocaleStrings->page->form->trusted->false;?>">
                    <input type="hidden" name="company[trusted]" value="<?= (@$postData["company"]["trusted"]?$LocaleStrings->page->form->trusted->true:$LocaleStrings->page->form->trusted->false); ?>">
                </div>
            </div>
        </div>
        <div class="column3 item-help"><?=$LocaleStrings->page->form->trusted->help;?></div>
    </div>
    <div class="item-row">
        <div class="column1 item-title">
            <span><?=$LocaleStrings->page->form->ownerEmail->title;?></span>
        </div>
        <div class="column2 item-value">
            <div>
                <input
                    type="email"
                    class="text-field"
                    data-owner-id="null"
                    maxlength="64"
                    name="owner[email]"
                    placeholder="owner@company.com"
                    autocomplete="off"
                    value="<?=@$postData["owner"]["email"];?>"
                    />
            </div>
            <div class="btn-block btn-big btnGradient check-email"><?=$LocaleStrings->page->form->ownerEmail->btn;?></div>
        </div>
        <div class="column3 item-help">
            <? if(isset($postData["owner"]["error"]["email"])){
            echo '<div class="popover">'.@$LocaleStrings->page->form->errors->owner->email->$postData["owner"]["error"]["email"].'</div>';
        } ?>
            <? if(isset($postData["owner"]["error"]["username"])){
            echo '<div class="popover">'.@$LocaleStrings->page->form->errors->owner->username->$postData["owner"]["error"]["username"].'</div>';
        } ?>
        </div>
    </div>
    <div class="item-row">
        <div class="column1 item-title">
            <span><?=$LocaleStrings->page->form->comment->title;?></span>
        </div>
        <div class="column2 item-value">
            <div>
                <textarea class="text-field" rows="3" autocomplete="off" maxlength="128" name="company[comment]"></textarea>
            </div>
        </div>
        <div class="column3 item-help"></div>
    </div>

    <div class="bottomBtns">
        <div class="btn btn-short saveBtn"><?=$LocaleStrings->page->btn->save;?></div>
        <a href="/companies/" class="btn btn-short btn-grow cancelBtn"><?=$LocaleStrings->page->btn->cancel;?></a>
    </div>
</form>