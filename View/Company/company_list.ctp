<div class="pageMenu-firstPart">
    <a href="/companies/add/" class="btn big-size over-right createBtn" title="<?=$LocaleStrings->page->addCompany;?>">+</a>
</div>
<div class="cols">
    <div class="shadow-block"></div>
    <div class="col filters">
        <div class="rows">
            <div class="row row-item active" data-filter=""><div class="row-title"><span><?=$LocaleStrings->page->filter->all;?></span><div class="overlap"></div></div></div>
            <div class="row row-item" data-filter="10"><div class="row-title"><span><?=$LocaleStrings->page->filter->credits;?> <= 10</span><div class="overlap"></div></div></div>
            <div class="row row-item" data-filter="0"><div class="row-title"><span><?=$LocaleStrings->page->filter->disabled;?></span><div class="overlap"></div></div></div>
        </div>
    </div><div class="col content-list">
        <div class="empty placeholder"><?=$LocaleStrings->page->noCompany;?></div>
        <div class="rows">
            <? if( !empty( $companiesList ) ) foreach( $companiesList as $companyId => $company ) { ?>
                <a href="/companies/edit/<?=$companyId;?>/" class="row row-item" data-count="<?= $company[ 'credits' ]; ?>">
                    <div class="row-status<?=($company[ 'trusted' ])?" trusted":"";?>" title="<?=$LocaleStrings->page->company->trusted;?>"></div>
                    <div class="row-count"><?= $company[ 'credits' ]; ?></div>
                    <div class="hover-icon icon icon-edit-gray"></div>
                    <div class="row-title">
                        <span><?=$company[ 'name' ];?></span>
                        <div class="overlap"></div>
                    </div>
                </a>
            <? } ?>
        </div>
    </div>
    <div class="clear-fix"></div>
</div>