<div class="pageMenu-firstPart">
    <div class="ico-edit-white"></div>
    <div class="btn btn-short saveBtn"><?=$LocaleStrings->page->btn->save;?></div>
    <a href="/companies/" class="btn btn-short btn-blue cancelBtn"><?=$LocaleStrings->page->btn->cancel;?></a>
</div>

<div class="itemsForm confirm-form" id="confirm-form">
    <div class="item-row">
        <div class="column1 item-title">
            <span><?=@$LocaleStrings->page->confirm->title;?></span>
        </div>
        <div class="column2 item-value">
            <input class="text-field" maxlength="64" name="password" type="password" placeholder="<?=@$LocaleStrings->page->confirm->password->placeholder;?>" value="" autocomplete="off" />
        </div>
        <div class="column3 item-help">
            <div class="btn-block btn-big btnGradient" id="confirm"><?=@$LocaleStrings->page->confirm->btn;?></div>
        </div>
    </div>
</div>
<form method="post" class="itemsForm" id="form" enctype="multipart/form-data" data-company-id="<?= $company[ 'objectId' ]; ?>">
    <div class="full-overlap"></div>
    <div class="item-row">
        <div class="column1 item-title"></div>
        <div class="column2 item-value">
            <div class="section-title"><?=$LocaleStrings->page->form->companyData->title;?></div>
        </div>
        <div class="column3 item-help"></div>
    </div>
    <div class="item-row">
        <div class="column1 item-title">
            <span><?=$LocaleStrings->page->form->companyData->name->title;?></span>
        </div>
        <div class="column2 item-value">
            <div>
                <input class="text-field" maxlength="64" name="company[name]" placeholder="<?=$LocaleStrings->page->form->companyData->name->placeholder;?>" value="<?= $company[ 'name' ]; ?>" autocomplete="off" />
            </div>
        </div>
        <div class="column3 item-help">
            <? if(isset($postData["company"]["error"]["name"])){
                echo '<div class="popover">'.@$LocaleStrings->page->form->errors->company->name->$postData["company"]["error"]["name"].'</div>';
            } ?>
        </div>
    </div>
    <div class="item-row">
        <div class="column1 item-title">
            <span><?=$LocaleStrings->page->form->companyData->credits->title;?></span>
        </div>
        <div class="column2 item-value">
            <div class="creditsBlock">
                <input class="text-field" name="company[credits]" placeholder="0" value="<?= $company[ 'credits' ]; ?>" prevValue="<?= $company[ 'credits' ]; ?>" autocomplete="off" />
                <b class="btnCredits btnGradient" data-action="plus">+</b>
                <b class="btnCredits btnGradient" data-action="minus">&mdash;</b>
            </div>
        </div>
        <div class="column3 item-help">
            <? if(isset($postData["error"]["credits"])){
                echo '<div class="popover">'.@$LocaleStrings->page->form->errors->credits->$postData["error"]["credits"].'</div>';
            } ?>
        </div>
    </div>
    <div class="item-row">
        <div class="column1 item-title">
            <span><?=$LocaleStrings->page->form->trusted->title;?></span>
        </div>
        <div class="column2 item-value">
            <div>
                <div class="status-box" data-trusted-text="<?=$LocaleStrings->page->form->trusted->true;?>" data-usual-text="<?=$LocaleStrings->page->form->trusted->false;?>">
                    <input type="hidden" name="company[trusted]" value="<?=((@$company["trusted"])?'true':'false');?>">
                </div>
            </div>
        </div>
        <div class="column3 item-help"><?=$LocaleStrings->page->form->trusted->help;?></div>
    </div>
    <div class="item-row">
        <div class="column1 item-title">
            <span><?=$LocaleStrings->page->form->ownerEmail->title;?></span>
        </div>
        <div class="column2 item-value">
            <div>
                <input
                    type="email"
                    class="text-field"
                    data-owner-id="<?= ($company[ 'ownerId' ]?$company[ 'ownerId' ]:'null'); ?>"
                    maxlength="64"
                    name="owner[email]"
                    placeholder="owner@company.com"
                    value="<?= $owner[ 'email' ]; ?>"
                    autocomplete="off"
                    />
                <input type="hidden" class="text-field" maxlength="64" name="owner[objectId]" value="<?= $company[ 'ownerId' ]; ?>" />
            </div>
            <div class="btn-block btn-big btnGradient check-email"><?=$LocaleStrings->page->form->ownerEmail->btn;?></div>
        </div>
        <div class="column3 item-help">
            <? if(isset($postData["owner"]["error"]["email"])){
            echo '<div class="popover">'.@$LocaleStrings->page->form->errors->owner->email->$postData["owner"]["error"]["email"].'</div>';
        } ?>
            <? if(isset($postData["owner"]["error"]["username"])){
            echo '<div class="popover">'.@$LocaleStrings->page->form->errors->owner->username->$postData["owner"]["error"]["username"].'</div>';
        } ?>
            <div class="generate-new-password" data-owner-id="<?= $owner[ 'objectId' ]; ?>" data-on-generate="<?=$LocaleStrings->page->form->ownerEmail->password->onSuccess;?>"><?=$LocaleStrings->page->form->ownerEmail->password->text;?></div>
        </div>
    </div>
    <div class="item-row">
        <div class="column1 item-title">
            <span><?=$LocaleStrings->page->form->comment->title;?></span>
        </div>
        <div class="column2 item-value">
            <div>
                <textarea class="text-field" rows="3" autocomplete="off" maxlength="128" name="company[comment]"><?= $company[ 'comment' ]; ?></textarea>
            </div>
        </div>
        <div class="column3 item-help"></div>
    </div>

    <div class="item-row">
        <div class="column1 item-title">
        </div>
        <div class="column2 item-value">
            <div class="btn-block btn-big btnGradient remove-company">REMOVE COMPANY</div>
        </div>
        <div class="column3 item-help"></div>
    </div>

    <div class="bottomBtns">
        <div class="btn btn-short saveBtn"><?=$LocaleStrings->page->btn->save;?></div>
        <a href="/companies/" class="btn btn-short btn-grow cancelBtn"><?=$LocaleStrings->page->btn->cancel;?></a>
    </div>
</form>