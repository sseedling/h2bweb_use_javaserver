<!DOCTYPE html>
<html>
    <head>
        <?=$this->element('headerIncludes');?>
    </head>
    <body>
        <!-- LOG -->
        <div id="logBox"></div>

        <div id="topGradientBlock"></div>

        <? if(Auth::inRoles(array(Roles::$authorizeRole))){ ?>
            <div id="pageMenu">
                <div class="firstPart">
                    <?=$this->element('title',array('tileObj'=>((isset($LocaleStrings->page->title))?$LocaleStrings->page->title:"Server internal error."),"mode"=>"breadcrumbs"));?>
                </div>
                <div class="secondPart"></div>
            </div>
        <? } ?>

        <!-- IMPORT TOP MENU -->
        <? if(Auth::inRoles(array(Roles::$authorizeRole))){ ?>
            <div id="topMenu">
                <?=$this->element('topMenu');?>
            </div>
        <? } ?>

        <!-- IMPORT LEFT MENU -->
        <? if(Auth::inRoles(array(Roles::$authorizeRole))){ ?>
            <div id="leftMenu">
                <?=$this->element('leftMenu');?>
            </div>
        <? } ?>

        <!-- IMPORT SELECTED VIEW -->
        <div id="mainContainer" class="container">
            <div id="contentBgBlock"></div>
            <div id="middleContent">
                <?=$this->fetch('content');?>
            </div>
        </div>
        <!--  DEFAULT CSS-->
        <link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700">
    </body>
</html>
