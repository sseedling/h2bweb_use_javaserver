<div class="topMenu-items">
    <div class="topMenu-item logo <?=($this->request->params["action"]=="users_profile")?"active":"";?>">
        <a href="/" class="projectLogo"><?=$Company->parseObj->name;?></a>
    </div>
    <? if(Auth::inRoles(array("Owner"))) {
        echo '<div class="topMenu-item credits '.(($this->request->params["action"]=="users_profile")?"active":"").'">';
        echo '<a href="mailto:'.$Company->superAdmin["email"].'" class="creditsNotify"><span class="creditsCount">'.$LocaleStrings->topMenu->credits->before.': <span>'.$Company->parseObj->credits.'</span></span><br /><span class="info">'.$LocaleStrings->topMenu->credits->after.'</span></a>';
        echo '</div>';
    } ?>
    <div class="topMenu-item logOut"><a href="/logout/"></a></div>
    <div class="topMenu-item helpFile">
        <a href="/files/H4Happy.pdf" target="_blank">
            <div class="icon icon-help"></div>
        </a>
    </div>
    <div class="topMenu-item userInfo <?=($this->request->params["action"]=="users_profile")?"active":"";?>">
        <a href="/profile/">
            <div class="icon icon-settings"></div>
        </a>
    </div>
    <div class="topMenu-item locale">
        <div class="dropdown-btn">
            <img class="flag flag-<?= $locales[ 'selected' ]; ?>" src="/img/flags/<?= $locales[ 'selected' ]; ?>.png" />
        </div>
        <div class="dropdown-list">
            <? foreach( $locales[ 'allLang' ] as $lang => $langData ) { ?>
                <form method="post" class="<?= $lang == $locales[ 'selected' ] ? 'selected' : ''; ?>">
                    <img class="flag flag-<?= $lang; ?>" src="/img/flags/<?= $lang; ?>.png" />
                    <input name="locale" type="hidden" value="<?= $lang; ?>">
                    <input type="<?= $lang == $locales[ 'selected' ] ? 'button' : 'submit'; ?>" value="<?= $langData[ 'text' ]; ?>">
                </form>
            <? } ?>
        </div>
    </div>
</div>
