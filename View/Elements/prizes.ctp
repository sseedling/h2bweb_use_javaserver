<div class="hidden" id="fieldMustCompletePGCreation"><?=@$LocaleStrings->page->btn->prizes->require->field;?></div>

<div class="hidden" id="baseLocaleTab">
    <div class="tab"><img alt="eng" src="/img/flags/eng.png" class="flag" /><div class="remove"><i class="icon icon-close"></i></div><span class="text"></span></div>
</div>

<div class="hidden" id="exemplarPrizeForm">
    <div class="item-row">
        <div class="column1 item-title"><?=@$LocaleStrings->page->prizes->info;?></div>
        <div class="column2 item-value">
            <div>
                <div class="prize-data-name"></div>
                <b class="btnSum minus btnGradient">-</b>
                <input class="text-field prize-count" autocomplete="off" name="prizes[%index%][count]" value="%count%" />
                <b class="btnSum plus btnGradient">+</b>
                <div class="prize-data-note"></div>
                <div class="chances">
                    <input name="prizes[%index%][chance]" type="hidden" value="%chance%" />
                    <b class="btnGradient" data-chance="0.01">0.01%</b>
                    <b class="btnGradient" data-chance="1">1%</b>
                    <b class="btnGradient" data-chance="15">15%</b>
                    <b class="btnGradient" data-chance="25">25%</b>
                    <b class="btnGradient" data-chance="50">50%</b>
                </div>
            </div>
        </div>
        <div class="column3 item-help">
            <div class="prize-remove">
                <div class="icon icon-close"></div>
                <span><?=@$LocaleStrings->page->prizes->remove;?></span>
            </div>
        </div>
        <input class="prize-id" type="hidden" name="prizes[%index%][objectId]" value="%objectId%">
    </div>
</div>

<div class="hidden" id="exemplarPrizeDataName">
    <input class="text-field prize-title" data-lang="%lang%" name="prizes[%index%][prizeData][%lang%][name]" placeholder="<?=@$LocaleStrings->page->prizes->input->name->placeholder;?>" autocomplete="off" value="%name%" />
</div>

<div class="hidden" id="exemplarPrizeDataNote">
    <textarea class="text-field prize-note" data-lang="%lang%" name="prizes[%index%][prizeData][%lang%][note]" placeholder="<?=@$LocaleStrings->page->prizes->input->note->placeholder;?>" autocomplete="off">%note%</textarea>
</div>

<div class="hidden" id="exemplarQuiz">
    <div class="quiz">
        <input type="hidden" name="quizzes[]" />
        <div class="icon icon-close over-right"></div>
        <span></span>
    </div>
</div>

<div class="hidden" id="exemplarGroup">
    <div class="row group-item">
        <div class="row-title group-title">
            <span></span>
            <div class="overlap"></div>
        </div>
    </div>
</div>

<div class="hidden" id="exemplarPrize">
    <div class="row prize-item">
        <div class="view">
            <div class="hover-icon icon icon-edit-gray"></div>
            <div class="prize-count"></div>
            <div class="row-title prize-title">
                <span></span>
                <div class="overlap"></div>
            </div>
        </div>
        <div class="edit">
            <div class="controls">
                <label>
                    <input class="prize-count text-field" type="text" value="">
                </label>
                <div class="btn cancel">
                    <div class="icon icon-close-big"></div>
                </div>
                <div class="btn done">
                    <div class="icon icon-ok"></div>
                </div>
            </div>
            <div class="row-title prize-title">
                <span></span>
                <div class="overlap"></div>
            </div>
        </div>
    </div>
</div>

<div class="hidden" id="exemplarQuiz2">
    <div class="row prize-item quiz-item">
        <div class="view">
            <div class="row-title quiz-text">
                <span></span>
                <div class="overlap"></div>
            </div>
        </div>
    </div>
</div>

<div class="hidden" id="exemplarPrizeNote">
    <textarea class="text-field" data-lang="%lang%" maxlength="64" rows="3" name="note[%lang%]" placeholder="<?=@$LocaleStrings->page->group->note->win->default;?>" autocomplete="off">%note%</textarea>
</div>

<div class="hidden" id="exemplarPrizeUnwinNote">
    <textarea class="text-field" data-lang="%lang%" maxlength="64" rows="3" name="unwinNote[%lang%]" placeholder="<?=@$LocaleStrings->page->group->note->unWin->default;?>" autocomplete="off">%unwinNote%</textarea>
</div>