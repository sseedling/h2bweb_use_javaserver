<div class="hidden" id="noDataToDisplay">
    <span><?=$LocaleStrings->page->chart->noData;?></span>
</div>

<div class="hidden" id="viewLargeChart"><?=$LocaleStrings->page->chart->largeChart;?></div>

<div class="hidden" id="weatherReportBase">
    <div class="chartBox">
        <div class="chartArea"></div>
        <div class="infoArea">
            <div class="quizzName"></div>
            <div class="date"></div>
            <div class="report-count">
                <span class="top-info">
                    <?=$LocaleStrings->page->chart->type->weather->subQuestions->before;?> <span class="top-count"></span> <?=$LocaleStrings->page->chart->type->weather->subQuestions->after;?>
                </span>
                <a href="/" target="_blank"></a>
                <?=$LocaleStrings->page->chart->type->weather->subQuestions->collected;?>
            </div>
            <div class="top-questions"></div>
            <div class="actions">
                <div class="btnGradient export"><?=$LocaleStrings->page->chart->type->weather->export;?></div>
                <div class="remove"></div>
            </div>
        </div>
        <div class="locationsList" data-title="<?=$LocaleStrings->page->chart->type->weather->chart->locations;?>">
            <div class="icon icon-location"></div>
            <span class="list"></span>
        </div>
    </div>
</div>

<div class="hidden" id="exeChartQuestion">
    <div class="chart-question">
        <div class="grade"><i></i><span></span></div>
        <div class="info">
            <div class="notes-count"><i></i><span></span></div>
            <div class="title"></div>
            <div class="sub-questions"></div>
        </div>
    </div>
</div>
<div class="hidden" id="exeChartSubQuestion">
    <span class="chart-sub-question">
        <span class="sub-title"></span> (<span class="grade-plus"></span>, <span class="grade-minus"></span>, <span class="grade-neutral"></span>)
    </span>
</div>