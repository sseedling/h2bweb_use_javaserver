<div class="hidden" id="textWrite"><?=$LocaleStrings->page->form->text->write;?></div>
<div class="hidden" id="textRead"><?=$LocaleStrings->page->form->text->read;?></div>
<div class="hidden" id="textActive"><?=$LocaleStrings->page->form->permissions->status->onText;?></div>
<div class="hidden" id="textDisabled"><?=$LocaleStrings->page->form->permissions->status->offText;?></div>
<div class="hidden" id="textDeselectAll"><?=$LocaleStrings->page->form->permissions->locations->btn->off;?></div>
<div class="hidden" id="textSelectAll"><?=$LocaleStrings->page->form->permissions->locations->btn->on;?></div>
<div class="hidden" id="textAnd">and</div>
<div class="hidden" id="textUsersAreShown">users are shown</div>
