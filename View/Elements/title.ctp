<?php
/**
 * Created by Ramotion.
 * User: Seedling
 * Date: 08.07.13
 * Time: 18:35
 * To change this template use File | Settings | File Templates.
 */
    $out = '';
     switch($mode){
         case "breadcrumbs":{
             if(is_string($tileObj)){
                 $out = $tileObj;
             }
             else{
                 $url = $tileObj["root"];
                 $counter  = count($tileObj["parts"]);
                 foreach($tileObj["parts"] as $part){
                     $counter--;
                     $url .= $part["url"]."/";
                     if($out!="")$out.='<span class="parts-separator">&nbsp;</span>';
                     $out.=($counter!=0)?'<a href="'.$url.'">'.$part["text"]."</a>":$part["text"];
                 }
             }
            echo '<div class="page-title"><div class="title-wrapper">'.$out.'</div></div>';
            break;
         }
         case "meta":{
             if(is_string($tileObj)){
                 echo $tileObj;
             }
             else{
                 foreach($tileObj["parts"] as $part){
                     if($out!="")$out.=" :: ";
                     $out.=$part["text"];
                 }
                 echo $out;
             }
         }
     }
?>