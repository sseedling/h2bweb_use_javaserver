<!--  META-->
<title><?=$this->element('title',array('tileObj'=>((isset($LocaleStrings->page->title))?$LocaleStrings->page->title:"Server internal error."),"mode"=>"meta"));?></title>
<meta content="width=940" name="viewport">
<meta name="format-detection" content="telephone=no">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="keywords" content="<?=(isset($LocaleStrings->page->meta->keywords))?$LocaleStrings->page->meta->keywords:"Server internal error.";?>" />
<meta name="description" content="<?=(isset($LocaleStrings->page->meta->description))?$LocaleStrings->page->meta->description:"Server internal error.";?>" />
<!--  FAVICONS-->
<link rel="shortcut icon" href="/img/favicon.png" type="image/png" />
<link rel="apple-touch-icon-precomposed" sizes="57x57" href="/img/web-clip/touch-icon-iphone-precomposed.png?rndkey=<?=VERSION_RND_KEY;?>" />
<link rel="apple-touch-icon-precomposed" sizes="72x72" href="/img/web-clip/touch-icon-ipad-precomposed.png?rndkey=<?=VERSION_RND_KEY;?>" />
<link rel="apple-touch-icon-precomposed" sizes="114x114" href="/img/web-clip/touch-icon-iphone4-precomposed.png?rndkey=<?=VERSION_RND_KEY;?>" />

<!--  DEFAULT CSS-->
<link rel="stylesheet" type="text/css" href="/css/all-in-one-page.css?rndkey=<?=VERSION_RND_KEY;?>" />

<!--  DEFAULT JS-->
<script src="/js/libs/out/jquerymin.js?rndkey=<?=VERSION_RND_KEY;?>" type="text/javascript"></script>
<script src="/js/libs/in/utils.js?rndkey=<?=VERSION_RND_KEY;?>" type="text/javascript"></script>
<script src="/js/libs/in/InputLengthLimiter.js?rndkey=<?=VERSION_RND_KEY;?>" type="text/javascript"></script>
<script src="/js/libs/in/MainMenu.js?rndkey=<?=VERSION_RND_KEY;?>" type="text/javascript"></script>
<script src="/js/loader.js?rndkey=<?=VERSION_RND_KEY;?>" type="text/javascript"></script>

<!-- PAGE CSS -->
<link rel="stylesheet" type="text/css" href="/css/pages/<?=$this->action;?>.css?rndkey=<?=VERSION_RND_KEY;?>" />

<!-- PAGE JS -->
<script src="/js/pages/<?=$this->action;?>.js?rndkey=<?=VERSION_RND_KEY;?>" type="text/javascript"></script>