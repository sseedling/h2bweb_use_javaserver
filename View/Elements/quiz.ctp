<div class="hidden" id="confirmRemoved"><?=$LocaleStrings->page->Label->questions->removeLocale;?></div>

<div class="hidden" id="baseLocaleTab">
    <div class="tab"><img class="flag" src="/img/flags/eng.png" alt="eng" /><div class="remove"><i class="icon icon-close"></i></div><span class="text"></span></div>
</div>

<div class="hidden" id="baseLocaleQuestionList">
    <div class="oneQuzestionsList" lang=""></div>
</div>

<div class="hidden" id="baseLocaleQuestion">
    <div class="question-item">
        <div class="question">
        <h3><?=$LocaleStrings->page->form->SurveyInfo->title;?> <span>%index%</span></h3>
        <div>
            <input class="text-field" maxlength="<?= $Company->limits->inputs->survey["question"]["name"];?>" name="questions[%index%][questionData][%lang%][name]" placeholder="<?= $LocaleStrings->page->Label->questions->placeholder->name;?>" type="text" value="%name%">
            <input type="hidden" value="%objectId%" name="questions[%index%][objectId]" />
            <textarea class="text-field" maxlength="<?= $Company->limits->inputs->survey["question"]["text"];?>" name="questions[%index%][questionData][%lang%][text]" placeholder="<?= $LocaleStrings->page->Label->questions->placeholder->text;?>">%text%</textarea>
        </div>
    </div>
        <div class="second-level-of-questions">
            <input type="hidden" name="questions[%index%][questionData][%lang%][subQuestionsEnabled]" value="%subQuestionsEnabled%">
            <h4><?=$LocaleStrings->page->form->SurveyInfo->subSurvey->title;?></h4>
        </div>
        <div class="message"><i class="icon icon-attention"></i><span><?=$LocaleStrings->page->form->SurveyInfo->publish;?></span></div>
    </div>
</div>

<div class="hidden" id="baseSubQuestion">
    <input
        class="text-field"
        maxlength="<?= $Company->limits->inputs->survey["subQuestion"];?>"
        name="questions[%index%][questionData][%lang%][subQuestions][]"
        placeholder="Subquestion %index%"
        type="text"
        value="%text%">
</div>