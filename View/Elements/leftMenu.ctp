<div class="leftMenu-items">
    <? if(Auth::inRoles(array("SuperAdmin"))) { ?>
        <a href="/companies/" class="enable leftMenu-item<?=((isset($sectionName)&&($sectionName=="company")))?" sub-active":"";?><?=($this->request->params["action"]=="company_list")?" active":"";?>">
            <span class="leftMenu-item-ico company-ico"></span>
            <span class="leftMenu-item-text"><?=$LocaleStrings->leftMenu->company;?></span>
        </a>
    <? } ?>
    <? if(Auth::inRoles(array("LocalesW"))) { ?>
        <a href="/locales/" class="enable leftMenu-item<?=((isset($sectionName)&&($sectionName=="locales")))?" sub-active":"";?><?=($this->request->params["action"]=="locales_all")?" active":"";?>">
            <span class="leftMenu-item-ico logs-ico"></span>
            <span class="leftMenu-item-text"><?=$LocaleStrings->leftMenu->locales;?></span>
        </a>
    <? } ?>
    <? if(Auth::inRoles(array("QuizzesR"))) { ?>
        <a href="/locations/" class="enable leftMenu-item<?=((isset($sectionName)&&($sectionName=="locations")))?" sub-active":"";?><?=($this->request->params["action"]=="locations_all")?" active":"";?>">
            <span class="leftMenu-item-ico locations-ico"></span>
            <span class="leftMenu-item-text"><?=$LocaleStrings->leftMenu->locations;?></span>
        </a>
    <? } ?>
    <? if((Auth::inRoles(array("Owner")))||(Auth::inRoles(array("SuperAdmin")))) { ?>
        <a href="/users/" class="enable leftMenu-item<?=((isset($sectionName)&&($sectionName=="users")))?" sub-active":"";?><?=($this->request->params["action"]=="users_list")?" active":"";?>">
            <span class="leftMenu-item-ico users-ico"></span>
            <span class="leftMenu-item-text"><?=$LocaleStrings->leftMenu->users;?></span>
        </a>
    <? } ?>
    <? if(Auth::inRoles(array("ReportsR"))) { ?>
        <a href="/reports/" class="enable leftMenu-item<?=((isset($sectionName)&&($sectionName=="reports")))?" sub-active":"";?><?=($this->request->params["action"]=="reports_view")?" active":"";?>">
            <span class="leftMenu-item-ico logs-ico"></span>
            <span class="leftMenu-item-text"><?=$LocaleStrings->leftMenu->reports;?></span>
        </a>
    <? } ?>
    <? if(Auth::inRoles(array("PrizesR"))) { ?>
        <a href="/prizes/" class="enable leftMenu-item<?=((isset($sectionName)&&($sectionName=="prizes")))?" sub-active":"";?><?=($this->request->params["action"]=="prizes_list")?" active":"";?>">
            <span class="leftMenu-item-ico prizes-ico"></span>
            <span class="leftMenu-item-text"><?=$LocaleStrings->leftMenu->prizes;?></span>
        </a>
    <? } ?>
    <? if(!Auth::inRoles(array("SuperAdmin","LocalesW","LocalesR"))) { ?>
        <a href="/winners/" class="enable leftMenu-item<?=((isset($sectionName)&&($sectionName=="winners")))?" sub-active":"";?><?=($this->request->params["action"]=="winners")?" active":"";?>">
            <span class="leftMenu-item-ico winners-ico"></span>
            <span class="leftMenu-item-text"><?=$LocaleStrings->leftMenu->winners;?></span>
        </a>
    <? } ?>
</div>
