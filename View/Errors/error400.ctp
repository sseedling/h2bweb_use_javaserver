<link rel="stylesheet" type="text/css" href="/css/errors/400.css?rndkey=<?=VERSION_RND_KEY;?>" />
<a href="/" class="btnGradient">&larr; <?=SesHelper::read("mainPage");?></a>
<div class="number"><?=SesHelper::read("ErrorCode");?></div>
<div class="message">
    <div class="text">
        <h1><?=SesHelper::read("ErrorBodyTitle");?></h1>
        <p><?=SesHelper::read("ErrorBodyText");?></p>
    </div>
</div>
<? SesHelper::clearError(); ?>