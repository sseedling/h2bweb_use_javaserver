<link rel="stylesheet" type="text/css" href="/css/pages/login.css?rndkey=<?=VERSION_RND_KEY;?>" />
<script src="/js/pages/login.js?rndkey=<?=VERSION_RND_KEY;?>" type="text/javascript"></script>
<div class="hidden">
    <div class="msg-empty"><?=$LocaleStrings->page->alert->errorEmpty;?></div>
</div>
<div id="loginForm" class="table">
    <div class="cell">
        <div class="form">
            <div class="title"><?=$LocaleStrings->page->title;?></div>
            <? echo (isset($errorMsg))?$this->element("printMsg",array("msg"=>$LocaleStrings->page->alert->error,"addClass"=>"loginMsg")):$this->element("printMsg",array("msg"=>$LocaleStrings->page->alert->default,"classMsg"=>"text-default","addClass"=>"loginMsg"));?>
            <form id="login" method="post">
                <input type="text" class="text-field" name="login" value="<?=$login;?>" placeholder="<?=$LocaleStrings->page->placeholder->login;?>" autocomplete="off" />
                <input type="password" class="text-field" value="" name="password" placeholder="<?=$LocaleStrings->page->placeholder->password;?>" autocomplete="off" />
                <input type="submit" class="btn btn-large btn-login" id="doLoginBtn" value="<?=$LocaleStrings->page->submit;?>" />
            </form>
        </div>
        <div class="locales">
            <?
                $counter = 0;
                foreach( $locales[ 'allLang' ] as $lang => $langData ) {
                if(($counter % 3) ==0)echo "<div>";
                ?>
                <form method="post"><input name="locale" type="hidden" value="<?= $lang; ?>"><input type="<?= $lang == $locales[ 'selected' ] ? 'button' : 'submit'; ?>" value="<?= $langData[ 'text' ]; ?>"></form>
                <?
                $counter++;
                if(($counter % 3) ==0)echo "</div><br />";
            } ?>
        </div>
    </div>
</div>