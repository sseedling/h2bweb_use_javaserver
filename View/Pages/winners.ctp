<link rel="stylesheet" type="text/css" href="/css/ui-darkness/jquery-ui.min.css">
<script src="/js/libs/out/jquery-ui.min.js" type="text/javascript"></script>

<div class="pageMenu-secondPart">
    <form class="menu">
        <div class="col">
            <input autocomplete="off" class="text-field minDate date-field" placeholder="<?=$LocaleStrings->page->filter->minDate;?>" type="text">
            <input name="minDate" type="hidden">
            <input autocomplete="off" class="text-field maxDate date-field" placeholder="<?=$LocaleStrings->page->filter->maxDate;?>" type="text">
            <input name="maxDate" type="hidden">
        </div>
        <div class="col">
            <input autocomplete="off" class="text-field" name="email" placeholder="<?=$LocaleStrings->page->filter->email;?>" type="email">
            <input class="text-field btnGradient" type="submit" value="<?=$LocaleStrings->page->filter->btn;?>">
        </div>
    </form>

    <div class="row-items-list title">
        <div class="row-item">
            <div class="row-item-cell"><?=$LocaleStrings->page->table->email;?></div>
            <div class="row-item-cell"><span><?=$LocaleStrings->page->table->prize;?></span>/<span><?=$LocaleStrings->page->table->status;?></span></div>
            <div class="row-item-cell"><span><?=$LocaleStrings->page->table->location;?></span>/<span><?=$LocaleStrings->page->table->date;?></span></div>
        </div>
    </div>
</div>

<div class="row-items-list data">
    <? foreach($reports as $report){ ?>
        <div class="row-item" data-report-id="<?=$report["objectId"];?>">
            <div class="row-item-cell"><?=$report["email"];?></div>
            <div class="row-item-cell">
                <div class="prize-name"><?=$report["prize"];?></div>
                <?if(!$report["givenStatus"]){?>
                    <div class="text-field btnGradient added"><?=$LocaleStrings->page->table->btn;?></div>
                <? } ?>
            </div>
            <div class="row-item-cell">
                <div class="location-name"><?=$report["location"];?></div>
                <div class="date"><?=date("m/d/y h:i a",$this->Time->fromString(@$report["date"]["iso"]));?></div>
            </div>
        </div>
    <? } ?>
</div>