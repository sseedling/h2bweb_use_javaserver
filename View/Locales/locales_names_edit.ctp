<div class="pageMenu-firstPart">
    <div class="btn btn-short saveBtn">Save</div>
    <a class="btn btn-short btn-blue cancelBtn" href="/locales/">Cancel</a>
</div>
<?
    $titles = array("web"=>"Locales list","app"=>"iPad App");
?>
<form method="post" id="localeForm" class="itemsForm" >
    <?
        foreach($obj as $localeType=>$localeList){ ?>
        <div class="item-row">
            <div class="column1 item-title"></div>
            <div class="column2 item-value">
                <div class="section-title"><?=$titles[$localeType];?></div>
            </div>
            <div class="column3 item-help">
                <a class="btn btn-short btn-blue addNew" href="/locales/names/add/">Add new</a>
            </div>
        </div>
        <?
            foreach($localeList as $locale){ ?>
                <div class="item-row">
                    <div class="column1 item-title">
                        <span><?=$locale["code"];?></span>
                    </div>
                    <div class="column2 item-value">
                        <div>
                            <input type="text" name="localeNames[<?=$localeType;?>][<?=$locale["id"];?>][name]" class="text-field" value="<?=$locale["name"];?>">
                        </div>
                    </div>
                    <div class="column3 item-help">
                        <img src="/img/flags/<?=$locale["code"];?>.png" alt="<?=$locale["code"];?>" />
                    </div>
                </div>
            <? } ?>
        <? } ?>
</form>
