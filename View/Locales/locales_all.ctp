<div class="pageMenu-secondPart">
    <div class="row-items-list title">
        <div class="row-item">
            <div class="row-item-cell">
                <a href="/locales/names/edit/">Edit existed 'Locales'</a>
            </div>
        </div>
    </div>
</div>
<div class="row-items-list data localesList">
    <?
        if(isset($localesKeys[0])){
            foreach($localesKeys as $localeKey){
                $rowElem = ($localeKey[ModelHelper::$DbLocaleKey]["key"]=="email!s")?"div":"a";
                ?>
                    <<?=$rowElem;?> class="row-item" location-name="<?=$localeKey[ModelHelper::$DbLocaleKey]["id"];?>" href="/locales/view/<?=$localeKey[ModelHelper::$DbLocaleKey]["id"];?>">
                        <div class="row-item-cell text">
                            <?=@$localeKey[ModelHelper::$DbLocaleKey]["info"];?>
                            <div class="green-arrow"></div>
                        </div>
                    </<?=$rowElem;?>>
                <?
            }
        }
    ?>
</div>
