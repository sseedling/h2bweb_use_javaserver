<?
    if($obj["key"]["key"]=="email!s"){
        $obj["data"] = array();
        $obj["webLocales"] = array();
    }
?>
<link rel="stylesheet" type="text/css" href="/css/jquery.cleditor.css?rndkey=<?=VERSION_RND_KEY;?>" />
<?php
    $localeHelper = new LocaleHelper();
?>
<div class="pageMenu-firstPart">
    <div class="btn btn-short saveBtn">Save</div>
    <a class="btn btn-short btn-blue cancelBtn" href="/locales/<?=(($obj["key"]["parent"])?"view/".$obj["key"]["parent"]."/":"");?>">Cancel</a>
</div>
<div class="pageMenu-secondPart">
    <nav class="languages">
        <?
            $activeLang = null;

            if(!empty($obj["webLocales"])){
                foreach($obj["webLocales"] as $localeItem){
                    if(!$activeLang)$activeLang = $localeItem["id"];
                    echo '<div class="locale'.(($activeLang==$localeItem["id"])?' active':'').'" data-locale="'.$localeItem["id"].'"><span>'.$localeItem["name"].'</span></div>';
                }
            }
        ?>
    </nav>
</div>
<h2><?=$obj["key"]["info"];?></h2>
<table id="editWrapper">
    <tr>
        <td class="box">
            <div id="editForm">
                <form method="post" id="localeForm">
                <?
                    if(!empty($obj["data"])){
                        foreach($obj["data"] as $dataItem){
                            //get lang
                            foreach($obj["webLocales"] as $localeItem){
                                if($localeItem["id"]==$dataItem["lang"]){
                                    break;
                                }
                            }

                            //parse data
                            $parsedData = $localeHelper->parseData($dataItem);

                            //print data
                            echo '<div class="locale-tab'.(($activeLang==$localeItem["id"])?' active':'').'" data-locale="'.$localeItem["id"].'">';
                            echo '<div class="hidden"><input type="hidden" name="'.$obj["key"]["key"].'['.$localeItem["id"].'][id]" value="'.$dataItem["id"].'" /></div>';
                            foreach($parsedData as $parsedItem){
                                echo '<div class="oneItem">';
                                echo '<span>'.$obj["key"]["key"].$parsedItem["key"].'</span><br />';
                                echo '<textarea class="text-field" width="100%" height="50" type="text" name="'.$obj["key"]["key"].'['.$localeItem["id"].'][data]'.$parsedItem["key"].'">'.$parsedItem["obj"].'</textarea>';
                                echo '</div>';
                            }
                            echo '</div>';
                        }
                    }
                ?>
                </form>
            </div>
        </td>
    </tr>
</table>