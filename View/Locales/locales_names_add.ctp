<div class="pageMenu-firstPart">
    <div class="btn btn-short saveBtn">Save</div>
    <a class="btn btn-short btn-blue cancelBtn" href="/locales/names/edit/">Cancel</a>
</div>
<form method="post" id="localeForm" class="itemsForm" enctype="multipart/form-data">
    <div class="item-row">
        <div class="column1 item-title"></div>
        <div class="column2 item-value">
            <div class="section-title">Add new locale</div>
        </div>
        <div class="column3 item-help"></div>
    </div>
    <div class="item-row">
        <div class="column1 item-title">
            <span>Code-name</span>
        </div>
        <div class="column2 item-value">
            <div>
                <input name="<?=ModelHelper::$DbWebLocale;?>[code]" class="text-field" type="text"  autocomplete="off" />
                <input name="<?=ModelHelper::$DbAppLocale;?>[code]" class="text-field" type="hidden" id="appLocaleCode" />
            </div>
        </div>
        <div class="column3 item-help">
            <?
                if(isset($errors["code"])){
                    echo '<div class="popover">'.$errors["code"].'</div>';
                }
            ?>
            Identifier word-key<br />
            Examples: eng,spa,rus.<br />
            Notice:latin-charset only.
        </div>
    </div>
    <div class="item-row">
        <div class="column1 item-title">
            <span>Read-name</span>
        </div>
        <div class="column2 item-value">
            <div>
                <input name="<?=ModelHelper::$DbWebLocale;?>[name]" class="text-field" type="text" autocomplete="off" />
                <input name="<?=ModelHelper::$DbAppLocale;?>[name]" class="text-field" type="hidden" id="appLocaleCode" />
            </div>
        </div>
        <div class="column3 item-help">
            <?
                if(isset($errors["name"])){
                    echo '<div class="popover">'.$errors["name"].'</div>';
                }
            ?>
            Examples: English, Français, Русский.
        </div>
    </div>
    <div class="item-row">
        <div class="column1 item-title">
            <span>Image-icon</span>
        </div>
        <div class="column2 item-value">
            <div class="file-preview">
                <table><tr><td><img src="/img/flags/eng.png" default="true" alt="preview" onload="this.className='active';" /></td></tr></table>
            </div>
            <div class="file-selector">
                <input type="file" name="new_locale_icon" />
                <div class="btnGradient">UPLOAD IMAGE</div>
            </div>
        </div>
        <div class="column3 item-help">
            <?
                if(isset($errors["new_locale_icon"])){
                    echo '<div class="popover">'.$errors["new_locale_icon"].'</div>';
                }
            ?>
            26x26 px PNG-image are strongly recommended
        </div>
    </div>
</form>
