<div class="pageMenu-firstPart">
    <a class="btn btn-short btn-blue cancelBtn" href="/locales/">Cancel</a>
</div>
<div class="row-items-list data localesList">
    <?
        if(isset($subKeys[0])){
            foreach($subKeys as $key){
                ?>
                    <a class="row-item" location-name="<?=$key[ModelHelper::$DbLocaleKey]["id"];?>" href="/locales/edit/<?=$key[ModelHelper::$DbLocaleKey]["id"];?>/">
                        <div class="row-item-cell text">
                            <?=@$key[ModelHelper::$DbLocaleKey]["info"];?>
                            <div class="green-arrow"></div>
                        </div>
                    </a>
                <?
            }
        }
    ?>
</div>
