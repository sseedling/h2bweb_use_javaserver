<div class="pageMenu-firstPart">
    <a href="/users/add/" class="btn big-size over-right createBtn" title="<?=@$LocaleStrings->page->addUser->title;?>"><?=@$LocaleStrings->page->addUser->text;?></a>
</div>
<div class="cols">
    <div class="shadow-block"></div>
    <div class="col filters">
        <div class="rows">
            <? if( isset( $rolesList[0] ) ) foreach( $rolesList as $role ) { ?>
                <div class="row row-item" data-role="<?=$role?>"><div class="row-title"><span><?=@$LocaleStrings->roles->$role;?></span><div class="overlap"></div></div><div class="icon icon-checkbox"></div></div>
            <? } ?>
            <div class="row row-item" data-status="0"><div class="row-title"><span><?=@$LocaleStrings->page->filters->disabled;?></span><div class="overlap"></div></div><div class="icon icon-checkbox"></div></div>
            <div class="row row-item" data-role=""><div class="row-title"><span><?=@$LocaleStrings->page->filters->noRoles;?></span><div class="overlap"></div></div><div class="icon icon-checkbox"></div></div>
        </div>
    </div>
    <div class="col content-list empty-list">
        <div class="checked-filters"></div>
        <div class="empty placeholder"><?=@$LocaleStrings->page->noUsers;?></div>
        <div class="rows">
            <? if( !empty( $userList ) )
                foreach( $userList as $userId => $user ) {
                ?>
                    <a href="/users/edit/<?=$userId;?>/" class="row row-item" data-status="<?=$user[ 'status' ] ? 1 : 0;?>" data-role="<?=implode( ',' , $user[ 'roles' ] );?>"><div class="hover-icon icon icon-edit-gray"></div><div class="row-title"><span><?=$user[ 'publicName' ];?></span><div class="overlap"></div></div></a>
                <?
                }
            ?>
        </div>
    </div>
    <div class="clear-fix"></div>
</div>
