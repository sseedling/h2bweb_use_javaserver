<?=$this->element("users",array("LocaleStrings"=>$LocaleStrings));?>

<div class="pageMenu-firstPart">
    <div class="ico-edit-white"></div>
    <div class="btn btn-short saveBtn"><?=@$LocaleStrings->page->form->btn->save;?></div>
    <a href="/users/" class="btn btn-short btn-blue cancelBtn"><?=@$LocaleStrings->page->form->btn->cancel;?></a>
</div>

<form method="post" class="itemsForm" enctype="multipart/form-data">
    <div class="item-row">
        <div class="column1 item-title"></div>
        <div class="column2 item-value">
            <div class="section-title"><?=@$LocaleStrings->page->form->personal->title;?></div>
        </div>
        <div class="column3 item-help"></div>
    </div>
    <div class="item-row">
        <div class="column1 item-title">
            <span><?=@$LocaleStrings->page->form->personal->name->title;?></span>
        </div>
        <div class="column2 item-value">
            <div>
                <input class="text-field" maxlength="64" name="publicName" placeholder="<?=@$LocaleStrings->page->form->personal->name->placeholder;?>" value="<?=@$postData["publicName"];?>" autocomplete="off" />
            </div>
        </div>
        <div class="column3 item-help">
            <? if(isset($postData["error"]["publicName"])){
                echo '<div class="popover">'.@$LocaleStrings->page->form->errors->user->publicName->$postData["error"]["publicName"].'</div>';
            } ?>
        </div>
    </div>
    <div class="item-row">
        <div class="column1 item-title">
            <span><?=@$LocaleStrings->page->form->personal->account->title;?></span>
        </div>
        <div class="column2 item-value">
            <div>
                <input
                    type="email"
                    class="text-field"
                    maxlength="64"
                    data-user-id="null"
                    name="email"
                    placeholder="<?=@$LocaleStrings->page->form->personal->account->placeholder;?>"
                    value="<?=@$postData["email"];?>"
                    autocomplete="off"
                />
                <input type="hidden" class="text-field" maxlength="64" name="username" value="<?=@$postData["username"];?>" />
            </div>
            <div class="password">
                <input name="password" class="text-field device_password" type="password" data-placeholder="<?=@$LocaleStrings->page->form->personal->account->password->placeholder;?>" value="" />
                <div class="ico-lock btnGradient"><div></div></div>
            </div>
            <div class="btn-block btn-big btnGradient check-email"><?=@$LocaleStrings->page->form->personal->account->btn;?></div>
        </div>
        <div class="column3 item-help">
            <? if(isset($postData["error"]["username"])){
                echo '<div class="popover">'.@$LocaleStrings->page->form->errors->user->email->$postData["error"]["username"].'</div>';
            } ?>

            <span><?=@$LocaleStrings->page->form->personal->account->help;?></span>
        </div>
    </div>

    <div class="item-row">
        <div class="column1 item-title"></div>
        <div class="column2 item-value">
            <div class="section-title"><?=@$LocaleStrings->page->form->permissions->title;?></div>
        </div>
        <div class="column3 item-help"></div>
    </div>
    <div class="item-row">
        <div class="column1 item-title">
            <span><?=@$LocaleStrings->page->form->permissions->roles->title;?></span>
        </div>
        <div class="column2 item-value">
            <div class="rows roles-list">
                <? if( isset( $rolesList[0] ) ) foreach( $rolesList as $index => $role ) { ?>
                    <div class="row" id="role-<?= $role; ?>">
                        <input name="roles[<?= $role; ?>][R]" value="false" type="hidden">
                        <input name="roles[<?= $role; ?>][W]" value="false" type="hidden">
                        <div class="row-title"><?= @$LocaleStrings->roles->$role; ?></div>
                        <div class="icon icon-checkbox"></div>
                    </div>
                <? } ?>
            </div>
        </div>
        <div class="column3 item-help"><?=@$LocaleStrings->page->form->permissions->roles->help;?></div>
    </div>
    <div class="item-row">
        <div class="column1 item-title">
            <span><?=@$LocaleStrings->page->form->permissions->status->title;?></span>
        </div>
        <div class="column2 item-value">
            <div>
                <div class="status-box">
                    <input type="hidden" name="status" value="true" data-status-on="<?=@$LocaleStrings->page->form->permissions->status->list["on"];?>" data-status-off="<?=@$LocaleStrings->page->form->permissions->status->list["off"];?>">
                </div>
            </div>
        </div>
        <div class="column3 item-help"><?=@$LocaleStrings->page->form->permissions->status->help;?></div>
    </div>
    <? if(isset($locationList)){ ?>
        <div class="item-row">
            <div class="column1 item-title"></div>
            <div class="column2 item-value">
                <div class="section-title"><?=@$LocaleStrings->page->form->permissions->locations->title;?></div>
            </div>
            <div class="column3 item-help"></div>
        </div>
        <div class="item-row">
            <div class="column1 item-title">
                <span><?=@$LocaleStrings->page->form->permissions->locations->help;?></span>
            </div>
            <div class="column2 item-value">
                <div class="rows" id="locations-list">
                    <? if( !empty( $locationList ) ) foreach( $locationList as $locationId => $locationName ) { ?>
                        <div class="row" data-id="<?= $locationId; ?>">
                            <div class="row-title"><?= $locationName; ?></div>
                            <div class="icon icon-checkbox"></div>
                        </div>
                    <? } ?>
                </div>
            </div>
            <div class="column3 item-help">
                <div class="btn-block btn-big btnGradient" id="select-locations"><?=@$LocaleStrings->page->form->permissions->locations->btn->off;?></div>
            </div>
        </div>
    <? }?>
    <div class="bottomBtns">
        <div class="btn btn-short saveBtn"><?=@$LocaleStrings->page->form->btn->save;?></div>
        <a href="/users/" class="btn btn-short btn-grow cancelBtn"><?=@$LocaleStrings->page->form->btn->cancel;?></a>
    </div>
</form>