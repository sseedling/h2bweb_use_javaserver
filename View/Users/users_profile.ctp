<div class="pageMenu-firstPart">
    <div class="ico-edit-white"></div>
    <div class="btn btn-short saveBtn disabled"><?=@$LocaleStrings->page->form->btn->save;?></div>
    <div class="btn btn-short btn-blue cancelBtn disabled"><?=@$LocaleStrings->page->form->btn->cancel;?></div>
</div>

<div class="itemsForm confirm-form" id="confirm-form">
    <div class="item-row">
        <div class="column1 item-title">
            <span><?=@$LocaleStrings->page->confirm->title;?></span>
        </div>
        <div class="column2 item-value">
            <input class="text-field" maxlength="64" name="password" type="password" placeholder="<?=@$LocaleStrings->page->confirm->password->placeholder;?>" value="" autocomplete="off" />
        </div>
        <div class="column3 item-help">
            <div class="btn-block btn-big btnGradient" id="confirm"><?=@$LocaleStrings->page->confirm->btn;?></div>
        </div>
    </div>
</div>
<form method="post" class="itemsForm" id="form" enctype="multipart/form-data">
    <div class="full-overlap"></div>
    <!--    cur email-->
    <div class="item-row">
        <div class="column1 item-title"></div>
        <div class="column2 item-value">
            <div class="section-title"><?= @$userData->publicName; ?></div>
            <div class="email"><?= @$userData->email; ?></div>
        </div>
        <div class="column3 item-help"></div>
    </div>

    <!--    name-->
    <div class="item-row">
        <div class="column1 item-title">
            <span><?=@$LocaleStrings->page->form->name->title;?></span>
        </div>
        <div class="column2 item-value">
            <div>
                <input class="text-field" name="publicName" placeholder="<?=@$LocaleStrings->page->form->name->placeholder;?>" value="<?= @$userData -> publicName; ?>" autocomplete="off" />
            </div>
        </div>
        <div class="column3 item-help">
            <? if(isset($postData["error"]["publicName"])){
                echo '<div class="popover">'.@$LocaleStrings->page->form->errors->publicName->$postData["error"]["publicName"].'</div>';
            } ?>
        </div>
    </div>

    <!--    default locale-->
    <div class="item-row">
        <div class="column1 item-title">
            <span><?=@$LocaleStrings->page->form->locale->title;?></span>
        </div>
        <div class="column2 item-value">
            <div>
                <select class="text-field" style="width: 340px;" name="defaultLocale" autocomplete="off" id="defaultLocaleSelector" >
                    <option value="undefined" ><?=@$LocaleStrings->page->form->locale->placeholder;?></option>
                    <? foreach( $locales[ 'allLang' ] as $lang => $langData ) { ?>
                        <option <?= $lang == $userData->defaultLocale ? 'selected="selected"' : ''; ?> value="<?= $lang; ?>"><?= $langData[ 'text' ];?></option>
                    <? } ?>
                </select>
            </div>
        </div>
        <div class="column3 item-help">
            <? if(isset($postData["error"]["publicName"])){
                echo '<div class="popover">'.@$LocaleStrings->page->form->errors->publicName->$postData["error"]["publicName"].'</div>';
            } ?>
        </div>
    </div>

    <!--    Account-->
    <div class="item-row">
        <div class="column1 item-title">
            <span><?=@$LocaleStrings->page->form->account->title;?></span>
        </div>
        <div class="column2 item-value">
            <div>
                <input type="email" class="text-field" name="email" placeholder="<?=@$LocaleStrings->page->form->account->email->placeholder;?>" value="<?= @$userData -> email; ?>" autocomplete="off" />
                <input type="hidden" class="text-field" name="username" value="<?= @$userData -> username; ?>" autocomplete="off" />
            </div>
            <div class="password">
                <input name="password" class="text-field device_password" type="password" data-placeholder="<?=@$LocaleStrings->page->form->account->password->placeholder;?>" value="" />
                <div class="ico-lock btnGradient"><div></div></div>
            </div>
            <div class="btn-block btn-big btnGradient check-email"><?=@$LocaleStrings->page->form->account->email->check->btn;?></div>
        </div>
        <div class="column3 item-help">
            <? if(isset($postData["error"]["email"])){
                echo '<div class="popover">'.@$LocaleStrings->page->form->errors->email->$postData["error"]["email"].'</div>';
            } ?>
            <? if(isset($postData["error"]["username"])){
                echo '<div class="popover">'.@$LocaleStrings->page->form->errors->username->$postData["error"]["username"].'</div>';
            } ?>
            <span><?=@$LocaleStrings->page->form->account->email->check->help;?></span>
        </div>
    </div>

    <div class="bottomBtns">
        <div class="btn btn-short saveBtn disabled"><?=@$LocaleStrings->page->form->btn->save;?></div>
        <input type="reset" class="btn btn-short btn-grow cancelBtn disabled" value="<?=@$LocaleStrings->page->form->btn->cancel;?>">
    </div>
</form>