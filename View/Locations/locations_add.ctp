<?=$this->element("locations");?>

<div class="pageMenu-firstPart">
    <div class="ico-edit-white"></div>
    <?=((isset($errorMsg))?$this->element("printMsg",array("msg"=>$errorMsg)):"");?>
    <div class="btn btn-short saveBtn" mode="save" ><?=$LocaleStrings->page->btns->save;?></div>
    <a href="/locations/" class="btn btn-short btn-blue cancelBtn"><?=$LocaleStrings->page->btns->cancel;?></a>
</div>
<form method="post" class="itemsForm" enctype="multipart/form-data" >
    <div class="hidden"></div>

    <!--    Location Data  -->
    <div class="item-row">
        <div class="column1 item-title"></div>
        <div class="column2 item-value">
            <div class="section-title"><?=$LocaleStrings->page->form->LocationInfo->title;?></div>
        </div>
        <div class="column3 item-help"></div>
    </div>
    <div class="item-row">
        <div class="column1 item-title">
            <span><?=$LocaleStrings->page->form->LocationInfo->name->label;?></span>
        </div>
        <div class="column2 item-value">
            <div>
                <input name="location[name]" maxlength="<?=$Company->limits->inputs->location["name"];?>" class="text-field location_name" type="text" placeholder="<?=$LocaleStrings->page->form->LocationInfo->name->placeholder->val;?>" value="<?=@$postData["location"]["name"];?>" />
            </div>
            <div>
                <textarea name="location[about]" maxlength="<?=$Company->limits->inputs->location["about"];?>" class="text-field location_note" placeholder="<?=$LocaleStrings->page->form->LocationInfo->name->placeholder->description;?>" ><?=@$postData["location"]["about"];?></textarea>
            </div>
        </div>
        <div class="column3 item-help">
            <?
                if(isset($postData["location"]["error"]["name"])){
                    echo '<div class="popover">'.@$LocaleStrings->page->form->errors->location->name->$postData["location"]["error"]["name"].'</div>';
                }
                if(isset($postData["location"]["error"]["about"])){
                    echo '<div class="popover">'.@$LocaleStrings->page->form->errors->location->about->$postData["location"]["error"]["about"].'</div>';
                }
            ?>
        </div>
    </div>
    <div class="item-row">
        <div class="column1 item-title">
            <span><?=$LocaleStrings->page->form->LocationInfo->phones->label;?></span>
        </div>
        <div class="column2 item-value">
            <?
                for($i=0;$i<3;$i++){
                    echo "<div>";
                    echo '<input maxlength="'.$Company->limits->inputs->location["phones"].'" name="location[phones][list]['.$i.']" class="text-field phone" type="text" placeholder="'.
                            $LocaleStrings->page->form->LocationInfo->phones->placeholder.
                            '" value="'.
                            @$postData["location"]["phones"]["list"][$i].
                            '" />';
                    echo "</div>";
                }
            ?>
        </div>
        <div class="column3 item-help">
            <?=$LocaleStrings->page->form->LocationInfo->phones->help;?>
        </div>
    </div>

    <!--    Company Logo-->
    <div class="item-row">
        <div class="column1 item-title"></div>
        <div class="column2 item-value">
            <div class="section-title"><?=$LocaleStrings->page->form->Logo->title;?></div>
        </div>
        <div class="column3 item-help"></div>
    </div>
    <div class="item-row">
        <div class="column1 item-title">
            <span><?=$LocaleStrings->page->form->Logo->img->label;?></span>
        </div>
        <div class="column2 item-value">
            <div class="file-preview">
                <table><tr><td><img src="/<?=$locationLogo;?>" default="true" alt="preview" onload="this.className='active';" /></td></tr></table>
                <div style="display: none;"></div>
            </div>
            <div class="file-selector">
                <input type="file" name="logo" />
                <div class="btnGradient"><?=$LocaleStrings->page->form->Logo->img->btn;?></div>
            </div>
        </div>
        <div class="column3 item-help">
            <?
                if(!$postData["imageExist"])echo $LocaleStrings->page->form->Logo->img->help;
                else echo '<div class="popover">'.$LocaleStrings->page->form->Logo->img->reselect.'</div>';
            ?>
        </div>
    </div>

    <!--    Attraction screen-->
    <div class="item-row">
        <div class="column1 item-title"></div>
        <div class="column2 item-value">
            <div class="section-title"><?=$LocaleStrings->page->form->Attraction->title;?></div>
        </div>
        <div class="column3 item-help"></div>
    </div>
    <div class="item-row">
        <div class="column1 item-title">
            <span><?=$LocaleStrings->page->form->Attraction->img->label;?></span>
        </div>
        <div class="column2 item-value">
            <div class="file-preview">
                <table><tr><td><img src="/<?=$attractionScreen;?>" default="true" alt="preview" onload="this.className='active';" /></td></tr></table>
                <div style="display: none;"></div>
            </div>
            <div class="file-selector">
                <input type="file" name="screen" value="" />
                <div class="btnGradient"><?=$LocaleStrings->page->form->Attraction->img->btn;?></div>
            </div>
        </div>
        <div class="column3 item-help">
            <?
                if(!$postData["imageExist"])echo $LocaleStrings->page->form->Attraction->img->help;
                else echo '<div class="popover">'.$LocaleStrings->page->form->Attraction->img->reselect.'</div>';
            ?>
        </div>
    </div>

    <!--    Attraction texts-->
    <div class="item-row">
        <div class="column1 item-title">
            <span><?=$LocaleStrings->page->form->Attraction->msg->label;?></span>
        </div>
        <div class="column2 item-value attraction-messages">
            <input
                maxlength="<?=$Company->limits->inputs->location["attraction"]["title"];?>"
                class="text-field attraction_text"
                name="attraction[title]"
                type="text"
                placeholder="<?=$LocaleStrings->page->form->Attraction->msg->placeholder->val;?>"
                value="<?=@$postData["attraction"]["title"];?>" />

            <textarea
                maxlength="<?=$Company->limits->inputs->location["attraction"]["text"];?>"
                class="text-field attraction_note"
                name="attraction[text]"
                placeholder="<?=$LocaleStrings->page->form->Attraction->msg->placeholder->description;?>" ><?=@$postData["attraction"]["text"];?></textarea>
        </div>
        <div class="column3 item-help">
            <?=$LocaleStrings->page->form->Attraction->msg->help;?>
        </div>
    </div>

    <!--    Banners-->
    <div class="item-row">
        <div class="column1 item-title"></div>
        <div class="column2 item-value">
            <div class="section-title"><?=$LocaleStrings->page->form->Banner->title;?></div>
        </div>
        <div class="column3 item-help"></div>
    </div>
    <div class="item-row">
        <div class="column1 item-title">
            <span><?=$LocaleStrings->page->form->Banner->subTitle;?></span>
        </div>
        <div class="column2 item-value">
            <div id="banners"></div>
            <div class="image-upload">
                <div class="btnGradient"><?=$LocaleStrings->page->form->Banner->btn;?></div>
            </div>
        </div>
        <div class="column3 item-help">
        </div>
    </div>

    <div class="bottomBtns">
        <div class="btn btn-short saveBtn"><?=$LocaleStrings->page->btns->save;?></div>
        <a href="/locations/" class="btn btn-short btn-grow cancelBtn"><?=$LocaleStrings->page->btns->cancel;?></a>
    </div>
</form>