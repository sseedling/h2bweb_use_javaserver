<div class="pageMenu-firstPart">
    <?
        if(Auth::inRoles(array("Owner"))){
           echo '<a href="edit/" class="btn btn-ico btn-blue editBtn"><div class="ico ico-edit"></div></a>';
        }
    ?>
</div>
<div class="pageMenu-secondPart">
    <div class="location-details">
        <div class="location-about">
            <p><?=(isset($location->about))?$location->about:$LocaleStrings->page->about->empty;?></p>
        </div>
        <div class="location-phones">
            <h4><?=$LocaleStrings->page->phone;?></h4>
            <p><?
                if(isset($location->phones->list[0])){
                    foreach($location->phones->list as $phone){
                        echo '<div>'.$phone.'</div>';
                    }
                }
            ?></p>
        </div>
    </div>
    <div class="row-items-list title">
        <div class="row-item">
            <div class="row-item-cell surveys-status"></div>
            <div class="row-item-cell text surveys-name">
                <? if(Auth::inRoles(array("QuizzesW"))){ ?>
                    <a class="btn btn-short add-survey" href="survey/add">
                        <div class="ico-plus"></div>
                        <?=$LocaleStrings->page->addSurveyBtn;?>
                    </a>
                    <? if(isset($quizzAvailableList[0])){
                        echo '<div class="action-divider">'.$LocaleStrings->page->surveysActionText[0].'</div>';
                        echo '<select class="text-field attach-survey" locationUrl="'.$location->urlName.'" confirmMsg="'.$LocaleStrings->page->attachSurveyBtn->confirmMsg.'">';
                        echo '<option selected="selected" value="">'.$LocaleStrings->page->attachSurveyBtn->select.'</option>';
                            foreach($quizzAvailableList as $quizz){
                                echo '<option value="'.$quizz->urlName.'">'.$quizz->name.'</option>';
                            }
                        echo '</select>';
                    }?>
                <? }
                    else{
                        echo $LocaleStrings->page->attachSurveyBtn->noNewQuizzes;
                    }
                ?>
            </div>
            <div class="row-item-cell surveys-questions-count"><?=$LocaleStrings->page->surveysList->question;?></div>
            <div class="row-item-cell surveys-modified"><?=$LocaleStrings->page->surveysList->modified;?></div>
        </div>
    </div>
</div>
<div class="row-items-list data surveysList">
    <?
        if($surveys){
            $tmp=null;
            foreach($surveys as $survey){
                $tmp[$survey->status][]=$survey;
            }
            $out=null;
            if(isset($tmp["unpublish"]))foreach($tmp["unpublish"] as $survey)$out[]=$survey;
            if(isset($tmp["publish"]))foreach($tmp["publish"] as $survey)$out[]=$survey;
            if(isset($tmp["archive"]))foreach($tmp["archive"] as $survey)$out[]=$survey;

            foreach($out as $survey){
                ?>
                    <div class="row-item status-<?=@$survey->status;?>" survey-name="<?=$survey->urlName;?>">
                        <div class="row-item-cell surveys-status"><div class="<?=@$survey->status;?>"></div></div>
                        <div class="row-item-cell text surveys-name"><?=@$survey->name;?><div class="green-arrow"></div></div>
                        <div class="row-item-cell surveys-questions-count"><?=@$survey->subItemsCount;?></div>
                        <div class="row-item-cell surveys-modified"><?=date("m/d/y h:i a",$this->Time->fromString(@$survey->updatedAt));?></div>
                    </div>
                <?
            }
        }
    else{
        echo "<div class='placeholder' style='display: block;'>".$LocaleStrings->page->surveysList->empty."</div>";
    }
    ?>
</div>