<div class="pageMenu-secondPart">
    <div class="row-items-list title">
        <div class="row-item">
            <div class="row-item-cell text location-name"><?=@$LocaleStrings->page->locationsList->name;?></div>
            <div class="row-item-cell location-survey-count"><?=@$LocaleStrings->page->locationsList->surveys;?></div>
            <div class="row-item-cell location-modified"><?=@$LocaleStrings->page->locationsList->modified;?></div>
        </div>
    </div>
</div>
<div class="pageMenu-firstPart">
    <? if(Auth::inRoles(array("Owner")))echo '<a href="add/" class="btn btn-short newLocationBtn">'.$LocaleStrings->page->btns->add.'</a>';?>
</div>
<div class="row-items-list data locationsList">
    <?
        if($locations){
            foreach($locations as $location){
                ?>
                    <div class="row-item" location-name="<?=@$location->urlName;?>">
                        <div class="row-item-cell text location-name"><?=@$location->name;?><div class="green-arrow"></div></div>
                        <div class="row-item-cell location-survey-count"><?=@count($location->quizzes);?></div>
                        <div class="row-item-cell location-modified"><?=date("m/d/y h:i a",$this->Time->fromString(@$location->updatedAt));?></div>
                    </div>
                <?
            }
        }
        else{
            echo "<div class='placeholder' style='display: block;'>".$LocaleStrings->page->noLocations."</div>";
        }
    ?>
</div>