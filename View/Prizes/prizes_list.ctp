<?=$this->element("prizes",array("LocaleStrings"=>$LocaleStrings));?>

<div class="pageMenu-firstPart">
    <?
    $isWrite = Auth::inRoles( array( 'PrizesW' ) );
    if( $isWrite ) { ?><a href="/prizes/add/" class="btn big-size over-right createBtn" title="<?=$LocaleStrings->page->btn->addGroup;?>">+</a><? } ?>
</div>
<div class="cols">
    <div class="shadow-block"></div>
    <div class="col groups-prize">
        <div class="rows">
            <div class="row group-item active" id="group-all"><div class="row-title"><span><?=$LocaleStrings->page->filterList->left->title;?></span><div class="overlap"></div></div></div>
        </div>
    </div><div class="col prizes-list">
        <div class="information" style="display: none;">
            <? if( $isWrite ) { ?><a href="" title="Edit group" class="btnEdit btnGradient over-right"><div class="icon icon-edit-gray"></div></a><? } ?>
            <div class="active-group">
                <div class="group-name"></div>
                <div class="group-note"></div>
            </div>
            <div class="tabs">
                <div class="tab tab-half active" data-id="prizes-container" style="border-radius: 0 3px 0 0;"><?=$LocaleStrings->page->filterList->right->tabs->prizes;?></div>
                <div class="tab tab-half" data-id="quizzes-container"><?=$LocaleStrings->page->filterList->right->tabs->quizzes;?></div>
            </div>
        </div>
        <div class="clear-fix"></div>
        <div id="prizes-container" class="tab-container active">
            <div class="rows <?= $isWrite ? 'write' : ''; ?>" id="prizes-list"></div>
            <div class="placeholder"><?=$LocaleStrings->page->noPrizes;?></div>
            <div id="add-prizes"><div class="icon icon-refresh"></div><?=$LocaleStrings->page->filterList->right->btns->more;?></div>
        </div>
        <div id="quizzes-container" class="tab-container rows">
            <div class="list"></div>
            <div class="placeholder"><?=$LocaleStrings->page->noQuizz;?></div>
            <a href="" id="add-quizzes"><?=$LocaleStrings->page->filterList->right->btns->addQuizzes;?></a>
        </div>
    </div>
    <div class="clear-fix"></div>
</div>

<script>MainPage.data = <?=$response;?>;</script>