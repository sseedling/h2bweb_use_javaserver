<?=$this->element("prizes",array("LocaleStrings"=>$LocaleStrings));?>

<div class="pageMenu-firstPart">
    <div class="ico-edit-white"></div>
    <div class="btn btn-short saveBtn"><?=$LocaleStrings->page->btn->save;?></div>
    <a href="/prizes/" class="btn btn-short btn-blue cancelBtn"><?=$LocaleStrings->page->btn->cancel;?></a>
</div>

<form method="post" class="itemsForm" enctype="multipart/form-data">
    <div class="item-row">
        <div class="column1 item-title"></div>
        <div class="column2 item-value">
            <div class="section-title"><?=$LocaleStrings->page->group->title;?></div>
        </div>
        <div class="column3 item-help"></div>
    </div>
    <div class="item-row">
        <div class="column1 item-title">
            <span><?=$LocaleStrings->page->group->name;?></span>
        </div>
        <div class="column2 item-value">
            <div>
                <input class="text-field" maxlength="64" name="name" autocomplete="off" />
            </div>
        </div>
        <div
            class="group-name-help column3 item-help"
            data-lang-alert="<?=$LocaleStrings->page->form->group->error->locale->require;?>"
            data-name-alert="<?=$LocaleStrings->page->form->group->error->name->require;?>"
        ></div>
    </div>

    <div class="lang-tabs">
        <div class="col" id="locales"></div><div class="col"><div class="tab" data-tab="add"><i class="icon icon-earth-plus"></i></div></div>
    </div>

    <div id="add-lang">
        <div class="item-row">
            <div class="column1 item-title"></div>
            <div class="column2 item-value">
                <div class="section-title">Select language</div>
            </div>
        </div>
        <div class="item-row">
            <div class="column1 item-title"></div>
            <div class="column2 item-value">
                <div class="rows">
                    <? foreach( $appLocales as $locale => $localeData ) { ?>
                        <div class="row locale-item" data-lang="<?= $locale; ?>" data-lang-text="<?= $localeData[ 'text' ]; ?>"><div class="row-title"><img alt="<?= $locale; ?>" class="flag flag-big flag-<?= $locale; ?>" src="/img/flags/<?= $locale; ?>.png" /><?= $localeData[ 'text' ]; ?></div></div>
                    <? } ?>
                </div>
            </div>
        </div>
    </div>
    <div id="lang">
        <div class="item-row">
            <div class="column1 item-title">
                <span><?=$LocaleStrings->page->group->note->title;?></span>
            </div>
            <div class="column2 item-value">
                <div id="note"></div>
            </div>
            <div class="column3 item-help"><?=$LocaleStrings->page->group->note->win->help;?></div>
            <div class="clear-fix"></div>
            <div class="column1 item-title"></div>
            <div class="column2 item-value">
                <div id="unwinNote"></div>
            </div>
            <div class="column3 item-help"><?=$LocaleStrings->page->group->note->unWin->help;?></div>
        </div>

        <div class="item-row">
            <div class="column1 item-title"></div>
            <div class="column2 item-value">
                <div class="section-title"><?=$LocaleStrings->page->prizes->title;?></div>
            </div>
            <div class="column3 item-help"></div>
        </div>
        <div id="prizes"></div>
        <div class="item-row">
            <div class="column1 item-title"></div>
            <div class="column2 item-value">
                <div class="btnAddPrize btnGradient"><?=$LocaleStrings->page->prizes->btns->addNewOne;?></div>
            </div>
            <div class="column3 item-help"></div>
        </div>
    </div>

    <div class="item-row">
        <div class="column1 item-title"></div>
        <div class="column2 item-value">
            <div class="section-title"><?=$LocaleStrings->page->quizzes->title;?></div>
        </div>
        <div class="column3 item-help"></div>
    </div>
    <div class="item-row">
        <div class="column1 item-title">
            <span><?=$LocaleStrings->page->quizzes->info;?></span>
        </div>
        <div class="column2 item-value">
            <div>
                <div id="quizzes"></div>
                <script>
                    function initParts() {
                        MainPage.locales = <?= json_encode( $appLocales ); ?> || {};
                        <? if( !empty( $allQuizes ) ) foreach( $allQuizes as $quizId => $quiz ) { ?>
                            MainPage.quizzesObj[ '<?=$quizId;?>' ] = '<?=$quiz;?>';
                        <? } ?>}
                </script>
            </div>
        </div>
        <div class="column3 item-help"></div>
    </div>

    <div class="bottomBtns">
        <div class="btn btn-short saveBtn"><?=$LocaleStrings->page->btn->save;?></div>
        <a href="/prizes/" class="btn btn-short btn-grow cancelBtn"><?=$LocaleStrings->page->btn->cancel;?></a>
    </div>
</form>