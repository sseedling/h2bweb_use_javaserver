<?php
    /**
     * Created by Ramotion.
     * User: seedling
     * Date: 10.07.13
     * Time: 15:19
     * To change this template use File | Settings | File Templates.
     */
    class LocaleStrings{
        private static $allLocales = null;

        private static $nonSimpleFields = null;
        private static $nonSimpleSubFields = null;
        private static $nonSimpleSubFieldsId = null;

        public static function __init($curLocale=null){

            //Load Locale Base Data---------------------------------------------
            LocaleStrings::loadLocaleBaseData();

            //Load Locale Data---------------------------------------------
            LocaleStrings::loadLocaleData($curLocale);

        }

        private static function loadLocaleBaseData(){
            $tmp =array();
            //Init Models----------------------------------------------
            $webLocaleModel = Utils::loadModels(ModelHelper::$DbWebLocale);
            $keyModel = Utils::loadModels(ModelHelper::$DbLocaleKey);

            //load parse all locales--------------------------------------------------
            $allLocales = $webLocaleModel->find("all");
            foreach($allLocales as $locale){
                $tmp["allLocales"][$locale[$webLocaleModel->name]["code"]] = array("text"=>$locale[$webLocaleModel->name]["name"],"id"=>$locale[$webLocaleModel->name]["id"]);
            }
            LocaleStrings::$allLocales = $tmp["allLocales"];

            //load parse all nonSimpleFields-----------------------------------------------------------------
            $allNonSimpleFields = $keyModel->find("all",array("conditions"=>array("parent"=>null)));
            foreach($allNonSimpleFields as $field){
                $tmp["nonSimpleFields"][$field[$keyModel->name]["id"]] = $field[$keyModel->name]["key"];
            }
            LocaleStrings::$nonSimpleFields = $tmp["nonSimpleFields"];

            //load parse all nonSimple-Sub-Fields-----------------------------------------------------------------
            $allNonSimpleSubFields = $keyModel->find("all",array("conditions"=>array("parent !="=>null)));
            $tmp = array();
            foreach($allNonSimpleSubFields as $subField){
                $tmp["idList"][$subField[$keyModel->name]["parent"]][] = $subField[$keyModel->name]["id"];
                $tmp["subFieldsData"][$subField[$keyModel->name]["parent"]][$subField[$keyModel->name]["id"]] = $subField[$keyModel->name]["key"];
            }
            LocaleStrings::$nonSimpleSubFields = $tmp["subFieldsData"];
            LocaleStrings::$nonSimpleSubFieldsId = $tmp["idList"];
        }

        private static function loadLocaleData($curLocale){
            //Init Models----------------------------------------------
            $dataModel = Utils::loadModels(ModelHelper::$DbLocaleString);

            //Load all strings---------------------------------------------
            foreach(LocaleStrings::$allLocales as $localeCode=>$localeData){
                if((!$curLocale)||($curLocale==$localeCode)){
                    foreach(LocaleStrings::$nonSimpleFields as $keyId=>$field){

                        if(isset(LocaleStrings::$nonSimpleSubFieldsId[$keyId])){
                            $idList = LocaleStrings::$nonSimpleSubFieldsId[$keyId];
                            $idList[] = $keyId;
                        }
                        else $idList = $keyId;

                        $localeStringsData = $dataModel->find("all",array("conditions"=>array("lang"=>$localeData["id"],"key"=>$idList)));
                        foreach($localeStringsData as $item){
                            if(isset(LocaleStrings::$nonSimpleSubFields[$keyId][$item[$dataModel->name]["key"]]))
                                $tmp["localeStringsData"][$field][LocaleStrings::$nonSimpleSubFields[$keyId][$item[$dataModel->name]["key"]]] = json_decode($item[$dataModel->name]["data"]);
                            else
                                $tmp["localeStringsData"][$field] = json_decode($item[$dataModel->name]["data"]);
                        }
                        LocaleStrings::$allLocales[$localeCode][$field] = $tmp["localeStringsData"][$field];
                    }
                }
            }
        }

        public static function getAllLang($simple=true){
            $tmp = array();
            foreach (LocaleStrings::$allLocales as $k => $v) {
                unset($v["data"]);
                if($simple){
                    foreach(LocaleStrings::$nonSimpleFields as $field)unset($v[$field]);
                }
                $tmp[$k] = $v;
            }
            return $tmp;
        }

        public static function getFullStructure(){
            return LocaleStrings::$allLocales;
        }

        public static function getNonSimpleFields(){
            return LocaleStrings::$nonSimpleFields;
        }

        public static function isAvailable($localeName){
            foreach (LocaleStrings::$allLocales as $k => $v) {
                if($localeName==$k)return true;
            }
            return false;
        }

        public static function getLocalesNames(){
            $tmp = array();
            foreach (LocaleStrings::$allLocales as $k => $v) {
                $tmp[] = $k;
            }
            return $tmp;
        }

        public static function get($locale,$pageName){
            $pageStrings=null;
            $val = LocaleStrings::$allLocales[$locale];
            foreach(LocaleStrings::$nonSimpleFields as $key){
                if($key=="page"){
                    $pageStrings->$key = (isset($val[$key][$pageName]))?$val[$key][$pageName]:null;
                }
                else{
                    $pageStrings->$key = (isset($val[$key]))?$val[$key]:null;
                }
            }

            //todo move emails to page
            return $pageStrings;
        }

        public static function getAllLocalesDefaultByName($name){
            return null;
            //todo don`t tested
            $result = array();
            foreach(LocaleStrings::$allLocales as $locale=>$data){
                $result[$locale] = $data["defaults"]->$name;
            }
            return $result;
        }

    }
