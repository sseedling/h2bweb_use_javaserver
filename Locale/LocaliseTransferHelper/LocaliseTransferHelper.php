<?php
/**
 * Created by PhpStorm.
 * User: seedling
 * Date: 07.11.13
 * Time: 10:38
 */

/**
 * Перенос данных из файлов в базу.
 * статус:завершен.
 *
 */
class LocaliseTransferHelper {
    public static function pushLocalesToDb($thisCake){
        $localeNames = LocaleStrings::getLocalesNames();
        $localeStructure = Utils::objToArr(LocaleStrings::getFullStructure());

        foreach($localeNames as $localeName){
            $fileKeys["leftMenu"][$localeName] = $localeStructure[$localeName]["leftMenu"][$localeName."LeftMenu"][0];
            $fileKeys["emails"][$localeName] = $localeStructure[$localeName]["emails"][$localeName."Emails"][0];
            $fileKeys["roles"][$localeName] = $localeStructure[$localeName]["roles"][$localeName."Roles"][0];
            $fileKeys["topMenu"][$localeName] = $localeStructure[$localeName]["topMenu"][$localeName."TopMenu"][0];
            $fileKeys["defaults"][$localeName] = $localeStructure[$localeName]["defaults"][$localeName."Defaults"][0];
            $fileKeys["page"][$localeName] = $localeStructure[$localeName]["page"][$localeName."Pages"][0];
        }

        foreach($fileKeys["page"] as $localeName=>$pageData){
            foreach($pageData as $subKey=>$data){
                $pageSubKey[$subKey][$localeName] = $data;
            }
        }
        $fileKeys["page"] = $pageSubKey;
        LocaliseTransferHelper::prepareLocaleObjAndSave($thisCake,$fileKeys,null,true);
    }

    private static function prepareLocaleObjAndSave($thisCake,$fileKeys,$parent=null,$clearDb=false){
        $webLocaleModel = ModelHelper::$DbWebLocale;
        $keyModel = ModelHelper::$DbLocaleKey;
        $dataModel = ModelHelper::$DbLocaleString;
        $thisCake->loadModels(array($keyModel,$dataModel,$webLocaleModel));

        if($clearDb)$thisCake->$dataModel->deleteAll(array("id >"=>"0"));

        $allWebLocales = $thisCake->$webLocaleModel->find("all");
        $allKeys = $thisCake->$keyModel->find("all",array("conditions"=>array("parent"=>$parent)));//all base keys
        foreach($allKeys as $baseKey){
            $curBaseKey = $baseKey[$keyModel]["key"];
            $curBaseId = $baseKey[$keyModel]["id"];
            if(isset($fileKeys[$curBaseKey])){
                if($curBaseKey!="page"){
                    foreach($allWebLocales as $localeName){
                        $curLocaleName = $localeName[$webLocaleModel]["code"];
                        $curLocaleId = $localeName[$webLocaleModel]["id"];

                        $keyData["data"] = json_encode($fileKeys[$curBaseKey][$curLocaleName]);
                        $keyData["key"] = $curBaseId;
                        $keyData["lang"] = $curLocaleId;
                        $keyData["id"] = "";
                        $thisCake->$dataModel->Save($keyData);
                    }
                }
                else {
                    LocaliseTransferHelper::prepareLocaleObjAndSave($thisCake,$fileKeys[$curBaseKey],29);
                }
            }
        }
    }
} 