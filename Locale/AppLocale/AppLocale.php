<?php
    /**
     * Created by Ramotion.
     * User: seedling
     * Date: 10.07.13
     * Time: 15:19
     * To change this template use File | Settings | File Templates.
     */
    class AppLocale{
        private static $allLocales = null;

        public static function __init($curLocale=null){

            //Load Locale Base Data---------------------------------------------
            AppLocale::loadLocaleBaseData();

        }

        private static function loadLocaleBaseData(){
            $tmp = array();
            //Init Models----------------------------------------------
            $appLocaleModel = Utils::loadModels(ModelHelper::$DbAppLocale);

            //load all app-locales--------------------------------------------------
            $allLocales = $appLocaleModel->find("all");
            foreach($allLocales as $locale){
                $tmp["allLocales"][$locale[$appLocaleModel->name]["code"]] = array("text"=>$locale[$appLocaleModel->name]["name"],"id"=>$locale[$appLocaleModel->name]["id"]);
            }
            AppLocale::$allLocales = $tmp["allLocales"];
        }

        public static function getAllLang(){
            return AppLocale::$allLocales;
        }

        public static function getLocalesNames(){
            $tmp = array();
            foreach (AppLocale::$allLocales as $k => $v) {
                $tmp[] = $k;
            }
            return $tmp;
        }
    }
