<?php
/**
 * Routes configuration
 *
 * In this file, you set up routes to your controllers and their actions.
 * Routes are very important mechanism that allows you to freely connect
 * different urls to chosen controllers and their actions (functions).
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Config
 * @since         CakePHP(tm) v 0.2.9
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */
/**
 * Here, we are connecting '/' (base path) to controller called 'Pages',
 * its action called 'display', and we pass a param to select the view file
 * to use (in this case, /app/View/Pages/home.ctp)...
 */

/* PAGES */
        // Authorisation
        Router::connect('/login/', array('controller' => 'pages', 'action' => 'login'));
        Router::connect('/logout/', array('controller' => 'pages', 'action' => 'logout'));

        // Home page
        Router::connect('/', array('controller' => 'pages', 'action' => 'home','acl'=>array("Authorize")));

        // Winners page
        Router::connect('/winners/', array('controller' => 'pages', 'action' => 'winners','acl'=>array("Authorize")));

        //Design elements
        Router::connect('/design/', array('controller' => 'pages', 'action' => 'design','acl'=>array("SuperAdmin")));

        // Locations pages
        Router::connect('/locations/', array('controller' => 'locations', 'action' => 'locations_all','acl'=>array("Owner","QuizzesR","QuizzesW")));
        Router::connect('/locations/add/', array('controller' => 'locations', 'action' => 'locations_add','acl'=>array("Owner","QuizzesW")));
        Router::connect('/locations/:location/', array('controller' => 'locations', 'action' => 'locations_view','acl'=>array("Owner","QuizzesR","QuizzesW")));
        Router::connect('/locations/:location/edit/', array('controller' => 'locations', 'action' => 'locations_edit','acl'=>array("Owner","QuizzesW")));
        Router::connect('/locations/:location/drop/', array('controller' => 'locations', 'action' => 'locations_drop','acl'=>array("Owner","QuizzesW")));

        //Reports
        Router::connect('/reports/', array('controller' => 'reports', 'action' => 'reports_view','acl'=>array("Owner","ReportsR","ReportsW")));
        Router::connect('/reports/weather/details/', array('controller' => 'reports_emotion_chart', 'action' => 'reports_emotion_details','acl'=>array("Owner","ReportsR","ReportsW")));
        Router::connect('/reports/classic/details/', array('controller' => 'reports_classic_chart', 'action' => 'reports_classic_details','acl'=>array("Owner","ReportsR","ReportsW")));

        //Prizes
        Router::connect('/prizes/', array('controller' => 'prizes', 'action' => 'prizes_list','acl'=>array("Owner","PrizesR","PrizesW")));
        Router::connect('/prizes/edit/:groupId/', array('controller' => 'prizes', 'action' => 'prizes_edit','acl'=>array("Owner","PrizesW")));
        Router::connect('/prizes/add/', array('controller' => 'prizes', 'action' => 'prizes_add','acl'=>array("Owner","PrizesW")));

        //Users
        Router::connect('/users/', array('controller' => 'users', 'action' => 'users_list','acl'=>array("SuperAdmin","Owner")));
        Router::connect('/profile/', array('controller' => 'users', 'action' => 'users_profile','acl'=>array("Authorize")));
        Router::connect('/users/edit/:userId/', array('controller' => 'users', 'action' => 'users_edit','acl'=>array("SuperAdmin","Owner")));
        Router::connect('/users/add/', array('controller' => 'users', 'action' => 'users_add','acl'=>array("SuperAdmin","Owner")));

        //Company
        Router::connect('/companies/', array('controller' => 'company', 'action' => 'company_list','acl'=>array("SuperAdmin")));
        Router::connect('/companies/edit/:companyId/', array('controller' => 'company', 'action' => 'company_edit','acl'=>array("SuperAdmin")));
        Router::connect('/companies/add/', array('controller' => 'company', 'action' => 'company_add','acl'=>array("SuperAdmin")));

        // Surveys pages
        //----------------:: All survey list
        Router::connect('/quizz/', array('controller' => 'survey', 'action' => 'survey_all','acl'=>array("Owner","QuizzesR","QuizzesW")));
        //---------------------------------
        Router::connect('/locations/:location/:survey/', array('controller' => 'survey', 'action' => 'survey_location_view','acl'=>array("Owner","QuizzesR","QuizzesW")));
        Router::connect('/locations/:location/:survey/drop/', array('controller' => 'survey', 'action' => 'survey_location_drop','acl'=>array("Owner","QuizzesW")));
        Router::connect('/locations/:location/survey/add/', array('controller' => 'survey', 'action' => 'survey_location_add','acl'=>array("Owner","QuizzesW")));
        Router::connect('/locations/:location/:survey/edit/', array('controller' => 'survey', 'action' => 'survey_location_edit','acl'=>array("Owner","QuizzesW")));
        Router::connect('/locations/:location/:survey/edit/:status/', array('controller' => 'survey', 'action' => 'survey_set_status','acl'=>array("Owner","QuizzesW")));
        Router::connect('/survey/attach/:location/:survey/:mode/', array('controller' => 'survey', 'action' => 'survey_attach_to_location','acl'=>array("Owner","QuizzesW")));

        //Locales
        Router::connect('/locales/', array('controller' => 'locales', 'action' => 'locales_all','acl'=>array("SuperAdmin","LocalesW","LocalesR")));
        Router::connect('/locales/view/:keyId/', array('controller' => 'locales', 'action' => 'locales_view','acl'=>array("SuperAdmin","LocalesW","LocalesR")));
        Router::connect('/locales/edit/:keyId/', array('controller' => 'locales', 'action' => 'locales_edit','acl'=>array("SuperAdmin","LocalesW")));
        Router::connect('/locales/names/edit/', array('controller' => 'locales', 'action' => 'locales_names_edit','acl'=>array("SuperAdmin","LocalesW")));
        Router::connect('/locales/names/add/', array('controller' => 'locales', 'action' => 'locales_names_add','acl'=>array("SuperAdmin","LocalesW")));


/* API */
        //company
        Router::connect('/api/companies/drop/', array('controller' => 'company', 'action' => 'api_company_drop','acl'=>array("SuperAdmin")));

        //locations
        Router::connect('/api/locations/banner/drop/:locationId/:bannerId/', array('controller' => 'locations', 'action' => 'api_locations_drop_banner','acl'=>array("Owner","QuizzesW")));

        //users
        Router::connect('/api/user/check/username/', array('controller' => 'users', 'action' => 'api_user_check_username','acl'=>array("Owner","QuizzesW")));
        Router::connect('/api/user/check/password/:password/', array('controller' => 'users', 'action' => 'api_user_check_password','acl'=>array("Authorize")));
        Router::connect('/api/user/check/email/:email/:userId/', array('controller' => 'users', 'action' => 'api_user_check_email','acl'=>array("SuperAdmin","Owner")));
        Router::connect('/api/user/check/email/:email/', array('controller' => 'users', 'action' => 'api_user_check_email','acl'=>array("Authorize")));

        //reports
        Router::connect('/api/reports/chart/weather/collect/', array('controller' => 'reports_emotion_chart', 'action' => 'reports_emotion_chart_collect','acl'=>array("Owner","ReportsW")));
        Router::connect('/api/reports/chart/weather/img-list/', array('controller' => 'reports_emotion_chart', 'action' => 'reports_emotion_img_list','acl'=>array("Owner","ReportsW")));
        Router::connect('/api/reports/chart/classic/collect/', array('controller' => 'reports_classic_chart', 'action' => 'reports_classic_chart_collect','acl'=>array("Owner","ReportsW")));

        //send email
        Router::connect('/api/send-email-notice/reports/new/:reportId/', array('controller' => 'emails', 'action' => 'emails_send_report_new_notice'));
        //Router::connect('/api/send-email-notice/credits/expired/:ownerEmail/:superAdminEmail/', array('controller' => 'emails', 'action' => 'emails_send_credits_expire_notice'));
        Router::connect('/api/send-email-notice/credits/expiredPost/', array('controller' => 'emails', 'action' => 'emails_send_credits_expire_notice'));

        //company
        Router::connect('/api/company/generate-new-password/:ownerId/', array('controller' => 'company', 'action' => 'company_generate_password','acl'=>array("SuperAdmin")));

        //prizes
        Router::connect('/api/prizes/get-prizes-list/', array('controller' => 'prizes', 'action' => 'prizes_get_prizes_list','acl'=>array("Owner","PrizesR","PrizesW")));
        Router::connect('/api/prizes/set-prise-count/', array('controller' => 'prizes', 'action' => 'prizes_set_prise_count','acl'=>array("Owner","PrizesW")));
        Router::connect('/api/prizes/give/:reportId/', array('controller' => 'prizes', 'action' => 'prizes_giv_prise','acl'=>array("Authorize")));

        //test
        //Router::connect('/api/parse/getUserList/:companyId/', array('controller' => 'parse', 'action' => 'parse_get_user_list','acl'=>array("Owner")));
        //Router::connect('/api/parse/getAnswerList/', array('controller' => 'parse', 'action' => 'parse_get_answer_list','acl'=>array("SuperAdmin")));
        //Router::connect('/api/session/refresh/', array('controller' => 'parse', 'action' => 'parse_refresh_session','acl'=>array("Owner","SuperAdmin")));

        //test email template
        Router::connect('/emails/new_report_alert/:reportId/:locale/', array('controller' => 'email_templates', 'action' => 'emails_view_report_alert','acl'=>array("SuperAdmin")));
        Router::connect('/emails/credits_expire/:ownerId/:locale/', array('controller' => 'email_templates', 'action' => 'emails_view_credits_expire','acl'=>array("SuperAdmin")));
        Router::connect('/emails/rare_prize/:prizeId/:locale/', array('controller' => 'email_templates', 'action' => 'emails_view_rare_prize','acl'=>array("SuperAdmin")));
        Router::connect('/emails/win_a_prize/:prizeId/:locale/', array('controller' => 'email_templates', 'action' => 'emails_view_win_a_prize','acl'=>array("SuperAdmin")));

/* ERRORS */
	    Router::connect('/*', array('controller' => 'pages', 'action' => 'error404'));

/**
 * Load all plugin routes. See the CakePlugin documentation on
 * how to customize the loading of plugin routes.
 */
	CakePlugin::routes();

/**
 * Load the CakePHP default routes. Only remove this if you do not want to use
 * the built-in default routes.
 */
	require CAKE . 'Config' . DS . 'routes.php';
