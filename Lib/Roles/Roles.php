<?php
/**
 * Created by Ramotion.
 * User: Seedling
 * Date: 10.07.13
 * Time: 12:10
 * To change this template use File | Settings | File Templates.
 */
class Roles{
    public static $rolesPool;
    public static $authorizeRole = "Authorize";

    public static function getSharedRoles() {
        $out = array();
        $highRole = Auth::getHighLevelUserRole();
        switch($highRole["name"]){
            case "SuperAdmin":{
                $out[] = "Locales";//todo this is hardcore
                break;
            }
            default:{
                foreach(Roles::$rolesPool as $role=>$info){
                    if($info["shared"]) {
                        $role = substr($role,0,strlen($role)-1);
                        if(!in_array($role,$out))$out[] = $role;
                    }
                }
                break;
            }
        }

        return $out;
    }

    private function get($role) {
        return Roles::$rolesPool[$role];
    }

    public static function getAllNames($level = 0,$exceptions=array()) {
        if(empty(Roles::$rolesPool))return array();
        foreach(Roles::$rolesPool as $k=>$v)
            if(($v["roleLevel"] >= $level)&&(!in_array($k,$exceptions)))$out[]=$k;
        return $out;
    }

    public static function setRoles($roles){
        Roles::$rolesPool = $roles;
    }
}
