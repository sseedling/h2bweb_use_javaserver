<?php
/**
 * Created by PhpStorm.
 * User: seedling
 * Date: 14.11.13
 * Time: 15:24
 */

class SesHelper {
    private static $sessionLiveTime = 18000;//5 hours
    private static $isInit = false;

    public static function init(){
        ini_set('session.gc_maxlifetime',SesHelper::$sessionLiveTime);
        ini_set('session.name', 'BusinesWeather');

        session_set_cookie_params ( SesHelper::$sessionLiveTime);

        SesHelper::$isInit = true;

        SesHelper::open();
    }

    /**
     * Return json of $_SESSION
     * @return string
     */
    public static function toString(){
        if(!SesHelper::$isInit)SesHelper::init();
        return json_encode($_SESSION);
    }

    public static function close(){
        if(!SesHelper::$isInit)SesHelper::init();
        session_write_close();
    }

    public static function open(){
        if(!SesHelper::$isInit)SesHelper::init();
        session_start();
        SesHelper::refreshCookieTimeOut();
    }

    private static function refreshCookieTimeOut(){
        $sesName = session_name();
        $sesId = session_id();

        if(isset($_COOKIE[$sesName])) {// update time

            $expireTime = time()+SesHelper::$sessionLiveTime;

            setcookie($sesName,$sesId,$expireTime,"/",false,0);
        }
    }

    public static function status(){
        if(!SesHelper::$isInit)SesHelper::init();
        return session_cache_expire();
    }

    public static function saveSession(){
        SesHelper::close();
        SesHelper::open();
    }

    public static function read($key){
        if(!SesHelper::$isInit)SesHelper::init();
        return isset($_SESSION[$key])?$_SESSION[$key]:null;
    }

    public static function write($key,$data){
        if(!SesHelper::$isInit)SesHelper::init();
        $_SESSION[$key]=$data;
    }

    public static function delete($key){
        if(!SesHelper::$isInit)SesHelper::init();
        unset($_SESSION[$key]);
    }

    public static function check($key){
        if(!SesHelper::$isInit)SesHelper::init();
        return isset($_SESSION[$key]);
    }

    public static function _print($doExit=true){
        if(!SesHelper::$isInit)SesHelper::init();
        Utils::_print($_SESSION,$doExit);
    }

    /**
     * remove from session errors (404,403) texts
     */
    public static function clearError(){
        if(!SesHelper::$isInit)SesHelper::init();
        SesHelper::delete("ErrorBodyTitle");
        SesHelper::delete("ErrorBodyText");
        SesHelper::delete("ErrorCode");
        SesHelper::delete("mainPage");
    }

    public static function clearUserInfo($additions=null){
        SesHelper::delete("locale_change");
        if($additions){
            foreach($additions as $k=>$v){
                SesHelper::write($k,$v);
            }
        }
    }
}