<?php
/**
 * Created by PhpStorm.
 * User: seedling
 * Date: 07.11.13
 * Time: 10:55
 */

class ConfigHelper {
    public static function initRoles(){
        //todo this data must be loaded from db
        //todo change roles structure
        Roles::setRoles(array(
            "SuperAdmin"=>array("home"=>"/companies/","roleLevel"=>0,"shared"=>false),
            "Owner"=>array("home"=>"/locations/","roleLevel"=>1,"shared"=>false),

            "LocalesW"=>array("home"=>"/locales/","roleLevel"=>2,"shared"=>false),

            "QuizzesW"=>array("home"=>"/locations/","roleLevel"=>2,"shared"=>true),
            "ReportsW"=>array("home"=>"/reports/","roleLevel"=>2,"shared"=>true),
            "PrizesW"=>array("home"=>"/prizes/","roleLevel"=>2,"shared"=>true),

            "LocalesR"=>array("home"=>"/locales/","roleLevel"=>3,"shared"=>false),

            "QuizzesR"=>array("home"=>"/locations/","roleLevel"=>3,"shared"=>true),
            "ReportsR"=>array("home"=>"/reports/","roleLevel"=>3,"shared"=>true),
            "PrizesR"=>array("home"=>"/prizes/","roleLevel"=>3,"shared"=>true),

            "Authorize"=>array("home"=>"/profile/","roleLevel"=>4,"shared"=>false),

            "DeviceManager"=>array("home"=>"/logout/","roleLevel"=>4,"shared"=>false),
        ));
    }

    public static function initCompany() {
        //todo this data must be loaded from db
        $company = null;
        //$company->useApiValidation = false;
        //$company->apiValidateKey = "JZEK4FHgztE3ZtVJlpoN4NzF";
        $company->serverName = "http://".$_SERVER["HTTP_HOST"];
        $company->base = "SuperAdminCompany";
        $company->name = 'Business Weather, Inc.';
        $company->version = "v.1.1.0";

        $company->superAdmin = null;
        $company->email = 'info@h4happy.com';
        $company->default->lang = 'eng';
        $company->default->path->attraction = "img/defaultAttraction/Default-Location-Background-image.jpg";
        $company->default->path->logo = "img/defaultLogo/defaultLogo.png";

        $company->limits->inputs->survey=array("name"=>40,"note"=>140,"question"=>array("name"=>40,"text"=>140) , "subQuestion" => 48);
        $company->limits->inputs->location=array("name"=>60,"about"=>140,"user"=>18,"attraction"=>array("title"=>64,"text"=>90),"phones"=>32);
        Configure::write('Company', $company);
        return $company;
    }
}