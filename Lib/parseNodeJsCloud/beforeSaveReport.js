Parse.Cloud.beforeSave("LocationReport", function(request, response) {
    var companyId = request.object.get("company");
    var query = new Parse.Query("Company");
    query.get(companyId,{
        success:function(company) {
            if( (company.get("credits") <= 0) && (!company.get("trusted")) ){//credits expired	
                Parse.Cloud.httpRequest({
                    method: 'POST',
                    url: API_EMAL_NOTICE+'credits/expiredPost/',
                    body:{
                        companyId:companyId,
                        answerPointers:JSON.stringify(request.object.get("answers"))
                    },
                    success: function(httpResponse) {
                        console.log(httpResponse.text);
                    },
                    error: function(httpResponse) {
                        console.error('Request failed with response code ' + httpResponse.status);
                    }
                });
                response.error("no credits");
            }
            else{//all ok : set new credits value
                company.increment("credits",-1);
                company.save();
                response.success();
            }
        },
        error:function(object, error){
            response.error();
            console.log("'before save' report error:" + error);
        }
    });
});
