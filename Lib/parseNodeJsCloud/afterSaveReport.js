Parse.Cloud.afterSave("LocationReport", function(request,response) {
    var prizeId = request.object.get("prizeId");
    if(prizeId){
        var query = new Parse.Query("Prize");
        query.get(prizeId,{
            success:function(prize) {
                prize.increment("count",-1);
                prize.save();
                response.success();
            },
            error:function(object, error){
                response.error();
                console.log("'before save' report error:" + error);
            }
        });
    }

    Parse.Cloud.httpRequest({
        url: API_EMAL_NOTICE+'reports/new/'+request.object.id+'/',
        success: function(httpResponse) {
//            console.log(httpResponse.text);
        },
        error: function(httpResponse) {
//            console.error('Request failed with response code ' + httpResponse.status);
        }
    });
});