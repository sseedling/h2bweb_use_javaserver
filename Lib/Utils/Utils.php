<?php
/**
 * Created by JetBrains PhpStorm.
 * User: seedling
 * Date: 24.09.13
 * Time: 14:18
 * To change this template use File | Settings | File Templates.
 */
class Utils{

    public static function _print($obj,$exit=true){
        echo"<pre>";
        print_r($obj);
        echo"</pre>";
        if($exit)exit;
    }

    public static function removeWhiteSpases($obj){
        if(empty($obj))return $obj;
        foreach($obj as $k1=>$v1){
            $obj[$k1] = (is_array($v1))?Utils::removeWhiteSpases($v1):trim($v1);
        }
        return $obj;
    }

    public static function removeEmpty($obj){
        $tmp = null;
        if(!$obj)return $tmp;
        foreach($obj as $k1=>$v1){
            if(is_array($v1))$v1 = Utils::removeEmpty($v1);
            if(($v1!="")&&(!empty($v1))&&($v1)){
                $tmp[$k1] = $v1;
            }
            if(($v1=="0")||($v1===0))$tmp[$k1] = 0;
        }
        return $tmp;
    }

    public static function createUrlName($str){
        return Slugify::slug($str);
    }

    public static function objToArr($obj){
        if(is_object($obj)){
            $className = get_class($obj);
            if("stdClass" == $className ){
                $obj = (array) $obj;
            }
            else{
                $obj = (array) $obj;
                $tmpObj = array();
                foreach($obj as $key=>$val){
                    $tmpObj[$className][] = $val;
                }
                $obj = $tmpObj;
            }
        }
        if(is_array($obj)){
            foreach($obj as $key=>$value){
                $obj[$key] = Utils::objToArr($value);
            }
        }
        return $obj;
    }

    public static function apiRender($code=200,$msg="success",$data=null){
        $obj = array("status"=>array("code"=>$code,"msg"=>$msg),"data"=>$data);
        header("HTTP/1.0 ".$code." OK",false,$code);
        header('Content-Type: application/json');
        echo json_encode($obj);
        SesHelper::close();
        exit;
    }

    public static function htmlRender($code=200,$data=null){
        header("HTTP/1.0 ".$code." OK",false,$code);
        header('Content-Type: text/html');
        echo $data;
        SesHelper::close();
        exit;
    }

    //conver date range to iso format
    public static function prepareDatesForParse($params){
        if(!isset($params["minDate"]))$params["minDate"] = null;
        if(!isset($params["maxDate"]))$params["maxDate"] = null;

        if((!$params["maxDate"])||(!$params["minDate"]))return $params;

        $params["maxDate"] = $params["maxDate"] + 86400000;//24*60*60*1000 - 1 day;

        $params["minDate"] = date("c", ($params["minDate"]/1000));//1000ms
        $params["maxDate"] = date("c", ($params["maxDate"]/1000));
        return $params;
    }

    public static function simplifyLocaleObjData($objects,$curLocale,$fields,$key){
        if(empty($objects))return array();
        $objects = Utils::objToArr($objects);
        $allLocalesNames = AppLocale::getLocalesNames();
        foreach($objects as $objId=>$obj){
            foreach($allLocalesNames as $locale)
                if(isset($obj[$key][$locale])){
                    $firstFoundLocale = $locale;
                    break;
                }

            $usedLocale = (isset($obj[$key][$curLocale]))?$curLocale:$firstFoundLocale;
            foreach($fields as $field)
                $objects[$objId][$field] = (isset($obj[$key][$usedLocale][$field]))?$obj[$key][$usedLocale][$field]:null;
            unset($objects[$objId][$key]);
        }
        return $objects;
    }

    public static function loadModels($modelNames){
        if(is_array($modelNames)){
            $returnObj = array();
            foreach($modelNames as $name){
                App::uses($name, 'Model');
                $returnObj[$name] = new $name();
            }
        }
        else{
            App::uses($modelNames, 'Model');
            $returnObj = new $modelNames();
        }
        return $returnObj;
    }
}
