<?php
/**
 * Created by PhpStorm.
 * User: seedling
 * Date: 20.11.13
 * Time: 12:13
 */


class FileHelper {

    public static function save($key,$params){
        switch($key){
            case "new-locale":{
                return FileHelper::saveNewLocaleIcon($params);
                break;
            }
        }
    }

    private static function saveNewLocaleIcon($saveName){
        $name = "new_locale_icon";
        if((!isset($_FILES[$name]))||(!is_file($_FILES[$name]["tmp_name"])))return false;

        App::uses('File', 'Utility');
        $myFile = new File($_FILES[$name]["tmp_name"]);
        return $myFile->copy(IMAGES."flags".DS.$saveName.".png",true);
    }
} 