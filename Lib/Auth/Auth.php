<?php
/**
 * Created by Ramotion.
 * User: Seedling
 * Date: 05.06.13
 * Time: 11:01
 * To change this template use File | Settings | File Templates.
 */
class Auth{
    private static $loginStatus = "isLogin";
    private static $user = "User";
    private static $loadedUserObj=false;

    private static $errorMsg = array("success"=>"All ok","code"=>400,"error"=>"Wrong login or password");

    public static function isLogin(){
        return (SesHelper::read(Auth::$loginStatus) && (SesHelper::read(Auth::$user)));
    }

    public static function logout(){
        SesHelper::clearUserInfo(array(Auth::$loginStatus=>false,Auth::$user=>null));
    }

    //Логин
    public static function login($login,$password){
        return (Auth::verifyLogin(array($login,$password)))?Auth::doLogin($login,$password):false;
    }

    //Получение ошибки логина в PARS
    public static function getError(){
        switch(Auth::$errorMsg["code"]){
            case 200:{
                return Auth::$errorMsg["success"];
            }
            default:{
                return Auth::$errorMsg["error"];
            }
        }
    }

    //Получение пользователя из сессии
    public static function getUser(){
        //В этом запросе пользователя уже загружали
        if(Auth::$loadedUserObj)return Auth::$loadedUserObj;

        //Пользователь не залогинет или нет его в сессии
        if((!Auth::isLogin())||(!SesHelper::check(Auth::$user)))return false;
        $savedUser = SesHelper::read(Auth::$user);//загружаем из сессии

        //Получаем пользователя по ID с PARSE
        $parse = Parse::Obj("_User");
        $refreshedUser = $parse->get($savedUser->objectId);

        //Сохраняем пользователя в реквесте но не в сессию.
        Auth::$loadedUserObj = (isset($refreshedUser->objectId))?$refreshedUser:false;
        return Auth::$loadedUserObj;
    }

    //Проверка наличия у пользователя хоть одной роли из списка $ACL;
    public static function inRoles($ACL){
        $user  = Auth::getUser();
        if((!$user)||(!isset($user->ACL))||(!$user->status))return false;
        $userAcl = $user->ACL;
        foreach($ACL as $v) {
            if(isset($userAcl->$v))return true;
        }
        return false;
    }

    //Получение роли с наибольшим уровнем доступа
    public static function getHighLevelUserRole(){
        $user  = Auth::getUser();
        if((!$user)||(!isset($user->ACL)))return false;
        $userAcl = $user->ACL;

        $result = false;
        foreach(Roles::$rolesPool as $roleName=>$roleData){
            if(isset($userAcl->$roleName)){
                if(!$result)$result = $roleData;
                else if($result["roleLevel"]>$roleData["roleLevel"])$result = $roleData;
                if($result == $roleData)$result["name"] = $roleName;
            }
        }
        if(!$result) {
            $result = Roles::$rolesPool["Authorize"];
            $result["name"] = "Authorize";
        }
        return $result;
    }

    //Получение всех ролей пользователя в виде массива
    public static function getAclArr($ACL,$except=array()){
        $out = array();
        foreach(Roles::$rolesPool as $role=>$data){
            if((isset($ACL->$role))&&(!in_array($role,$except)))$out[] = $role;
        }
        return $out;
    }

    //Авторизацая пользователя средствами PARS
    private static function doLogin($login,$password){
//        LogHelper::push("108 doLogin !!! must be just one");
        $parse = Parse::User()->login($login,$password);
        if(isset($parse->code)){//Error
            Auth::$errorMsg["code"] = $parse->code;
            SesHelper::write(Auth::$loginStatus,false);
            return false;
        }
        if(!$parse->status)return false;

        Auth::$errorMsg["code"] = 200;
        Auth::$loadedUserObj = $parse;//save for request

        SesHelper::write(Auth::$user,$parse);//save for session
        SesHelper::write(Auth::$loginStatus,true);
        return true;
    }

    //проверка login|pass перед логинном в PARS
    private static function verifyLogin($params){
//        LogHelper::push("127 verifyLogin");
        if(empty($params))return false;
        foreach($params as $val){
            if(!$val)return false;
            //todo add other verify
        }
        return true;
    }
}