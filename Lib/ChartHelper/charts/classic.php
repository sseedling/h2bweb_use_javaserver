<?php
/**
 * Created by Ramotion.
 * User: seedling
 * Date: 18.09.13
 * Time: 14:24
 * To change this template use File | Settings | File Templates.
 */
class classic {
    public $localeName = null;

    /**
     * Create report for classic chart
     *
     * @param $params
     * @return array
     */
    public function makeReport($params){

        $result = array("reports"=>array(),"quizzes"=>$this->getQuizzList($params));

        if(empty($params["reportsList"]))return $result;
        foreach($params["reportsList"] as $quiz=>$reportList){
            if(empty($reportList))unset($params["reportsList"][$quiz]);
        }
        if(empty($params["reportsList"]))return $result;

        $answersList = ParseAnswer::extract($params["reportsList"]);

        foreach($params["reportsList"] as $quiz=>$reportList){
            foreach($reportList as $report){
                $result["reports"][] = array(
                    "date"=>$report->date->iso,
                    "grade"=>$this->calculateGrade(Utils::removeEmpty($report->answers),$quiz,$answersList),
                    "locationId"=>$report->location,
                    "quizId"=>$quiz,
                    "answers"=>$this->collectAnswers(Utils::removeEmpty($report->answers),$quiz,$answersList,array("details","objectId","grade","question"))
                );
            }
        }

        if(isset($result["reports"])){
            $result["questions"] = $this->loadQuestionList($answersList);
        }

        return $result;
    }

    /**
     * create report for detail classic
     * @param $params
     * @return array
     */
    public function makeDetailReportData($params){
        $result = array();

        if(empty($params["reportsList"]))return $result;
        foreach($params["reportsList"] as $quiz=>$reportList){
            if(empty($reportList))unset($params["reportsList"][$quiz]);
        }
        if(empty($params["reportsList"]))return $result;

        $answersList = ParseAnswer::extract($params["reportsList"]);

        foreach($params["reportsList"] as $quiz=>$reportList){
            foreach($reportList as $k=>$report){
                $params["reportsList"][$quiz][$k]->answers = $this->collectAnswers(Utils::removeEmpty($report->answers),$quiz,$answersList);
            }
        }
        return $params["reportsList"];
    }


    /*-------------------PRIVATE_METHODS---------------------------*/

    /**
     * calculate grade for one report
     * @param $reportAnswerList
     * @param $quiz
     * @param $answersList
     * @return float|null
     */
    private function calculateGrade($reportAnswerList,$quiz,$answersList){
        if((!isset($answersList[$quiz]))||(empty($reportAnswerList))||(empty($answersList[$quiz])))return null;

        $countAnswers = count($answersList[$quiz]);
        $grade = 0;
        foreach($reportAnswerList as $answerPointer){
            for($i=0;$i<$countAnswers;$i++){
                if($answersList[$quiz][$i]->objectId == $answerPointer->objectId){
                    $grade += $answersList[$quiz][$i]->grade;
                    break;
                }
            }
        }
        return ($grade/count($reportAnswerList));
    }

    /**
     * collect answers data for one report
     * @param $reportAnswerList
     * @param $quiz
     * @param $answersList
     * @param $columns
     * @return array
     */
    private function collectAnswers($reportAnswerList,$quiz,$answersList,$columns=null){
        $result = array();
        if((!isset($answersList[$quiz]))||(empty($reportAnswerList)))return $result;
        $countAnswers = count($answersList[$quiz]);
        foreach($reportAnswerList as $answerPointer){
            for($i=0;$i<$countAnswers;$i++){
                if(isset($answersList[$quiz][$i]->objectId)){
                    if($answersList[$quiz][$i]->objectId == $answerPointer->objectId){
                        if($columns){
                            $tmp = null;
                            foreach($columns as $field)
                                $tmp->$field = isset($answersList[$quiz][$i]->$field)?$answersList[$quiz][$i]->$field:null;
                            $result[] = $tmp;
                        }
                        else $result[] = $answersList[$quiz][$i];
                        break;
                    }
                }
            }
        }
        return $result;
    }

    /**
     * Collect questions data [name,subquestions]
     * @param $answersList
     * @return array
     */
    private function loadQuestionList($answersList){
        $result = array();
        if(!empty($answersList)){
            $tmp = array();
            foreach($answersList as $list){
                if(!empty($list)){
                    foreach($list as $answer){
                        $tmp[] = $answer->question;
                    }
                }
            }
            $result = ChartHelper::getQuestions(array("objList"=>$tmp,"mode"=>"collect"));
        }
        return $result;
    }

    /**
     * collect all quizzesId from answers list
     *
     * @param $params
     * @return array
     */
    private function getQuizzList($params){
        if(empty($params["reportsList"])){
            return $params["quizIdList"];
        }

        $result = array();
        foreach ($params["reportsList"] as $quizId=>$answerIdList){
            $result[] = $quizId;
        }
        return $result;
    }
}
ChartHelper::pushChartObj("classic",new classic());