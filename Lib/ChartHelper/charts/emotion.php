<?php
/**
 * Created by Ramotion.
 * User: seedling
 * Date: 18.09.13
 * Time: 14:24
 * To change this template use File | Settings | File Templates.
 */
class emotion{
    public $localeName = null;

    /**
     * make one calculated report
     * @param $answerIdList
     * @return array
     */
    public function makeReport($reportList){
        $result = array();
        if($reportList){
            $quizzAnswersList = ParseAnswer::extract($reportList);
            if($quizzAnswersList){
                $data = $this->makeReportValues($quizzAnswersList);
                $result = $this->calculateReportData($data);
            }
        }
        return $result;
    }

    /**
     * get all reports without calculate
     * @param $reportObjList
     * @return mixed
     */
    public function makeDetailReportData($reportObjList){
        if($reportObjList){
            $quizAnswersList = ParseAnswer::extract($reportObjList);
            if($quizAnswersList){
                foreach($reportObjList as $quiz=>$reportList){
                    foreach($reportList as $key=>$report){
                        foreach($report->answers as $k2=>$answerPointer){
                            //проход по другому объекту
                            foreach($quizAnswersList as $answerList){
                                foreach($answerList as $answer){
                                    if($answerPointer->objectId == $answer->objectId)
                                        $reportObjList[$quiz][$key]->answers[$k2] = $answer;
                                }
                            }
                        }
                    }
                }
            }
        }
        return $reportObjList;
    }

    /**
     * adaptive format report list for "collect api"
     *
     * @param $params
     * @return array
     */
    public function formatReport($params){
        $result = array();
        if(!$params["report"]){
            foreach($params["queryData"]["quizzes"] as $quizId=>$locationsList){
                $result["quizId"] = $quizId;
                break;
            }
        }
        else{
            if( count($params["report"]) > 1)$result["quizzes"] = $params["report"];
            else{
                foreach($params["report"] as $quizId=>$oneReport){
                    $result = $oneReport;
                    $result["quizId"] = $quizId;
                    break;
                }
            }
        }

        $result["reports"]["count"] = 0;
        if(!empty($params["reportsList"]))
            foreach($params["reportsList"] as $reportPool){
                if(is_array($reportPool))
                    $result["reports"]["count"] += count($reportPool);
            }

        return $result;

    }


    /*-------------------PRIVATE_METHODS---------------------------*/

    /**
     * Collect to all locations and all quizess all data from all reports
     * @param $quizzAnswersList
     * @return array
     */
    private function makeReportValues($quizzAnswersList){
        $values = array();
        $questionList = array();

        foreach($quizzAnswersList as $quizz=>$answerList){
            $questionList[$quizz] = array();
            if(!empty($quizzAnswersList[$quizz])){
                foreach($answerList as $answer){
                    if(isset($answer->location)){
                        $values[$quizz]["locations"][$answer->location][$answer->question]["grade"][] = $answer->grade;
                        $values[$quizz]["locations"][$answer->location][$answer->question]["priority"][] = $answer->priority;

                        if(isset($answer->details->subAnswers)){
                            if($answer->grade > 2)
                                $values[$quizz]["locations"][$answer->location][$answer->question]["subAnswers"]["positive"][] = $answer->details->subAnswers;
                            else
                                $values[$quizz]["locations"][$answer->location][$answer->question]["subAnswers"]["negative"][] = $answer->details->subAnswers;
                        }

                        if(!in_array($answer->question,$questionList[$quizz]))$questionList[$quizz][] = $answer->question;
                    }
                }
            }
        }

        return array("values"=>$values,"questionList"=>$questionList);
    }

    /**
     * Calculate statistic
     * @param $data
     * @return array
     */
    private function calculateReportData($data){
        $result = array();
        foreach($data["values"] as $quizz=>$locationList){
            foreach($locationList["locations"] as $location=>$questions){
                foreach($questions as $question=>$statistic){
                    $gSum = 0;
                    $pSum = 0;
                    foreach($statistic["grade"] as $gVal)$gSum += $gVal;
                    foreach($statistic["priority"] as $pVal)$pSum += $pVal;

                    $result[$quizz]["locations"][$location][$question]["grade"] = $gSum / count($statistic["grade"]);
                    $result[$quizz]["locations"][$location][$question]["priority"] = $pSum / count($statistic["priority"]);

                    foreach($statistic["subAnswers"] as $rate=>$rateList){
                        foreach($rateList as $answerList){
                            foreach($answerList as $k=>$answer){
                                if(!isset($result[$quizz]["locations"][$location][$question]["subAnswers"][$rate][$k]))
                                    $result[$quizz]["locations"][$location][$question]["subAnswers"][$rate][$k]=0;
                                $result[$quizz]["locations"][$location][$question]["subAnswers"][$rate][$k] += ($answer)?1:0;
                            }
                        }
                    }
                }
            }
            if((isset($data["questionList"][$quizz]) && (!empty($data["questionList"][$quizz]))))$result[$quizz]["questions"] = ChartHelper::getQuestions(array("objList"=>$data["questionList"][$quizz],"mode"=>"collect"));

            if(!isset($result[$quizz]["locations"]))$result[$quizz]["locations"] = array();
            if(!isset($result[$quizz]["questions"]))$result[$quizz]["questions"] = array();
            $result[$quizz]["topSubAnswers"] = $this->foundThreeMaxSubQuestions($result[$quizz]["locations"],$result[$quizz]["questions"]);
        }

        return $result;
    }

    /**
     * Found max of answers was woted as negative
     * @param $locations
     * @param $questions
     * @return array
     */
    private function foundThreeMaxSubQuestions($locations,$questions){
        function sortVotes($a,$b){//sorting by 'val'
            if($a["val"]==$b["val"])return 0;
            return ($a["val"]<$b["val"])?1:-1;
        }

        $maxNegativeAnswersList = array();
        foreach($locations as $location){
            foreach($location as $questionId=>$question){
                if(isset($question["subAnswers"]["negative"]))
                    foreach($question["subAnswers"]["negative"] as $k=>$val){
                        if(isset($questions[$questionId]["subQuestions"][$k])){//check for exist subQuestion
                            if(((empty($maxNegativeAnswersList)) || (count($maxNegativeAnswersList)<3))&&($val>0)) {
                                $maxNegativeAnswersList[] = array("name"=>$questions[$questionId]["subQuestions"][$k],"val"=>$val);
                                usort($maxNegativeAnswersList,"sortVotes");
                            }
                            else{
                                if($val>0)for($i=2;$i>=0;$i--){
                                    if($maxNegativeAnswersList[$i]["val"] > $val)
                                        $maxNegativeAnswersList[$i] = array("name"=>$questions[$questionId]["subQuestions"][$k],"val"=>$val);
                                }
                            }
                        }
                    }
            }
        }
        return $maxNegativeAnswersList;
    }
}

ChartHelper::pushChartObj("emotion",new emotion());