<?php
/**
 * Created by Ramotion.
 * User: seedling
 * Date: 18.09.13
 * Time: 14:12
 * To change this template use File | Settings | File Templates.
 */
class ChartHelper{
    public static $localeName = null;
    private static $chartObjArr = array();
    public static $queryData = null;

    public function __construct($queryData,$localeName){
        ChartHelper::$queryData = $queryData;
        ChartHelper::$localeName = $localeName;
    }

    /**
     * save new loaded chart object
     * @static
     * @param $name
     * @param $obj
     */
    public static function pushChartObj($name,$obj){
        ChartHelper::$chartObjArr[$name] = $obj;
    }

    /**
     * get all reports details from requested report
     *
     * @return reportObjectList|bool|null
     */
    public function getReportList(){//todo recode all chart core for return array
        $tmp = ParseLocationReport::find(ChartHelper::$queryData);
        $tmpObj = $tmp;
        $tmp = Utils::objToArr($tmp);
        $tmp = Utils::removeEmpty($tmp);
        $result = array();
        if(!empty($tmp)){
            foreach($tmp as $quiz=>$reportList){
                if(!empty($reportList)){
                    foreach($reportList as $k=>$report){
                        if(isset($report["answers"])){
                            $result[$quiz][] = $tmpObj[$quiz][$k];
                        }
                    }
                }
            }
        }
        return $result;
    }

    /**
     * collect all answer id from reports list
     * @param $reportsList
     * @return bool
     */
    public function makeAnswerIdList($reportsList){
        if($reportsList){
            foreach($reportsList as $quizz){
                if(!empty($quizz))
                    foreach($quizz as $report){
                        foreach($report->answers as $answer){
                            if((isset($report->survey))&&(isset($answer->objectId))){
                                $answerIdList[$report->survey][] = $answer->objectId;//собираем все $answer->objectId для всех отчетов за весь период для одного quizzId
                            }
                        }
                    }
            }
        }
        return (isset($answerIdList))?$answerIdList:false;
    }

    /**
     * Make report collected data by chart type
     * @param $params
     * @param $type
     * @return array|bool
     */
    public function makeReportData($params,$type) {
        $chartObj = $this->loadChartType($type);
        if(!$chartObj)return false;
        return $chartObj->makeReport($params);
    }

    /**
     * create datail report
     *
     * @param $params
     * @param $type
     * @return mixed
     */
    public function makeDetailReportData($params,$type){
        $chartObj = $this->loadChartType($type);
        return $chartObj->makeDetailReportData($params);
    }

    /**
     * get all question details from requested report
     * @param $params['objList']
     * @param $params['mode']
     * @return array|null
     */
    public static function getQuestions($params){
        $list = array();
        switch($params["mode"]){
            case "table":{
                if(count($params["objList"])>0){
                    foreach($params["objList"] as $quizz=>$reportList)
                        if(!empty($reportList))
                            foreach($reportList as $report)
                                foreach($report->answers as $answer)
                                    if((isset($answer->question))&&(!in_array($answer->question,$list)))$list[] = $answer->question;
                }
                return (count($list)!=0)?ChartHelper::loadQuestions(array("whereIn"=>array("objectId"=>$list)),$params["mode"]):null;
            }
            case "collect":{
              return ChartHelper::loadQuestions(array("whereIn"=>array("objectId"=>$params["objList"])),$params["mode"]);
            }
        }
    }

    /**
     * get all location details from requested report
     * @param $reportsList - list of report list
     * @return locationObjectList|bool|null
     */
    public function getLocations($reportsList){
        $locationList = array();
        if(count($reportsList)>0){
            foreach($reportsList as $quizz=>$list)
                if(!empty($list))
                    foreach($list as $report)
                        if(!in_array($report->location,$locationList))$locationList[] = $report->location;
        }
        return (count($locationList)>0)?ParseLocation::find(array("whereIn"=>array("objectId"=>$locationList))):null;
    }

    /**
     * get all quizz details from requested report
     * @param null $columns
     * @param bool $queryOnly
     * @return array|bool
     */
    public static function getQuizzList($columns=null,$queryOnly=false){
        $quizzList = array();
        if(isset(ChartHelper::$queryData["quizzes"])){
            foreach(ChartHelper::$queryData["quizzes"] as $quizz=>$data){
                $quizzList[] = $quizz;
            }
        }
        if($queryOnly)return $quizzList;

        $tmp = ParseSurvey::find($quizzList);

        if((!$columns)||(!$tmp))return $tmp;

        foreach($tmp as $k=>$quiz){
            if(count($columns)>1)
                foreach($columns as $column){
                    $result[$k][$column] = isset($quiz->$column)?$quiz->$column:null;
                }
            else {
                $column = $columns[0];
                $result[$k] = isset($quiz->$column)?$quiz->$column:null;
            }
        }
        return $result;
    }

    /**
     * call custom chart method
     *
     * @param $methodName
     * @param $params
     * @param $chartType
     * @return null
     */
    public function call($methodName,$params,$chartType){
        try{
            $chart = $this->loadChartType($chartType);
            return $chart->$methodName($params);
        }
        catch (Exception $e){
            return null;
        }
    }

    /*-------------------PRIVATE_METHODS---------------------------*/

    /**
     * include chart type object
     * @param $type
     * @return null
     */
    private function loadChartType($type){
        include_once("charts".DS.$type.".php");
        if(isset(ChartHelper::$chartObjArr[$type])){
            ChartHelper::$chartObjArr[$type]->localeName = ChartHelper::$localeName;
            return ChartHelper::$chartObjArr[$type];
        }
        return null;
    }

    /**
     * load question list and format it
     *
     * @param $conditions
     * @param $mode
     * @return array|null
     */
    private static function loadQuestions($conditions,$mode){
        $result = null;
        $questions = Utils::objToArr(ParseQuestion::find($conditions));
        if(!$questions)return null;

        $allLocalesNames = AppLocale::getLocalesNames();
        $firstFoundLocale = ChartHelper::$localeName;
        foreach ($questions as $k=>$question) {
            foreach($allLocalesNames as $locale)
                if(isset($question["questionData"][$locale])){
                    $firstFoundLocale = $locale;
                    break;
                }
            $usedLocale = (isset($prize["prizeData"][ChartHelper::$localeName]))?ChartHelper::$localeName:$firstFoundLocale;

            if($mode=="collect"){
                $result[$question["objectId"]] = array("name"=>$question["questionData"][$usedLocale]["name"],"subQuestions"=>@$question["questionData"][$usedLocale]["subQuestions"]);
                if(!$result[$question["objectId"]]["subQuestions"])$result[$question["objectId"]]["subQuestions"] = array();
            }
            else{
                $questions[$k]["name"] = $question["questionData"][$usedLocale]["name"];
                $questions[$k]["subQuestions"] = $question["questionData"][$usedLocale]["subQuestions"];
            }
        }
        return ($mode=="collect")?$result:$questions;
    }

}
