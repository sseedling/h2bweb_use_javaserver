<?php

class parseFile extends parseRestClient{

	private $_fileName;
	private $_contentType;

	public function __construct($data=false,$filename=false,$contentType=false){
        parent::__construct();
        $this->_contentType = $contentType;
        $this->data = $data;
        $this->_fileName = $filename;
	}

	public function save($fileName=false){
        if(!$fileName)$fileName = $this->_fileName;
		if($fileName && $this->_contentType && $this->data){
			$request = $this->request(array(
				'method' => 'POST',
				'requestUrl' => 'files/'.$fileName,
				'contentType' => $this->_contentType,
				'data' => $this->data,
			));
			return $request;
		}
		else{
			$this->throwError('Please make sure you are passing a proper filename as string (e.g. hello.txt)',300);
		}
	}

	public function delete($parseFileName=false){
        if(!$parseFileName)$parseFileName = $this->_fileName;
		if($parseFileName){
			$request = $this->request(array(
				'method' => 'DELETE',
				'requestUrl' => 'files/'.$parseFileName
			));
			return $request;
		}
	}
}

?>