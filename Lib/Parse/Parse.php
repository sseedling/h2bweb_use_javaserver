<?php
include 'core/parse.php';

class Parse{
    public static function Obj($class=''){
        return ($class!='')?new parseObject($class):new parseObject();
    }
    public static function User(){
        return new parseUser();
    }
    public static function ACL(){
        return new parseACL();
    }

    public static function Cloud($function=''){
        return ($function != '')?new parseCloud($function):new parseCloud();
    }

    public static function File($data=false,$filename=false,$contentType=false){
        return new parseFile($data,$filename,$contentType);
    }

    public static function Push($globalMsg){
        return ($globalMsg!='')?new parsePush($globalMsg):new parsePush();
    }

    public static function Query($param){
        return new parseQuery($param);
    }
}
?>