<?php
/**
 * Created by PhpStorm.
 * User: seedling
 * Date: 05.11.13
 * Time: 12:13
 */

class ModelHelper {
    public static $DbAppLocale = "DbAppLocale";//"locales of iPadApp
    public static $DbWebLocale = "DbWebLocale";//locales of WebAdmin
    public static $DbLocaleKey = "DbLocaleKey";//key for DbLocaleString
    public static $DbLocaleString = "DbLocaleString";//All locale data

    public static function getAll(){
        return array(
            ModelHelper::$DbAppLocale,
            ModelHelper::$DbWebLocale,
            ModelHelper::$DbLocaleKey,
            ModelHelper::$DbLocaleString,
        );
    }
} 