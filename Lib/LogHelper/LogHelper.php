<?php
/**
 * Created by PhpStorm.
 * User: seedling
 * Date: 05.11.13
 * Time: 14:34
 */

class LogHelper {
    public static $enabled = false;

    private static $log = array();

    public static function clear(){
        LogHelper::$log = array();
    }

    public static function push($data=null){
        $log["time"] = date("H:i:s",time());
        if(isset($data))$log["data"] = $data;
        LogHelper::$log[] = $log;
    }

    public static function printLog(){
        Utils::_print(LogHelper::$log);
    }

    public static function write($data,$key="session-check"){
        if(LogHelper::$enabled){

            if(is_array($data)){
                foreach($data as $logItem){
                    CakeLog::write($key,$logItem);
                }
            }

            if(is_string($data)){
                CakeLog::write($key,$data);
            }
        }
    }
} 