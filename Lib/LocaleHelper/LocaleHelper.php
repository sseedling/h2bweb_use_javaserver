<?php

class LocaleHelper {
    private $keyArr = array();

    private function extractTexts($obj,$key){
        if(!is_array($obj)){
            $this->keyArr[] = array("key"=>$key,"obj"=>$obj);
        }
        else {
            foreach($obj as $subKey=>$data) $this->extractTexts($data,$key.'['.$subKey.']');
        }
    }

    public function parseData($item){
        $this->keyArr = array();

        $item["data"] = json_decode($item["data"],true);

        $this->extractTexts($item["data"],"");
        return $this->keyArr;
    }
}