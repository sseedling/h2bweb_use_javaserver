<?php
/**
 * Created by PhpStorm.
 * User: seedling
 * Date: 07.11.13
 * Time: 10:22
 */

class EmailHelper {
    private $emails=null;
    private $emailsAll=null;
    private $emailBody = null;

    private $htmlWrapper = array("first"=>"<html><body>","last"=>"</body></html>");

    public function setHtmlWrapper($arr){
        if(isset($arr["first"])) $this->htmlWrapper["first"] = $arr["first"];
        if(isset($arr["second"])) $this->htmlWrapper["second"] = $arr["second"];
    }

    public function setStrings($strings,$locale){
        $this->emailsAll[$locale] = $strings;
    }

    public function get($emailName,$params,$locale) {
        $this->emails = $this->emailsAll[$locale];
        $this->emailBody = $this->emails->$emailName->body;
        $this->emails->$emailName->body = $this->draw($emailName,$params);
        return $this->emails->$emailName;
    }

    private function draw($emailName,$params){
        $drawMethod = "draw". ucfirst($emailName);
        return $this->$drawMethod($params);
    }

    private function drawPrize($params) {
        $body =  $this->htmlWrapper["first"].$this->emailBody.$this->htmlWrapper["last"];
        $body = str_replace("PARAM_SERVER_URL",$params["server"],$body);
        $body = str_replace("PARAM_LOGO_URL",$params["logo"],$body);
        $body = str_replace("PARAM_PRIZE_NAME",$params["prize"]["name"],$body);
        $body = str_replace("PARAM_LOCATION_NAME",$params["location"]["name"],$body);
        $body = str_replace("PARAM_PRIZE_NOTE",$params["prize"]["note"],$body);

        return $body;
    }

    private function drawPassword($params) {
        $body =  $this->htmlWrapper["first"].$this->emailBody.$this->htmlWrapper["last"];
        $body = str_replace("PARAM_LOGO_URL",$params["logo"],$body);
        $body = str_replace("PARAM_PASSWORD",$params["password"],$body);
        $body = str_replace("PARAM_LOGIN",$params["login"],$body);

        return $body;
    }

    private function drawRarePrize($params){
        $body =  $this->htmlWrapper["first"].$this->emailBody.$this->htmlWrapper["last"];
        $body = str_replace("PARAM_LOGO_URL",$params["logo"],$body);
        $body = str_replace("PARAM_PRIZE_NAME",$params["prizeName"],$body);
        $body = str_replace("PARAM_LOCATION_NAME",$params["location"],$body);
        $body = str_replace("PARAM_WINNER_EMAIL",$params["email"],$body);
        $body = str_replace("PARAM_WIN_DATE",$params["date"],$body);

        return $body;
    }

    private function drawReport($params){
        $gradeColors = array("-100"=>"f03b40","-50"=>"fb9d9d","0"=>"8f8f8f","+50"=>"a4d07c","+100"=>"00b91e");
        $list = "<table style='width:100%;font-size: 14px;color: #333333;border-spacing: 0;border: 0px;'>";
        foreach($params["questions"] as $q=>$v){
            $list .="<tr style='padding:0; background: #".((!$q || !($q%2))?"f8f8f8":"ffffff").";'>";
            $list .="<td style='text-align: center;vertical-align: middle; padding: 14px 0; border: 0;border-top:1px solid #cccccc; width:41px;'>";
            $list .="<div style='display:inline-block;width:12px;height: 12px;background:#".$gradeColors[$v["grade"]["val"]]."; '></div>";//rectangle
            $list .="</td>";
            $list .="<td style='min-height:41px;text-align: left;vertical-align: middle; padding: 14px 0; border: 0;border-top:1px solid #cccccc; width:475px;'>";
            $list .="<div style='line-height: 14px;'>".$v["name"]."</div></td>";//question name
            $list .="<td style='min-height:41px;text-align: right;vertical-align: middle; padding: 14px 0; border: 0;border-top:1px solid #cccccc; '>";
            $list .="<div style='line-height: 11px;font-size:11px;color:#8d8d8d;padding-right:12px;'>".$v["grade"]["text"]."</div></td>";//grade
            $list .="</td>";
            $list .="<tr>";
        }
        $list .= "</table>";

        $body =  $this->htmlWrapper["first"].$this->emailBody.$this->htmlWrapper["last"];
        $body = str_replace("PARAM_LOGO_URL",$params["logo"],$body);
        $body = str_replace("PARAM_LOCATION_NAME",$params["location"],$body);
        $body = str_replace("PARAM_QUIZ_NAME",$params["quiz"],$body);
        $body = str_replace("PARAM_USER_EMAIL",$params["email"],$body);
        $body = str_replace("PARAM_COMPLETE_DATE",$params["date"],$body);
        $body = str_replace("PARAM_QUESTIONS_LIST",$list,$body);

        return $body;
    }

    private function drawCreditsExpire($params){
        $body =  $this->htmlWrapper["first"].$this->emailBody.$this->htmlWrapper["last"];
        $body = str_replace("PARAM_LOGO_URL",$params["logo"],$body);
        $body = str_replace("PARAM_ADMIN_EMAIL",$params["email"],$body);

        return $body;
    }

}