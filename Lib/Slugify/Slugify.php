<?php
/**
 * Created by Ramotion.
 * User: Seedling
 * Date: 09.07.13
 * Time: 18:29
 * To change this template use File | Settings | File Templates.
 */

class Slugify {
    public static function slug($string, $length = -1, $separator = '-',$lowerCase=true) {
        // transliterate
        $string = Slugify::transliterate($string);

        // lowercase
        if($lowerCase)$string = strtolower($string);

        // replace non alphanumeric and non underscore charachters by separator
        $string = preg_replace('/[^a-z0-9]/i', $separator, $string);

        // replace multiple occurences of separator by one instance
        $string = preg_replace('/'. preg_quote($separator) .'['. preg_quote($separator) .']*/', $separator, $string);

        // cut off to maximum length
        if ($length > -1 && strlen($string) > $length) {
          $string = substr($string, 0, $length);
        }

        // remove separator from start and end of string
        $string = preg_replace('/'. preg_quote($separator) .'$/', '', $string);
        $string = preg_replace('/^'. preg_quote($separator) .'/', '', $string);

        return $string;
      }

  /**
   * Transliterate a given string.
   *
   * @param $string
   *   The string you want to transliterate.
   * @return
   *   A string representing the transliterated version of the input string.
   */
    public function transliterate($text) {
        $text = strtolower(preg_replace("/[-]{2,}/", '-', $text));
        $text = @iconv('UTF-8','windows-1251//TRANSLIT',$text);
        $text = @iconv('windows-1251','UTF-8',$text);

        preg_match_all('/./u', $text, $text);
        $text = $text[0];
        $simplePairs = array( 'а' => 'a' , 'л' => 'l' , 'у' => 'u' , 'б' => 'b' , 'м' => 'm' , 'т' => 't' , 'в' => 'v' , 'н' => 'n' , 'ы' => 'y' , 'г' => 'g' , 'о' => 'o' , 'ф' => 'f' , 'д' => 'd' , 'п' => 'p' , 'и' => 'i' , 'р' => 'r' , 'А' => 'A' , 'Л' => 'L' , 'У' => 'U' , 'Б' => 'B' , 'М' => 'M' , 'Т' => 'T' , 'В' => 'V' , 'Н' => 'N' , 'Ы' => 'Y' , 'Г' => 'G' , 'О' => 'O' , 'Ф' => 'F' , 'Д' => 'D' , 'П' => 'P' , 'И' => 'I' , 'Р' => 'R' , );
        $complexPairs = array( 'з' => 'z' , 'ц' => 'c' , 'к' => 'k' , 'ж' => 'zh' , 'ч' => 'ch' , 'х' => 'kh' , 'е' => 'e' , 'с' => 's' , 'ё' => 'jo' , 'э' => 'eh' , 'ш' => 'sh' , 'й' => 'jj' , 'щ' => 'shh' , 'ю' => 'ju' , 'я' => 'ja' , 'З' => 'Z' , 'Ц' => 'C' , 'К' => 'K' , 'Ж' => 'ZH' , 'Ч' => 'CH' , 'Х' => 'KH' , 'Е' => 'E' , 'С' => 'S' , 'Ё' => 'JO' , 'Э' => 'EH' , 'Ш' => 'SH' , 'Й' => 'JJ' , 'Щ' => 'SHH' , 'Ю' => 'JU' , 'Я' => 'JA' , 'Ь' => "" , 'Ъ' => "" , 'ъ' => "" , 'ь' => "" , );
        $specialSymbols = array( "_" => "-", "'" => "", "`" => "", "^" => "", " " => "-", '.' => '', ',' => '', ':' => '', '"' => '', "'" => '', '<' => '', '>' => '', '«' => '', '»' => '', ' ' => '-', );
        $translitLatSymbols = array( 'a','l','u','b','m','t','v','n','y','g','o', 'f','d','p','i','r','z','c','k','e','s', 'A','L','U','B','M','T','V','N','Y','G','O', 'F','D','P','I','R','Z','C','K','E','S', );
        $simplePairsFlip = array_flip($simplePairs);
        $complexPairsFlip = array_flip($complexPairs);
        $specialSymbolsFlip = array_flip($specialSymbols);
        $charsToTranslit = array_merge(array_keys($simplePairs),array_keys($complexPairs));
        $translitTable = array();
        foreach($simplePairs as $key => $val) $translitTable[$key] = $simplePairs[$key];
        foreach($complexPairs as $key => $val) $translitTable[$key] = $complexPairs[$key];
        foreach($specialSymbols as $key => $val) $translitTable[$key] = $specialSymbols[$key];
        $result = "";
        $nonTranslitArea = false;
        foreach($text as $char) {
            if(in_array($char,array_keys($specialSymbols))) {
                $result.= $translitTable[$char];
            }
            elseif(in_array($char,$charsToTranslit)) {
                if($nonTranslitArea) {
                    $result.= "";
                    $nonTranslitArea = false;
                }
                $result.= $translitTable[$char];
            }
            else {
                if(!$nonTranslitArea && in_array($char,$translitLatSymbols)) {
                    $result.= "";
                    $nonTranslitArea = true;
                }
                $result.= $char;
            }
        }

        return $result;
    }

    public function is_slug($str) {
        return $str == Slugify::slug($str);
    }
}