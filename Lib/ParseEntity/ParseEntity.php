<?php
    /**
     * Created by Ramotion.
     * User: seedling
     * Date: 15.07.13
     * Time: 18:10
     * To change this template use File | Settings | File Templates.
     */
    class ParseEntity{
        private static $ctx = null;

        public $parseResponse = null;

        public static $maxLimit = 1000;
        public static $appLocales = null;
        public static $localeStrings = null;

        protected $data = null;
        protected $entityName = null;

        /**
         * Check for exist object[$className] with $objectId and $conditions
         * @param $className
         * @param $conditions
         * @param null $objectId
         * @return bool
         */
        public static function isExist($className,$conditions,$objectId=null){
            $test = ParseEntity::parseGetQuery($className,$conditions);
            if($objectId)return ((isset($test[0]))&&($test[0]->objectId!=$objectId));
            else return isset($test[0]);
        }

        /**
         * Получение PARSE объекта по его ID
         *
         * @static
         * @param $obName
         * @param $objectId
         * @return array|bool|mixed
         */
        public static function get($obName,$objectId){
            $parse = Parse::Obj($obName);
            $response = $parse->get($objectId);
            return (isset($response->objectId))?$response:false;
        }

        /**
         * Получние объекта текущего класса по id
         * @param $objId
         * @return bool
         */
        public function load($objId){
            $obj = $this->parseGetQuery(
                $this->entityName,
                array(
                    "where"=>array(
                        "objectId"=>$objId
                    )
                )
            );

            return (!$obj)?false:$this->applyObj($obj[0]);
        }


        /**
         * Load from PARSE list of results with conditions
         *
         * @static
         * @param $class - Pars object name
         * @param $conditions => array("where"=>array("key"=>"value"));
         * @param $fields => fields to export
         * @return bool
         */
        public static function parseGetQuery($class,$conditions=null,$fields=null) {
            ParseEntity::$ctx = null;
            $parse = ParseEntity::makeConditionsRequest($class,$conditions);

            $allFounded = ParseEntity::loadAllRecords($parse);

            if(!$fields)return (!isset($allFounded->results[0]))?false:$allFounded->results;
            if(!isset($allFounded->results[0]))return false;

            $result = array();
            foreach($allFounded->results as $k=>$question){
                foreach($fields as $field){
                    $result[$k][$field] = (isset($question->$field))?$question->$field:"";
                }
            }

            return $result;
        }

        /**
         * create condition for query
         * @param $class
         * @param $conditions
         * @return parseQuery
         */
        private static function makeConditionsRequest($class,$conditions){
            $ordered = false;
            $parse = Parse::Query($class);
            ParseEntity::$ctx["limited"] = false;
            ParseEntity::$ctx["skipped"] = false;

            if($conditions){
                foreach($conditions as $mode=>$condition){
                    switch($mode){
                        case "setSkip":{
                            ParseEntity::$ctx["skipped"] = true;
                            $parse->$mode($condition);
                            break;
                        }
                        case "setLimit":{
                            ParseEntity::$ctx["limited"] = true;
                            $parse->$mode($condition);
                            break;
                        }
                        case "orderByDescending":
                        case "orderByAscending":{
                            $ordered = true;
                            $parse->$mode($condition);
                            break;
                        }
                        case "orderBy":
                            $ordered = true;
                        case "whereInclude":{
                            foreach($condition as $val)$parse->$mode($val);
                            break;
                        }
                        default:{//All avalible methods see in Parse::parseObject::
                            foreach($condition as $key=>$val)$parse->$mode($key,$val);
                            break;
                        }
                    }
                }
            }

            if(!$ordered)$parse->orderBy("name");

            return $parse;
        }

        /**
         * Load all available records from parse without limits
         * @param $parse
         * @return parseQuery
         */
        private static function loadAllRecords($parse){
            $page = 1;
            $allFounded = null;
            if(!ParseEntity::$ctx["limited"]){
                $parse->setLimit(ParseEntity::$maxLimit);
            }

            while(1){
                $founded = $parse->find();
                if(!isset($founded->results[0])){
                    break;
                }

                if(!isset($allFounded->results)){
                    $allFounded = $founded;
                }
                else{
                    foreach($founded->results as $result){
                        $allFounded->results[] = $result;
                    }
                }

                if(ParseEntity::$ctx["limited"] || ParseEntity::$ctx["skipped"] || (count($founded->results) < ParseEntity::$maxLimit)){//manual skip or in db no more records
                    break;
                }
                $parse->setSkip(ParseEntity::$maxLimit*$page);
                $page++;
            }
            return $allFounded;
        }

        /**
         * Calculate count of results
         *
         * @static
         * @param $class
         * @param $keyField
         * @param $objList
         * @return mixed
         */
        public static function getCount($class,$keyField,$objList){
            if(!$objList)return $objList;
            foreach($objList as $obj)$allObjId[] = $obj->objectId;

            $foundedItems = ParseEntity::parseGetQuery($class,array("whereContainedIn"=>array($keyField=>$allObjId)));
            foreach($objList as $key=>$obj){
                $objList[$key]->subItemsCount = 0;
                if(isset($foundedItems[0])){
                    foreach($foundedItems as $item){
                        if($obj->objectId == $item->$keyField)$objList[$key]->subItemsCount++;
                    }
                }
            }
            return $objList;
        }

        /**
         * count objects
         *
         * @param $class
         * @param $keyField
         * @param $arr
         * @return bool
         */
        public function counting($class,$keyField,$arr){
            $parse = Parse::Query($class);
            $parse->whereIn($keyField,$arr);
            $founded = $parse->getCount();
            return (!isset($founded->count))?false:$founded->count;
        }

        /**
         * Save entity to Parse
         * @param $obj
         * @return null
         */
        public function save($obj){
            $parse = $this->createParse($obj);
            $this->parseResponse = $parse->save();
            $this->applyObj($this->parseResponse);
            return (isset($this->parseResponse->objectId))?$this->parseResponse->objectId:null;
        }

        /**
         * remove fields from parse data container
         *
         */
        protected function preUpdateUnset(){
            //Safe from update manualy
            unset($this->data["objectId"]);
            unset($this->data["createdAt"]);
            unset($this->data["updatedAt"]);
            unset($this->data["sessionToken"]);
        }

        /**
         * Update entity on Parse
         * @param $obj
         * @return null
         */
        public function update($obj){
            $parse = $this->createParse($obj);
            $this->parseResponse = $parse->update($this->data["objectId"]);
            return (isset($this->parseResponse->objectId))?$this->parseResponse->objectId:null;
        }

        /**
         * Drop entity from Parse
         * @param null $objectId
         * @param null $objectClass
         * @return bool
         */
        public function drop($objectId=null,$objectClass=null){
            if(null!=$objectId)$this->data["objectId"] = $objectId;
            if(!isset($this->data["objectId"]))return false;
            $parse = $this->createParse($objectClass);
            $this->parseResponse = $parse->delete($this->data["objectId"]);
        }

        /**
         * Create new parse obj
         * @param null $obj
         * @return parseObject
         */
        protected function createParse($obj=null){
            $parseClass = $this->entityName;

            if(null!=$obj){
                if(is_string($obj))$parseClass = $obj;
                else $this->applyObj($obj);
            }

            $parse = Parse::Obj($parseClass);
            $parse->data=$this->data;
            return $parse;
        }

        /**
         * add objects to parse container
         * @param $obj
         * @return bool
         */
        protected function applyObj($obj){
            try{
                foreach($obj as $k=>$v){
                    $this->$k = $v;
                    $this->data[$k] = $v;
                }
                return true;
            }
            catch(Exception $e){
                return false;
            }
        }

    }
