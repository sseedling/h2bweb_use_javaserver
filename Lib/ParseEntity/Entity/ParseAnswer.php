<?php
    /**
     * Created by Ramotion.
     * User: seedling
     * Date: 15.07.13
     * Time: 17:55
     * To change this template use File | Settings | File Templates.
     */
    class ParseAnswer extends ParseEntity{
        public static $parseEntityName = "Answer";
        public $companyObj=null;

        public function __construct(){
            $this->entityName = ParseAnswer::$parseEntityName;
        }

        public static function find($objectIdList){
            $response = null;
            foreach($objectIdList as $quizz=>$answerIdList){
                $response[$quizz] = ParseEntity::parseGetQuery(
                    ParseAnswer::$parseEntityName,
                    array(
                        "whereContainedIn"=>array(
                            "objectId"=>$answerIdList,
                        )
                    )
                );
            }
            return $response;
        }

        public static function dropAll($answerIdList){
            if(empty($answerIdList))return false;
            $parse = Parse::Obj(ParseAnswer::$parseEntityName);
            foreach($answerIdList as $answerId){
                $parse->delete($answerId);
            }
        }

        public static function extract($reportListByQuiz){
            $response = null;
            if(!empty($reportListByQuiz)){
                foreach($reportListByQuiz as $quiz=>$reportList){
                    if(!empty($reportList)){
                        foreach($reportList as $report){
                            if((isset($report->answers))&&(!empty($report->answers))){
                                foreach($report->answers as $answer){
                                    if(!empty($answer)){
                                        $response[$quiz][] = $answer;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            return $response;
        }
    }
