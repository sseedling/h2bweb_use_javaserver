<?php
    class ParsePrize extends ParseEntity{
        public static $parseEntityName = "Prize";
        public static $postData = null;
        public $prizes = null;
        public $companyId = null;

        public function __construct(){
            $this->entityName = ParsePrize::$parseEntityName;
        }

        public function load($objectId){
            $obj = $this->get($this->entityName,$objectId);
            return (!$obj)?false:$this->applyObj($obj[0]);
        }

        public static function loadList($prizeList){
            return ParseEntity::parseGetQuery(
                ParsePrize::$parseEntityName,
                array(
                    "whereContainedIn"=>array(
                        "objectId"=>$prizeList
                    ),
                )
            );
        }

        public static function getList($count,$filterObj,$companyId,$order=null){
            $conditions = array(
                "orderByAscending"=>($order?$order:"count"),
            );

            if($count!=null)$conditions["setLimit"]=$count;
            $conditions["where"] = array("company"=>$companyId);

            if(is_array($filterObj)) {

                if((!isset($filterObj["all"]))||(empty($filterObj["all"]))||($filterObj["all"]==""))$filterObj["all"] = array("");
                if((!isset($filterObj["existed"]))||(empty($filterObj["existed"]))||($filterObj["existed"]==""))$filterObj["existed"] = array("");

                $filter = array();
                foreach($filterObj["all"] as $v)
                    if(($v!="")&&(!in_array($v,$filterObj["existed"])))
                        $filter[]= $v;
                if($filterObj["existed"][0]=="")unset($filterObj["existed"][0]);

                if(!empty($filter))$conditions["whereIn"]= array("objectId"=>$filter);
                if(!empty($filterObj["existed"]))$conditions["whereNotIn"]= array("objectId"=>$filterObj["existed"]);
            }
            $data = ParseEntity::parseGetQuery(ParsePrize::$parseEntityName,$conditions);

            $response = array();
            if($data)
                foreach($data as $prize){
                    $response[$prize->objectId] = Utils::objToArr($prize);
                    unset($response[$prize->objectId]["company"]);
                    unset($response[$prize->objectId]["createdAt"]);
                    unset($response[$prize->objectId]["updatedAt"]);
                }
            return $response;
        }

        public function update($objectId,$prize){
            $parse = Parse::Obj(ParsePrize::$parseEntityName);
            $parse->data = $prize;
            return $parse->update($objectId);
        }

        public function updateAll($prizesList){
            $prizesList = Utils::removeEmpty($prizesList);
            $result = array("change"=>false);
            $parse = Parse::Obj($this->entityName);
            foreach($prizesList as $prize){
                $objectId = null;
                if(isset($prize["objectId"])){
                    $objectId = $prize["objectId"];
                    if(isset($prize["count"]))$prize["count"] *= 1;
                    if(isset($prize["chance"]))$prize["chance"] *= 1;

                    unset($prize["objectId"]);

                    $parse->data = $prize;
                    $parse->update($objectId);
                }
                else {//Новый приз - сохраняем
                    if(isset($prize["prizeData"])){
                        $test = true;
                        foreach($prize["prizeData"] as $data)if(!isset($data["name"])){
                            $test=false;
                            break;
                        }

                        if($test){
                            $result["change"] = true;
                            $onSave = $this->saveAll(array($prize));
                            if((!($onSave["error"]))&&(isset($onSave["prizes"][0])))$objectId = $onSave["prizes"][0];
                        }
                    }
                }

                if($objectId)$result["prizes"][] = $objectId;
            }
            return $result;
        }

        public function saveAll($prizesList){
            $prizesList = Utils::removeEmpty($prizesList);
            $parse = Parse::Obj($this->entityName);
            $result = array("error"=>false);
            foreach($prizesList as $prize){
                if(isset($prize["prizeData"])){
                    $prize["company"] = $this->companyId;
                    $prize["count"] *= 1;
                    $prize["chance"] *= 1;
                    $parse->data = $prize;
                    $onSave = $parse->save();
                    if(isset($onSave->objectId))$result["prizes"][] = $onSave->objectId;
                    else {
                        $result["error"] = true;
                        break;
                    }
                }
            }
            return $result;
        }

        public static function findAll($companyId){
            return ParseEntity::parseGetQuery(
                ParsePrize::$parseEntityName,
                array(
                    "whereContainedIn"=>array(
                        "company"=>$companyId
                    ),
                )
            );
        }

        public static function countingAll($companyId){
            return ParseEntity::counting(ParsePrize::$parseEntityName,"company",array($companyId));
        }

        public function dropAll($objList){
            foreach($objList as $obj) $this->drop($obj);
        }

        public function drop($objId){
            parent::drop($objId,$this->entityName);
        }

        public static function validate($postData){
            ParsePrize::$postData = $postData;
            ParsePrize::$postData = Utils::removeEmpty(Utils::removeWhiteSpases(ParsePrizeGroup::$postData));

            if(!isset(ParsePrize::$postData["name"]))
                ParsePrizeGroup::$postData["error"]["name"] = "require";

            if(!isset(ParsePrize::$postData["chance"]))
                ParsePrizeGroup::$postData["error"]["chance"] = "require";

            if(!isset(ParsePrize::$postData["count"]))
                ParsePrizeGroup::$postData["error"]["chance"] = "require";

            return !isset(ParseSurvey::$postData["error"]);
        }

        private function makeData($obj=null){
            if(ParsePrize::$postData!=null)$this->applyObj(ParsePrize::$postData);
            if($obj!=null)$this->applyObj($obj);

            //Fill location fields
            if(!isset($this->company))$this->applyObj(array("company"=>$this->companyId));
            if(!isset($this->note))$this->applyObj(array("note"=>""));
            if(!isset($this->status))$this->applyObj(array("status"=>"on"));
        }

    }
