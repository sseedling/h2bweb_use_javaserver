<?php
    /**
     * Created by Ramotion.
     * User: seedling
     * Date: 18.07.13
     * Time: 18:27
     * To change this template use File | Settings | File Templates.
     */
    class ParseImage extends ParseEntity{
        public static $parseEntityName = "Image";
        private static $availableAttractionExtantions = array(
            'png' => 'image/png',
            'jpe' => 'image/jpeg',
            'jpeg' => 'image/jpeg',
            'jpg' => 'image/jpeg',
        );

        public function __construct(){
            $this->entityName = ParseImage::$parseEntityName;
        }

        public static function saveAllImg($list){
            $imgIdList = array();
            foreach($list["error"] as $bannerIndex=>$loadError){
                if($loadError==0){
                    $imgId =  ParseImage::saveOneImg($list["tmp_name"][$bannerIndex],$list["type"][$bannerIndex],$list["name"][$bannerIndex]);
                    if($imgId)$imgIdList[] = $imgId;
                }
            }
            return $imgIdList;
        }

        public static function saveOneImg($path,$mimeType=null,$name=null){
            if(!is_file($path))return null;

            $info = pathinfo($path);
            if(!$mimeType){
                $mimeType = (isset(ParseImage::$availableAttractionExtantions[$info["extension"]]))?ParseImage::$availableAttractionExtantions[$info["extension"]]:"image/jpeg";
            }

            $f = fopen($path,"r");

            $img = new ParseImage();
            $img->preUpdateUnset();
            unset($img->objectId);

            $outImgName = ($name)?Utils::createUrlName($name):time().$info["basename"];

            $img->save(
                fread($f,filesize($path)),
                $outImgName,
                $mimeType
            );

            return (isset($img->objectId))?$img->objectId:null;
        }

        public function save($fileContent,$fileName,$fileType){
            //Save image to PARSE
            $parsFile = Parse::File($fileContent,$fileName,$fileType);
            $response = $parsFile->save();

            if(!isset($response->name))return false;
            //link file to object
            $this->applyObj(
                array(
                    "imgData"=>array(
                        "name"=> $response->name,
                        "__type"=> "File"
                    )
                )
            );
            $parse = Parse::Obj($this->entityName);
            $parse->data = $this->data;
            $this->applyObj($parse->save());
            return  isset($this->objectId);
        }

        public function update($obj=null){
            if(null!=$obj)$this->applyObj($obj);
            if(!isset($this->objectId))return false;

            //Safe from update manualy
            $this->preUpdateUnset();

            $parse = Parse::Obj($this->entityName);
            $parse->data = $this->data;

            $this->applyObj($parse->update($this->objectId));
            return isset($this->updatedAt);
        }

        public function drop($fileName=false){
            if((!$fileName)&&(isset($this->fileName)))$fileName = $this->fileName;
            if(!$fileName)return false;

            $parsFile = Parse::File(false,$fileName,false);
            $parsFile->delete();
        }

        public static function dropById($imgId){
            $imgObj = new ParseImage();
            $img = $imgObj->get(ParseImage::$parseEntityName,$imgId);
            if(!$img)return;

            $imgObj->drop($img->imgData->name);//drop file
            $parse = $imgObj->createParse($imgObj->entityName);
            $parse->delete($img->objectId);//drop relation
        }

        public function find($mode,$params=null){
            $response = null;
            switch($mode){
                case "all":{
                    $response = ParseEntity::parseGetQuery($this->entityName,$params);
                    break;
                }
            }
            return $response;
        }

        public function findEmotionImgList($companyId){
            //testimg
            $response = $this->testLocalImages();

            /*
            $emotionImages = array(0=>"grade",1=>"stick",2=>"divider",3=>"backgroud",4=>"topCloud",5=>"bottomCloud");
            $allEmotionImages = array();

            foreach($emotionImages as $v)$allEmotionImages[] = "emotion:".$v;
            $imgList = $this->find("all",array("whereContainedIn"=>array("type"=>$allEmotionImages)));

            if(!isset($imgList[0]))return null;

            $response = array();
            foreach($imgList as $img){
                $type = explode(":",$img->type);
                switch($type[1]){
                    case $emotionImages[0]:{//grade
                        $response[$type[1]][$img->addition->val] = $img->imgData->url;
                        break;
                    }
                    case $emotionImages[1]:{//stick
                        $response[$type[1]] = $img->imgData->url;
                        break;
                    }
                    case $emotionImages[2]:{//divider
                        $response[$type[1]] = $img->imgData->url;
                        break;
                    }
                    case $emotionImages[3]:{//backgroud
                        $response["texture"] = $img->imgData->url;
                        $response["backgroundColors"] = $img->addition->colors;
                        break;
                    }
                    case $emotionImages[4]:{//top
                        $response[$type[1]][$img->addition->val] = $img->imgData->url;
                        break;
                    }
                    case $emotionImages[5]:{//bottom
                        $response[$type[1]][$img->addition->val] = $img->imgData->url;
                        break;
                    }
                }
            }
            */
            return $response;
        }

        private function testLocalImages(){
            $path = "/img/chart/emotion/";
            $response["texture"] = $path."light.png";
            $response["bottomCloud"]["4"] = $path."bottom.png";
            $response["bottomCloud"]["3"] = $path."bottom.png";
            $response["bottomCloud"]["2"] = $path."bottom.png";
            $response["bottomCloud"]["1"] = $path."bottom.png";
            $response["bottomCloud"]["0"] = $path."bottom.png";

            $response["topCloud"]["4"] = $path."top.png";
            $response["topCloud"]["3"] = $path."top.png";
            $response["topCloud"]["2"] = $path."top.png";
            $response["topCloud"]["1"] = $path."top.png";
            $response["topCloud"]["0"] = $path."top.png";

            $response["grade"]["4"] = $path."priority_cloud-excellent.png";
            $response["grade"]["3"] = $path."priority_cloud-good.png";
            $response["grade"]["2"] = $path."priority_cloud-neutral.png";
            $response["grade"]["1"] = $path."priority_cloud-bad.png";
            $response["grade"]["0"] = $path."priority_cloud-horrifying.png";
            $response["grade"]["-1"] = $path."priority_cloud-skipped.png";

            $response["divider"] = $path."priority-divider.png";
            $response["stick"] = $path."stick.png";

            $response["priority_low"] = $path."priority_low.png";
            $response["priority_hight"] = $path."priority_hight.png";

            $response["backgroundColors"] = array(array(22,54,85,1),array(74,112,144,1),array(173,195,213,1),array(82,133,204,1),array(22,114,238,1));
            return $response;
        }

        public static function getUrl($objectId){
            if(!$objectId)return null;
            $imgObj = new ParseImage();
            $img = $imgObj->get(ParseImage::$parseEntityName,$objectId);
            return (!$img)?null:$img->imgData->url;
        }

    }