<?php
    /**
     * Created by Ramotion.
     * User: seedling
     * Date: 15.07.13
     * Time: 17:55
     * To change this template use File | Settings | File Templates.
     */
    class ParseCompany extends ParseEntity{
        public static $postData = null;
        public static $parseEntityName = "Company";

        public function __construct(){
            $this->entityName = ParseCompany::$parseEntityName;
        }

        public function load($companyId){
            $obj = ParseEntity::parseGetQuery(
                $this->entityName,
                array(
                    "where"=>array(
                        "company"=>$companyId
                    )
                )
            );
            return (!$obj)?false:$this->applyObj($obj[0]);
        }

        public function save($obj) {
            $this->applyObj($obj);
            return $this->update(null,"save");
        }

        public function update($objectId,$mode="update"){
            //Prepare data
            $this->makeData();

            //check for already exist
            if($this->isExist($this->entityName,array("where"=>array("name"=>$this->name)),$objectId)){
                ParseCompany::$postData["error"]["name"] = "unique";
                return false;
            }

            //Safe from update manualy
            $this->preUpdateUnset();

            //update
            $parse = Parse::Obj($this->entityName);
            $parse->data = $this->data;

            if($mode=="update"){
                $tmp  = $parse->update($objectId);
                $this->applyObj($parse->update($objectId));
                return isset($this->data["updatedAt"]);
            }

            if($mode=="save"){
                $this->applyObj($parse->save());
                if(!isset($this->data["objectId"]))exit;
                return isset($this->data["objectId"]);
            }
        }

        public static function findAll($columns=false,$conditions) {
            $response = ParseEntity::parseGetQuery(ParseCompany::$parseEntityName,$conditions);
            if((!$response)||(!$columns))return $response;

            foreach($response as $company){
                $companyData = array();
                foreach($columns as $column){
                    $companyData[$column] = isset($company->$column)?$company->$column:null;
                }
                $companyList[$company->objectId] = $companyData;
            }
            return $companyList;
        }

        public static function get($objectId,$columns=null){
            $response = ParseEntity::get(ParseCompany::$parseEntityName,$objectId);
            if((!$response)||(!$columns))return $response;

            $company = array();
            foreach($columns as $column){
                $company[$column] = isset($response->$column)?$response->$column:null;
            }
            return $company;
        }

        public static function validate($obj){
            ParseCompany::$postData = $obj;
            ParseCompany::$postData = Utils::removeEmpty(Utils::removeWhiteSpases(ParseCompany::$postData));

            if(empty(ParseCompany::$postData))return false;

            //name
            if(!isset(ParseCompany::$postData["name"])){
                ParseCompany::$postData["error"]["name"] = "require";
            }

            //credits
            if(!isset(ParseCompany::$postData["credits"])){
                ParseCompany::$postData["error"]["credits"] = "require";
            }
            else{
                if(!is_numeric(ParseCompany::$postData["credits"])){
                    ParseCompany::$postData["error"]["credits"] = "format";
                }
                else{
                    /*
                    ParseCompany::$postData["credits"] = ParseCompany::$postData["credits"] *1;
                    if((ParseCompany::$postData["credits"]<0)&&(!ParseCompany::$postData["trusted"])){
                        ParseCompany::$postData["error"]["credits"] = "format";
                    }
                    */
                }
            }
            return !isset(ParseCompany::$postData["error"]);
        }

        private function makeData(){
            if(ParseCompany::$postData!=null)$this->applyObj(ParseCompany::$postData);

            //Fill location fields
            //
            unset($this->data["owner"]);
            if(isset($this->credits))$this->applyObj(array("credits"=>$this->credits * 1));
            if(!isset($this->comment))$this->applyObj(array("comment"=>""));
            if(!isset($this->trusted))$this->applyObj(array("trusted"=>false));
            if(is_string($this->trusted))$this->applyObj(array("trusted"=>($this->trusted=="true")));
        }

        public function drop($companyId){
            $parse = new ParseEntity();

            //drop all users;
            $objList = ParseEntity::parseGetQuery(Parse_User::$parseEntityName,array("where"=>array("company"=>$companyId)));
            $this->dropList(Parse_User::$parseEntityName,$parse,$objList); //+

            //drop all localtionReports,answers
            $this->dropGroup(ParseLocationReport::$parseEntityName,"answers",ParseAnswer::$parseEntityName,$companyId,$parse);

            //drop all prizesGroup,prizes
            $this->dropGroup(ParsePrizeGroup::$parseEntityName,"prizes",ParsePrize::$parseEntityName,$companyId,$parse);

            //drop all quizzes,questions;
            $this->dropGropByField(
                array(
                    "fieldKey"=>null,
                    "fieldName"=>"company",
                    "class"=>ParseSurvey::$parseEntityName,
                    "subClass"=>array(
                        "fieldKey"=>"objectId",
                        "fieldName"=>"survey",
                        "class"=>ParseQuestion::$parseEntityName,
                        "subClass"=>null,
                    ),
                ),
                $companyId,
                $parse
            );

            //drop all locations,attractions,images;
            $this->dropGropByField(
                array(
                    "fieldKey"=>null,
                    "fieldName"=>"company",
                    "class"=>ParseLocation::$parseEntityName,
                    "subClass"=>array(
                        "fieldKey"=>"objectId",
                        "fieldName"=>"location",
                        "class"=>ParseAttraction::$parseEntityName,
                        "subClass"=>array(
                            "fieldKey"=>"image",
                            "fieldName"=>"objectId",
                            "class"=>ParseImage::$parseEntityName,
                            "subClass"=>null,
                        ),
                    ),
                ),
                $companyId,
                $parse
            );

            //drop company
            $parse->drop($companyId,ParseCompany::$parseEntityName);
        }

        //Drop recursive by field in other table
        private function dropGropByField($params,$fieldValue,$parse){
            $objList = ParseEntity::parseGetQuery($params["class"],array("where"=>array($params["fieldName"]=>$fieldValue)));
            if(empty($objList))return;
            foreach($objList as $objData){
                if($params["subClass"]){
                    $fieldKey = $params["subClass"]["fieldKey"];
                    $this->dropGropByField($params["subClass"],$objData->$fieldKey,$parse);
                }
                $parse->drop($objData->objectId,$params["class"]);
            }
        }

        //Drop group that contain other pointer gropup
        private function dropGroup($groupClass,$subObjField,$subObjClass,$companyId,$parse){
            $objList = ParseEntity::parseGetQuery($groupClass,array("where"=>array("company"=>$companyId)));
            if(empty($objList))return;
            foreach($objList as $objData){
                $this->dropList($subObjClass,$parse,$objData->$subObjField);
                $parse->drop($objData->objectId,$groupClass);
            }
        }

        //foreach list item simple drop obj
        private function dropList($objClass,$parse,$objList){
            if(empty($objList))return;
            foreach($objList as $obj){
                $parse->drop($obj->objectId,$objClass);
            }
        }


    }
