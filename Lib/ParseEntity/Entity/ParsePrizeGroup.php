<?php
    class ParsePrizeGroup extends ParseEntity{
        public static $parseEntityName = "PrizeGroup";
        public static $postData = null;
        public $prizes = null;
        public $companyId = null;

        public function __construct(){
            $this->entityName = ParsePrizeGroup::$parseEntityName;
        }

        public function load($companyId,$prizeGroupUrl){
            $obj = $this->parseGetQuery(
                $this->entityName,
                array(
                    "where"=>array(
                        "urlName"=>$prizeGroupUrl,
                        "company"=>$companyId
                    ),
                )
            );
            return (!$obj)?false:$this->applyObj($obj[0]);
        }

        public function update($postData=null){
            //Prepare data
            $quizzes = isset(ParsePrizeGroup::$postData["quizzes"])?ParsePrizeGroup::$postData["quizzes"]:array();
            $removedQuizzes = isset(ParsePrizeGroup::$postData["removedQuizzes"])?ParsePrizeGroup::$postData["removedQuizzes"]:array();
            $prizes = isset(ParsePrizeGroup::$postData["prizes"])?ParsePrizeGroup::$postData["prizes"]:array();

            unset(ParsePrizeGroup::$postData["removedQuizzes"]);
            unset(ParsePrizeGroup::$postData["quizzes"]);
            unset(ParsePrizeGroup::$postData["prizes"]);

            $this->makeData($postData);

            if(!empty($quizzes)){//update quizzes
                ParseSurvey::attachPrizeGroup($quizzes,$this->objectId);
            }

            if(!empty($removedQuizzes)){//unattach quizzes
                ParseSurvey::unAttachPrizeGroup($removedQuizzes);
            }

            if(!empty($prizes)){//update prizes - if new - save
                $prize = new ParsePrize();
                $prize->companyId = $this->companyId;
                $prizesList = $prize->updateAll($prizes);
                if($prizesList["change"])
                    $this->applyPrizesList($prizesList["prizes"]);
            }

            //Update
            $parse = Parse::Obj($this->entityName);
            $parse->data = $this->data;
            $this->applyObj($parse->update($this->objectId));
            return isset($this->data["updatedAt"]);
        }

        public function save($postData=null){
            $onSave = null;
            if(isset(ParsePrizeGroup::$postData["prizes"])){
                $prize = new ParsePrize();
                $prize->companyId = $this->companyId;
                $onSave = $prize->saveAll(ParsePrizeGroup::$postData["prizes"]);
                if($this->rollBackOnSaveError($onSave,$prize))return false;
                unset($onSave["error"]);
            }

            //Prepare data
            $quizzes = isset(ParsePrizeGroup::$postData["quizzes"])?ParsePrizeGroup::$postData["quizzes"]:array();
            unset(ParsePrizeGroup::$postData["quizzes"]);
            unset(ParsePrizeGroup::$postData["prizes"]);
            $this->makeData($onSave);

            //Save
            $parse = Parse::Obj($this->entityName);
            $parse->data = $this->data;
            $this->applyObj($parse->save());

            //Check for save
            if(!isset($this->objectId)){
                $onSave["error"]=true;
                if(isset($prize))
                    $this->rollBackOnSaveError($onSave,$prize);
                return false;
            }

            //Attach quizes to parize group
            if(!empty($quizzes))ParseSurvey::attachPrizeGroup($quizzes,$this->objectId);

            return true;
        }

        private function rollBackOnSaveError($onSave,$prizes){
            if($onSave["error"]){
                if(isset($onSave["prises"])){
                    $prizes->dropAll($onSave["prises"]);
                }
                return true;
            }
            return false;
        }

        public static function find($prizeGroupList){
            return ParseEntity::parseGetQuery(
                ParsePrizeGroup::$parseEntityName,
                array(
                    "whereContainedIn"=>array(
                        "objectId"=>$prizeGroupList
                    ),
                )
            );
        }

        public static function findAll($companyId){
            return ParseEntity::parseGetQuery(
                ParsePrizeGroup::$parseEntityName,
                array(
                    "where"=>array(
                        "company"=>$companyId
                    ),
                )
            );
        }

        public static function validate($postData){
            ParsePrizeGroup::$postData = $postData;
            ParsePrizeGroup::$postData = Utils::removeEmpty(Utils::removeWhiteSpases(ParsePrizeGroup::$postData));

            if(!isset(ParsePrizeGroup::$postData["name"]))
                ParsePrizeGroup::$postData["error"]["name"] = "require";

            if(!isset(ParsePrizeGroup::$postData["note"]))
                ParsePrizeGroup::$postData["error"]["note"] = "require";

            if(!isset(ParsePrizeGroup::$postData["unwinNote"]))
                ParsePrizeGroup::$postData["error"]["unwinNote"] = "require";

            return !isset(ParsePrizeGroup::$postData["error"]);
        }

        public static function loadPrizeGroup($params){
            $groupId = isset($params["groupId"])?$params["groupId"]:null;
            if($groupId==null)return false;
            return ParseEntity::get(ParsePrizeGroup::$parseEntityName,$groupId);
        }

        private function makeData($obj=null){
            if(ParsePrizeGroup::$postData!=null)$this->applyObj(ParsePrizeGroup::$postData);
            if($obj!=null)$this->applyObj($obj);

            //Fill company field
            if(!isset($this->company))$this->applyObj(array("company"=>$this->companyId));

            //Fill locates texts
            $this->prepareGroupData();

            //create prizes field
            $this->applyPrizesList();
        }

        //create plisez pointers list
        private function applyPrizesList($prizesList=null){
            if($prizesList==null)$prizesList = ((isset($this->prizes))&&(!isset($this->prizes[0]->objectId)))?$this->prizes:null;
            if(!$prizesList)return;

            $tmp = array();
            if(!empty($prizesList))
                foreach($prizesList as $prize){
                    $tmp[] = array("__type"=>"Pointer","className"=>"Prize","objectId"=>$prize);
                }

            unset($this->prizes);
            unset($this->data["prizes"]);

            $this->applyObj(array("prizes"=>$tmp));
        }

        //convert input obj for save
        private function prepareGroupData(){
            $groupData = array();
            foreach($this->data["note"] as $locale=>$note){
                $groupData[$locale]["note"] = $note;
            }
            foreach($this->data["unwinNote"] as $locale=>$unwinNote){
                $groupData[$locale]["unwinNote"] = $unwinNote;
            }
            unset($this->data["note"]);
            unset($this->data["unwinNote"]);
            $this->applyObj(array("groupData"=>$groupData));
        }

    }
