<?php
    /**
     * Created by Ramotion.
     * User: seedling
     * Date: 15.07.13
     * Time: 17:55
     * To change this template use File | Settings | File Templates.
     */
    class ParseSurvey extends ParseEntity{
        public static $parseEntityName = "Survey";
        public static $postData = null;
        public $questions = null;
        public $companyObj = null;

        public static $avalibleSurveyStatus = array("publish","unpublish","archive");
        private $surveyData = null;

        public function __construct(){
            $this->entityName = ParseSurvey::$parseEntityName;
        }

        public function load($companyId,$surveyUrl){
            $obj = $this->parseGetQuery(
                $this->entityName,
                array(
                    "where"=>array(
                        "urlName"=>$surveyUrl,
                        "company"=>$companyId
                    ),
                )
            );
            return (!$obj)?false:$this->applyObj($obj[0]);
        }

        public function update(){
//            echo"<pre>";
//            print_r(ParseSurvey::$postData);
//            echo"</pre>";
            $this->makeSurveyData();

            //check for already exist
            if($this->isExist($this->entityName,array("where"=>array("company"=>$this->company,"urlName"=>$this->urlName)),$this->objectId)) {
                ParseSurvey::$postData["error"]["name"] = "unique";
                return false;
            }

            //Safe from update manualy
            $this->preUpdateUnset();

            $parse = $this->createParse($this->surveyData);
            $this->parseResponse = $parse->update($this->objectId);

            //update questions list
            if((isset($this->parseResponse->updatedAt))&&($this->questions)){
                $questionObj = new ParseQuestion();
                $questionObj->updateAll($this->questions,$this->objectId);
            }

            return isset($this->parseResponse->updatedAt);
        }

        public static function attachPrizeGroup($quizzId,$groupId){
            $parse = Parse::Obj(ParseSurvey::$parseEntityName);
            $parse->data = array("prizeGroup"=>$groupId);
            if(is_array($quizzId))foreach($quizzId as $id)$parse->update($id);
            else $parse->update($quizzId);
        }

        public static function unAttachPrizeGroup($quizzId){
            $parse = Parse::Obj(ParseSurvey::$parseEntityName);
            $parse->data = array("prizeGroup"=>null);
            if(is_array($quizzId))foreach($quizzId as $id)$parse->update($id);
            else $parse->update($quizzId);
        }

        public function save($postData=null){
            //Prepare data
            $this->makeSurveyData($postData);

            //check for already exist
            if($this->isExist($this->entityName,array("where"=>array("company"=>$this->companyObj->parseObj->objectId,"urlName"=>$this->urlName)))){
                ParseSurvey::$postData["error"]["name"] = "unique";
                return false;
            }

            //Save survey
            $parse = Parse::Obj($this->entityName);
            foreach($this->data as $k=>$v)$parse->$k=$v;
            $this->applyObj($parse->save());

            //Error on save survey
            if(!isset($this->objectId))return false;

            //Save questions if exists and survey saved
            if($this->questions){
                $questionObj = new ParseQuestion();
                $questionObj->saveAll($this->questions,$this->objectId);
            }

            //success
            return true;
        }

        public static function find($quizzessList){
            if(empty($quizzessList))return false;
            return ParseEntity::parseGetQuery(
                ParseSurvey::$parseEntityName,
                array(
                    "whereContainedIn"=>array(
                        "objectId"=>$quizzessList
                    ),
                )
            );
        }

        public static function findByPrizeGroup($prizeGroupId){
            return ParseEntity::parseGetQuery(
                ParseSurvey::$parseEntityName,
                array(
                    "where"=>array(
                        "prizeGroup"=>$prizeGroupId
                    ),
                )
            );
        }

        public function setStatus($status){
            $this->data["status"] = $status;
            $this->status = $status;
            $this->update();
        }

        public static function validate($postData){
            ParseSurvey::$postData = $postData;
            ParseSurvey::$postData = Utils::removeEmpty(Utils::removeWhiteSpases(ParseSurvey::$postData));
            if(!isset(ParseSurvey::$postData["name"]))
                ParseSurvey::$postData["error"]["name"] = "require";
            return !isset(ParseSurvey::$postData["error"]);
        }

        private function makeSurveyData($obj=null){
            if($obj!=null)$this->surveyData = $obj;
            else $this->surveyData = ParseSurvey::$postData;
            if($this->surveyData==null)$this->surveyData = $this->data;

            if(!isset($this->surveyData["urlName"]))$this->surveyData["urlName"] = Utils::createUrlName($this->surveyData["name"]);
            if($this->surveyData["urlName"]=="")$this->surveyData["urlName"] = md5($this->surveyData["name"]);

            if(!isset($this->surveyData["company"]))$this->surveyData["company"] = $this->companyObj->parseObj->objectId;

            if(!isset($this->surveyData["note"]))$this->surveyData["note"] = "";

            //Push questions to buff
            if(isset($this->surveyData["questions"])){
                $this->questions = (isset($this->surveyData["questions"]))?$this->surveyData["questions"]:false;
                $this->applyObj(array("localesPool"=>$this->makeUsesLocalesPool($this->questions)));
            }
            unset($this->surveyData["questions"]);

            if(!isset($this->surveyData["status"]))$this->surveyData["status"] = ($this->questions)?"publish":"unpublish";

            //Push all survey data on ParseEntity::data
            $this->applyObj($this->surveyData);
        }

        private function makeUsesLocalesPool($questions){
            $result = array();
            if($questions){
                foreach($questions as $question){
                    foreach($question["questionData"] as $locale=>$data)
                        if((!empty($data))&&(!in_array($locale,$result)))
                            $result[] = $locale;
                }
            }
            return $result;
        }

        public static function finadAllQuizes($conditions,$outFields=null){
            $quizzList = ParseSurvey::parseGetQuery(ParseSurvey::$parseEntityName, $conditions);
            if((!isset($quizzList[0]))||($outFields==null))return $quizzList;

            $groupQuizzes = array();
            foreach($quizzList as $quiz){
                $quizData = array();

                if(count($outFields)>1)
                    foreach($outFields as $field){
                        if(isset($quiz->$field))$quizData[$field]=$quiz->$field;
                    }
                else
                    if(isset($quiz->$outFields[0])){
                        $quizData=$quiz->$outFields[0];
                    }

                $groupQuizzes[$quiz->objectId]=$quizData;
            }
            return $groupQuizzes;
        }
    }
