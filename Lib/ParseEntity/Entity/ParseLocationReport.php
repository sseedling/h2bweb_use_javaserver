<?php
    /**
     * Created by Ramotion.
     * User: seedling
     * Date: 15.07.13
     * Time: 17:55
     * To change this template use File | Settings | File Templates.
     */
    class ParseLocationReport extends ParseEntity{
        public static $parseEntityName = "LocationReport";
        public $localeName = null;
        public $companyId=null;
        public static $log = array();

        public function __construct(){
            $this->entityName = ParseLocationReport::$parseEntityName;
        }

        /**
         * Выборка списка
         *
         * @static
         * @param $params
         * @return list|bool|null
         */
        public static function find($params){
            if(!ParseLocationReport::validate($params))return null;
            $params = Utils::prepareDatesForParse($params);
            if(!isset($params["orderMode"]))$params["orderMode"] = "orderByDescending";

            foreach($params["quizzes"] as $quiz=>$locationList){
                $conditions = array(
                    "whereGreaterThanOrEqualTo"=>array(
                        "date"=>array("__type"=>"Date","iso"=>$params["minDate"]),
                    ),
                    "whereLessThanOrEqualTo"=>array(
                        "date"=>array("__type"=>"Date","iso"=>$params["maxDate"]),
                    ),
                    "where"=>array(
                        "survey"=>$quiz,
                    ),
                    "whereInclude"=>array("answers"),
                    "whereContainedIn"=>array(
                        "location"=>$locationList,
                    ),
                    $params["orderMode"]=>"date"
                );

                $reports[$quiz] = ParseEntity::parseGetQuery(ParseLocationReport::$parseEntityName,$conditions);
            }
            $reports = Utils::removeEmpty($reports);
            return count($reports)>0?$reports:null;
        }

        /**
         * $post query with no-require params - date range,email
         * @param $queryData
         * @return array
         */
        public function findWinners($queryData){
            $conditions = array();
            $queryData = Utils::removeEmpty(Utils::removeWhiteSpases($queryData));

            $conditions["where"] = array("company"=>$this->companyId,"winStatus"=>"win");
            $conditions["whereIn"] = array("location"=>ParseLocation::$avalibleLocations);
            $conditions["setLimit"] = ParseEntity::$maxLimit;
            $conditions["orderByDescending"] = "date";

            if($queryData){
                $queryData = Utils::prepareDatesForParse($queryData);
                if($queryData["minDate"])$conditions["whereGreaterThanOrEqualTo"] = array("date"=>array("__type"=>"Date","iso"=>$queryData["minDate"]));
                if($queryData["maxDate"])$conditions["whereLessThanOrEqualTo"] = array("date"=>array("__type"=>"Date","iso"=>$queryData["maxDate"]));
            }

            if(isset($queryData["email"]))$conditions["where"]["email"]=$queryData["email"];
            else $conditions["whereNotEqualTo"]["email"]=null;

            $reports = ParseEntity::parseGetQuery(ParseLocationReport::$parseEntityName,$conditions);
            return $this->formatWinnersReport($reports,array("date","objectId","location","email","givenStatus","prize"));
        }

        /**
         * set report prize given status
         * @param $reportId
         * @param $giverUserId
         */
        public static function givePrize($reportId,$giverUserId){
            $parse = Parse::Obj(ParseLocationReport::$parseEntityName);
            $parse->data = array("givenStatus"=>true,"givenUser"=>$giverUserId);
            $log = $parse->update($reportId);
        }

        private static function validate($params){
            $params = Utils::removeWhiteSpases($params);
            $params = Utils::removeEmpty($params);

            if(!isset($params["quizzes"]))return false;
            if(!isset($params["minDate"]))return false;
            if(!isset($params["maxDate"]))return false;

            if($params["maxDate"]<$params["minDate"])return false;

            //all ok
            return true;
        }

        private function formatWinnersReport($reportsList,$columns){
            $result = array();
            $prizeIdList = array();
            if(empty($reportsList))return $result;
            foreach($reportsList as $report){
                if(!in_array($report->prizeId,$prizeIdList))$prizeIdList[] = $report->prizeId;
            }
            $locations = ParseLocation::find();
            $prizes = empty($prizeIdList)?array():ParsePrize::getList(ParseEntity::$maxLimit,array("all"=>$prizeIdList),$this->companyId,null);
            $errorStr = ParseEntity::$localeStrings->page->error->notFoundLocation;
            foreach($reportsList as $k=>$report){
                $locationName = $errorStr;
                if(!empty($locations)){
                    foreach($locations as $location){
                        if($reportsList[$k]->location == $location->objectId){
                            $locationName = $location->name;
                            break;
                        }
                    }
                }

                $reportsList[$k]->location = $locationName;
                $reportsList[$k]->prize = $this->extractPrizeName($prizes[$reportsList[$k]->prizeId]["prizeData"],$errorStr);

                foreach($columns as $column){
                    $result[$k][$column] = (isset($reportsList[$k]->$column))?Utils::objToArr($reportsList[$k]->$column):null;
                }
            }
            return $result;
        }

        private function extractPrizeName($prizeData,$errorStr){
            if(is_object($prizeData))$prizeData = Utils::objToArr($prizeData);
            $allLocalesNames = AppLocale::getLocalesNames();
            foreach($allLocalesNames as $locale)
                if(isset($prizeData[$locale])){
                    $firstFoundLocale = $locale;
                    break;
                }

            $usedLocale = (isset($prizeData[$this->localeName]))?$this->localeName:$firstFoundLocale;
            return isset($prizeData[$usedLocale]["name"])?$prizeData[$usedLocale]["name"]:$errorStr;
        }
    }
