<?php
    /**
     * Created by Ramotion.
     * User: seedling
     * Date: 18.07.13
     * Time: 18:16
     * To change this template use File | Settings | File Templates.
     */
    class ParseAttraction extends ParseEntity {
        public static $parseEntityName = "Attraction";

        public function __construct(){
            $this->entityName = ParseAttraction::$parseEntityName;
        }

        public function load($objId){
            return $this->parseGetQuery(
                $this->entityName,
                array(
                    "where"=>array(
                        "location"=>$objId,
                    ),
                )
            );
        }

        public function save($attraction,$locationId,$imageId,$default){
            $parse = Parse::Obj($this->entityName);
            $parse->location = $locationId;
            if((!isset($attraction["title"]))||(trim($attraction["title"]=="")))$attraction["title"] = $default->title;
            if((!isset($attraction["text"]))||(trim($attraction["text"]=="")))$attraction["text"] = $default->text;
            if($imageId)$parse->image=$imageId;
            $parse->title=$attraction["title"];
            $parse->text=(isset($attraction["text"]))?$attraction["text"]:"";
            $parse->save();
        }

        public function update($attraction,$locationId,$imageId=null,$default){//save image before attrection
            if(isset($attraction["objectId"])){
                $parse = Parse::Obj($this->entityName);
                if($imageId){
                    $parse->image = $imageId;
                    if(isset($attraction->imageId))ParseImage::dropById($attraction->imageId);
                }
                if(trim($attraction["title"]==""))$attraction["title"] = $default->title;
                if(trim($attraction["text"]==""))$attraction["text"] = $default->text;
                $parse->title=$attraction["title"];
                $parse->text=$attraction["text"];
                $parse->update($attraction["objectId"]);
            }
            else {//Новый attraction text - сохраняем
                $this->save($attraction,$locationId,$imageId,$default);
            }
        }
    }
