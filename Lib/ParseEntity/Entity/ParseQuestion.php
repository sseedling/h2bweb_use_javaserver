<?php
    /**
     * Created by Ramotion.
     * User: seedling
     * Date: 15.07.13
     * Time: 17:55
     * To change this template use File | Settings | File Templates.
     */
    class ParseQuestion extends ParseEntity{
        public static $parseEntityName = "Question";
        public function __construct(){
            $this->entityName = ParseQuestion::$parseEntityName;
        }

        public static function find($conditions,$fields=null){
            return ParseEntity::parseGetQuery(ParseQuestion::$parseEntityName,$conditions,$fields);
        }

        public function load($objId){
            $obj = $this->parseGetQuery(
                $this->entityName,
                array(
                    "where"=>array(
                        "survey"=>$objId,
                    ),
                )
            );
            return (!$obj)?false:$this->applyObj($obj[0]);
        }

        public function save($question,$surveyId,$order,$parse=null){
            $question = $this->prepareQuestionData($question);
            if(!$question)return false;

            if(!$parse)$parse = Parse::Obj($this->entityName);
            $parse->survey = $surveyId;
            $parse->order=$order;
            $parse->questionData = $question["questionData"];
            $parse->save();
        }

        public function saveAll($questions,$surveyId){
            $parse = Parse::Obj($this->entityName);
            foreach($questions as $order=>$question){
                $this->save($question,$surveyId,$order,$parse);
            }
        }

        public function updateAll($questions,$surveyId){
            $parse = Parse::Obj($this->entityName);
            foreach($questions as $order=>$question){
                if(isset($question["objectId"])){
                    $objectId = $question["objectId"];
                    unset($question["objectId"]);
                    $question = $this->prepareQuestionData($question);
                    if(!$question){//drop
                        $parse->delete($objectId);
                    }
                    else {//update
                        $parse->questionData = $question["questionData"];
                        $parse->order=$order;
                        $parse->update($objectId);
                    }
                }
                else {//Новый вопрос - сохраняем
                    $this->save($question,$surveyId,$order,$parse);
                }
            }
        }

        private function prepareQuestionData($question){
            if(is_object($question["questionData"]))
                $question["questionData"] =  Utils::objToArr($question["questionData"]);

            foreach($question["questionData"] as $locale=>$data){
                if(isset($data["name"])){
                    if(!isset($data["subQuestions"]))$question["questionData"][$locale]["subQuestions"] = null;
                    if(!isset($data["text"]))$question["questionData"][$locale]["text"] = "";
                    $question["questionData"][$locale]["subQuestionsEnabled"] = (isset($data["subQuestionsEnabled"])) && ($data["subQuestionsEnabled"]=="true");
                }
                else return false;
//                else unset($question["questionData"][$locale]);
            }
            return (empty($question["questionData"]))?false:$question;
        }
    }
