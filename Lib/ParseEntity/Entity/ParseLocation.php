<?php
    /**
     * Created by Ramotion.
     * User: seedling
     * Date: 15.07.13
     * Time: 17:55
     * To change this template use File | Settings | File Templates.
     */
    class ParseLocation extends ParseEntity{
        public static $parseEntityName = "Location";
        public static $postData = null;
        public $companyObj=null;
        public static $availibleActions = array("attach-survey","unattach-survey");
        public static $avalibleLocations = array();

        public function __construct(){
            $this->entityName = ParseLocation::$parseEntityName;
            if(ParseLocation::$postData!=null)$this->applyObj(ParseLocation::$postData);
        }

        public static function validate($obj) {
            if(!$obj)return false;
            ParseLocation::$postData = $obj;
            ParseLocation::$postData = Utils::removeEmpty(Utils::removeWhiteSpases(ParseLocation::$postData));
            if(!isset(ParseLocation::$postData["name"]))
                ParseLocation::$postData["error"]["name"] = "require";
            return !isset(ParseLocation::$postData["error"]);
        }

        public function save($postData=null) {
            //Prepare data
            $this->makeData($postData);

            //check for already exist
            if($this->isExist($this->entityName,array("where"=>array("company"=>$this->companyObj->parseObj->objectId,"urlName"=>$this->urlName)))){
                ParseLocation::$postData["error"]["name"]="unique";
                return false;
            }

            //reorder phones
            if(isset($this->data["phones"]["list"]))$this->applyObj(array("phones"=>array("list"=>$this->reOrderArr($this->data["phones"]["list"]))));

            //Save location
            $parse = Parse::Obj($this->entityName);
            $parse->data = $this->data;
            $this->applyObj($parse->save());
            //Check for save location
            return  isset($this->objectId);
        }

        public function update($postData=null) {
            //Prepare data
            $this->makeData($postData);
            if((empty(ParseLocation::$avalibleLocations))||(!in_array($this->objectId,ParseLocation::$avalibleLocations)))return false;//check permissions

            //check for already exist
            if($this->isExist($this->entityName,array("where"=>array("company"=>$this->company,"urlName"=>$this->urlName)),$this->objectId)){
                ParseLocation::$postData["error"]["name"]="unique";
                return false;
            }

            //Safe from update manualy
            $this->preUpdateUnset();

            //Save location
            $parse = Parse::Obj($this->entityName);
            $parse->data = $this->data;

            $this->applyObj($parse->update($this->objectId));

            return isset($this->data["updatedAt"]);
        }

        public function load($companyId,$locationUrl) {
            $conditions = ParseLocation::addLocationsFilter(array(
                "where"=>array(
                    "urlName"=>$locationUrl,
                    "company"=>$companyId
                ),
            ));
            if(!$conditions)return false;//check permissions
            $obj = ParseEntity::parseGetQuery($this->entityName,$conditions);
            return (!$obj)?false:$this->applyObj($obj[0]);
        }

        public static function find($conditions=null) {
            $conditions = ParseLocation::addLocationsFilter($conditions);
            if(!$conditions)return false;

            return ParseEntity::parseGetQuery(ParseLocation::$parseEntityName,$conditions);
        }

        private static function addLocationsFilter($conditions) {//check permissions
            if(empty(ParseLocation::$avalibleLocations))return false;

            if(!isset($conditions["whereIn"]["objectId"]))
                $conditions["whereIn"]["objectId"] = array();

            $conditions["whereIn"]["objectId"] = $conditions["whereIn"]["objectId"] + ParseLocation::$avalibleLocations;

            return $conditions;
        }

        public static function findAll($companyId,$fields=null){
            $conditions = ParseLocation::addLocationsFilter(array(//check permissions
                "where"=>array(
                    "company"=>$companyId
                )
            ));
            if(!$conditions)return false;

            $list = ParseEntity::parseGetQuery(ParseLocation::$parseEntityName,$conditions);

            if(!($list && $fields))return $list;

            $out = array();
            foreach($list as $item){
                $data = array();
                if(count($fields)>1)
                    foreach($fields as $field){
                        if(isset($item->$field))$data[$field] = $item->$field;
                    }
                else $data = $item->$fields[0];
                $out[$item->objectId] = $data;
            }
            return $out;
        }

        public function attachSurvey($objectId,$mode=null){
            if(!in_array($this->objectId,ParseLocation::$avalibleLocations))return false;//check permissions

            if(!$mode)$mode = ParseLocation::$availibleActions[0];
            $parse = Parse::Obj($this->entityName);

            switch($mode){
                case ParseLocation::$availibleActions[0]:{//attach
                    $parse->AddToArray("quizzes",array(array("objectId"=>$objectId,"__type"=>"Pointer","className"=>"Survey")));
                    break;
                }
                case ParseLocation::$availibleActions[1]:{//unattach
                    $parse->RemoveFromArray("quizzes",array(array("objectId"=>$objectId,"__type"=>"Pointer","className"=>"Survey")));
                    break;
                }
            }
            $parse->update($this->objectId);
        }

        public function addLogo($logoId,$objectId=null){
            if(!$objectId){
                if(!$this->objectId)return;
                $objectId = $this->objectId;
            }
            $parse = Parse::Obj($this->entityName);
            $parse->logo = $logoId;
            $parse->update($objectId);
        }

        private function reOrderArr($arr){
            foreach($arr as $item){
                $tmp[] = $item;
            }
            return isset($tmp)?$tmp:null;
        }

        private function makeData($obj=null){
            if(ParseLocation::$postData!=null)$this->applyObj(ParseLocation::$postData);
            if($obj!=null)$this->applyObj($obj);

            //Fill location fields
            //
            $this->applyObj(array("urlName"=>Utils::createUrlName($this->data["name"])));
            if($this->urlName=="")$this->applyObj(array("urlName"=>md5($this->data["name"])));
            //
            if(!isset($this->company))$this->applyObj(array("company"=>$this->companyObj->parseObj->objectId));
            if(!isset($this->about))$this->applyObj(array("about"=>""));

            if(isset($this->data->phones)){
                unset($this->data->phones);
            }

            if((isset($this->data["phones"]))&&(is_object($this->data["phones"]))){
                unset($this->data["phones"]);
            }

            if(isset($this->data["phones"])){//есть новый список телефонов
                foreach($this->data["phones"]["list"] as $phone)
                    $tmp[] = $phone;
                unset($this->data["phones"]["list"]);
            }
            $this->data["phones"]["list"] = isset($tmp)?$tmp:array();
            $this->phones = $this->data["phones"];
            unset($this->data["banners"]);
            unset($this->data->banners);
        }

        public static function removeBanner($locationId,$bannerId){
            $parse = Parse::Obj(ParseLocation::$parseEntityName);
            $parse->RemoveFromArray("banners",array($bannerId));
            $parse->update($locationId);
        }

        public static function addNewBanners($locationId,$bannerList){
            $parse = Parse::Obj(ParseLocation::$parseEntityName);
            $parse->AddToArray("banners",$bannerList);
            $parse->update($locationId);
        }
    }