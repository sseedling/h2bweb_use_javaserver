<?php
    /**
     * Created by Ramotion.
     * User: seedling
     * Date: 15.07.13
     * Time: 17:55
     * To change this template use File | Settings | File Templates.
     */
    class Parse_User extends ParseEntity{
        public static $postData=null;
        public $companyObj=null;
        public static $parseEntityName = "_User";
        public static $userMode = null;

        public function __construct(){
            $this->entityName = Parse_User::$parseEntityName;
            if(Parse_User::$postData!=null)$this->applyObj(Parse_User::$postData);
        }

        public function load($objId) {
            $obj = $this->parseGetQuery(
                $this->entityName,
                array(
                    "where"=>array(
                        "objectId"=>$objId
                    )
                )
            );

            return (!$obj)?false:$this->applyObj($obj[0]);
        }

        public static function get($objectId,$columns=null){
            if(!$objectId){
                if(!$columns)return null;
                $userData = array();
                foreach($columns as $column)$userData[$column] = null;
                return $userData;
            }

            $user = ParseEntity::parseGetQuery(
                Parse_User::$parseEntityName,
                array(
                    "where"=>array(
                        "objectId"=>$objectId
                    ),
                    "setLimit"=>1
                )
            );

            if(!isset($user[0]))return null;

            if(!$columns)return $user[0];
            $userData = array();
            foreach($columns as $column){
                $userData[$column] = isset($user[0]->$column)?$user[0]->$column:null;
            }
            return $userData;
        }

        public function save($postData=null) {
            //Prepare data
            $this->makeData($postData);

            //check for already exist
            if($this->isExist($this->entityName,array("where"=>array("username"=>$this->username)))){
                Parse_User::$postData["error"]["username"] = "unique";
                return false;
            }

            //Safe from update manualy
            unset($this->data["roles"]);

            //Save new User
            $parse = Parse::Obj($this->entityName);
            $parse->data = $this->data;
            $onSave = $parse->save();
            $this->applyObj($onSave);

            //Error on save
            if(!isset($this->objectId))return false;

            //success
            return true;

        }

        public function update($postData=null) {

            //Prepare data
            $this->makeData($postData);

            //check for already exist
            if($this->isExist($this->entityName,array("where"=>array("username"=>$this->username)),$this->objectId)){
                Parse_User::$postData["error"]["username"] = "unique";
                return false;
            }

            //Safe from update manualy
            $this->preUpdateUnset();
            unset($this->data["roles"]);

            //Save new User
            $parse = Parse::Obj($this->entityName);
            $parse->data = $this->data;
            $this->applyObj($parse->update($this->objectId));

            return isset($this->data["updatedAt"]);

        }

        public static function validate($obj) {
            Parse_User::$postData = $obj;
            Parse_User::$postData = Utils::removeEmpty(Utils::removeWhiteSpases(Parse_User::$postData));

            if(empty(Parse_User::$postData))return false;

            switch(Parse_User::$userMode){
                case"addManager":{
                    if(!isset(Parse_User::$postData["password"]))
                        Parse_User::$postData["error"]["password"] = "require";
                }
                case"editManager":{
                    if(!isset(Parse_User::$postData["publicName"]))
                        Parse_User::$postData["error"]["publicName"] = "require";
                    if(!isset(Parse_User::$postData["email"]))
                        Parse_User::$postData["error"]["email"] = "require";
                    if(!isset(Parse_User::$postData["username"]))
                        Parse_User::$postData["error"]["username"] = "require";
                    break;
                }
                case"CompanyOwner":{
                    if(!isset(Parse_User::$postData["email"]))
                        Parse_User::$postData["error"]["email"] = "require";
                    break;
                }
            }

            return !isset(Parse_User::$postData["error"]);
        }

        public function drop($mode="simple",$objectId=null) {
            if((!$objectId)&&(isset($this->objectId)))$objectId = $this->objectId;
            switch($mode){
                case "simple":{
                    parent::drop($objectId,$this->entityName);
                    break;
                }
                case "full":{
                    //todo remove user from all sources - i.e. location->user
                    break;
                }
            }
        }

        public static function findAll($company) {
            $usersList = ParseEntity::parseGetQuery(
                Parse_User::$parseEntityName,
                array(
                    "where"=>array(
                        "company"=>$company->parseObj->objectId
                    ),
                )
            );

            $out = array();
            if(isset($usersList[0])) {
                foreach($usersList as $k=>$user) {
                    if((!isset($user->ACL->Owner))&&(!isset($user->ACL->SuperAdmin))) {
                        $out[$user->objectId] = array(
                            "username"=>$user->username,
                            "publicName"=>$user->publicName,
                            "roles"=>Auth::getAclArr(@$user->ACL,array(Roles::$authorizeRole)),
                            "createdAt"=>$user->createdAt,
                            "status"=>(($user->status)?$user->status:false),
                        );
                    }
                }
            }

            return $out;
        }

        private function makeData($obj=null) {
            if(Parse_User::$postData!=null){
                $this->applyObj(Parse_User::$postData);
            }
            if($obj!=null){
                $this->applyObj($obj);
            }

            if($this->companyObj){
                $this->applyObj(array("company"=>$this->companyObj->parseObj->objectId,));
            }

            if((isset($this->status))&&(is_string($this->status))){
                $this->applyObj(array("status" => ($this->status=="true")));
            }

            if((isset($this->password))&&($this->password=="")) {

                unset($this->password);
                unset($this->data["password"]);

            }

            if((isset($this->data->ACL))||((isset($this->data["ACL"])&&(is_object($this->data["ACL"]))))){
                unset($this->data["ACL"]);
            }
            if((isset($this->data["ACL"]))&&(!isset($this->data["ACL"][Roles::$authorizeRole]))){
                $this->data["ACL"][Roles::$authorizeRole]=array("read"=>true);
            }

            if((!isset($this->username))&&(isset($this->email))){
                $this->username = $this->email;
            }
            $this->applyObj(array("username"=>$this->username));
        }

        public static function shareLocation($userId,$locationId){
            //Save new User
            $parse = Parse::Obj(Parse_User::$parseEntityName);
            $parse->AddToArray("locationList",array($locationId));
            $parse->update($userId);
        }

        public static function setPassword($password,$userId){
            $parse = Parse::Obj(Parse_User::$parseEntityName);
            $parse->data = array("password"=>$password);
            $parse->update($userId);
        }

        public static function setCompany($companyId,$userId){
            $parse = Parse::Obj(Parse_User::$parseEntityName);
            $parse->data = array("company"=>$companyId);
            $parse->update($userId);
        }
    }
